package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.DataStoreIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.StudyRelated.StudySeries;

public class DataStoreLibrary extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final DataStoreIndicator[] contains;

	public final Boolean[] isOrdered;

	public DataStoreRelationStructure[] dataStoreRelationStructure;

	public StructuredCollection[] structuredCollection;

	public Concept[] concept;

	public StudySeries[] studySeries;

}
