package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.InstanceVariableIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.SimpleCollection;
import ClassLibrary.Conceptual.Concept;

public abstract class ViewpointRole extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final InstanceVariableIndicator[] contains;

	public final Boolean[] isOrdered;

	public SimpleCollection[] simpleCollection;

	public Concept[] concept;

}
