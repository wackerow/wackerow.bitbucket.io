package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.InstanceVariableRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class InstanceVariableRelationStructure extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] criteria;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType totality;

	public final RelationSpecification hasRelationSpecification;

	public final InstanceVariableRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public UnitDataRecord[] unitDataRecord;

}
