package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;

public class UnitDataViewpoint extends AnnotatedIdentifiable {

	public AttributeRole[] attributeRole;

	public IdentifierRole[] identifierRole;

	public MeasureRole[] measureRole;

	public UnitDataRecord[] unitDataRecord;

}
