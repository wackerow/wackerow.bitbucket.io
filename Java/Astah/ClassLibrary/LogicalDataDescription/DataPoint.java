package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.DataPointRelation;
import ClassLibrary.ComplexDataTypes.DataPointIndicator;
import ClassLibrary.Conceptual.InstanceVariable;
import ClassLibrary.FormatDescription.ValueMapping;
import ClassLibrary.CollectionsPattern.CollectionMember;

public class DataPoint extends AnnotatedIdentifiable {

	public Datum datum;

	public DataPointRelation[] dataPointRelation;

	public DataPointIndicator dataPointIndicator;

	public DataPointRelation[] dataPointRelation;

	public InstanceVariable[] instanceVariable;

	public ValueMapping valueMapping;

	public CollectionMember[] collectionMember;

}
