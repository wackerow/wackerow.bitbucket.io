package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.InstanceVariableIndicator;
import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.BusinessProcessCondition;
import ClassLibrary.FormatDescription.PhysicalSegmentLayout;
import ClassLibrary.ComplexDataTypes.LogicalRecordRelation;
import ClassLibrary.FormatDescription.PhysicalRecordSegment;
import ClassLibrary.CollectionsPattern.SimpleCollection;
import ClassLibrary.ComplexDataTypes.LogicalRecordIndicator;
import ClassLibrary.Conceptual.Concept;

public abstract class LogicalRecord extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final InstanceVariableIndicator[] contains;

	public final Boolean[] isOrdered;

	public BusinessProcessCondition businessProcessCondition;

	public PhysicalSegmentLayout physicalSegmentLayout;

	public LogicalRecordRelation[] logicalRecordRelation;

	public PhysicalRecordSegment physicalRecordSegment;

	public SimpleCollection[] simpleCollection;

	public LogicalRecordIndicator logicalRecordIndicator;

	public Concept[] concept;

	public LogicalRecordRelation logicalRecordRelation;

}
