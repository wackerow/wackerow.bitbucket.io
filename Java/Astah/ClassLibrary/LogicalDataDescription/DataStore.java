package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import Primitive Package.Integer;
import ClassLibrary.ComplexDataTypes.LogicalRecordIndicator;
import Primitive Package.Boolean;
import ClassLibrary.FormatDescription.PhysicalDataSet;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.ComplexDataTypes.DataStoreRelation;
import ClassLibrary.ComplexDataTypes.DataStoreIndicator;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;

public class DataStore extends AnnotatedIdentifiable {

	public final String[] characterSet;

	public final ExternalControlledVocabularyEntry[] dataStoreType;

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final Integer[] recordCount;

	public final InternationalStructuredString[] aboutMissing;

	public final LogicalRecordIndicator[] contains;

	public final Boolean[] isOrdered;

	public PhysicalDataSet[] physicalDataSet;

	public Study study;

	public DataStoreRelation[] dataStoreRelation;

	public DataStoreIndicator dataStoreIndicator;

	public DataStoreRelation[] dataStoreRelation;

	public Concept[] concept;

	public RecordRelation recordRelation;

	public StructuredCollection[] structuredCollection;

	public LogicalRecordRelationStructure[] logicalRecordRelationStructure;

}
