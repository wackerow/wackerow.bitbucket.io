package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.LogicalRecordRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class LogicalRecordRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final LogicalRecordRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public DataStore[] dataStore;

}
