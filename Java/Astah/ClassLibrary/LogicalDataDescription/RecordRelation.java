package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.InstanceVariableValueMap;
import ClassLibrary.CollectionsPattern.Comparison;

public class RecordRelation extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] purpose;

	public final InternationalStructuredString[] usage;

	public final InstanceVariableValueMap[] correspondence;

	public UnitDataRecord[] unitDataRecord;

	public Comparison[] comparison;

	public DataStore[] dataStore;

}
