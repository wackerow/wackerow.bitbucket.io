package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Representations.Designation;
import ClassLibrary.ComplexDataTypes.ValueString;
import ClassLibrary.Conceptual.InstanceVariable;

public class Datum extends Designation {

	public final ValueString representation;

	public DataPoint[] dataPoint;

	public InstanceVariable[] instanceVariable;

}
