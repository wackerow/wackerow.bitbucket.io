package ClassLibrary.LogicalDataDescription;

import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.Conceptual.Concept;

public class UnitDataRecord extends LogicalRecord {

	public RecordRelation[] recordRelation;

	public UnitDataViewpoint[] unitDataViewpoint;

	public InstanceVariableRelationStructure[] instanceVariableRelationStructure;

	public StructuredCollection[] structuredCollection;

	public Concept[] concept;

}
