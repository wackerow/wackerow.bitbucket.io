package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.DataStoreRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class DataStoreRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final DataStoreRelation[] hasMemberRelation;

	public DataStoreLibrary[] dataStoreLibrary;

	public RelationStructure[] relationStructure;

}
