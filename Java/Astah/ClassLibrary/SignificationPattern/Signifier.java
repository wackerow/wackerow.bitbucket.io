package ClassLibrary.SignificationPattern;

import Primitive Package.String;
import ClassLibrary.EnumerationsRegExp.WhiteSpaceRule;
import ClassLibrary.ComplexDataTypes.ValueString;

public abstract class Signifier {

	public final String[] content;

	public final WhiteSpaceRule[] whiteSpace;

	public ValueString valueString;

}
