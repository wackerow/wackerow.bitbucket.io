package ClassLibrary.SignificationPattern;

import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.Representations.Designation;

public abstract class Sign extends CollectionMember {

	public final Signifier representation;

	public Designation designation;

	public Signified[] signified;

}
