package ClassLibrary.SignificationPattern;

import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.Conceptual.Concept;

public abstract class Signified extends CollectionMember {

	public Concept concept;

	public Sign sign;

}
