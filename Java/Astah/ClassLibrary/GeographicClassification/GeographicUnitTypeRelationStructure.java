package ClassLibrary.GeographicClassification;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.GeographicUnitTypeRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class GeographicUnitTypeRelationStructure extends Identifiable {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final GeographicUnitTypeRelation[] hasMemberRelation;

	public GeographicUnitTypeClassification geographicUnitTypeClassification;

	public RelationStructure[] relationStructure;

}
