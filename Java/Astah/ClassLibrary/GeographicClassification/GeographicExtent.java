package ClassLibrary.GeographicClassification;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.Polygon;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.ComplexDataTypes.AreaCoverage;
import ClassLibrary.ComplexDataTypes.SpatialPoint;
import ClassLibrary.ComplexDataTypes.SpatialLine;
import ClassLibrary.Discovery.BoundingBox;

public class GeographicExtent extends AnnotatedIdentifiable {

	public final Polygon[] boundingPolygon;

	public final Polygon[] excludingPolygon;

	public final DateRange[] geographicTime;

	public final AreaCoverage[] hasAreaCoverage;

	public final SpatialPoint[] hasCentroid;

	public final SpatialPoint[] locationPoint;

	public final SpatialLine[] isSpatialLine;

	public BoundingBox[] boundingBox;

	public GeographicUnit[] geographicUnit;

}
