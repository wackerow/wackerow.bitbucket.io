package ClassLibrary.GeographicClassification;

import ClassLibrary.Representations.CodeList;
import ClassLibrary.ComplexDataTypes.GeographicUnitIndicator;
import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.ComplexDataTypes.DateRange;
import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Conceptual.ConceptSystem;
import ClassLibrary.Agents.Organization;
import ClassLibrary.Discovery.SpatialCoverage;

public class GeographicUnitClassification extends CodeList {

	public final GeographicUnitIndicator[] contains;

	public final Date[] releaseDate;

	public final DateRange[] validDates;

	public final Boolean[] isCurrent;

	public final Boolean[] isFloating;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public GeographicUnitClassification[] geographicUnitClassification;

	public GeographicUnitClassification[] geographicUnitClassification;

	public GeographicUnitClassification[] geographicUnitClassification;

	public GeographicUnitClassification[] geographicUnitClassification;

	public GeographicUnitRelationStructure[] geographicUnitRelationStructure;

	public ConceptSystem[] conceptSystem;

	public GeographicUnitClassification[] geographicUnitClassification;

	public GeographicUnitClassification geographicUnitClassification;

	public Organization[] organization;

	public SpatialCoverage[] spatialCoverage;

}
