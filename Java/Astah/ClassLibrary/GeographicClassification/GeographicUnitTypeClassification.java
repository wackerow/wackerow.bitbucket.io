package ClassLibrary.GeographicClassification;

import ClassLibrary.Representations.CodeList;
import ClassLibrary.ComplexDataTypes.GeographicUnitTypeIndicator;
import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.ComplexDataTypes.DateRange;
import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Discovery.SpatialCoverage;
import ClassLibrary.Conceptual.ConceptSystem;
import ClassLibrary.Agents.Organization;

public class GeographicUnitTypeClassification extends CodeList {

	public final GeographicUnitTypeIndicator[] contains;

	public final Date[] releaseDate;

	public final DateRange[] validDates;

	public final Boolean[] isCurrent;

	public final Boolean[] isFloating;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public SpatialCoverage[] spatialCoverage;

	public ConceptSystem[] conceptSystem;

	public GeographicUnitTypeRelationStructure[] geographicUnitTypeRelationStructure;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	public Organization[] organization;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	public GeographicUnitTypeClassification geographicUnitTypeClassification;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

}
