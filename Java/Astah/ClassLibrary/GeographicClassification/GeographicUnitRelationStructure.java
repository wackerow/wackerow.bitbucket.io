package ClassLibrary.GeographicClassification;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.GeographicUnitRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class GeographicUnitRelationStructure extends Identifiable {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final GeographicUnitRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public GeographicUnitClassification geographicUnitClassification;

}
