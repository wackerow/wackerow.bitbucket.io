package ClassLibrary.GeographicClassification;

import ClassLibrary.Conceptual.Unit;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.ComplexDataTypes.SpatialRelationship;
import ClassLibrary.Discovery.SpatialCoverage;
import ClassLibrary.ComplexDataTypes.GeographicUnitIndicator;
import ClassLibrary.ComplexDataTypes.GeographicUnitRelation;
import ClassLibrary.Conceptual.Population;

public class GeographicUnit extends Unit {

	public final DateRange[] geographicTime;

	public final SpatialRelationship[] supercedes;

	public final SpatialRelationship[] precedes;

	public SpatialRelationship spatialRelationship;

	public SpatialCoverage[] spatialCoverage;

	public GeographicUnitIndicator geographicUnitIndicator;

	public GeographicUnitRelation[] geographicUnitRelation;

	public GeographicUnitRelation geographicUnitRelation;

	public Population[] population;

	public GeographicExtent[] geographicExtent;

}
