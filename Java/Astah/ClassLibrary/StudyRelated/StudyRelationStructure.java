package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.StudyRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class StudyRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final StudyRelation[] hasMemberRelation;

	public StudySeries[] studySeries;

	public RelationStructure[] relationStructure;

}
