package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.Conceptual.InstanceVariable;
import ClassLibrary.Conceptual.Population;
import ClassLibrary.Utility.FundingInformation;
import ClassLibrary.SimpleMethodologyOverview.DesignOverview;
import ClassLibrary.Representations.AuthorizationSource;
import ClassLibrary.LogicalDataDescription.DataStore;
import ClassLibrary.Discovery.Access;
import ClassLibrary.DataCapture.ImplementedInstrument;
import ClassLibrary.SamplingMethodology.SamplingProcedure;
import ClassLibrary.Conceptual.Universe;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.SimpleMethodologyOverview.MethodologyOverview;
import ClassLibrary.ComplexDataTypes.StudyIndicator;
import ClassLibrary.BusinessWorkflow.DataPipeline;
import ClassLibrary.ComplexDataTypes.StudyRelation;
import ClassLibrary.Conceptual.VariableCollection;
import ClassLibrary.SimpleMethodologyOverview.AlgorithmOverview;
import ClassLibrary.SimpleMethodologyOverview.ProcessOverview;
import ClassLibrary.Discovery.Coverage;
import ClassLibrary.Conceptual.UnitType;

public class Study extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] kindOfData;

	public final InternationalStructuredString[] overview;

	public final InternationalStructuredString[] bibliographicCitation;

	public StudyControl[] studyControl;

	public CollectionMember[] collectionMember;

	public InstanceVariable[] instanceVariable;

	public Population[] population;

	public FundingInformation[] fundingInformation;

	public DesignOverview[] designOverview;

	public AuthorizationSource[] authorizationSource;

	public QualityStatement[] qualityStatement;

	public DataStore[] dataStore;

	public StudySeries[] studySeries;

	public Access[] access;

	public ImplementedInstrument[] implementedInstrument;

	public SamplingProcedure[] samplingProcedure;

	public ExPostEvaluation exPostEvaluation;

	public Universe[] universe;

	public Embargo[] embargo;

	public Concept[] concept;

	public MethodologyOverview[] methodologyOverview;

	public Budget[] budget;

	public StudyIndicator studyIndicator;

	public DataPipeline dataPipeline;

	public StudyRelation[] studyRelation;

	public StudyRelation studyRelation;

	public VariableCollection[] variableCollection;

	public AlgorithmOverview[] algorithmOverview;

	public ProcessOverview[] processOverview;

	public Coverage[] coverage;

	public UnitType[] unitType;

}
