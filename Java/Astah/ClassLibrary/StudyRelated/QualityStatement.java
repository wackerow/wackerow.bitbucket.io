package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;

public class QualityStatement extends AnnotatedIdentifiable {

	public final ObjectName[] name;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] overview;

	public final InternationalStructuredString[] rationale;

	public final InternationalStructuredString[] usage;

	public Study study;

	public Standard[] standard;

}
