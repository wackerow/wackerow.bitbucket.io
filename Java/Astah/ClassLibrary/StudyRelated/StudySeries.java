package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.StudyIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.LogicalDataDescription.DataStoreLibrary;
import ClassLibrary.Conceptual.Concept;

public class StudySeries extends AnnotatedIdentifiable {

	public final ObjectName[] name;

	public final InternationalStructuredString[] overview;

	public final CollectionType[] type;

	public final InternationalStructuredString[] purpose;

	public final StudyIndicator[] contains;

	public final Boolean[] isOrdered;

	public StudyRelationStructure[] studyRelationStructure;

	public StructuredCollection[] structuredCollection;

	public Study[] study;

	public DataStoreLibrary dataStoreLibrary;

	public Concept[] concept;

}
