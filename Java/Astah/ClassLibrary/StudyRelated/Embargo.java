package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Agents.Agent;

public class Embargo extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public final DateRange[] embargoDates;

	public final InternationalStructuredString[] rationale;

	public Agent[] agent;

	public Study study;

	public Agent[] agent;

}
