package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.Utility.ExternalMaterial;

public class Standard extends AnnotatedIdentifiable {

	public ExternalMaterial[] externalMaterial;

	public ComplianceStatement complianceStatement;

	public QualityStatement qualityStatement;

}
