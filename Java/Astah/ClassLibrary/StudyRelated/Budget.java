package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.Utility.ExternalMaterial;

public class Budget extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public final ObjectName[] name;

	public ExternalMaterial[] externalMaterial;

	public Study[] study;

}
