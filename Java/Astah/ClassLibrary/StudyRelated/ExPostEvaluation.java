package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.Agents.Agent;

public class ExPostEvaluation extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] typeOfEvaluation;

	public final InternationalStructuredString[] evaluationProcess;

	public final InternationalStructuredString[] outcomes;

	public final Date[] completionDate;

	public Study[] study;

	public Agent[] agent;

}
