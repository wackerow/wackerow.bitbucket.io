package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Conceptual.Concept;

public class ComplianceStatement extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] externalComplianceCode;

	public final InternationalStructuredString[] usage;

	public Concept concept;

	public Standard[] standard;

}
