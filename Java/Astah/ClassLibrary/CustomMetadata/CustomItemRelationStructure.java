package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.CustomItemRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class CustomItemRelationStructure extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] criteria;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final CustomItemRelation[] hasMemberRelation;

	public CustomStructure[] customStructure;

	public RelationStructure[] relationStructure;

}
