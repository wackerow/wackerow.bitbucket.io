package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.ComplexDataTypes.VocabularyEntryRelation;
import ClassLibrary.ComplexDataTypes.VocabularyEntryIndicator;

public class VocabularyEntry extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] entryTerm;

	public final InternationalStructuredString[] definition;

	public CollectionMember[] collectionMember;

	public VocabularyEntryRelation vocabularyEntryRelation;

	public VocabularyEntryRelation vocabularyEntryRelation;

	public VocabularyEntryIndicator vocabularyEntryIndicator;

}
