package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.VocabularyEntryIndicator;
import Primitive Package.Boolean;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;

public class ControlledVocabulary extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final VocabularyEntryIndicator[] contains;

	public final Boolean[] isOrdered;

	public VocabularyRelationStructure[] vocabularyRelationStructure;

	public Concept[] concept;

	public StructuredCollection[] structuredCollection;

}
