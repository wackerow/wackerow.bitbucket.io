package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import Primitive Package.Integer;
import Primitive Package.String;
import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.Conceptual.RepresentedVariable;
import ClassLibrary.Representations.ValueDomain;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.ComplexDataTypes.CustomItemIndicator;
import ClassLibrary.ComplexDataTypes.CustomItemRelation;

public class CustomItem extends AnnotatedIdentifiable {

	public final Integer[] maxOccurs;

	public final Integer[] minOccurs;

	public final String[] key;

	public CustomValue customValue;

	public CollectionMember[] collectionMember;

	public RepresentedVariable[] representedVariable;

	public ValueDomain[] valueDomain;

	public Concept[] concept;

	public CustomItemIndicator customItemIndicator;

	public CustomItemRelation customItemRelation;

	public CustomItemRelation[] customItemRelation;

}
