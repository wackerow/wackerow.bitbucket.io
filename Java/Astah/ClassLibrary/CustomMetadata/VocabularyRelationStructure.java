package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.VocabularyEntryRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class VocabularyRelationStructure extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] criteria;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final VocabularyEntryRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public ControlledVocabulary[] controlledVocabulary;

}
