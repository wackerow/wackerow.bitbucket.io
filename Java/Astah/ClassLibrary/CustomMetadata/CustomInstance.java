package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.CustomValueIndicator;
import Primitive Package.Boolean;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.SimpleCollection;

public class CustomInstance extends Identifiable {

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final CollectionType[] type;

	public final CustomValueIndicator[] contains;

	public final Boolean[] isOrdered;

	public Concept[] concept;

	public CustomStructure[] customStructure;

	public SimpleCollection[] simpleCollection;

}
