package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.CustomItemIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.Conceptual.Concept;

public class CustomStructure extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final CustomItemIndicator[] contains;

	public final Boolean[] isOrdered;

	public StructuredCollection[] structuredCollection;

	public Concept[] concept;

	public CustomItemRelationStructure[] customItemRelationStructure;

	public CustomInstance customInstance;

}
