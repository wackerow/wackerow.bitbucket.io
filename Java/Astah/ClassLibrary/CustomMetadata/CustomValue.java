package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.ValueString;
import Primitive Package.String;
import ClassLibrary.Conceptual.InstanceVariable;
import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.ComplexDataTypes.CustomValueIndicator;
import ClassLibrary.ComplexDataTypes.CustomValueRelation;

public class CustomValue extends Identifiable {

	public final ValueString[] value;

	public final String[] key;

	public InstanceVariable[] instanceVariable;

	public CustomItem[] customItem;

	public CollectionMember[] collectionMember;

	public CustomValueIndicator customValueIndicator;

	public CustomValueRelation customValueRelation;

	public CustomValueRelation[] customValueRelation;

}
