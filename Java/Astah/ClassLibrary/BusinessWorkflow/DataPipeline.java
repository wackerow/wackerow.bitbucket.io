package ClassLibrary.BusinessWorkflow;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.BusinessProcessIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.SimpleCollection;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.StudyRelated.Study;

public class DataPipeline extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final BusinessProcessIndicator[] contains;

	public final Boolean[] isOrdered;

	public SimpleCollection[] simpleCollection;

	public Concept[] concept;

	public Study[] study;

}
