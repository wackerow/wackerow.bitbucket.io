package ClassLibrary.BusinessWorkflow;

import ClassLibrary.Workflows.WorkflowProcess;
import ClassLibrary.ComplexDataTypes.PairedExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.BusinessProcessCondition;
import ClassLibrary.ComplexDataTypes.BusinessProcessIndicator;

public class BusinessProcess extends WorkflowProcess {

	public final PairedExternalControlledVocabularyEntry[] standardModelUsed;

	public final BusinessProcessCondition[] preCondition;

	public final BusinessProcessCondition[] postCondition;

	public BusinessProcessIndicator businessProcessIndicator;

}
