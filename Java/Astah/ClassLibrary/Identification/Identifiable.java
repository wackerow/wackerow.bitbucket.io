package ClassLibrary.Identification;

import Primitive Package.String;
import ClassLibrary.EnumerationsRegExp.IsoDateType;
import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.LocalIdFormat;
import ClassLibrary.ComplexDataTypes.BasedOnObjectInformation;
import ClassLibrary.ComplexDataTypes.DescribedRelationship;

public abstract class Identifiable {

	public final String agency;

	public final String id;

	public final String version;

	public final String[] versionResponsibility;

	public final String[] versionRationale;

	public final IsoDateType[] versionDate;

	public final Boolean isUniversallyUnique;

	public final Boolean isPersistent;

	public final LocalIdFormat[] localId;

	public final BasedOnObjectInformation[] basedOnObject;

	public BasedOnObjectInformation[] basedOnObjectInformation;

	public DescribedRelationship describedRelationship;

}
