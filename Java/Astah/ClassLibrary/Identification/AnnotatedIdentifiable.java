package ClassLibrary.Identification;

import ClassLibrary.ComplexDataTypes.Annotation;
import ClassLibrary.Methodologies.AppliedUse;
import ClassLibrary.Utility.ExternalMaterial;

public abstract class AnnotatedIdentifiable extends Identifiable {

	public final Annotation[] hasAnnotation;

	public AppliedUse[] appliedUse;

	public ExternalMaterial[] externalMaterial;

}
