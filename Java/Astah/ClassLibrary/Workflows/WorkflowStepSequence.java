package ClassLibrary.Workflows;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.WorkflowStepIndicator;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import Primitive Package.Boolean;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.SimpleCollection;
import ClassLibrary.ComplexDataTypes.WorkflowStepSequenceIndicator;
import ClassLibrary.SamplingMethodology.SamplingProcess;
import ClassLibrary.ComplexDataTypes.ElseIfAction;

public class WorkflowStepSequence extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] typeOfWorkflowStepSequence;

	public final CollectionType[] type;

	public final WorkflowStepIndicator[] contains;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final Boolean[] isOrdered;

	public IfThenElse[] ifThenElse;

	public Concept[] concept;

	public SimpleCollection[] simpleCollection;

	public WorkflowStepSequenceIndicator workflowStepSequenceIndicator;

	public SamplingProcess[] samplingProcess;

	public ConditionalControlConstruct[] conditionalControlConstruct;

	public ElseIfAction elseIfAction;

	public WorkflowProcess[] workflowProcess;

}
