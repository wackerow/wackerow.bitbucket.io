package ClassLibrary.Workflows;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.Binding;
import ClassLibrary.ProcessPattern.ProcessStep;
import ClassLibrary.ComplexDataTypes.WorkflowStepIndicator;
import ClassLibrary.ComplexDataTypes.WorkflowStepRelation;

public abstract class WorkflowStep extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final InternationalStructuredString[] usage;

	public final InternationalStructuredString[] overview;

	public final Binding[] hasInformationFlow;

	public Parameter[] parameter;

	public Parameter[] parameter;

	public WorkflowService[] workflowService;

	public Split[] split;

	public SplitJoin[] splitJoin;

	public ProcessStep[] processStep;

	public WorkflowStepIndicator workflowStepIndicator;

	public WorkflowStepRelation[] workflowStepRelation;

	public WorkflowStepRelation workflowStepRelation;

}
