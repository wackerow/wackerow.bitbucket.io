package ClassLibrary.Workflows;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.SimpleMethodologyOverview.DesignOverview;
import ClassLibrary.SimpleMethodologyOverview.AlgorithmOverview;
import ClassLibrary.ProcessPattern.Process;

public class WorkflowProcess extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final InternationalStructuredString[] usage;

	public DesignOverview[] designOverview;

	public AlgorithmOverview[] algorithmOverview;

	public Process[] process;

	public WorkflowStepSequence workflowStepSequence;

}
