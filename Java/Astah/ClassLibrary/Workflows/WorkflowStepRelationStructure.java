package ClassLibrary.Workflows;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.WorkflowStepRelation;
import ClassLibrary.EnumerationsRegExp.TemporalRelationSpecification;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class WorkflowStepRelationStructure extends AnnotatedIdentifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final WorkflowStepRelation[] hasMemberRelation;

	public final TemporalRelationSpecification[] hasTemporalRelationSpecification;

	public RelationStructure[] relationStructure;

	public StructuredWorkflowSteps[] structuredWorkflowSteps;

}
