package ClassLibrary.Workflows;

import Primitive Package.UnlimitedNatural;
import ClassLibrary.ComplexDataTypes.ValueString;
import Primitive Package.Boolean;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Methodologies.Result;
import ClassLibrary.DataCapture.ResponseDomain;
import ClassLibrary.Representations.ValueDomain;
import ClassLibrary.ComplexDataTypes.Binding;
import ClassLibrary.ProcessPattern.ProcessStep;
import ClassLibrary.SamplingMethodology.SampleFrame;

public class Parameter {

	public final UnlimitedNatural[] alias;

	public final ValueString[] defaultValue;

	public final Boolean[] isArray;

	public final UnlimitedNatural[] limitArrayIndex;

	public final String agency;

	public final String id;

	public final String version;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public Result[] result;

	public WorkflowStep workflowStep;

	public ResponseDomain responseDomain;

	public WorkflowStep workflowStep;

	public ValueDomain[] valueDomain;

	public Binding binding;

	public ProcessStep processStep;

	public ProcessStep processStep;

	public SampleFrame sampleFrame;

	public Binding binding;

	public SampleFrame sampleFrame;

	public Result[] result;

}
