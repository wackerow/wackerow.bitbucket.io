package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.ElseIfAction;

public class IfThenElse extends ConditionalControlConstruct {

	public final ElseIfAction[] elseIf;

	public WorkflowStepSequence workflowStepSequence;

}
