package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.CommandCode;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;

public class ComputationAction extends Act {

	public final InternationalStructuredString[] activityDescription;

	public final CommandCode[] usesCommandCode;

	public final ExternalControlledVocabularyEntry[] typeOfComputation;

}
