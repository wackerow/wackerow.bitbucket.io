package ClassLibrary.Workflows;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.ProcessPattern.Service;
import ClassLibrary.Agents.Agent;

public class WorkflowService extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] serviceInterface;

	public final ExternalControlledVocabularyEntry[] serviceLocation;

	public final Date[] estimatedDuration;

	public WorkflowStep workflowStep;

	public Service[] service;

	public Agent[] agent;

}
