package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;

public class MetadataDrivenAction extends Act {

	public final InternationalStructuredString[] activityDescription;

	public final ExternalControlledVocabularyEntry[] typeOfMetadataDrivenAction;

	public final InternationalStructuredString[] quasiVTL;

}
