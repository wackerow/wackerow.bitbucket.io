package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.CommandCode;

public class Loop extends ConditionalControlConstruct {

	public final CommandCode[] initialValue;

	public final CommandCode[] stepValue;

}
