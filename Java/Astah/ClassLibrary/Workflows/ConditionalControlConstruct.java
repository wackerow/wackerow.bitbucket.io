package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.CommandCode;

public class ConditionalControlConstruct extends WorkflowStep {

	public final CommandCode[] condition;

	public WorkflowStepSequence workflowStepSequence;

}
