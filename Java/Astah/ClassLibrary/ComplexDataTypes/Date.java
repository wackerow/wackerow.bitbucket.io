package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.IsoDateType;

public class Date {

	public final IsoDateType[] isoDate;

	public final NonIsoDateType[] nonIsoDate;

}
