package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.ValueRelationshipType;
import ClassLibrary.CollectionsPattern.ComparisonMap;
import ClassLibrary.Conceptual.InstanceVariable;

public class InstanceVariableValueMap {

	public final ValueRelationshipType valueRelationship;

	public final ValueString[] setValue;

	public final CorrespondenceType hasCorrespondenceType;

	public ComparisonMap[] comparisonMap;

	public InstanceVariable[] instanceVariable;

	public InstanceVariable[] instanceVariable;

}
