package ClassLibrary.ComplexDataTypes;

public class StandardKeyValuePair {

	public final ExternalControlledVocabularyEntry[] attributeKey;

	public final ExternalControlledVocabularyEntry[] attributeValue;

}
