package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import Primitive Package.Boolean;

public class NumberRange {

	public final LabelForDisplay[] label;

	public final NumberRangeValue[] highCode;

	public final DoubleNumberRangeValue[] highCodeDouble;

	public final NumberRangeValue[] lowCode;

	public final DoubleNumberRangeValue[] lowCodeDouble;

	public final String[] regExp;

	public final Boolean isTopCoded;

	public final Boolean isBottomCoded;

}
