package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.EnumerationsRegExp.TemporalRelationSpecification;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.Workflows.WorkflowStep;

public class WorkflowStepRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final TemporalRelationSpecification[] hasTemporalRelationSpecification;

	public MemberRelation[] memberRelation;

	public WorkflowStep[] workflowStep;

	public WorkflowStep[] workflowStep;

}
