package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.anyUri;
import Primitive Package.String;

public class Polygon {

	public final anyUri[] uri;

	public final String[] polygonLinkCode;

	public final ExternalControlledVocabularyEntry[] shapeFileFormat;

	public final SpatialPoint[] point;

}
