package ClassLibrary.ComplexDataTypes;

import ClassLibrary.Identification.Identifiable;

public class DescribedRelationship {

	public final InternationalStructuredString[] rationale;

	public Identifiable[] identifiable;

}
