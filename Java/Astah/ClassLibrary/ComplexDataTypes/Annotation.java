package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.LanguageSpecification;
import Primitive Package.String;
import ClassLibrary.EnumerationsRegExp.IsoDateType;

public class Annotation {

	public final InternationalString[] title;

	public final InternationalString[] subTitle;

	public final InternationalString[] alternativeTitle;

	public final AgentAssociation[] creator;

	public final AgentAssociation[] publisher;

	public final AgentAssociation[] contributor;

	public final AnnotationDate[] date;

	public final LanguageSpecification[] languageOfObject;

	public final InternationalIdentifier[] identifier;

	public final InternationalString[] copyright;

	public final ExternalControlledVocabularyEntry[] typeOfResource;

	public final InternationalString[] informationSource;

	public final String[] versionIdentification;

	public final AgentAssociation[] versioningAgent;

	public final InternationalString[] summary;

	public final ResourceIdentifier[] relatedResource;

	public final InternationalString[] provenance;

	public final InternationalString[] rights;

	public final IsoDateType[] recordCreationDate;

	public final IsoDateType[] recordLastRevisionDate;

}
