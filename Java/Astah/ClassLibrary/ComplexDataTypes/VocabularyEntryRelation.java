package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.CustomMetadata.VocabularyEntry;

public class VocabularyEntryRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public MemberRelation[] memberRelation;

	public VocabularyEntry[] vocabularyEntry;

	public VocabularyEntry[] vocabularyEntry;

}
