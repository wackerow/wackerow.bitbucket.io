package ClassLibrary.ComplexDataTypes;

import Primitive Package.Integer;
import ClassLibrary.Representations.ClassificationSeries;
import ClassLibrary.CollectionsPattern.MemberIndicator;

public class ClassificationSeriesIndicator {

	public final Integer[] index;

	public ClassificationSeries[] classificationSeries;

	public MemberIndicator[] memberIndicator;

}
