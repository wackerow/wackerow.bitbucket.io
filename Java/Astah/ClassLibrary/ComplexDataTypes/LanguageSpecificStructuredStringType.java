package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import ClassLibrary.XMLSchemaDatatypes.language;
import Primitive Package.Boolean;
import Primitive Package.UnlimitedNatural;

public class LanguageSpecificStructuredStringType {

	public final String[] formattedContent;

	public final language[] language;

	public final Boolean[] isTranslated;

	public final Boolean[] isTranslatable;

	public final language[] translationSourceLanguage;

	public final UnlimitedNatural[] translationDate;

	public final Boolean[] isPlainText;

}
