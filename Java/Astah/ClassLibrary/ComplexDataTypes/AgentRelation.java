package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.Agents.Agent;

public class AgentRelation {

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final RelationSpecification hasRelationSpecification;

	public final TotalityType[] totality;

	public MemberRelation[] memberRelation;

	public Agent[] agent;

	public Agent[] agent;

}
