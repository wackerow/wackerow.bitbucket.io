package ClassLibrary.ComplexDataTypes;

public class ContactInformation {

	public final WebLink[] website;

	public final Email[] hasEmail;

	public final ElectronicMessageSystem[] electronicMessaging;

	public final Address[] hasAddress;

	public final Telephone[] hasTelephone;

}
