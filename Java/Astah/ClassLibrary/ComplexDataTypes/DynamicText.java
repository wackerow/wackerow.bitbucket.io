package ClassLibrary.ComplexDataTypes;

import Primitive Package.Boolean;
import ClassLibrary.XMLSchemaDatatypes.LanguageSpecification;

public class DynamicText {

	public final DynamicTextContent[] textContent;

	public final Boolean[] isStructureRequired;

	public final LanguageSpecification[] audienceLanguage;

}
