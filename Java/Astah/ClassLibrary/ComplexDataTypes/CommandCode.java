package ClassLibrary.ComplexDataTypes;

public class CommandCode {

	public final InternationalStructuredString[] description;

	public final CommandFile[] usesCommandFile;

	public final Command[] usesCommand;

}
