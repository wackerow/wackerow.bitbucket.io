package ClassLibrary.ComplexDataTypes;

import Primitive Package.Integer;
import ClassLibrary.Representations.ClassificationIndexEntry;
import ClassLibrary.CollectionsPattern.MemberIndicator;

public class ClassificationIndexEntryIndicator {

	public final Integer[] index;

	public ClassificationIndexEntry[] classificationIndexEntry;

	public MemberIndicator[] memberIndicator;

}
