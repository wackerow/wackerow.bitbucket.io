package ClassLibrary.ComplexDataTypes;

public class CategoryStatistic {

	public final ExternalControlledVocabularyEntry[] typeOfCategoryStatistic;

	public final Statistic[] hasStatistic;

	public final ValueString[] categoryValue;

	public final ValueString[] filterValue;

	public CodeIndicator[] codeIndicator;

}
