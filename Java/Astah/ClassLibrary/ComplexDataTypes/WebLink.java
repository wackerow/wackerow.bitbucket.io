package ClassLibrary.ComplexDataTypes;

import Primitive Package.Boolean;
import ClassLibrary.XMLSchemaDatatypes.anyUri;

public class WebLink {

	public final Boolean[] isPreferred;

	public final anyUri[] uri;

	public final ExternalControlledVocabularyEntry[] typeOfWebsite;

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] privacy;

}
