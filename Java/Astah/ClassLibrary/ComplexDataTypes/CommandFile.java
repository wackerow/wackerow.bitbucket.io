package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.anyUri;

public class CommandFile {

	public final ExternalControlledVocabularyEntry[] programLanguage;

	public final InternationalString[] location;

	public final anyUri[] uri;

}
