package ClassLibrary.ComplexDataTypes;

import Primitive Package.Integer;
import ClassLibrary.Representations.ClassificationItem;
import ClassLibrary.CollectionsPattern.MemberIndicator;

public class ClassificationItemIndicator {

	public final Integer[] index;

	public final Integer[] hasLevel;

	public ClassificationItem[] classificationItem;

	public MemberIndicator[] memberIndicator;

}
