package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.Representations.ClassificationSeries;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class ClassificationSeriesRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public ClassificationSeries[] classificationSeries;

	public ClassificationSeries[] classificationSeries;

	public MemberRelation[] memberRelation;

}
