package ClassLibrary.ComplexDataTypes;

import ClassLibrary.Identification.Identifiable;

public class BasedOnObjectInformation {

	public final InternationalString[] basedOnRationaleDescription;

	public final ExternalControlledVocabularyEntry[] basedOnRationaleCode;

	public Identifiable[] identifiable;

}
