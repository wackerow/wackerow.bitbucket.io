package ClassLibrary.ComplexDataTypes;

public class PrivateImage extends Image {

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] privacy;

}
