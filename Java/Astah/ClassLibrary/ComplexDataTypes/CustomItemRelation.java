package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.CustomMetadata.CustomItem;

public class CustomItemRelation {

	public final RelationSpecification hasRelationSepcification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public MemberRelation[] memberRelation;

	public CustomItem[] customItem;

	public CustomItem[] customItem;

}
