package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.FormatDescription.PhysicalRecordSegment;

public class PhysicalRecordSegmentRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public MemberRelation[] memberRelation;

	public PhysicalRecordSegment[] physicalRecordSegment;

	public PhysicalRecordSegment[] physicalRecordSegment;

}
