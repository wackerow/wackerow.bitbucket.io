package ClassLibrary.ComplexDataTypes;

public class TypedDescriptiveText {

	public final ExternalControlledVocabularyEntry[] typeOfContent;

	public final InternationalStructuredString[] descriptiveText;

}
