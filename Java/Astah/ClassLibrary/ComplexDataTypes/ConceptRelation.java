package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.Conceptual.Concept;

public class ConceptRelation {

	public final RelationSpecification hasRelationSepcification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public MemberRelation[] memberRelation;

	public Concept[] concept;

	public Concept[] concept;

}
