package ClassLibrary.ComplexDataTypes;

import Primitive Package.Boolean;
import Primitive Package.Integer;
import Primitive Package.Real;
import ClassLibrary.Conceptual.UnitType;
import ClassLibrary.Conceptual.Universe;

public class TargetSample {

	public final Boolean isPrimary;

	public final Integer[] targetSize;

	public final Real[] targetPercent;

	public UnitType unitType;

	public Universe universe;

}
