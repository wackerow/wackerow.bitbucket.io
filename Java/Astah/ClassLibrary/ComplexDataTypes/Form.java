package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import ClassLibrary.XMLSchemaDatatypes.anyUri;
import Primitive Package.Boolean;

public class Form {

	public final String[] formNumber;

	public final anyUri[] uri;

	public final InternationalString[] statement;

	public final Boolean[] isRequired;

}
