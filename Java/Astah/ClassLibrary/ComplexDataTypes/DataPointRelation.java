package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.LogicalDataDescription.DataPoint;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class DataPointRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public DataPoint[] dataPoint;

	public DataPoint[] dataPoint;

	public MemberRelation[] memberRelation;

}
