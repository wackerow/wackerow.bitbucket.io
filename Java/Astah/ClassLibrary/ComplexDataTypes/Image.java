package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.anyUri;
import Primitive Package.Integer;
import ClassLibrary.XMLSchemaDatatypes.LanguageSpecification;

public class Image {

	public final anyUri[] uri;

	public final ExternalControlledVocabularyEntry[] typeOfImage;

	public final Integer[] dpi;

	public final LanguageSpecification[] languageOfImage;

}
