package ClassLibrary.ComplexDataTypes;

import Primitive Package.Boolean;
import ClassLibrary.EnumerationsRegExp.ComputationBaseList;
import Primitive Package.Real;

public class Statistic {

	public final Boolean[] isWeighted;

	public final ComputationBaseList[] computationBase;

	public final Real[] decimalValue;

	public final Real[] doubleValue;

}
