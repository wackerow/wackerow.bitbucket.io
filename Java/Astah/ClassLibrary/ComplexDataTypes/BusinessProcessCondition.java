package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import ClassLibrary.LogicalDataDescription.LogicalRecord;

public class BusinessProcessCondition {

	public final String[] sql;

	public final CommandCode[] rejectionCriteria;

	public final InternationalStructuredString[] dataDescription;

	public LogicalRecord[] logicalRecord;

}
