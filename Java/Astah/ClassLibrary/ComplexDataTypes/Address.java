package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import Primitive Package.Boolean;

public class Address {

	public final ExternalControlledVocabularyEntry[] typeOfAddress;

	public final String[] line;

	public final String[] cityPlaceLocal;

	public final String[] stateProvince;

	public final String[] postalCode;

	public final ExternalControlledVocabularyEntry[] countryCode;

	public final ExternalControlledVocabularyEntry[] timeZone;

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] privacy;

	public final Boolean[] isPreferred;

	public final SpatialPoint[] geographicPoint;

	public final ExternalControlledVocabularyEntry[] regionalCoverage;

	public final ExternalControlledVocabularyEntry[] typeOfLocation;

	public final ObjectName[] locationName;

}
