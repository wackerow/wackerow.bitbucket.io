package ClassLibrary.ComplexDataTypes;

import Primitive Package.Integer;
import ClassLibrary.Representations.StatisticalClassification;
import ClassLibrary.CollectionsPattern.MemberIndicator;

public class StatisticalClassificationIndicator {

	public final Integer[] index;

	public StatisticalClassification[] statisticalClassification;

	public MemberIndicator[] memberIndicator;

}
