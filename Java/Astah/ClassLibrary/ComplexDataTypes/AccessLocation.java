package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.anyUri;

public class AccessLocation {

	public final anyUri[] uri;

	public final ExternalControlledVocabularyEntry[] mimeType;

	public final InternationalString[] physicalLocation;

}
