package ClassLibrary.ComplexDataTypes;

import Primitive Package.Boolean;

public class OrganizationName extends ObjectName {

	public final InternationalString[] abbreviation;

	public final ExternalControlledVocabularyEntry[] typeOfOrganizationName;

	public final DateRange[] effectiveDates;

	public final Boolean[] isFormal;

}
