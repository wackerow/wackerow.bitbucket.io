package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.LogicalDataDescription.LogicalRecord;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class LogicalRecordRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public LogicalRecord[] logicalRecord;

	public MemberRelation[] memberRelation;

	public LogicalRecord[] logicalRecord;

}
