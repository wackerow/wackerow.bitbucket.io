package ClassLibrary.ComplexDataTypes;

import Primitive Package.Real;

public class AreaCoverage {

	public final ExternalControlledVocabularyEntry[] typeOfArea;

	public final ExternalControlledVocabularyEntry[] measurementUnit;

	public final Real[] areaMeasure;

}
