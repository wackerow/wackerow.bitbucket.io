package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.SpatialRelationSpecification;
import Primitive Package.Boolean;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.Conceptual.UnitType;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class GeographicUnitTypeRelation {

	public final RelationSpecification hasRelationSpecification;

	public final SpatialRelationSpecification[] hasSpatialRelationSpecification;

	public final Boolean[] isExhaustiveCoverage;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public UnitType[] unitType;

	public UnitType[] unitType;

	public MemberRelation[] memberRelation;

}
