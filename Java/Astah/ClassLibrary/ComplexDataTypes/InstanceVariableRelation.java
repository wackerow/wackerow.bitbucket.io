package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.Conceptual.InstanceVariable;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class InstanceVariableRelation {

	public final TotalityType[] totality;

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public InstanceVariable[] instanceVariable;

	public InstanceVariable[] instanceVariable;

	public MemberRelation[] memberRelation;

}
