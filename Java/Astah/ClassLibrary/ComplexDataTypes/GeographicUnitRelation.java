package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.SpatialRelationSpecification;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.GeographicClassification.GeographicUnit;

public class GeographicUnitRelation {

	public final RelationSpecification hasRelationSpecification;

	public final SpatialRelationSpecification[] hasSpatialRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public MemberRelation[] memberRelation;

	public GeographicUnit[] geographicUnit;

	public GeographicUnit[] geographicUnit;

}
