package ClassLibrary.ComplexDataTypes;

import ClassLibrary.Agents.Agent;

public class AgentAssociation {

	public final BibliographicName[] agentName;

	public final PairedExternalControlledVocabularyEntry[] role;

	public Agent[] agent;

}
