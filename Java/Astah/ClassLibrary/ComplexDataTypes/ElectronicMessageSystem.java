package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import Primitive Package.Boolean;

public class ElectronicMessageSystem {

	public final String[] contactAddress;

	public final ExternalControlledVocabularyEntry[] typeOfService;

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] privacy;

	public final Boolean[] isPreferred;

}
