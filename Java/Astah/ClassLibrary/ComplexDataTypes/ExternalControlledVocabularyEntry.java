package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import ClassLibrary.XMLSchemaDatatypes.anyUri;
import ClassLibrary.XMLSchemaDatatypes.language;

public class ExternalControlledVocabularyEntry {

	public final String[] controlledVocabularyID;

	public final String[] controlledVocabularyName;

	public final String[] controlledVocabularyAgencyName;

	public final String[] controlledVocabularyVersionID;

	public final String[] otherValue;

	public final anyUri[] uri;

	public final String[] content;

	public final language[] language;

}
