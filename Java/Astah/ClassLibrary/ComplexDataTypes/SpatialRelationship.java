package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.SpatialObjectPairs;
import ClassLibrary.EnumerationsRegExp.SpatialRelationSpecification;
import ClassLibrary.GeographicClassification.GeographicUnit;

public class SpatialRelationship {

	public final SpatialObjectPairs[] hasSpatialObjectPair;

	public final SpatialRelationSpecification[] hasSpatialRelationSpecification;

	public final Date[] eventDate;

	public GeographicUnit[] geographicUnit;

}
