package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.anyUri;
import Primitive Package.String;

public class SpatialLine {

	public final anyUri[] uri;

	public final String[] lineLinkCode;

	public final ExternalControlledVocabularyEntry[] shapeFileFormat;

	public final SpatialPoint[] point;

}
