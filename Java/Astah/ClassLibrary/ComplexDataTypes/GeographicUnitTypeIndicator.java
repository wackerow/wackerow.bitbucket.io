package ClassLibrary.ComplexDataTypes;

import Primitive Package.Integer;
import ClassLibrary.CollectionsPattern.MemberIndicator;
import ClassLibrary.Conceptual.UnitType;

public class GeographicUnitTypeIndicator {

	public final Integer[] index;

	public final Integer[] isInLevel;

	public MemberIndicator[] memberIndicator;

	public UnitType[] unitType;

}
