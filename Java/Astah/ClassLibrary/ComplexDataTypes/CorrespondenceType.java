package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.MappingRelation;

public class CorrespondenceType {

	public final InternationalStructuredString[] commonality;

	public final InternationalStructuredString[] difference;

	public final ExternalControlledVocabularyEntry[] commonalityTypeCode;

	public final MappingRelation[] hasMappingRelation;

}
