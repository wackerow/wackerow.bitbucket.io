package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.CollectionsPattern.MemberRelation;
import ClassLibrary.Representations.ClassificationIndexEntry;

public class IndexEntryRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public MemberRelation[] memberRelation;

	public ClassificationIndexEntry[] classificationIndexEntry;

	public ClassificationIndexEntry[] classificationIndexEntry;

}
