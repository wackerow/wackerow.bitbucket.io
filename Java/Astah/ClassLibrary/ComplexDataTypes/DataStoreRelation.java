package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.LogicalDataDescription.DataStore;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class DataStoreRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public DataStore[] dataStore;

	public DataStore[] dataStore;

	public MemberRelation[] memberRelation;

}
