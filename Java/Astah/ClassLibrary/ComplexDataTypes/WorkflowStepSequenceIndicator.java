package ClassLibrary.ComplexDataTypes;

import Primitive Package.Integer;
import ClassLibrary.CollectionsPattern.MemberIndicator;
import ClassLibrary.Workflows.WorkflowStepSequence;

public class WorkflowStepSequenceIndicator {

	public final Integer[] index;

	public MemberIndicator[] memberIndicator;

	public WorkflowStepSequence[] workflowStepSequence;

}
