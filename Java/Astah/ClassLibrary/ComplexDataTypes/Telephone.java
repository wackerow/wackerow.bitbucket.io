package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import Primitive Package.Boolean;

public class Telephone {

	public final String[] telephoneNumber;

	public final ExternalControlledVocabularyEntry[] typeOfTelephone;

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] privacy;

	public final Boolean[] isPreferred;

}
