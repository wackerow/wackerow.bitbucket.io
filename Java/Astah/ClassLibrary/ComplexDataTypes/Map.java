package ClassLibrary.ComplexDataTypes;

import ClassLibrary.CollectionsPattern.ComparisonMap;
import ClassLibrary.Conceptual.Concept;

public class Map {

	public final DateRange[] validDates;

	public final CorrespondenceType[] hasCorrespondenceType;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public ComparisonMap[] comparisonMap;

	public Concept[] concept;

	public Concept[] concept;

}
