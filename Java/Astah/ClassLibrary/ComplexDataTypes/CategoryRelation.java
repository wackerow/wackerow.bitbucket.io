package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.Conceptual.Category;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class CategoryRelation {

	public final TotalityType[] totality;

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public Category[] category;

	public MemberRelation[] memberRelation;

	public Category[] category;

}
