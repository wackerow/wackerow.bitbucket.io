package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.FormatDescription.ValueMapping;
import ClassLibrary.CollectionsPattern.MemberRelation;

public class ValueMappingRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public ValueMapping[] valueMapping;

	public MemberRelation[] memberRelation;

	public ValueMapping[] valueMapping;

}
