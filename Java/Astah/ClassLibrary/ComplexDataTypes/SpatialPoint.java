package ClassLibrary.ComplexDataTypes;

public class SpatialPoint {

	public final SpatialCoordinate[] xCoordinate;

	public final SpatialCoordinate[] yCoordinate;

}
