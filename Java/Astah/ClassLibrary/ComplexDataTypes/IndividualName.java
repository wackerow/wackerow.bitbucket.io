package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import ClassLibrary.EnumerationsRegExp.SexSpecificationType;
import Primitive Package.Boolean;

public class IndividualName {

	public final String[] prefix;

	public final String[] firstGiven;

	public final String[] middle;

	public final String[] lastFamily;

	public final String[] suffix;

	public final InternationalString[] fullName;

	public final DateRange[] effectiveDates;

	public final InternationalString[] abbreviation;

	public final ExternalControlledVocabularyEntry[] typeOfIndividualName;

	public final SexSpecificationType[] sex;

	public final Boolean[] isPreferred;

	public final ExternalControlledVocabularyEntry[] context;

	public final Boolean[] isFormal;

}
