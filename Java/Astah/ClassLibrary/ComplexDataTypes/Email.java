package ClassLibrary.ComplexDataTypes;

import Primitive Package.String;
import Primitive Package.Boolean;

public class Email {

	public final String[] internetEmail;

	public final ExternalControlledVocabularyEntry[] typeOfEmail;

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] privacy;

	public final Boolean[] isPreferred;

}
