package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.InternationalString;
import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.Agents.Agent;

public class AuthorizationSource extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] statementOfAuthorization;

	public final InternationalString[] legalMandate;

	public final Date[] authorizationDate;

	public final InternationalStructuredString[] purpose;

	public Study[] study;

	public Agent[] agent;

	public ClassificationItem[] classificationItem;

}
