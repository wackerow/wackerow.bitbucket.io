package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalString;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.ComplexDataTypes.CommandCode;
import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.ComplexDataTypes.ClassificationIndexEntryIndicator;
import ClassLibrary.ComplexDataTypes.IndexEntryRelation;

public class ClassificationIndexEntry extends AnnotatedIdentifiable {

	public final InternationalString[] entry;

	public final DateRange[] validDates;

	public final CommandCode[] codingInstruction;

	public CollectionMember[] collectionMember;

	public ClassificationIndexEntryIndicator classificationIndexEntryIndicator;

	public IndexEntryRelation indexEntryRelation;

	public IndexEntryRelation indexEntryRelation;

}
