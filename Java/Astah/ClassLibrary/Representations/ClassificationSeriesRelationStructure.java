package ClassLibrary.Representations;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.ClassificationSeriesRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class ClassificationSeriesRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final ClassificationSeriesRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public ClassificationFamily[] classificationFamily;

}
