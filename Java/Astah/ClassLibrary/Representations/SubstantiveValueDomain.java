package ClassLibrary.Representations;

import ClassLibrary.Conceptual.RepresentedVariable;
import ClassLibrary.Conceptual.SubstantiveConceptualDomain;

public class SubstantiveValueDomain extends ValueDomain {

	public RepresentedVariable representedVariable;

	public ValueAndConceptDescription[] valueAndConceptDescription;

	public CodeList[] codeList;

	public SubstantiveConceptualDomain[] substantiveConceptualDomain;

}
