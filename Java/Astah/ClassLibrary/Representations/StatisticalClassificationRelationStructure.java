package ClassLibrary.Representations;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.StatisticalClassificationRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class StatisticalClassificationRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final StatisticalClassificationRelation[] hasMemberRelation;

	public ClassificationSeries[] classificationSeries;

	public RelationStructure[] relationStructure;

}
