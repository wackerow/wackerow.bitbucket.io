package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.CodeIndicator;
import Primitive Package.Boolean;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.Conceptual.CategorySet;
import ClassLibrary.DataCapture.CodeListResponseDomain;

public class CodeList extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final CodeIndicator[] contains;

	public final Boolean[] isOrdered;

	public LevelStructure[] levelStructure;

	public ClassificationRelationStructure classificationRelationStructure;

	public Concept[] concept;

	public StructuredCollection[] structuredCollection;

	public SentinelValueDomain sentinelValueDomain;

	public CategorySet[] categorySet;

	public SubstantiveValueDomain substantiveValueDomain;

	public CodeListResponseDomain codeListResponseDomain;

}
