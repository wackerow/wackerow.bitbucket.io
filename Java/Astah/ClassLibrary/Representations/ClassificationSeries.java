package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.StatisticalClassificationIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.Agents.Agent;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.ComplexDataTypes.ClassificationSeriesIndicator;
import ClassLibrary.ComplexDataTypes.ClassificationSeriesRelation;

public class ClassificationSeries extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] context;

	public final ExternalControlledVocabularyEntry[] objectsOrUnitsClassified;

	public final ExternalControlledVocabularyEntry[] subject;

	public final ExternalControlledVocabularyEntry[] keyword;

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final StatisticalClassificationIndicator[] contains;

	public final Boolean[] isOrdered;

	public StructuredCollection[] structuredCollection;

	public StatisticalClassificationRelationStructure[] statisticalClassificationRelationStructure;

	public Agent[] agent;

	public Concept[] concept;

	public ClassificationSeriesIndicator classificationSeriesIndicator;

	public ClassificationSeriesRelation classificationSeriesRelation;

	public ClassificationSeriesRelation[] classificationSeriesRelation;

}
