package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.Level;
import ClassLibrary.ComplexDataTypes.DateRange;

public class LevelStructure extends AnnotatedIdentifiable {

	public final ObjectName[] name;

	public final InternationalStructuredString[] usage;

	public final Level[] containsLevel;

	public final DateRange[] validDateRange;

	public CorrespondenceTable correspondenceTable;

	public CorrespondenceTable correspondenceTable;

	public CodeList codeList;

}
