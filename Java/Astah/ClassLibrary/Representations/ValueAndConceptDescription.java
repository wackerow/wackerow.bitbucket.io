package ClassLibrary.Representations;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.TypedString;
import Primitive Package.String;
import ClassLibrary.EnumerationsRegExp.CategoryRelationCode;
import ClassLibrary.Conceptual.ConceptualDomain;

public class ValueAndConceptDescription extends Identifiable {

	public final InternationalStructuredString[] description;

	public final ExternalControlledVocabularyEntry[] logicalExpression;

	public final TypedString[] regularExpression;

	public final String[] minimumValueInclusive;

	public final String[] maximumValueInclusive;

	public final String[] minimumValueExclusive;

	public final String[] maximumValueExclusive;

	public final CategoryRelationCode[] classificationLevel;

	public final ExternalControlledVocabularyEntry[] formatPattern;

	public ConceptualDomain conceptualDomain;

	public SentinelValueDomain sentinelValueDomain;

	public SubstantiveValueDomain substantiveValueDomain;

}
