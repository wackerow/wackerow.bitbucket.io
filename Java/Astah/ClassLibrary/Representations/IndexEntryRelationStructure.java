package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.IndexEntryRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class IndexEntryRelationStructure extends AnnotatedIdentifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final IndexEntryRelation[] hasMemberRelation;

	public ClassificationIndex[] classificationIndex;

	public RelationStructure[] relationStructure;

}
