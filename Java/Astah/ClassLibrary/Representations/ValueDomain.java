package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.Workflows.Parameter;
import ClassLibrary.CustomMetadata.CustomItem;

public abstract class ValueDomain extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public final ExternalControlledVocabularyEntry[] recommendedDataType;

	public Parameter parameter;

	public CustomItem customItem;

}
