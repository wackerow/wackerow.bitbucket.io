package ClassLibrary.Representations;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.CategoryRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class ClassificationRelationStructure extends Identifiable {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final LabelForDisplay[] displayLabel;

	public final CategoryRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public CodeList[] codeList;

}
