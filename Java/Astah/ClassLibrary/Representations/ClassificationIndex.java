package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.XMLSchemaDatatypes.LanguageSpecification;
import ClassLibrary.ComplexDataTypes.InternationalString;
import ClassLibrary.ComplexDataTypes.CommandCode;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ClassificationIndexEntryIndicator;
import Primitive Package.Boolean;
import ClassLibrary.Agents.Agent;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.Utility.ExternalMaterial;

public class ClassificationIndex extends AnnotatedIdentifiable {

	public final Date[] releaseDate;

	public final LanguageSpecification[] availableLanguage;

	public final InternationalString[] corrections;

	public final CommandCode[] codingInstruction;

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final ClassificationIndexEntryIndicator[] contains;

	public final Boolean[] isOrdered;

	public Agent agent;

	public Agent[] agent;

	public Concept[] concept;

	public StructuredCollection[] structuredCollection;

	public IndexEntryRelationStructure[] indexEntryRelationStructure;

	public ExternalMaterial[] externalMaterial;

	public StatisticalClassification[] statisticalClassification;

	public ClassificationFamily[] classificationFamily;

}
