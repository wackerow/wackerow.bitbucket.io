package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.ComplexDataTypes.Map;
import ClassLibrary.Agents.Agent;
import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.CollectionsPattern.Comparison;

public class CorrespondenceTable extends AnnotatedIdentifiable {

	public final DateRange[] effectiveDates;

	public final Map[] correspondence;

	public LevelStructure[] levelStructure;

	public LevelStructure[] levelStructure;

	public StatisticalClassification[] statisticalClassification;

	public Agent[] agent;

	public ExternalMaterial[] externalMaterial;

	public Comparison[] comparison;

	public Agent[] agent;

	public Agent[] agent;

}
