package ClassLibrary.Representations;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ClassificationSeriesIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.Conceptual.Concept;

public class ClassificationFamily extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final ClassificationSeriesIndicator[] contains;

	public final Boolean[] isOrdered;

	public StructuredCollection[] structuredCollection;

	public ClassificationSeriesRelationStructure[] classificationSeriesRelationStructure;

	public Concept[] concept;

	public ClassificationIndex[] classificationIndex;

}
