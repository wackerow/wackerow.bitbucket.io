package ClassLibrary.Representations;

import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.InternationalString;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.ClassificationItemIndicator;

public class ClassificationItem extends Code {

	public final Boolean[] isValid;

	public final Boolean[] isGenerated;

	public final InternationalStructuredString[] explanatoryNotes;

	public final InternationalString[] futureNotes;

	public final InternationalString[] changeLog;

	public final InternationalString[] changeFromPreviousVersion;

	public final DateRange[] validDates;

	public final ObjectName[] name;

	public AuthorizationSource[] authorizationSource;

	public ClassificationItemIndicator classificationItemIndicator;

	public ClassificationItem[] classificationItem;

	public ClassificationItem[] classificationItem;

}
