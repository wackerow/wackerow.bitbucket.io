package ClassLibrary.Representations;

import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.ComplexDataTypes.DateRange;
import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.InternationalString;
import ClassLibrary.XMLSchemaDatatypes.LanguageSpecification;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ClassificationItemIndicator;
import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.ComplexDataTypes.StatisticalClassificationIndicator;
import ClassLibrary.ComplexDataTypes.StatisticalClassificationRelation;
import ClassLibrary.Agents.Organization;

public class StatisticalClassification extends CodeList {

	public final Date[] releaseDate;

	public final DateRange[] validDates;

	public final Boolean[] isCurrent;

	public final Boolean[] isFloating;

	public final InternationalStructuredString[] changeFromBase;

	public final InternationalStructuredString[] purposeOfVariant;

	public final InternationalString[] copyright;

	public final InternationalStructuredString[] updateChanges;

	public final LanguageSpecification[] availableLanguage;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] rationale;

	public final InternationalStructuredString[] usage;

	public final ClassificationItemIndicator[] contains;

	public StatisticalClassification[] statisticalClassification;

	public StatisticalClassification[] statisticalClassification;

	public StatisticalClassification[] statisticalClassification;

	public StatisticalClassification statisticalClassification;

	public CorrespondenceTable[] correspondenceTable;

	public ExternalMaterial[] externalMaterial;

	public ClassificationIndex[] classificationIndex;

	public StatisticalClassificationIndicator statisticalClassificationIndicator;

	public StatisticalClassificationRelation[] statisticalClassificationRelation;

	public StatisticalClassificationRelation[] statisticalClassificationRelation;

	public Organization[] organization;

	public StatisticalClassification[] statisticalClassification;

	public StatisticalClassification[] statisticalClassification;

}
