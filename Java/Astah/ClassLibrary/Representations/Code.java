package ClassLibrary.Representations;

import ClassLibrary.ComplexDataTypes.ValueString;
import ClassLibrary.DataCapture.CodeListResponseDomain;
import ClassLibrary.Conceptual.Category;
import ClassLibrary.ComplexDataTypes.CodeIndicator;

public class Code extends Designation {

	public final ValueString representation;

	public CodeListResponseDomain codeListResponseDomain;

	public Category[] category;

	public CodeIndicator codeIndicator;

}
