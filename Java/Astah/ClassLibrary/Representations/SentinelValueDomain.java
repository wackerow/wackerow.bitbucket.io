package ClassLibrary.Representations;

import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.Conceptual.SentinelConceptualDomain;
import ClassLibrary.Conceptual.InstanceVariable;
import ClassLibrary.Conceptual.RepresentedVariable;

public class SentinelValueDomain extends ValueDomain {

	public final ExternalControlledVocabularyEntry[] platformType;

	public SentinelConceptualDomain[] sentinelConceptualDomain;

	public ValueAndConceptDescription[] valueAndConceptDescription;

	public CodeList[] codeList;

	public InstanceVariable instanceVariable;

	public RepresentedVariable[] representedVariable;

}
