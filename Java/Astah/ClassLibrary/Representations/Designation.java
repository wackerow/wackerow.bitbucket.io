package ClassLibrary.Representations;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.SignificationPattern.Signifier;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.SignificationPattern.Sign;

public abstract class Designation extends Identifiable {

	public final Signifier[] representation;

	public Concept[] concept;

	public Sign[] sign;

}
