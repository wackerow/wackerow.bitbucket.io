package ClassLibrary.CollectionsPattern;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.Representations.ClassificationIndexEntry;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.SimpleCodebook.VariableStatistics;
import ClassLibrary.CustomMetadata.CustomItem;
import ClassLibrary.CustomMetadata.CustomValue;
import ClassLibrary.CustomMetadata.VocabularyEntry;
import ClassLibrary.Agents.Agent;
import ClassLibrary.FormatDescription.ValueMapping;
import ClassLibrary.LogicalDataDescription.DataPoint;

public abstract class CollectionMember extends Identifiable {

	public MemberRelation[] memberRelation;

	public MemberRelation memberRelation;

	public ClassificationIndexEntry classificationIndexEntry;

	public Study study;

	public VariableStatistics variableStatistics;

	public CustomItem customItem;

	public CustomValue customValue;

	public VocabularyEntry vocabularyEntry;

	public MemberIndicator memberIndicator;

	public ComparisonMap[] comparisonMap;

	public ComparisonMap[] comparisonMap;

	public Agent agent;

	public ValueMapping valueMapping;

	public DataPoint dataPoint;

}
