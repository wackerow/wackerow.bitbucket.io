package ClassLibrary.CollectionsPattern;

import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import Primitive Package.Boolean;
import ClassLibrary.BusinessWorkflow.DataPipeline;
import ClassLibrary.Workflows.WorkflowStepSequence;
import ClassLibrary.LogicalDataDescription.ViewpointRole;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.LogicalDataDescription.LogicalRecord;
import ClassLibrary.CustomMetadata.CustomInstance;

public abstract class SimpleCollection extends CollectionMember {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final MemberIndicator[] contains;

	public final Boolean[] isOrdered;

	public DataPipeline dataPipeline;

	public WorkflowStepSequence workflowStepSequence;

	public ViewpointRole viewpointRole;

	public Concept[] concept;

	public LogicalRecord logicalRecord;

	public CustomInstance customInstance;

	public Comparison[] comparison;

}
