package ClassLibrary.CollectionsPattern;

import ClassLibrary.Representations.ClassificationIndex;
import ClassLibrary.Representations.ClassificationSeries;
import ClassLibrary.StudyRelated.StudySeries;
import ClassLibrary.Representations.ClassificationFamily;
import ClassLibrary.CustomMetadata.CustomStructure;
import ClassLibrary.Workflows.StructuredWorkflowSteps;
import ClassLibrary.Representations.CodeList;
import ClassLibrary.Conceptual.VariableCollection;
import ClassLibrary.FormatDescription.PhysicalDataSet;
import ClassLibrary.FormatDescription.PhysicalRecordSegment;
import ClassLibrary.Conceptual.ConceptSystem;
import ClassLibrary.LogicalDataDescription.DataStoreLibrary;
import ClassLibrary.LogicalDataDescription.UnitDataRecord;
import ClassLibrary.CustomMetadata.ControlledVocabulary;
import ClassLibrary.Agents.AgentListing;
import ClassLibrary.LogicalDataDescription.DataStore;
import ClassLibrary.FormatDescription.PhysicalSegmentLayout;

public abstract class StructuredCollection extends SimpleCollection {

	public ClassificationIndex classificationIndex;

	public ClassificationSeries classificationSeries;

	public StudySeries studySeries;

	public ClassificationFamily classificationFamily;

	public CustomStructure customStructure;

	public StructuredWorkflowSteps structuredWorkflowSteps;

	public CodeList codeList;

	public RelationStructure[] relationStructure;

	public VariableCollection variableCollection;

	public PhysicalDataSet physicalDataSet;

	public PhysicalRecordSegment physicalRecordSegment;

	public ConceptSystem conceptSystem;

	public DataStoreLibrary dataStoreLibrary;

	public UnitDataRecord unitDataRecord;

	public ControlledVocabulary controlledVocabulary;

	public AgentListing agentListing;

	public DataStore dataStore;

	public PhysicalSegmentLayout physicalSegmentLayout;

}
