package ClassLibrary.CollectionsPattern;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.Representations.CorrespondenceTable;
import ClassLibrary.Conceptual.ConceptSystemCorrespondence;
import ClassLibrary.LogicalDataDescription.RecordRelation;

public abstract class Comparison extends Identifiable {

	public final ComparisonMap[] correspondence;

	public CorrespondenceTable correspondenceTable;

	public ConceptSystemCorrespondence conceptSystemCorrespondence;

	public RecordRelation recordRelation;

	public SimpleCollection[] simpleCollection;

}
