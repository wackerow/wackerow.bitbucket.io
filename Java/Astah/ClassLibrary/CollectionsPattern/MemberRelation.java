package ClassLibrary.CollectionsPattern;

import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.CustomValueRelation;
import ClassLibrary.ComplexDataTypes.DataPointRelation;
import ClassLibrary.ComplexDataTypes.DataStoreRelation;
import ClassLibrary.ComplexDataTypes.PhysicalRecordSegmentRelation;
import ClassLibrary.ComplexDataTypes.CategoryRelation;
import ClassLibrary.ComplexDataTypes.ClassificationSeriesRelation;
import ClassLibrary.ComplexDataTypes.LogicalRecordRelation;
import ClassLibrary.ComplexDataTypes.StatisticalClassificationRelation;
import ClassLibrary.ComplexDataTypes.ConceptRelation;
import ClassLibrary.ComplexDataTypes.CustomItemRelation;
import ClassLibrary.ComplexDataTypes.InstanceVariableRelation;
import ClassLibrary.ComplexDataTypes.IndexEntryRelation;
import ClassLibrary.ComplexDataTypes.GeographicUnitTypeRelation;
import ClassLibrary.ComplexDataTypes.WorkflowStepRelation;
import ClassLibrary.ComplexDataTypes.VocabularyEntryRelation;
import ClassLibrary.ComplexDataTypes.AgentRelation;
import ClassLibrary.ComplexDataTypes.ValueMappingRelation;
import ClassLibrary.ComplexDataTypes.StudyRelation;
import ClassLibrary.ComplexDataTypes.ViewpointRoleRelation;
import ClassLibrary.ComplexDataTypes.GeographicUnitRelation;
import ClassLibrary.ComplexDataTypes.VariableRelation;

public abstract class MemberRelation {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public CollectionMember[] collectionMember;

	public CollectionMember[] collectionMember;

	public CustomValueRelation customValueRelation;

	public DataPointRelation dataPointRelation;

	public DataStoreRelation dataStoreRelation;

	public PhysicalRecordSegmentRelation physicalRecordSegmentRelation;

	public CategoryRelation categoryRelation;

	public ClassificationSeriesRelation classificationSeriesRelation;

	public LogicalRecordRelation logicalRecordRelation;

	public StatisticalClassificationRelation statisticalClassificationRelation;

	public ConceptRelation conceptRelation;

	public CustomItemRelation customItemRelation;

	public InstanceVariableRelation instanceVariableRelation;

	public IndexEntryRelation indexEntryRelation;

	public GeographicUnitTypeRelation geographicUnitTypeRelation;

	public WorkflowStepRelation workflowStepRelation;

	public VocabularyEntryRelation vocabularyEntryRelation;

	public AgentRelation agentRelation;

	public ValueMappingRelation valueMappingRelation;

	public StudyRelation studyRelation;

	public ViewpointRoleRelation viewpointRoleRelation;

	public GeographicUnitRelation geographicUnitRelation;

	public VariableRelation variableRelation;

}
