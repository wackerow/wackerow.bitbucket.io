package ClassLibrary.Discovery;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;

public class TopicalCoverage extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] subject;

	public final ExternalControlledVocabularyEntry[] keyword;

	public Coverage[] coverage;

}
