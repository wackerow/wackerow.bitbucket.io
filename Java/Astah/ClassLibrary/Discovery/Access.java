package ClassLibrary.Discovery;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.Form;
import ClassLibrary.ComplexDataTypes.AgentAssociation;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.Utility.DocumentInformation;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.SamplingMethodology.SampleFrame;

public class Access extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] purpose;

	public final InternationalStructuredString[] confidentialityStatement;

	public final Form[] accessPermission;

	public final InternationalStructuredString[] restrictions;

	public final InternationalStructuredString[] citationRequirement;

	public final InternationalStructuredString[] depositRequirement;

	public final InternationalStructuredString[] accessConditions;

	public final InternationalStructuredString[] disclaimer;

	public final AgentAssociation[] contactAgent;

	public final DateRange[] validDates;

	public DocumentInformation[] documentInformation;

	public Study study;

	public SampleFrame sampleFrame;

	public DocumentInformation[] documentInformation;

}
