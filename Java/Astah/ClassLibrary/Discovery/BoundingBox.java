package ClassLibrary.Discovery;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import Primitive Package.Real;
import ClassLibrary.GeographicClassification.GeographicExtent;

public class BoundingBox extends AnnotatedIdentifiable {

	public final Real eastLongitude;

	public final Real westLongitude;

	public final Real northLatitude;

	public final Real southLatitude;

	public SpatialCoverage[] spatialCoverage;

	public GeographicExtent geographicExtent;

}
