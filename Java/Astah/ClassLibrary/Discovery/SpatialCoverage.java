package ClassLibrary.Discovery;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.SpatialObjectType;
import ClassLibrary.GeographicClassification.GeographicUnit;
import ClassLibrary.Conceptual.UnitType;
import ClassLibrary.GeographicClassification.GeographicUnitTypeClassification;
import ClassLibrary.GeographicClassification.GeographicUnitClassification;

public class SpatialCoverage extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] description;

	public final ExternalControlledVocabularyEntry[] spatialAreaCode;

	public final SpatialObjectType[] spatialObject;

	public BoundingBox[] boundingBox;

	public GeographicUnit[] geographicUnit;

	public UnitType[] unitType;

	public UnitType[] unitType;

	public UnitType[] unitType;

	public Coverage[] coverage;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	public GeographicUnitClassification[] geographicUnitClassification;

}
