package ClassLibrary.Discovery;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Utility.DocumentInformation;
import ClassLibrary.StudyRelated.Study;

public class Coverage extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public TemporalCoverage temporalCoverage;

	public SpatialCoverage spatialCoverage;

	public DocumentInformation documentInformation;

	public TopicalCoverage topicalCoverage;

	public Study[] study;

}
