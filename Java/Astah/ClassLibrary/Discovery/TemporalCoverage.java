package ClassLibrary.Discovery;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ReferenceDate;

public class TemporalCoverage extends AnnotatedIdentifiable {

	public final ReferenceDate[] coverageDate;

	public Coverage[] coverage;

}
