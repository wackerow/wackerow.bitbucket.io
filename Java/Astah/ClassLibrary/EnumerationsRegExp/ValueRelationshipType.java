package ClassLibrary.EnumerationsRegExp;

public enum ValueRelationshipType {

	Equal,

	NotEqual,

	GreaterThan,

	GreaterThanOrEqualTo,

	LessThan,

	LessThanOrEqualTo;

}
