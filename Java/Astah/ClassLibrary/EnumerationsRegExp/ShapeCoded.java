package ClassLibrary.EnumerationsRegExp;

public enum ShapeCoded {

	Rectangle,

	Circle,

	Polygon,

	LinearRing;

}
