package ClassLibrary.EnumerationsRegExp;

public enum ComputationBaseList {

	Total,

	ValidOnly,

	MissingOnly;

}
