package ClassLibrary.EnumerationsRegExp;

public enum SpatialRelationSpecification {

	Equals,

	Disjoint,

	Intersects,

	Contains,

	Touches;

}
