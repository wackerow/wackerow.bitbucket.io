package ClassLibrary.EnumerationsRegExp;

public enum SpatialObjectType {

	Point,

	Polygon,

	Line,

	LinearRing,

	Raster;

}
