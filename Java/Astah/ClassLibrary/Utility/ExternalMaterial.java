package ClassLibrary.Utility;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.XMLSchemaDatatypes.anyUri;
import ClassLibrary.ComplexDataTypes.Segment;
import ClassLibrary.ComplexDataTypes.Annotation;
import ClassLibrary.SimpleMethodologyOverview.DesignOverview;
import ClassLibrary.StudyRelated.Standard;
import ClassLibrary.SimpleMethodologyOverview.AlgorithmOverview;
import ClassLibrary.Methodologies.Guide;
import ClassLibrary.DataCapture.Instruction;
import ClassLibrary.Methodologies.Goal;
import ClassLibrary.MethodologyPattern.Algorithm;
import ClassLibrary.MethodologyPattern.Design;
import ClassLibrary.Representations.ClassificationIndex;
import ClassLibrary.StudyRelated.Budget;
import ClassLibrary.Representations.StatisticalClassification;
import ClassLibrary.Representations.CorrespondenceTable;
import ClassLibrary.SimpleMethodologyOverview.MethodologyOverview;
import ClassLibrary.SamplingMethodology.SamplingProcess;
import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.Methodologies.Precondition;

public class ExternalMaterial extends Identifiable {

	public final ExternalControlledVocabularyEntry[] typeOfMaterial;

	public final InternationalStructuredString[] descriptiveText;

	public final anyUri[] uri;

	public final InternationalStructuredString[] relationshipDescription;

	public final ExternalControlledVocabularyEntry[] mimeType;

	public final Segment[] usesSegment;

	public final Annotation[] citationOfExternalMaterial;

	public DesignOverview[] designOverview;

	public Standard[] standard;

	public AlgorithmOverview[] algorithmOverview;

	public Guide[] guide;

	public Instruction[] instruction;

	public Goal[] goal;

	public Algorithm[] algorithm;

	public Design[] design;

	public ClassificationIndex[] classificationIndex;

	public Budget[] budget;

	public StatisticalClassification[] statisticalClassification;

	public CorrespondenceTable[] correspondenceTable;

	public DocumentInformation[] documentInformation;

	public MethodologyOverview[] methodologyOverview;

	public SamplingProcess[] samplingProcess;

	public AnnotatedIdentifiable[] annotatedIdentifiable;

	public Precondition[] precondition;

}
