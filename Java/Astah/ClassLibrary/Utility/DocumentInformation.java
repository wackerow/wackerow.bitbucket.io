package ClassLibrary.Utility;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.TypedDescriptiveText;
import Primitive Package.Boolean;
import Primitive Package.UnlimitedNatural;
import ClassLibrary.Discovery.Access;
import ClassLibrary.Discovery.Coverage;

public class DocumentInformation extends AnnotatedIdentifiable {

	public final TypedDescriptiveText[] contentCoverage;

	public final Boolean isPublished;

	public final UnlimitedNatural[] hasPrimaryContent;

	public final DDI4Version ofType;

	public Access[] access;

	public Coverage[] coverage;

	public FundingInformation[] fundingInformation;

	public ExternalMaterial[] externalMaterial;

	public Access[] access;

}
