package ClassLibrary.Utility;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Agents.Agent;
import ClassLibrary.StudyRelated.Study;

public class FundingInformation extends Identifiable {

	public final ExternalControlledVocabularyEntry[] funderRole;

	public final String[] grantNumber;

	public final InternationalStructuredString[] purpose;

	public Agent[] agent;

	public Study[] study;

	public DocumentInformation[] documentInformation;

}
