package ClassLibrary.SimpleCodebook;

import ClassLibrary.Identification.Identifiable;
import Primitive Package.Integer;
import ClassLibrary.ComplexDataTypes.SummaryStatistic;
import ClassLibrary.ComplexDataTypes.CategoryStatistic;
import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.FormatDescription.PhysicalDataSet;
import ClassLibrary.Conceptual.InstanceVariable;

public class VariableStatistics extends Identifiable {

	public final Integer[] totalResponses;

	public final SummaryStatistic[] hasSummaryStatistic;

	public final CategoryStatistic[] hasCategoryStatistic;

	public CollectionMember[] collectionMember;

	public PhysicalDataSet[] physicalDataSet;

	public StandardWeight[] standardWeight;

	public InstanceVariable[] instanceVariable;

	public InstanceVariable[] instanceVariable;

	public InstanceVariable[] instanceVariable;

}
