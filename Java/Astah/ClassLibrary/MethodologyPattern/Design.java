package ClassLibrary.MethodologyPattern;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Methodologies.Precondition;
import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.ProcessPattern.Process;
import ClassLibrary.SimpleMethodologyOverview.DesignOverview;
import ClassLibrary.Methodologies.Goal;

public abstract class Design extends Identifiable {

	public final InternationalStructuredString[] overview;

	public Precondition[] precondition;

	public Algorithm[] algorithm;

	public ExternalMaterial[] externalMaterial;

	public Process[] process;

	public Methodology[] methodology;

	public DesignOverview designOverview;

	public Goal[] goal;

}
