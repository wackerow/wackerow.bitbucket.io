package ClassLibrary.MethodologyPattern;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.SimpleMethodologyOverview.MethodologyOverview;
import ClassLibrary.ProcessPattern.Process;

public abstract class Methodology extends Identifiable {

	public final ObjectName[] name;

	public final InternationalStructuredString[] usage;

	public final InternationalStructuredString[] rationale;

	public final InternationalStructuredString[] overview;

	public Design[] design;

	public MethodologyOverview methodologyOverview;

	public Methodology[] methodology;

	public Methodology[] methodology;

	public Algorithm[] algorithm;

	public Process[] process;

}
