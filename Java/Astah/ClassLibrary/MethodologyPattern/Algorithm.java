package ClassLibrary.MethodologyPattern;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.SimpleMethodologyOverview.AlgorithmOverview;
import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.ProcessPattern.Process;

public abstract class Algorithm extends Identifiable {

	public final InternationalStructuredString[] overview;

	public AlgorithmOverview algorithmOverview;

	public Design[] design;

	public ExternalMaterial[] externalMaterial;

	public Process[] process;

	public Methodology[] methodology;

}
