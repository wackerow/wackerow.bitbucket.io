package ClassLibrary.DataCapture;

import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.CommandCode;

public class InstrumentCode extends InstrumentComponent {

	public final ExternalControlledVocabularyEntry[] purposeOfCode;

	public final CommandCode[] usesCommandCode;

}
