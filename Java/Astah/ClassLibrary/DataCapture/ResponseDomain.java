package ClassLibrary.DataCapture;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.Workflows.Parameter;
import ClassLibrary.Conceptual.RepresentedVariable;

public class ResponseDomain extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public Parameter[] parameter;

	public Capture[] capture;

	public RepresentedVariable[] representedVariable;

	public CodeListResponseDomain codeListResponseDomain;

}
