package ClassLibrary.DataCapture;

import Primitive Package.Integer;
import ClassLibrary.ComplexDataTypes.TypedString;

public class TextResponseDomain extends ResponseDomain {

	public final Integer[] maximumLength;

	public final Integer[] minimumLength;

	public final TypedString[] regularExpression;

}
