package ClassLibrary.DataCapture;

import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.NumberRange;

public class NumericResponseDomain extends ResponseDomain {

	public final ExternalControlledVocabularyEntry[] numericTypeCode;

	public final ExternalControlledVocabularyEntry[] unit;

	public final NumberRange[] usesNumberRange;

}
