package ClassLibrary.DataCapture;

import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;

public class ExternalAid extends ExternalMaterial {

	public final ExternalControlledVocabularyEntry[] stimulusType;

	public InstrumentComponent[] instrumentComponent;

	public Capture[] capture;

}
