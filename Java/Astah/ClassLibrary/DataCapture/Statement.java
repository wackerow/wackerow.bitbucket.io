package ClassLibrary.DataCapture;

import ClassLibrary.ComplexDataTypes.DynamicText;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;

public class Statement extends InstrumentComponent {

	public final DynamicText[] statementText;

	public final ExternalControlledVocabularyEntry[] purposeOfStatement;

}
