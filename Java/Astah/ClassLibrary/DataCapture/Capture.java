package ClassLibrary.DataCapture;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.Conceptual.InstanceVariable;

public abstract class Capture extends AnnotatedIdentifiable {

	public final ObjectName[] name;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public final InternationalStructuredString[] purpose;

	public final ExternalControlledVocabularyEntry[] captureSource;

	public final ExternalControlledVocabularyEntry[] analysisUnit;

	public ResponseDomain[] responseDomain;

	public Instruction[] instruction;

	public ExternalAid[] externalAid;

	public Concept[] concept;

	public ConceptualInstrument conceptualInstrument;

	public InstanceVariable instanceVariable;

}
