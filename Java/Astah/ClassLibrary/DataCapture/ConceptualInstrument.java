package ClassLibrary.DataCapture;

import ClassLibrary.Workflows.WorkflowProcess;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;

public class ConceptualInstrument extends WorkflowProcess {

	public final LabelForDisplay[] displayLabel;

	public final ObjectName[] name;

	public final InternationalStructuredString[] description;

	public final InternationalStructuredString[] usage;

	public ImplementedInstrument[] implementedInstrument;

	public Capture[] capture;

}
