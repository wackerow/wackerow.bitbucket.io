package ClassLibrary.DataCapture;

import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.Conceptual.RepresentedVariable;

public class RepresentedMeasurement extends Capture {

	public final ExternalControlledVocabularyEntry[] measurementType;

	public InstanceMeasurement instanceMeasurement;

	public RepresentedVariable[] representedVariable;

}
