package ClassLibrary.DataCapture;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.XMLSchemaDatatypes.anyUri;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.StudyRelated.Study;

public class ImplementedInstrument extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public final ExternalControlledVocabularyEntry[] typeOfInstrument;

	public final anyUri[] uri;

	public final ObjectName[] name;

	public final InternationalStructuredString[] usage;

	public ConceptualInstrument conceptualInstrument;

	public Study[] study;

}
