package ClassLibrary.DataCapture;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.DynamicText;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.Utility.ExternalMaterial;

public class Instruction extends AnnotatedIdentifiable {

	public final DynamicText[] instructionText;

	public final LabelForDisplay[] displayLabel;

	public final ObjectName[] name;

	public ExternalMaterial[] externalMaterial;

	public Capture[] capture;

	public InstrumentComponent[] instrumentComponent;

}
