package ClassLibrary.DataCapture;

import ClassLibrary.ComplexDataTypes.DynamicText;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import Primitive Package.Real;
import ClassLibrary.Conceptual.RepresentedVariable;

public class RepresentedQuestion extends Capture {

	public final DynamicText[] questionText;

	public final InternationalStructuredString[] questionIntent;

	public final Real[] estimatedResponseTimeInSeconds;

	public InstanceQuestion instanceQuestion;

	public RepresentedVariable[] representedVariable;

}
