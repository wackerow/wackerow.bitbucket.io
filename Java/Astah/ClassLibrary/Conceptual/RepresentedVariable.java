package ClassLibrary.Conceptual;

import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.Representations.SubstantiveValueDomain;
import ClassLibrary.CustomMetadata.CustomItem;
import ClassLibrary.DataCapture.ResponseDomain;
import ClassLibrary.DataCapture.RepresentedQuestion;
import ClassLibrary.DataCapture.RepresentedMeasurement;
import ClassLibrary.Representations.SentinelValueDomain;

public class RepresentedVariable extends ConceptualVariable {

	public final String[] unitOfMeasurement;

	public final ExternalControlledVocabularyEntry[] hasIntendedDataType;

	public SubstantiveValueDomain[] substantiveValueDomain;

	public CustomItem customItem;

	public ResponseDomain responseDomain;

	public Universe[] universe;

	public RepresentedQuestion[] representedQuestion;

	public RepresentedMeasurement representedMeasurement;

	public ConceptualVariable[] conceptualVariable;

	public SentinelValueDomain[] sentinelValueDomain;

	public InstanceVariable instanceVariable;

}
