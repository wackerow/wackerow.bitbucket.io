package ClassLibrary.Conceptual;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Representations.ClassificationIndex;
import ClassLibrary.Representations.ClassificationSeries;
import ClassLibrary.BusinessWorkflow.DataPipeline;
import ClassLibrary.Workflows.WorkflowStepSequence;
import ClassLibrary.Representations.ClassificationFamily;
import ClassLibrary.SignificationPattern.Signified;
import ClassLibrary.CustomMetadata.CustomStructure;
import ClassLibrary.StudyRelated.ComplianceStatement;
import ClassLibrary.Representations.CodeList;
import ClassLibrary.CustomMetadata.CustomItem;
import ClassLibrary.Representations.Designation;
import ClassLibrary.LogicalDataDescription.ViewpointRole;
import ClassLibrary.ComplexDataTypes.Map;
import ClassLibrary.FormatDescription.PhysicalDataSet;
import ClassLibrary.FormatDescription.PhysicalRecordSegment;
import ClassLibrary.ComplexDataTypes.ConceptIndicator;
import ClassLibrary.DataCapture.Capture;
import ClassLibrary.ComplexDataTypes.ConceptRelation;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.CollectionsPattern.SimpleCollection;
import ClassLibrary.LogicalDataDescription.DataStore;
import ClassLibrary.LogicalDataDescription.DataStoreLibrary;
import ClassLibrary.LogicalDataDescription.LogicalRecord;
import ClassLibrary.Agents.AgentListing;
import ClassLibrary.LogicalDataDescription.UnitDataRecord;
import ClassLibrary.CustomMetadata.CustomInstance;
import ClassLibrary.CustomMetadata.ControlledVocabulary;
import ClassLibrary.StudyRelated.StudySeries;
import ClassLibrary.FormatDescription.PhysicalSegmentLayout;
import ClassLibrary.ComplexDataTypes.Level;

public class Concept extends AnnotatedIdentifiable {

	public final ObjectName[] name;

	public final InternationalStructuredString[] definition;

	public ClassificationIndex[] classificationIndex;

	public ClassificationSeries[] classificationSeries;

	public DataPipeline[] dataPipeline;

	public WorkflowStepSequence[] workflowStepSequence;

	public ClassificationFamily[] classificationFamily;

	public Signified[] signified;

	public CustomStructure[] customStructure;

	public ComplianceStatement[] complianceStatement;

	public CodeList[] codeList;

	public CustomItem customItem;

	public Designation designation;

	public ViewpointRole[] viewpointRole;

	public Universe[] universe;

	public VariableCollection[] variableCollection;

	public Map[] map;

	public Map[] map;

	public PhysicalDataSet[] physicalDataSet;

	public PhysicalRecordSegment[] physicalRecordSegment;

	public ConceptIndicator conceptIndicator;

	public Capture[] capture;

	public ConceptRelation conceptRelation;

	public ConceptRelation conceptRelation;

	public Study[] study;

	public SimpleCollection[] simpleCollection;

	public ConceptSystem[] conceptSystem;

	public DataStore[] dataStore;

	public DataStoreLibrary[] dataStoreLibrary;

	public ConceptualVariable[] conceptualVariable;

	public Population[] population;

	public LogicalRecord[] logicalRecord;

	public AgentListing[] agentListing;

	public UnitType[] unitType;

	public UnitDataRecord[] unitDataRecord;

	public CustomInstance[] customInstance;

	public ControlledVocabulary[] controlledVocabulary;

	public StudySeries[] studySeries;

	public PhysicalSegmentLayout[] physicalSegmentLayout;

	public Level level;

}
