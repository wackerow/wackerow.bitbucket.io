package ClassLibrary.Conceptual;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.Representations.ValueAndConceptDescription;

public abstract class ConceptualDomain extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public ValueAndConceptDescription[] valueAndConceptDescription;

	public ConceptSystem[] conceptSystem;

}
