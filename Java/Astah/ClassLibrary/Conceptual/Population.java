package ClassLibrary.Conceptual;

import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.FormatDescription.PhysicalRecordSegment;
import ClassLibrary.SamplingMethodology.SamplePopulationResult;
import ClassLibrary.SamplingMethodology.SampleFrame;
import ClassLibrary.GeographicClassification.GeographicUnit;

public class Population extends Concept {

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] descriptiveText;

	public final DateRange[] timePeriodOfPopulation;

	public Study[] study;

	public PhysicalRecordSegment physicalRecordSegment;

	public Universe[] universe;

	public Unit[] unit;

	public SamplePopulationResult samplePopulationResult;

	public SampleFrame[] sampleFrame;

	public InstanceVariable instanceVariable;

	public Concept[] concept;

	public GeographicUnit[] geographicUnit;

}
