package ClassLibrary.Conceptual;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ObjectName;

public class Unit extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public final ObjectName[] name;

	public Population[] population;

	public UnitType[] unitType;

}
