package ClassLibrary.Conceptual;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.ConceptRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class ConceptRelationStructure extends Identifiable {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final ConceptRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public ConceptSystem[] conceptSystem;

}
