package ClassLibrary.Conceptual;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.VariableRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class VariableRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final VariableRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public VariableCollection[] variableCollection;

}
