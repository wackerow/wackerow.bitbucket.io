package ClassLibrary.Conceptual;

import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.CustomMetadata.CustomValue;
import ClassLibrary.LogicalDataDescription.Datum;
import ClassLibrary.SimpleCodebook.VariableStatistics;
import ClassLibrary.LogicalDataDescription.DataPoint;
import ClassLibrary.ComplexDataTypes.InstanceVariableIndicator;
import ClassLibrary.ComplexDataTypes.InstanceVariableRelation;
import ClassLibrary.Representations.SentinelValueDomain;
import ClassLibrary.ComplexDataTypes.InstanceVariableValueMap;
import ClassLibrary.ComplexDataTypes.ViewpointRoleRelation;
import ClassLibrary.Workflows.Act;
import ClassLibrary.DataCapture.Capture;

public class InstanceVariable extends RepresentedVariable {

	public final InternationalStructuredString[] variableRole;

	public final ExternalControlledVocabularyEntry[] physicalDataType;

	public final ExternalControlledVocabularyEntry[] platformType;

	public Study[] study;

	public CustomValue customValue;

	public Datum datum;

	public VariableStatistics variableStatistics;

	public VariableStatistics variableStatistics;

	public DataPoint dataPoint;

	public InstanceVariableIndicator instanceVariableIndicator;

	public InstanceVariableRelation[] instanceVariableRelation;

	public InstanceVariableRelation instanceVariableRelation;

	public Population[] population;

	public SentinelValueDomain[] sentinelValueDomain;

	public ConceptualVariable[] conceptualVariable;

	public InstanceVariableValueMap[] instanceVariableValueMap;

	public InstanceVariableValueMap[] instanceVariableValueMap;

	public ViewpointRoleRelation[] viewpointRoleRelation;

	public ViewpointRoleRelation viewpointRoleRelation;

	public Act[] act;

	public VariableStatistics variableStatistics;

	public RepresentedVariable[] representedVariable;

	public Capture[] capture;

}
