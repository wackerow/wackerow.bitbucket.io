package ClassLibrary.Conceptual;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ConceptIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.GeographicClassification.GeographicUnitTypeClassification;
import ClassLibrary.GeographicClassification.GeographicUnitClassification;

public class ConceptSystem extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final ConceptIndicator[] contains;

	public final Boolean[] isOrdered;

	public ConceptualDomain conceptualDomain;

	public ConceptSystemCorrespondence[] conceptSystemCorrespondence;

	public StructuredCollection[] structuredCollection;

	public Concept[] concept;

	public ConceptRelationStructure[] conceptRelationStructure;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	public GeographicUnitClassification[] geographicUnitClassification;

}
