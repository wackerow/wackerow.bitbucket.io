package ClassLibrary.Conceptual;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.CategoryRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class CategoryRelationStructure extends Identifiable {

	public final RelationSpecification hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType totality;

	public final CategoryRelation[] hasMemberRelation;

	public CategorySet[] categorySet;

	public RelationStructure[] relationStructure;

}
