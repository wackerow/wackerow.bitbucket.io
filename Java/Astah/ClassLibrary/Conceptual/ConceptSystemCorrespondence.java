package ClassLibrary.Conceptual;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.Map;
import ClassLibrary.CollectionsPattern.Comparison;

public class ConceptSystemCorrespondence extends AnnotatedIdentifiable {

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] purpose;

	public final InternationalStructuredString[] usage;

	public final Map[] correspondence;

	public ConceptSystem[] conceptSystem;

	public Comparison[] comparison;

}
