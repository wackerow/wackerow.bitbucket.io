package ClassLibrary.Conceptual;

import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Representations.Code;
import ClassLibrary.ComplexDataTypes.CategoryRelation;
import ClassLibrary.DataCapture.BooleanResponseDomain;
import ClassLibrary.ComplexDataTypes.CategoryIndicator;

public class Category extends Concept {

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] descriptiveText;

	public Code code;

	public CategoryRelation[] categoryRelation;

	public CategoryRelation categoryRelation;

	public BooleanResponseDomain booleanResponseDomain;

	public CategoryIndicator categoryIndicator;

}
