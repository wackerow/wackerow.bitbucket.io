package ClassLibrary.Conceptual;

import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.VariableRelation;
import ClassLibrary.ComplexDataTypes.VariableIndicator;

public class ConceptualVariable extends Concept {

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] descriptiveText;

	public SentinelConceptualDomain[] sentinelConceptualDomain;

	public InstanceVariable instanceVariable;

	public Concept[] concept;

	public RepresentedVariable representedVariable;

	public VariableRelation[] variableRelation;

	public VariableRelation[] variableRelation;

	public UnitType[] unitType;

	public VariableIndicator variableIndicator;

	public SubstantiveConceptualDomain[] substantiveConceptualDomain;

}
