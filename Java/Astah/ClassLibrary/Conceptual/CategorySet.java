package ClassLibrary.Conceptual;

import ClassLibrary.ComplexDataTypes.CategoryIndicator;
import ClassLibrary.Representations.CodeList;

public class CategorySet extends ConceptSystem {

	public final CategoryIndicator[] contains;

	public CategoryRelationStructure[] categoryRelationStructure;

	public CodeList codeList;

}
