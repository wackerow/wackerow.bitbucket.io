package ClassLibrary.Conceptual;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.VariableIndicator;
import Primitive Package.Boolean;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.StudyRelated.Study;

public class VariableCollection extends AnnotatedIdentifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public final ExternalControlledVocabularyEntry[] groupingSemantic;

	public final VariableIndicator[] contains;

	public final Boolean[] isOrdered;

	public Concept[] concept;

	public VariableRelationStructure[] variableRelationStructure;

	public StructuredCollection[] structuredCollection;

	public Study[] study;

}
