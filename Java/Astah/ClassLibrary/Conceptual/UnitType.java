package ClassLibrary.Conceptual;

import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.TargetSample;
import ClassLibrary.Discovery.SpatialCoverage;
import ClassLibrary.Methodologies.AppliedUse;
import ClassLibrary.SamplingMethodology.SampleFrame;
import ClassLibrary.SamplingMethodology.SamplePopulationResult;
import ClassLibrary.ComplexDataTypes.GeographicUnitTypeRelation;
import ClassLibrary.ComplexDataTypes.GeographicUnitTypeIndicator;
import ClassLibrary.StudyRelated.Study;

public class UnitType extends Concept {

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] descriptiveText;

	public TargetSample[] targetSample;

	public SpatialCoverage[] spatialCoverage;

	public SpatialCoverage[] spatialCoverage;

	public AppliedUse[] appliedUse;

	public SpatialCoverage[] spatialCoverage;

	public Unit[] unit;

	public SampleFrame[] sampleFrame;

	public Universe universe;

	public SamplePopulationResult samplePopulationResult;

	public SampleFrame[] sampleFrame;

	public GeographicUnitTypeRelation[] geographicUnitTypeRelation;

	public GeographicUnitTypeRelation geographicUnitTypeRelation;

	public Concept concept;

	public GeographicUnitTypeIndicator geographicUnitTypeIndicator;

	public ConceptualVariable conceptualVariable;

	public Study[] study;

}
