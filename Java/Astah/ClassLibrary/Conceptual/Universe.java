package ClassLibrary.Conceptual;

import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.TargetSample;
import ClassLibrary.StudyRelated.Study;

public class Universe extends Concept {

	public final Boolean[] isInclusive;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] descriptiveText;

	public Population population;

	public Concept[] concept;

	public TargetSample[] targetSample;

	public UnitType[] unitType;

	public Study[] study;

	public RepresentedVariable representedVariable;

}
