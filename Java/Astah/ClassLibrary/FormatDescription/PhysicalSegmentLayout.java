package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import Primitive Package.Boolean;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import Primitive Package.Integer;
import ClassLibrary.EnumerationsRegExp.TrimValues;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.EnumerationsRegExp.TableDirectionValues;
import ClassLibrary.EnumerationsRegExp.TextDirectionValues;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.ValueMappingIndicator;
import ClassLibrary.LogicalDataDescription.LogicalRecord;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;

public abstract class PhysicalSegmentLayout extends AnnotatedIdentifiable {

	public final Boolean isDelimited;

	public final String[] delimiter;

	public final Boolean isFixedWidth;

	public final String[] escapeCharacter;

	public final String[] lineTerminator;

	public final String[] quoteCharacter;

	public final String[] commentPrefix;

	public final ExternalControlledVocabularyEntry[] encoding;

	public final Boolean[] hasHeader;

	public final Integer[] headerRowCount;

	public final Boolean[] skipBlankRows;

	public final Integer[] skipDataColumns;

	public final Boolean[] skipInitialSpace;

	public final Integer[] skipRows;

	public final TrimValues[] trim;

	public final String[] nullSequence;

	public final Boolean[] headerIsCaseSensitive;

	public final Integer[] arrayBase;

	public final Boolean[] treatConsecutiveDelimitersAsOne;

	public final InternationalStructuredString[] overview;

	public final TableDirectionValues[] tableDirection;

	public final TextDirectionValues[] textDirection;

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final ValueMappingIndicator[] contains;

	public final Boolean[] isOrdered;

	public PhysicalRecordSegment physicalRecordSegment;

	public LogicalRecord[] logicalRecord;

	public PhysicalLayoutRelationStructure[] physicalLayoutRelationStructure;

	public Concept[] concept;

	public StructuredCollection[] structuredCollection;

}
