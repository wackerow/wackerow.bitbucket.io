package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import Primitive Package.String;
import Primitive Package.Integer;
import ClassLibrary.ComplexDataTypes.PhysicalRecordSegmentIndicator;
import Primitive Package.Boolean;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.LogicalDataDescription.DataStore;
import ClassLibrary.SimpleCodebook.VariableStatistics;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;

public class PhysicalDataSet extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public final String[] physicalFileName;

	public final Integer[] numberOfSegments;

	public final PhysicalRecordSegmentIndicator[] contains;

	public final Boolean[] isOrdered;

	public final CollectionType[] type;

	public final InternationalStructuredString[] purpose;

	public DataStore[] dataStore;

	public VariableStatistics[] variableStatistics;

	public Concept[] concept;

	public StructuredCollection[] structuredCollection;

	public PhysicalOrderRelationStructure[] physicalOrderRelationStructure;

}
