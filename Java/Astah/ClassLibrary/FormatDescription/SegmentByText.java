package ClassLibrary.FormatDescription;

import Primitive Package.Integer;

public class SegmentByText extends PhysicalSegmentLocation {

	public final Integer[] startLine;

	public final Integer[] endLine;

	public final Integer[] startCharacterPosition;

	public final Integer[] endCharacterPosition;

	public final Integer[] characterLength;

}
