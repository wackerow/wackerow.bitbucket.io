package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.PhysicalRecordSegmentRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class PhysicalOrderRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final PhysicalRecordSegmentRelation[] hasMemberRelation;

	public PhysicalDataSet[] physicalDataSet;

	public RelationStructure[] relationStructure;

}
