package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.ValueMappingRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class PhysicalLayoutRelationStructure extends Identifiable {

	public final InternationalStructuredString[] criteria;

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final ValueMappingRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public PhysicalSegmentLayout[] physicalSegmentLayout;

}
