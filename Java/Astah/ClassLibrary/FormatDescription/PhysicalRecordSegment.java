package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.DataPointIndicator;
import Primitive Package.Boolean;
import ClassLibrary.Conceptual.Population;
import ClassLibrary.ComplexDataTypes.PhysicalRecordSegmentRelation;
import ClassLibrary.ComplexDataTypes.PhysicalRecordSegmentIndicator;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;
import ClassLibrary.LogicalDataDescription.LogicalRecord;

public class PhysicalRecordSegment extends AnnotatedIdentifiable {

	public final String[] physicalFileName;

	public final DataPointIndicator[] contains;

	public final Boolean[] isOrdered;

	public PhysicalSegmentLayout[] physicalSegmentLayout;

	public Population[] population;

	public PhysicalRecordSegmentRelation[] physicalRecordSegmentRelation;

	public PhysicalRecordSegmentRelation physicalRecordSegmentRelation;

	public PhysicalRecordSegmentIndicator physicalRecordSegmentIndicator;

	public Concept[] concept;

	public DataPointRelationStructure[] dataPointRelationStructure;

	public StructuredCollection[] structuredCollection;

	public LogicalRecord[] logicalRecord;

}
