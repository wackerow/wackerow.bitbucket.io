package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.DataPointRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class DataPointRelationStructure extends Identifiable {

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final DataPointRelation[] hasMemberRelation;

	public RelationStructure[] relationStructure;

	public PhysicalRecordSegment[] physicalRecordSegment;

}
