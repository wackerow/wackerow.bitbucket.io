package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.EnumerationsRegExp.OneCharString;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.ValueString;
import Primitive Package.Integer;
import Primitive Package.Boolean;
import ClassLibrary.ComplexDataTypes.ValueMappingRelation;
import ClassLibrary.ComplexDataTypes.ValueMappingIndicator;
import ClassLibrary.LogicalDataDescription.DataPoint;
import ClassLibrary.CollectionsPattern.CollectionMember;

public class ValueMapping extends Identifiable {

	public final ExternalControlledVocabularyEntry[] physicalDataType;

	public final OneCharString[] defaultDecimalSeparator;

	public final OneCharString[] defaultDigitGroupSeparator;

	public final String[] numberPattern;

	public final ValueString[] defaultValue;

	public final String[] nullSequence;

	public final ExternalControlledVocabularyEntry[] format;

	public final Integer[] length;

	public final Integer[] minimumLength;

	public final Integer[] maximumLength;

	public final Integer[] scale;

	public final Integer[] decimalPositions;

	public final Boolean[] isRequired;

	public PhysicalSegmentLocation[] physicalSegmentLocation;

	public ValueMappingRelation valueMappingRelation;

	public ValueMappingIndicator valueMappingIndicator;

	public DataPoint[] dataPoint;

	public ValueMappingRelation valueMappingRelation;

	public CollectionMember[] collectionMember;

}
