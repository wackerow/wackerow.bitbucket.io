package ClassLibrary.SamplingMethodology;

import ClassLibrary.SimpleMethodologyOverview.DesignOverview;

public class SamplingDesign extends DesignOverview {

	public SamplingProcedure[] samplingProcedure;

	public SamplingProcess[] samplingProcess;

	public SamplingGoal[] samplingGoal;

	public SamplingAlgorithm[] samplingAlgorithm;

}
