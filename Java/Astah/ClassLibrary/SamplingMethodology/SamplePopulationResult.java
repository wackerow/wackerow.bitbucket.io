package ClassLibrary.SamplingMethodology;

import ClassLibrary.Methodologies.Result;
import Primitive Package.Integer;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.Date;
import ClassLibrary.Conceptual.Population;
import ClassLibrary.Conceptual.UnitType;

public class SamplePopulationResult extends Result {

	public final Integer[] sizeOfSample;

	public final String[] strataName;

	public final Date[] sampleDate;

	public Population[] population;

	public UnitType[] unitType;

	public SamplingProcess[] samplingProcess;

}
