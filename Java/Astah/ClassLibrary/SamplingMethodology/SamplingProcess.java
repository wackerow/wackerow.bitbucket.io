package ClassLibrary.SamplingMethodology;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ProcessPattern.Process;
import ClassLibrary.Workflows.WorkflowStepSequence;
import ClassLibrary.Utility.ExternalMaterial;

public class SamplingProcess extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public SamplingDesign[] samplingDesign;

	public Process[] process;

	public WorkflowStepSequence workflowStepSequence;

	public SamplePopulationResult[] samplePopulationResult;

	public ExternalMaterial[] externalMaterial;

	public SamplingAlgorithm[] samplingAlgorithm;

	public SamplingProcedure[] samplingProcedure;

}
