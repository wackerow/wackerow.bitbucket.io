package ClassLibrary.SamplingMethodology;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.LabelForDisplay;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.Agents.Agent;
import ClassLibrary.Conceptual.UnitType;
import ClassLibrary.Conceptual.Population;
import ClassLibrary.Workflows.Parameter;
import ClassLibrary.Discovery.Access;

public class SampleFrame extends AnnotatedIdentifiable {

	public final ObjectName[] name;

	public final LabelForDisplay[] displayLabel;

	public final InternationalStructuredString[] usage;

	public final InternationalStructuredString[] purpose;

	public final InternationalStructuredString[] limitations;

	public final InternationalStructuredString[] updateProcedures;

	public final DateRange[] referencePeriod;

	public final DateRange[] validPeriod;

	public SampleFrame[] sampleFrame;

	public SampleFrame[] sampleFrame;

	public Agent[] agent;

	public UnitType unitType;

	public Population population;

	public Parameter[] parameter;

	public UnitType[] unitType;

	public Access[] access;

	public Parameter[] parameter;

}
