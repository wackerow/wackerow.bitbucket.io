package ClassLibrary.SamplingMethodology;

import ClassLibrary.SimpleMethodologyOverview.AlgorithmOverview;
import ClassLibrary.ComplexDataTypes.CommandCode;

public class SamplingAlgorithm extends AlgorithmOverview {

	public final CommandCode[] codifiedExpressionOfAlgorithm;

	public SamplingProcedure[] samplingProcedure;

	public SamplingProcess[] samplingProcess;

	public SamplingDesign[] samplingDesign;

}
