package ClassLibrary.SamplingMethodology;

import ClassLibrary.Methodologies.Goal;
import Primitive Package.Integer;
import Primitive Package.Real;
import ClassLibrary.ComplexDataTypes.TargetSample;

public class SamplingGoal extends Goal {

	public final Integer[] overallTargetSampleSize;

	public final Real[] overallTargetSamplePercent;

	public final TargetSample[] targetSampleSize;

	public SamplingDesign[] samplingDesign;

}
