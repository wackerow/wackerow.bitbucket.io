package ClassLibrary.Agents;

import ClassLibrary.ComplexDataTypes.OrganizationName;
import Primitive Package.String;
import ClassLibrary.ComplexDataTypes.ContactInformation;
import ClassLibrary.Representations.StatisticalClassification;
import ClassLibrary.GeographicClassification.GeographicUnitTypeClassification;
import ClassLibrary.GeographicClassification.GeographicUnitClassification;

public class Organization extends Agent {

	public final OrganizationName[] hasOrganizationName;

	public final String[] ddiId;

	public final ContactInformation[] hasContactInformation;

	public StatisticalClassification[] statisticalClassification;

	public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	public GeographicUnitClassification[] geographicUnitClassification;

}
