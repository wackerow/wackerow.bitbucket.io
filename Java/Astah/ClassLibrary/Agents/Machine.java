package ClassLibrary.Agents;

import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.AccessLocation;
import ClassLibrary.ComplexDataTypes.ContactInformation;

public class Machine extends Agent {

	public final ExternalControlledVocabularyEntry[] typeOfMachine;

	public final ObjectName[] name;

	public final AccessLocation[] hasAccessLocation;

	public final ExternalControlledVocabularyEntry[] function;

	public final ExternalControlledVocabularyEntry[] machineInterface;

	public final ContactInformation[] ownerOperatorContact;

}
