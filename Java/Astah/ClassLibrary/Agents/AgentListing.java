package ClassLibrary.Agents;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.EnumerationsRegExp.CollectionType;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.AgentIndicator;
import Primitive Package.Boolean;
import ClassLibrary.Conceptual.Concept;
import ClassLibrary.CollectionsPattern.StructuredCollection;

public class AgentListing extends Identifiable {

	public final CollectionType[] type;

	public final ObjectName[] name;

	public final InternationalStructuredString[] purpose;

	public final AgentIndicator[] contains;

	public final Boolean[] isOrdered;

	public AgentRelationStructure[] agentRelationStructure;

	public Concept[] concept;

	public StructuredCollection[] structuredCollection;

	public Agent[] agent;

}
