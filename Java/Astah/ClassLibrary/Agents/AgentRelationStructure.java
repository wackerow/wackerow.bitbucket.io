package ClassLibrary.Agents;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.DateRange;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.RelationSpecification;
import ClassLibrary.EnumerationsRegExp.TotalityType;
import ClassLibrary.ComplexDataTypes.AgentRelation;
import ClassLibrary.CollectionsPattern.RelationStructure;

public class AgentRelationStructure extends Identifiable {

	public final DateRange[] effectiveDates;

	public final ExternalControlledVocabularyEntry[] privacy;

	public final InternationalStructuredString[] purpose;

	public final RelationSpecification[] hasRelationSpecification;

	public final ExternalControlledVocabularyEntry[] semantic;

	public final TotalityType[] totality;

	public final AgentRelation[] hasMemberRelation;

	public AgentListing[] agentListing;

	public RelationStructure[] relationStructure;

}
