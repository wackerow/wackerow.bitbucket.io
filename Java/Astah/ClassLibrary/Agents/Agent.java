package ClassLibrary.Agents;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.AgentId;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.PrivateImage;
import ClassLibrary.Representations.ClassificationIndex;
import ClassLibrary.Representations.ClassificationSeries;
import ClassLibrary.StudyRelated.Embargo;
import ClassLibrary.Utility.FundingInformation;
import ClassLibrary.SamplingMethodology.SampleFrame;
import ClassLibrary.Representations.AuthorizationSource;
import ClassLibrary.Representations.CorrespondenceTable;
import ClassLibrary.ProcessPattern.Service;
import ClassLibrary.StudyRelated.ExPostEvaluation;
import ClassLibrary.ComplexDataTypes.AgentRelation;
import ClassLibrary.ComplexDataTypes.AgentIndicator;
import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.Workflows.WorkflowService;
import ClassLibrary.ComplexDataTypes.AgentAssociation;

public abstract class Agent extends AnnotatedIdentifiable {

	public final AgentId[] hasAgentId;

	public final InternationalStructuredString[] purpose;

	public final PrivateImage[] image;

	public ClassificationIndex[] classificationIndex;

	public ClassificationIndex[] classificationIndex;

	public ClassificationSeries[] classificationSeries;

	public Embargo embargo;

	public FundingInformation[] fundingInformation;

	public SampleFrame sampleFrame;

	public AuthorizationSource[] authorizationSource;

	public CorrespondenceTable[] correspondenceTable;

	public CorrespondenceTable[] correspondenceTable;

	public CorrespondenceTable correspondenceTable;

	public Service[] service;

	public ExPostEvaluation[] exPostEvaluation;

	public Embargo embargo;

	public AgentRelation agentRelation;

	public AgentRelation agentRelation;

	public AgentIndicator agentIndicator;

	public CollectionMember[] collectionMember;

	public AgentListing agentListing;

	public WorkflowService[] workflowService;

	public AgentAssociation agentAssociation;

}
