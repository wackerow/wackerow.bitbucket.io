package ClassLibrary.Methodologies;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Utility.ExternalMaterial;

public class Guide extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public ExternalMaterial[] externalMaterial;

	public AppliedUse[] appliedUse;

}
