package ClassLibrary.Methodologies;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.Binding;
import ClassLibrary.Workflows.Parameter;

public abstract class Result extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public final Binding[] hasBinding;

	public Parameter[] parameter;

	public Precondition[] precondition;

	public AppliedUse[] appliedUse;

	public Goal[] goal;

	public Parameter[] parameter;

}
