package ClassLibrary.Methodologies;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Conceptual.UnitType;

public class AppliedUse extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public AnnotatedIdentifiable[] annotatedIdentifiable;

	public Guide[] guide;

	public UnitType[] unitType;

	public Result[] result;

}
