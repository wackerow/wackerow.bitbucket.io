package ClassLibrary.Methodologies;

import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.SimpleMethodologyOverview.DesignOverview;
import ClassLibrary.MethodologyPattern.Design;

public class Goal extends BusinessFunction {

	public ExternalMaterial[] externalMaterial;

	public DesignOverview[] designOverview;

	public Result[] result;

	public Design[] design;

}
