package ClassLibrary.Methodologies;

import ClassLibrary.SimpleMethodologyOverview.DesignOverview;
import ClassLibrary.MethodologyPattern.Design;
import ClassLibrary.Utility.ExternalMaterial;

public class Precondition extends BusinessFunction {

	public DesignOverview[] designOverview;

	public Design[] design;

	public Result[] result;

	public ExternalMaterial[] externalMaterial;

}
