package ClassLibrary.Methodologies;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;

public abstract class BusinessFunction extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

}
