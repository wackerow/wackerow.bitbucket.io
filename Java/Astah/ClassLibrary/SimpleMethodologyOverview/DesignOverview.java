package ClassLibrary.SimpleMethodologyOverview;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.Workflows.WorkflowProcess;
import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.Methodologies.Precondition;
import ClassLibrary.StudyRelated.Study;
import ClassLibrary.Methodologies.Goal;
import ClassLibrary.MethodologyPattern.Design;

public class DesignOverview extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] subectOfDesign;

	public final InternationalStructuredString[] overview;

	public WorkflowProcess[] workflowProcess;

	public ExternalMaterial[] externalMaterial;

	public Precondition[] precondition;

	public AlgorithmOverview[] algorithmOverview;

	public Study study;

	public Goal[] goal;

	public Design[] design;

	public MethodologyOverview[] methodologyOverview;

}
