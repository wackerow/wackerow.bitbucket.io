package ClassLibrary.SimpleMethodologyOverview;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ProcessPattern.Process;
import ClassLibrary.ProcessPattern.ProcessSequence;
import ClassLibrary.StudyRelated.Study;

public class ProcessOverview extends AnnotatedIdentifiable {

	public final InternationalStructuredString[] overview;

	public AlgorithmOverview[] algorithmOverview;

	public Process[] process;

	public ProcessSequence processSequence;

	public Study[] study;

}
