package ClassLibrary.SimpleMethodologyOverview;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.MethodologyPattern.Methodology;
import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.StudyRelated.Study;

public class MethodologyOverview extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] subjectOfMethodology;

	public final ObjectName[] name;

	public final InternationalStructuredString[] usage;

	public final InternationalStructuredString[] rationale;

	public final InternationalStructuredString[] overview;

	public MethodologyOverview[] methodologyOverview;

	public MethodologyOverview[] methodologyOverview;

	public AlgorithmOverview[] algorithmOverview;

	public Methodology[] methodology;

	public ExternalMaterial[] externalMaterial;

	public Study[] study;

	public DesignOverview[] designOverview;

}
