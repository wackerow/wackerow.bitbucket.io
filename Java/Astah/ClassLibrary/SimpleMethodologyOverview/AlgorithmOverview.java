package ClassLibrary.SimpleMethodologyOverview;

import ClassLibrary.Identification.AnnotatedIdentifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.MethodologyPattern.Algorithm;
import ClassLibrary.Utility.ExternalMaterial;
import ClassLibrary.Workflows.WorkflowProcess;
import ClassLibrary.StudyRelated.Study;

public class AlgorithmOverview extends AnnotatedIdentifiable {

	public final ExternalControlledVocabularyEntry[] subjectOfAlgorithm;

	public final InternationalStructuredString[] overview;

	public Algorithm[] algorithm;

	public ExternalMaterial[] externalMaterial;

	public WorkflowProcess[] workflowProcess;

	public DesignOverview[] designOverview;

	public ProcessOverview[] processOverview;

	public MethodologyOverview[] methodologyOverview;

	public Study[] study;

}
