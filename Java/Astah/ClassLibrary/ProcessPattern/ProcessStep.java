package ClassLibrary.ProcessPattern;

import ClassLibrary.CollectionsPattern.CollectionMember;
import ClassLibrary.ComplexDataTypes.Binding;
import ClassLibrary.Workflows.Parameter;
import ClassLibrary.Workflows.WorkflowStep;

public abstract class ProcessStep extends CollectionMember {

	public final Binding[] hasInformationFlow;

	public ProcessStepRelation processStepRelation;

	public Service[] service;

	public ProcessStepRelation processStepRelation;

	public Parameter[] parameter;

	public ProcessStepIndicator processStepIndicator;

	public WorkflowStep workflowStep;

	public Parameter[] parameter;

}
