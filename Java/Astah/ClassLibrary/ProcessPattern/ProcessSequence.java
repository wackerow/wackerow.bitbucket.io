package ClassLibrary.ProcessPattern;

import ClassLibrary.CollectionsPattern.SimpleCollection;
import ClassLibrary.SimpleMethodologyOverview.ProcessOverview;

public abstract class ProcessSequence extends SimpleCollection {

	public final ProcessStepIndicator[] hasMemberIndicator;

	public Process[] process;

	public ProcessOverview[] processOverview;

	public ProcessControlStep[] processControlStep;

}
