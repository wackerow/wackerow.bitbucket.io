package ClassLibrary.ProcessPattern;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.InternationalStructuredString;
import ClassLibrary.ComplexDataTypes.ObjectName;
import ClassLibrary.Workflows.WorkflowProcess;
import ClassLibrary.MethodologyPattern.Design;
import ClassLibrary.MethodologyPattern.Algorithm;
import ClassLibrary.SimpleMethodologyOverview.ProcessOverview;
import ClassLibrary.SamplingMethodology.SamplingProcess;
import ClassLibrary.MethodologyPattern.Methodology;

public abstract class Process extends Identifiable {

	public final InternationalStructuredString[] overview;

	public final ObjectName[] name;

	public WorkflowProcess workflowProcess;

	public Design[] design;

	public ProcessSequence processSequence;

	public Algorithm[] algorithm;

	public ProcessOverview processOverview;

	public SamplingProcess samplingProcess;

	public Methodology[] methodology;

}
