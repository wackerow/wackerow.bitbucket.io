package ClassLibrary.ProcessPattern;

import ClassLibrary.Identification.Identifiable;
import ClassLibrary.ComplexDataTypes.ExternalControlledVocabularyEntry;
import ClassLibrary.Workflows.WorkflowService;
import ClassLibrary.Agents.Agent;

public abstract class Service extends Identifiable {

	public final ExternalControlledVocabularyEntry[] serviceLocation;

	public ProcessStep[] processStep;

	public WorkflowService workflowService;

	public Agent[] agent;

}
