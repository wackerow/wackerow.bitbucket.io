package ClassLibrary.ProcessPattern;

import ClassLibrary.CollectionsPattern.MemberIndicator;
import ClassLibrary.ComplexDataTypes.WorkflowStepIndicator;

public abstract class ProcessStepIndicator extends MemberIndicator {

	public ProcessStep[] processStep;

	public WorkflowStepIndicator workflowStepIndicator;

}
