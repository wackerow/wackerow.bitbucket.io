package ClassLibrary.Methodologies;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Provides a guide for the usage of a result within a specified application
 * 
 * Examples
 * ==========
 * The applied use of a weight determined by a weighting process in analyzing a data set
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Guide extends AnnotatedIdentifiable {

	/**
	 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString overview;

}