package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Type of WorkflowStep in which the execution flow is determined by one or more conditions.
 * 
 * 
 * Examples
 * ==========
 * If-then-else and iterative structures.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ConditionalControlConstruct extends WorkflowStep {

	/**
	 * Condition to be evaluated to determine whether or not to execute the Workflow Sequence in contains. It can be an expression and/or programmatic code. The specialized sub-classes determine whether the Sequence is executed when the condition is true or false
	 */
	CommandCode condition;

}