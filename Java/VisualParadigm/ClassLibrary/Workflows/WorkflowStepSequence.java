package ClassLibrary.Workflows;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A WorkflowStepSequence is an enumerated (simple) collection of WorkflowSteps.
 * 
 * WorkflowSteps that may be used in a WorkflowStepSequence may be of many subtypes, covering simple or computational acts, conditional steps such as an IfThenElse, specific concurrent processing defined by Split and SplitJoin, or designed for various specializations of the WorkflowProcess. .
 * 
 * Members in a WorkflowStepSequence exchange variables and values by way of output parameters, input parameters and their bindings.
 * 
 * 
 * Examples
 * ==========
 * A WorkflowStepSequence can be used to describe the flow of questions in a questionnaire. 
 * 
 * A WorkflowStepSequence can be used to decompose a BusinessProcess like data integration or anonymization into a series of steps.
 * 
 * Explanatory notes
 * ===================
 * If more complex temporal or graph ordering is required use the subtype StructuredWorkflowSteps.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:SequenceType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class WorkflowStepSequence extends AnnotatedIdentifiable {

	/**
	 * Provides the ability to "type" a sequence for classification or processing purposes. Supports the use of an external controlled vocabulary.
	 */
	Collection<ExternalControlledVocabularyEntry> typeOfWorkflowStepSequence;
	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * The Workflow Steps included in the Collection with an option to provide an index for position within ordered array.
	 */
	Collection<WorkflowStepIndicator> contains;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Set to true if the members are in an ordered set. If unordered set to false.
	 */
	boolean isOrdered;

}