package ClassLibrary.Workflows;

/**
 * Definition
 * ============
 * This extends the WorkflowStepSequence allowing for a graph structuring of WorkflowSteps. It adds a WorkflowStepRelationStructure which describes the order of Workflow steps in StructuredWorkflowSteps that has multiple entry points and/or multiple exit points and/or, alternatively, a WorkflowStep can have multiple predecessors and/or multiple successors.
 * 
 * Examples
 * ==========
 * Data Integration might combine multiple data source and make multiple data products. This sequence would have several starting points and terminate in multiple endpoints.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class StructuredWorkflowSteps extends WorkflowStepSequence {
}