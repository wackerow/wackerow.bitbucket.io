package ClassLibrary.Workflows;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A means of performing a Workflow Step as part of a concrete implementation of a Business Function  (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve).
 * 
 * Examples
 * ==========
 * A specification for an air flow monitor used to capture a measure for an ImplementedMeasure (a subtype of Act).
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class WorkflowService extends AnnotatedIdentifiable {

	/**
	 * Specifies how to communicate with the service.
	 */
	Collection<ExternalControlledVocabularyEntry> serviceInterface;
	/**
	 * Specifies where the service can be accessed.
	 */
	ExternalControlledVocabularyEntry serviceLocation;
	/**
	 * The estimated time period associated with the operation of the Service. This may be expressed as a time, date-time, or duration.
	 */
	Date estimatedDuration;

}