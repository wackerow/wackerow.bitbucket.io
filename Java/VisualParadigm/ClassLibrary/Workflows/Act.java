package ClassLibrary.Workflows;

/**
 * Definition
 * ============
 * An Act is an indivisible, atomic step, i.e. not composed of other steps. An Act can also be viewed as a terminal node in a hierarchy of Workflow Steps. 
 * 
 * Examples
 * ==========
 * QuestionConstructType from DDI 3.x is an example of an Act.
 * GenerationInstructionType from DDI 3..x is an example of an Act extended to include a CommandCode used to create a derivation or recode for an InstanceVariable or other similar use 
 * 
 * Explanatory notes
 * ===================
 * Act is named after the act in the HL7 RIM. This act can take many forms including an observation, a procedure, a referral, a prescription, a consent and so forth.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Act extends WorkflowStep {
}