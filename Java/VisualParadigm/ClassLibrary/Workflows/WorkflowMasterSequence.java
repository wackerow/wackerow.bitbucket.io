package ClassLibrary.Workflows;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Allows for the organizing WorkflowStepSequences into a larger MasterSequence. The WorkflowMasterSequence is a succession of WorkflowStepSequences in which one WorkflowStepSequence precedes the next.
 * 
 * Examples
 * ==========
 * A common example is of a Questionnaire that has multiple blocks (sequences) of InstrumentComponents (InstanceQuestion, Statement, etc.) that are reused in various orders. The U.S. Current Population Survey has a block of standard demographic questions as well as specialized blocks of questions that are fielded periodically generally in a given month (ex. School Enrollment fielded in September).
 * In a data capture multiple instruments might be administered in a certain order. A WorkflowMasterSequence describes this order.  Note that in this example successive instruments cannot begin before a previous instrument ends: this would NOT be a SimpleSucession. In a Demographic Surveillance Business Process Model there is BusinessProcess that identifies data quality issues. This BusinessProcess has a WorkflowMasterSequence that includes a succession of WorkflowStepSequences. Each WorkflowStepSequence performs the work of identifying a specific data quality issue. The WorkflowMasterSequence determines the order in which each data quality issue is identified. When the order is consequential the WorkflowMasterSequence isOrdered property is set to true.
 * 
 * Explanatory notes
 * ===================
 * A WorkflowMasterSequence is a SimpleCollection that is normally ordered. It allows for blocks WorkflowSteps expressed as a WorkflowStepSequence to be ordered sequentially thereby creating a master sequence that is identified by the WorkflowProcess as the overall ordering for the process.  
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class WorkflowMasterSequence extends WorkflowStepSequence {

	/**
	 * Restricts contains to WorkflowSequenceIndicator which supports the use of an index to specify the position of the referenced Workflow Sequence in an ordered array
	 */
	Collection<WorkflowStepSequenceIndicator> contains;

}