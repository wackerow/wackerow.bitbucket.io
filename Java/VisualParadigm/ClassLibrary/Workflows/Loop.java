package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Iterative control structure to be repeated a specified number of times based on one or more conditions. Inside the loop, one or more Workflow Steps are evaluated and processed in the order they appear.
 * 
 * Examples
 * ==========
 * A loop is set to repeat a fixed number of times (initialValue). A stepValue is initially specified. At the end of the loop the current value is decremented by the stepValue. The current value might be the initialValue or it might be the current value after the initialValue has been decremented one or more times.
 * 
 * Explanatory notes
 * ===================
 * The Workflow Sequence contained in the Loop is executed until the condition is met, and then control is handed back to the containing control construct.
 * 
 * Synonyms
 * ==========
 * ForLoop
 * 
 * DDI 3.2 mapping
 * =================
 * d:LoopType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Loop extends ConditionalControlConstruct {

	/**
	 * The command used to set the initial value for the process. Could be a simple value.
	 */
	CommandCode initialValue;
	/**
	 * The command used to set the incremental or step value for the process. Could be a simple value.
	 */
	CommandCode stepValue;

}