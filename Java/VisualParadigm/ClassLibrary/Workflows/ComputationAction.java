package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Provides an extensible framework for specific computation or transformation objects.
 * 
 * Examples
 * ==========
 * In data processing, a ComputationAction might be a code statement in a code sequence.
 * 
 * In data capture, a ComputationAction might takes the form of a question, a measurement or instrument code.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:ComputationItemType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ComputationAction extends Act {

	/**
	 * Describes the transformation activity
	 */
	InternationalStructuredString activityDescription;
	/**
	 * The command code performed as a part of the transformation or computation
	 */
	CommandCode usesCommandCode;
	/**
	 * Allows for the specification of a computation or transformation type supporting the use of an external controlled vocabulary
	 */
	ExternalControlledVocabularyEntry typeOfComputation;

}