package ClassLibrary.Workflows;

import ClassLibrary.ComplexDataTypes.*;
import java.util.*;

/**
 * Definition
 * ============
 * An Input or Output to a Process Step defined by as a unique identifier. It may be a single value or an array. 
 * 
 * Examples
 * ==========
 * A question might have dynamic text or, again, "fills". Depending on the gender of the subject, a question might say "he" or "she" and/or "him" or "her". A gender variable is passed into the question, and question code resolves the dynamic text.
 * A computation action may be expecting a numeric array of values [result of looping an "Age in years" question through every member of a household]. isArray = "true"; valueRepresentation would link to a SubstantiveValueDomain defining a numeric value with a possible valid range; and because the parameter occurs in the context of a computation action it is necessary to specify an alias (e.g. AGE). The alias links the parameter to the code in the computation.
 * 
 * Explanatory notes
 * ===================
 * When used as an hasInputParameter it defines a value being used by Process Step as a direct input or as an alias value within an equation. When used as an hasOutputParameter it defines itself as the value of the variable, capture, or explicit value of the parent class or as a specific value in a computation output by assigning the related Alias and/or defining the item within an array. Providing a defaultValue ensures that when bound to another Parameter that some value will be passed or received. A Value Domain expressed as a SubstantiveValueDomain or a SentinalValueDomain may be designated to define the datatype and range( of the expected value.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Parameter {

	/**
	 * If the content of the parameter is being used in a generic set of code as an alias for the value of an object in a formula (for example source code for a statistical program) enter that name here. This provides a link from the identified parameter to the alias in the code.
	 */
	UnlimitedNatural alias;
	/**
	 * Provides a default value for the parameter if there is no value provided by the object it is bound to or the process that was intended to produce a value.
	 */
	ValueString defaultValue;
	/**
	 * If set to "true" indicates that the content of the parameter is a delimited array rather than a single object and should be processed as such.limitArrayIndex
	 */
	boolean isArray;
	/**
	 * When the Parameter represents an array of items, this attribute specifies the index identification of the items within the zero-based array which should be treated as that associated with the parameter. If not specified, the full array is treated as the parameter.
	 */
	UnlimitedNatural limitArrayIndex;
	/**
	 * This is the registered agency code with optional sub-agencies separated by dots. For example, diw.soep, ucl.qss, abs.essg.
	 */
	string agency;
	/**
	 * The ID of the object. This must conform to the allowed structure of the DDI Identifier and must be unique within the Agency.
	 */
	string id;
	/**
	 * The version number of the object. The version number is incremented whenever the non-administrative metadata contained by the object changes.
	 */
	string version;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of the Agent Listing. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;

}