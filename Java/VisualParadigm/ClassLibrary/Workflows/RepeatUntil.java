package ClassLibrary.Workflows;

/**
 * Definition
 * ============
 * Iterative control structure to be repeated until a specified condition is met. After each iteration the condition is tested. If the condition is not met, the associated Workflow Sequence in contains (inherited from Conditional Control Construct) is triggered. When the condition is met, control passes back to the containing Workflow Step.
 * 
 * Examples
 * ==========
 * A RepeatUntil loop is similar to a RepeatWhile loop, except that it tests the Boolean expression after the loop rather than before. Thus, a RepeatUntil loop always executes the loop once, whereas if the Boolean expression is initially False, a RepeatWhile loop does not execute the loop at all. For example, in a household there is at last one person and a loop might contain a block of person questions. After the first person the last question in the block might be "Anyone else in the household"? The RepeatUntil would continue iterating until there was no-one else in the household.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:RepeatUntilType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class RepeatUntil extends ConditionalControlConstruct {
}