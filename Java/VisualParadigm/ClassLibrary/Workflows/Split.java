package ClassLibrary.Workflows;

/**
 * Definition
 * ============
 * The components of a Split consists of a number of process steps to be executed concurrently with partial synchronization. Split completes as soon as all of its component process steps have been scheduled for execution.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Supports parallel processing that does not require completion to exit. 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Split extends WorkflowStep {
}