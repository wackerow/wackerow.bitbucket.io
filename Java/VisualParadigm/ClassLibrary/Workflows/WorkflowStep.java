package ClassLibrary.Workflows;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * One of the constituents of a Workflow. It can be a composition or atomic and might be performed by a Service.
 * 
 * Examples
 * ==========
 * A ControlConstruct that orchestrates steps and substeps in a Workflow is a WorkflowStep. An Act is a WorkflowStep.
 * 
 * Explanatory notes
 * ===================
 * An atomic Workflow Step has no Control Constructs -- it's an Act. A composition consists of a tree of Control Constructs. In this tree Acts are associated with the leaf nodes.
 * 
 * Furthermore, a composition might be a glass box or a black box from the perspective of a service. If a service performs a WorkflowStep, it might invoke just the topmost ControlConstruct -- for example a WorkflowSequence. It may know nothing about the internal workings of the sequence. In this case the WorkflowStep is a black box.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Process Step
 */
public abstract class WorkflowStep extends AnnotatedIdentifiable {

	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	Collection<InternationalStructuredString> usage;
	/**
	 * Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString overview;
	/**
	 * Mapping of specific information flow within the workflow
	 */
	Collection<Binding> hasInformationFlow;

}