package ClassLibrary.Workflows;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * IfThenElse describes an if-then-else decision type of control construct. If the stated condition is met, then the associated Workflow Sequence in containsSubSeqence is triggered, otherwise the Workflow Sequence that is triggered is the one associated via elseContains.
 * 
 * 
 * Examples
 * ==========
 * An IfThenElse object describes the conditional logic in the flow of a questionnaire or other data collection instrument, where if a stated condition is met, one path is followed through the flow, and if the stated condition is met, another path is taken.
 * 
 * Explanatory notes
 * ===================
 * Contains a condition and two associations:
 * - one to the Workflow Sequence that is triggered when the condition is true (containsSubSequence), and 
 * - another to the Workflow Sequence that is triggered when the condition is false (elseContains). 
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:IfThenElseType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class IfThenElse extends ConditionalControlConstruct {

	/**
	 * The condition to check if the IfThenElse condition is false.
	 */
	Collection<ElseIfAction> elseIf;

}