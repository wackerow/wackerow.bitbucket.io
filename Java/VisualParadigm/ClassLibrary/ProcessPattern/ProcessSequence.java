package ClassLibrary.ProcessPattern;

import ClassLibrary.CollectionsPattern.*;
import java.util.*;

/**
 * Definition
 * ============
 * A collection of Process Steps organized as a sequence.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class ProcessSequence extends SimpleCollection {

	/**
	 * Restricts target to ProcessStepIndicator
	 */
	Collection<ProcessStepIndicator> hasMemberIndicator;

}