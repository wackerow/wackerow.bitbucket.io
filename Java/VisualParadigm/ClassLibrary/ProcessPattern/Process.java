package ClassLibrary.ProcessPattern;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;

/**
 * Definition
 * ============
 * Process is an implementation of an algorithm. It is the set of process steps taken as a whole. It is decomposable into ProcessSteps organized by a Process Sequence or more complex Process Relation Structure, but this decomposition is not necessary. 
 * 
 * Examples
 * ==========
 * The Generic Longitudinal Business Process Model (GLBPM) identifies a series of processes like data integration, anonymization, analysis variable construction and so forth in the data lifecycle.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Process Design
 */
public abstract class Process extends Identifiable {

	/**
	 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString overview;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;

}