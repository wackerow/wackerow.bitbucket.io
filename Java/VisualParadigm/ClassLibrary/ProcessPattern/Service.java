package ClassLibrary.ProcessPattern;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A means of performing a Process Step as part of a Business Function (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve). 
 * 
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Business Service
 */
public abstract class Service extends Identifiable {

	/**
	 * Specifies where the service can be accessed.
	 */
	ExternalControlledVocabularyEntry serviceLocation;

}