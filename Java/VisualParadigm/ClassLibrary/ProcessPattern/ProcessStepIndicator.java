package ClassLibrary.ProcessPattern;

import ClassLibrary.CollectionsPattern.*;

/**
 * Definition
 * ============
 * Member Indicator limited to ProcessStep
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class ProcessStepIndicator extends MemberIndicator {
}