package ClassLibrary.ProcessPattern;

import ClassLibrary.CollectionsPattern.*;

/**
 * Definition
 * ============
 * Specifies MemberRelation to ProcessSteps for Process in Process Pattern
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class ProcessStepRelation extends MemberRelation {
}