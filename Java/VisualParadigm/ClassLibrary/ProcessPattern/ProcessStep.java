package ClassLibrary.ProcessPattern;

import ClassLibrary.CollectionsPattern.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * One of the constituents of a Process. It can be a composition or atomic and might be performed by a Service.
 * 
 * Examples
 * ==========
 * Each step and substep in the Generic Longitudinal Business Process Model is a ProcessStep. 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class ProcessStep extends CollectionMember {

	/**
	 * Bindings used to pass data into and out of a Process Step.
	 */
	Collection<Binding> hasInformationFlow;

}