package ClassLibrary.Agents;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * Defines the relationships between Agents in a collection. Clarifies valid period and the purpose the relationship serve
 * 
 * Examples
 * ==========
 * An individual employed by an Organization. A unit or project (organization) within another Organization.
 * 
 * Explanatory notes
 * ===================
 * Describes relations between agents not roles within a project or in relationship to a product. Roles are defined by the parent class and relationship name that uses an Agent as a target.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AgentRelationStructure extends Identifiable {

	/**
	 * The effective start and end date of the relationship
	 */
	DateRange effectiveDates;
	/**
	 * Define the level of privacy regarding this relationship. Supports the use of a controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry privacy;
	/**
	 * Explanation of the intent of creating the relation. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list. Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relation structure
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Agent relations that comprise the relation structure
	 */
	Collection<AgentRelation> hasMemberRelation;

}