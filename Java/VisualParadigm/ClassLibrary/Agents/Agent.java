package ClassLibrary.Agents;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * An actor that performs a role in relation to a process or product.
 * 
 * Examples
 * ==========
 * Analyst performing edits on data, interviewer conducting an interview, a relational database management system managing data, organization publishing data on a regular basis, creator or contributor of a publication.
 * 
 * Explanatory notes
 * ===================
 * foaf:Agent is: An agent (eg. person, group, software or physical artifact)
 * prov:Agent is An agent is something that bears some form of responsibility for an activity taking place, for the existence of an entity, or for another agent's activity.
 * 
 * Synonyms
 * ==========
 * Agent
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Agent extends AnnotatedIdentifiable {

	/**
	 * An identifier within a specified system for specifying an agent
	 */
	Collection<AgentId> hasAgentId;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.
	 */
	Collection<PrivateImage> image;

}