package ClassLibrary.Agents;

import ClassLibrary.Identification.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A listing of Agents of any type. The AgentList may be organized to describe relationships between members using AgentRelationStructure.
 * 
 * Examples
 * ==========
 * Organizations contributing to a project. Individuals within an agency. All organizations, indivduals, and machines identified within the collections of an archive.
 * 
 * Explanatory notes
 * ===================
 * Relationships between agents are fluid and reflect effective dates of the relationship. An agent may have multiple relationships which may be sequencial or concurrent. Relationships may or may not be hierarchical in nature. All Agents are serialized individually and brought into relationships as appropriate.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AgentListing extends Identifiable {

	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of the Agent Listing. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<AgentIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}