package ClassLibrary.Agents;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A framework of authority designated to act toward some purpose.
 * 
 * Examples
 * ==========
 * U.S. Census Bureau, University of Michigan/ISR, Norwegian Social Data Archive 
 * 
 * Explanatory notes
 * ===================
 * related to org:Organization which is described as "Represents a collection of people organized together into a community or other..."
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Organization extends Agent {

	/**
	 * Names by which the organization is known.
	 */
	Collection<OrganizationName> hasOrganizationName;
	/**
	 * The agency identifier of the organization as registered at the DDI Alliance register.
	 */
	Collection<string> ddiId;
	/**
	 * Contact information for the organization including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.
	 */
	ContactInformation hasContactInformation;

}