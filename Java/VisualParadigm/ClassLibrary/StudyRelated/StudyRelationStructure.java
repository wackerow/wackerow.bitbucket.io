package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * Allows for a complex nested structure for a Study Series
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class StudyRelationStructure extends Identifiable {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Member relations that comprise the relation structure
	 */
	Collection<StudyRelation> hasMemberRelation;

}