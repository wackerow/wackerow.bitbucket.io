package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Evaluation for the purpose of reviewing the study, data collection, data processing, or management processes. Results may feed into a revision process for future data collection or management. Identifies the type of evaluation undertaken, who did the evaluation, the evaluation process, outcomes and completion date.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ExPostEvaluationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ExPostEvaluation extends AnnotatedIdentifiable {

	/**
	 * Brief identification of the type of evaluation. Supports the use of an external controlled vocabulary.
	 */
	Collection<ExternalControlledVocabularyEntry> typeOfEvaluation;
	/**
	 * Describes the evaluation process. Supports multi-lingual content. Allows the optional use of structured content.
	 */
	Collection<InternationalStructuredString> evaluationProcess;
	/**
	 * Describes the outcomes of the evaluation process. Supports multi-lingual content. Allows the optional use of structured content.
	 */
	Collection<InternationalStructuredString> outcomes;
	/**
	 * Identifies the date the evaluation was completed.
	 */
	Date completionDate;

}