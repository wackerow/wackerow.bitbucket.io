package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A collection of studies which can be structured as a simple sequence, or with a more complex structure.
 * 
 * Examples
 * ==========
 * An annual series of surveys.
 * 
 * Explanatory notes
 * ===================
 * A set of studies may be defined in many ways. A study may be repeated over time. One or more studies may attempt to replicate an earlier study. The StudySeries allows for the description of the relationships among a set of studies.
 * 
 * A simple ordered or unordered sequence of studies can be described via the "contains StudyIndicator" property. More complex relationships among studies may also be described using the optional "isStructuredBy StudyRelationsStructure".
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class StudySeries extends AnnotatedIdentifiable {

	/**
	 * The name of the series as a whole
	 */
	Collection<ObjectName> name;
	/**
	 * A general descriptive overview of the series.
	 */
	InternationalStructuredString overview;
	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<StudyIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}