package ClassLibrary.StudyRelated;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Study is the conceptual anchor for an individual collection activity resulting in a data set or data file. It can be both a part of a StudySeries as a wave etc. or an unrelated activity like a single survey project.
 * 
 * Examples
 * ==========
 * ICPSR study 35575 Americans and the Arts [1973 - 1992] (ICPSR 35575). https://www.icpsr.umich.edu/icpsrweb/ICPSR/studies/35575?dataFormat%5B0%5D=SAS&keyword%5B0%5D=public+opinion&geography%5B0%5D=United+States&searchSource=revise  
 * 
 * Explanatory notes
 * ===================
 * The Study class brings together many properties and relationships describing a set of data � coverage, kind of data, methodology, citation information, access information and more.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * s:StudyUnitType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Study extends AnnotatedIdentifiable {

	/**
	 * Describes, with a string or a term from a controlled vocabulary, the kind of data documented in the logical product(s) of a study unit. Examples include survey data, census/enumeration data, administrative data, measurement data, assessment data, demographic data, voting data, etc.
	 */
	Collection<ExternalControlledVocabularyEntry> kindOfData;
	/**
	 * A general descriptive overview of the Study
	 */
	InternationalStructuredString overview;
	/**
	 * Complete bibliographic reference containing all of the standard elements of a citation that can be used to cite the work. The "format" attribute is provided to enable specification of the particular citation style used, e.g., APA, MLA, Chicago, etc.
	 */
	InternationalStructuredString bibliographicCitation;

}