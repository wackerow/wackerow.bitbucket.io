package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * A complex RelationStructure for use with various classification collections ( CodeList, Statistical Classification, etc.)
 * 
 * Examples
 * ==========
 * A ClassificationRelationStructure for ISCO-08 would describe each of the major classifications as a parent of its sub-classifications.   1 Managers, for example would be listed as a parent of four sub groups: 11 Chief Executives, Senior Officials and Legislators;  12 Chief Executives, Senior Officials and Legislators;  13 Production and Specialized Services Managers; and  14 Hospitality, Retail and Other Services Managers. 
 * 
 * Explanatory notes
 * ===================
 * The ClassificationRelationStructure has a set of CategoryRelations which are basically adjacency lists. A source Category has a described relationship to a target list of Categories. The semantic might be, for example, "parentOf", or "contains", etc.. 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ClassificationRelationStructure extends Identifiable {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Member relations that comprise the relation structure
	 */
	Collection<CategoryRelation> hasMemberRelation;

}