package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A Classification Series is an ensemble of one or more Statistical Classifications, based on the same concept, and related to each other as versions or updates. Typically, these Statistical Classifications have the same name.
 * 
 * Examples
 * ==========
 * ISIC or ISCO
 * SIC (with different published versions related to the publication year)
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Classification Series
 */
public class ClassificationSeries extends AnnotatedIdentifiable {

	/**
	 * ClassificationSeries can be designed in a specific context. Supports the use of and External Controlled Vocabulary.
	 */
	ExternalControlledVocabularyEntry context;
	/**
	 * A ClassificationSeries is designed to classify a specific type of object/unit according to a specific attribute.
	 */
	ExternalControlledVocabularyEntry objectsOrUnitsClassified;
	/**
	 * Areas of statistics in which the ClassificationSeries is implemented.
	 */
	Collection<ExternalControlledVocabularyEntry> subject;
	/**
	 * A ClassificationSeries can be associated with one or a number of keywords.
	 */
	Collection<ExternalControlledVocabularyEntry> keyword;
	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * MemberIndicator Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<StatisticalClassificationIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}