package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * A structure for describing the complex relationship between statistical classifications in a classification series
 * 
 * Examples
 * ==========
 * A classification series that branches into separately versioned classifications
 * 
 * Explanatory notes
 * ===================
 * Can use relation specification information to more fully describe the relationship between members such as parent/child, whole/part, general/specific, equivalence, etc.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class StatisticalClassificationRelationStructure extends Identifiable {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Member relations that comprise the relation structure
	 */
	Collection<StatisticalClassificationRelation> hasMemberRelation;

}