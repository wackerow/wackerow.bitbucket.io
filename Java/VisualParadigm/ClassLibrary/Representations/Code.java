package ClassLibrary.Representations;

import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A type of Designation that relates a representation expressed as a string with or without meaningful white space to a specific classification. For use in a Code List. The representation property (Value) is expressed as it would be found in a data file. Multiple representations may relate to the same Category but should be expressed as separate Codes.
 * 
 * Examples
 * ==========
 * The letter M might stand for the category Male in the CodeList called Gender.
 * 
 * Explanatory notes
 * ===================
 * A Code is a kind of Designation, in particular it is the assignment of an alphanumeric string to stand in place of a category. 
 * 
 * It should not be confused with a ClassificationItem which is a placeholder for a position in a StatisticalClassification. It contains a Designation, for which Code is a kind; a Category; and other things. For example, in 2012 NAICS (North American Industry Classification System) one Classification Item has the Code 23 and the Category construction.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * l:CodeType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Code
 */
public class Code extends Designation {

	/**
	 * The value as expressed in the data file.
	 */
	ValueString representation;

}