package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * The LevelStructure describes the nesting structure of a hierarchical collection. The levels within the structure begin at the root level '1' and continue as an ordered array through each level of nesting.
 * 
 * 
 * 
 * Examples
 * ==========
 * ISCO-08 (Major, Sub-Major, and Minor) or NAICS (2 digit sector codes, 3 digit subsector code list, 4 digit industry group code list, and 5 digit industry code list)
 * 
 * 
 * Explanatory notes
 * ===================
 * Levels are used to organize a hierarchy. Usually, a hierarchy often contains one root member at the top, though it could contain several. These are the first Level. All members directly related to those  in the first Level compose the second Level. The third and subsequent Levels are defined similarly. 
 * 
 * A Level often is associated with a Concept, which defines it. These correspond to kinds of aggregates. For example, in the US Standard Occupational Classification (2010), the Level below the top is called Major Occupation Groups, and the next Level is called Minor Occupational Groups. These ideas convey the structure. In particular, Health Care Practitioners (a Major Group) can be broken into Chiropractors, Dentists, Physicians, Vets, Therapists, etc. (Minor Groups) The Categories in the Nodes at the lower Level aggregate to the Category in Node above them.
 * 
 * "Classification schemes are frequently organized in nested levels of increasing detail. ISCO-08, for example, has four levels: at the top level are ten major groups, each of which contain sub-major groups, which in turn are subdivided in minor groups, which contain unit groups. Even when a classification is not structured in levels ("flat classification"), the usual convention, which is adopted here, is to consider that it contains one unique level." (http://rdf-vocabulary.ddialliance.org/xkos.html#) Individual classification items organized in a hierarchy may be associated with a specific level.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * l:LevelType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Level
 */
public class LevelStructure extends AnnotatedIdentifiable {

	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text. Use in locations where you need to relay information on the usage of the instantiated class.
	 */
	InternationalStructuredString usage;
	/**
	 * The association of a level number in an ordered array with the description of that level
	 */
	Collection<Level> containsLevel;
	/**
	 * The dates for which the level structure is valid.  A structured DateRange with start and end Date (both with the structure of Date and supporting the use of ISO and non-ISO date structures); Use to relate a period with a start and end date.
	 */
	DateRange validDateRange;

}