package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * The permitted range of values for a characteristic of a variable. [GSIM 1.1]
 * 
 * Examples
 * ==========
 * Age categories with a numeric code list; Age in years; Young, Middle-aged and Old
 * 
 * Explanatory notes
 * ===================
 * The values can be described by enumeration or by an expression. Value domains can be either substantive/sentinel, or described/enumeration
 * 
 * Synonyms
 * ==========
 * BUT NOT Grid/Numeric/Code_ResponseDomain [DDI-L/Questions] - this is "ResponseDomain"
 * 
 * DDI 3.2 mapping
 * =================
 * r:RepresentationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Value Domain
 */
public abstract class ValueDomain extends AnnotatedIdentifiable {

	/**
	 * A display label for the object. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * The data types that are recommended for use with this domain
	 */
	Collection<ExternalControlledVocabularyEntry> recommendedDataType;

}