package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Identifies the authorizing agency and allows for the full text of the authorization (law, regulation, or other form of authorization).
 * 
 * Examples
 * ==========
 *  May be used to list authorizations from oversight committees and other regulatory agencies.
 * 
 * Explanatory notes
 * ===================
 * Supports requirements for some statistical offices to identify the agency or law authorizing the collection or management of data or metadata.
 * 
 * Synonyms
 * ==========
 * Use for the Case Law, Case Law Description, and Case Law Date properties in ClassificationItem
 * 
 * DDI 3.2 mapping
 * =================
 * r:AuthorizationSourceType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AuthorizationSource extends AnnotatedIdentifiable {

	/**
	 * Text of the authorization (law, mandate, approved business case).
	 */
	InternationalStructuredString statementOfAuthorization;
	/**
	 * Provide a legal citation to a law authorizing the study/data collection. For example, a legal citation for a law authorizing a country's census.
	 */
	InternationalString legalMandate;
	/**
	 * Identifies the date of Authorization.
	 */
	Date authorizationDate;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;

}