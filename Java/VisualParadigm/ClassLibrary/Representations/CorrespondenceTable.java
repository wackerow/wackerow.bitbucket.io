package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;

/**
 * Definition
 * ============
 * A Correspondence Table expresses relationships between the members within or between StatisticalClassifications.
 * 
 * Examples
 * ==========
 * Correspondence between the U.S. Standard Industrial Classification (SIC) and North American Industrial Classification System (NAICS)
 * 
 * Explanatory notes
 * ===================
 * CorrespondenceTables are used with Statistical Classifications. For instance, it can relate two versions from the same Classification Series; Statistical Classifications from different Classification Series; a variant and the version on which it is based; or, different versions of a variant. In the first and last examples, the Correspondence Table facilitates comparability over time.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Correspondence Table
 */
public class CorrespondenceTable extends AnnotatedIdentifiable {

	/**
	 * Effective period of validity of the CorrespondenceTable. The correspondence table expresses the relationships between the two NodeSets as they existed on the period specified in the table.
	 */
	DateRange effectiveDates;
	/**
	 * Set of mappings between nodes that participate in the correspondence.
	 */
	Collection<Map> correspondence;

}