package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.XMLSchemaDatatypes.*;
import java.util.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A Classification Index is an ordered list (alphabetical, in code order etc) of Classification Index Entries. A Classification Index can relate to one particular or to several Statistical Classifications. [GSIM Statistical Classification Model]
 * 
 * Examples
 * ==========
 * An alphabetical index of a topically ordered Statistical Classification
 * 
 * Explanatory notes
 * ===================
 * A Classification Index shows the relationship between text found in statistical data sources (responses to survey questionnaires, administrative records) and one or more Statistical Classifications.  A Classification Index may be used to assign the codes for Classification Items to observations in statistical collections. 
 * A Statistical Classification is a subtype of Node Set. The relationship between Statistical Classification and Classification Index can also be extended to include the other Node Set types - Code List and Category Set.  [GSIM Statistical Classification Model]  Note that a GSIM Node is the equivalent of a DDI Member and a GSIM Node Set is a DDI Collection.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Classification Index
 */
public class ClassificationIndex extends AnnotatedIdentifiable {

	/**
	 * Date when the current version of the Classification Index was released.
	 */
	Date releaseDate;
	/**
	 * A Classification Index can exist in several languages. Indicates the languages available. If a Classification Index exists in several languages, the number of entries in each language may be different, as the number of terms describing the same phenomenon can change from one language to another. However, the same phenomena should be described in each language. Supports the indication of multiple languages within a single property. Supports use of codes defined by the RFC 1766.
	 */
	LanguageSpecification availableLanguage;
	/**
	 * Verbal summary description of corrections, which have occurred within the Classification Index. Corrections include changing the item code associated with an Classification Index Entry.
	 */
	Collection<InternationalString> corrections;
	/**
	 * Additional information which drives the coding process for all entries in a Classification Index.
	 */
	Collection<CommandCode> codingInstruction;
	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<ClassificationIndexEntryIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}