package ClassLibrary.Representations;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A formal description of a set of values. 
 * 
 * Examples
 * ==========
 * Example 1) 
 * The integers between 1 and 10 inclusive.  
 * The values of x satisfying the logicalExpression property:
 * " (1 <=x <= 10) AND mod(x,10)=0"
 * Also described with minimumValueInclusive = 1 and maximumValueInclusive = 10 (and datatype of integer)
 * 
 * Example 2)
 * The upper case letters A through C and X
 * described with the regularExpression "/[A-CX]/"
 * 
 * 
 * Example 3)
 * A date-time described with the Unicode Locale Data Markup Language pattern yyyy.MM.dd G 'at' HH:mm:ss zzz
 * 
 * 
 * Explanatory notes
 * ===================
 * The ValueAndConceptDescription may be used to describe either a value domain or a conceptual domain. For a value domain, the ValueAndConceptDescription contains the details for a �described� domain (as opposed to an enumerated domain). There are a number of properties which can be used for the description. The description could be just text such as: �an even number greater than zero�. Or a more formal logical expression like �x>0 and mod(x,2)=0�. A regular expression might be useful for describing a textual domain. It could also employ a format pattern from the Unicode Locale Data Markup Language (LDML) (http://www.unicode.org/reports/tr35/tr35.html.
 * Some Conceptual Domains might be described with just a narrative. Others, though, might be described in much the same way as a value domain depending on the specificity of the concept.
 * 
 * In ISO 11404 a value domain may be described either through enumeration or description, or both. This class provides the facility for that description. It may be just a text description, but a description through a logical expression is machine actionable and might, for example, be rendered as an integrity constraint. If both text and a logical expression are provided the logical expression is to be taken as the canonical description.
 * The logical expression should conform to the expression syntax of VTL. https://sdmx.org/?page_id=5096 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ValueAndConceptDescription extends Identifiable {

	/**
	 * A formal description of the set of values.
	 */
	InternationalStructuredString description;
	/**
	 * A logical expression where the values of "x" making the expression true are the members of the set of valid values.  Example: (all reals x such that  x >0) describes the real numbers greater than 0
	 */
	ExternalControlledVocabularyEntry logicalExpression;
	/**
	 * A regular expression where strings matching the expression belong to the set of valid values. Use typeOfContent to specify the syntax of the regularExpression found in content.
	 */
	TypedString regularExpression;
	/**
	 * A string denoting the minimum possible value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 minimum: �An atomic property that contains a single number or string that is the minimum valid value (inclusive); equivalent to minInclusive. The value of this property becomes the minimum annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.�
	 */
	string minimumValueInclusive;
	/**
	 * A string denoting the maximum possible value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 maximum: �An atomic property that contains a single number or string that is the maximum valid value (inclusive); equivalent to maxInclusive. The value of this property becomes the maximum annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.�
	 */
	string maximumValueInclusive;
	/**
	 * A string denoting the minimum possible value (excluding this value) From https://www.w3.org/TR/tabular-metadata/  5.11.2 minExclusive: �An atomic property that contains a single number or string that is the minimum valid value (exclusive). The value of this property becomes the minimum exclusive annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.�   DDI3.2 handles this with a Boolean isInclusive attribute.
	 */
	string minimumValueExclusive;
	/**
	 * A string denotFrom https://www.w3.org/TR/tabular-metadata/  5.11.2 maxExclusive: �An atomic property that contains a single number or string that is the maximum valid value (exclusive). The value of this property becomes the maximum exclusive annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.�   DDI3.2 handles this with a Boolean isInclusive attribute. ing the maximumpossible value (excluding this value)
	 */
	string maximumValueExclusive;
	/**
	 * Indicates the type of relationship, nominal, ordinal, interval, ratio, or continuous. Use where appropriate for the representation type.
	 */
	CategoryRelationCode classificationLevel;
	/**
	 * A pattern for a number as described in Unicode Locale Data Markup Language (LDML) (http://www.unicode.org/reports/tr35/tr35.html) Part 3: Numbers  (http://www.unicode.org/reports/tr35/tr35-numbers.html#Number_Format_Patterns) and Part 4. Dates (http://www.unicode.org/reports/tr35/tr35-dates.html#Date_Format_Patterns) . Examples would be    #,##0.### to describe the pattern for a decimal number, or yyyy.MM.dd G 'at' HH:mm:ss zzz for a datetime pattern.
	 */
	ExternalControlledVocabularyEntry formatPattern;

}