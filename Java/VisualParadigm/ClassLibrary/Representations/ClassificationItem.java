package ClassLibrary.Representations;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A Classification Item represents a Category at a certain Level within a Statistical Classification.
 * 
 * 
 * Examples
 * ==========
 * In the  2012 NAICS (North American Industry Classification System) one Classification Item has the Code 23 and the Category construction.
 * 
 * Explanatory notes
 * ===================
 * A Classification Item defines the content and the borders of the Category. A Unit can be classified to one and only one item at each Level of a Statistical Classification. As such a Classification Item is a placeholder for a position in a StatisitcalClassification. It contains a Designation, for which Code is a kind; a Category; and other things. 
 * 
 * This differentiates it from Code which is a kind of Designation, in particular it is an alphanumeric string assigned to stand in place of a category. For example, the letter M might stand for the category Male in the CodeList called Gender.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Classification Item
 */
public class ClassificationItem extends Code {

	/**
	 * Indicates whether or not the item is currently valid. If updates are allowed in the Statistical Classification, an item may be restricted in its validity, i.e. it may become valid or invalid after the Statistical Classification has been released.
	 */
	boolean isValid;
	/**
	 * Indicates whether or not the item has been generated to make the level to which it belongs complete
	 */
	boolean isGenerated;
	/**
	 * A Classification Item may be associated with explanatory notes, which further describe and clarify the contents of the Category. Explanatory notes consist of: General note: Contains either additional information about the Category, or a general description of the Category, which is not structured according to the "includes", "includes also", "excludes" pattern. Includes: Specifies the contents of the Category. Includes also: A list of borderline cases, which belong to the described Category. Excludes: A list of borderline cases, which do not belong to the described Category. Excluded cases may contain a reference to the Classification Items to which the excluded cases belong.
	 */
	Collection<InternationalStructuredString> explanatoryNotes;
	/**
	 * The future events describe a change (or a number of changes) related to an invalid item. These changes may e.g. have turned the now invalid item into one or several successor items. This allows the possibility to follow successors of the item in the future.
	 */
	Collection<InternationalString> futureNotes;
	/**
	 * Describes the changes, which the item has been subject to during the life time of the actual Statistical Classification.
	 */
	InternationalString changeLog;
	/**
	 * Describes the changes, which the item has been subject to from the previous version to the actual Statistical Classification
	 */
	InternationalString changeFromPreviousVersion;
	/**
	 * Dates for which the classification is valid. Date from which the item became valid. The date must be defined if the item belongs to a floating Statistical classification. Date at which the item became invalid. The date must be defined if the item belongs to a floating Statistical classification and is no longer valid
	 */
	DateRange validDates;
	/**
	 * A Classification Item has a name as provided by the owner or maintenance unit. The name describes the content of the category. The name is unique within the Statistical Classification to which the item belongs, except for categories that are identical at more than one level in a hierarchical classification. Use the context attribute to identify Official Name or alternate names for the Classification Item.
	 */
	Collection<ObjectName> name;

}