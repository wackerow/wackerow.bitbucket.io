package ClassLibrary.SignificationPattern;

import ClassLibrary.CollectionsPattern.*;

/**
 * Definition
 * ============
 * Concept or object denoted by the signifier associated to a sign.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Concept or object represented by the sign.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Signified extends CollectionMember {
}