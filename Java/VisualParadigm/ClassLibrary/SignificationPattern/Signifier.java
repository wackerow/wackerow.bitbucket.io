package ClassLibrary.SignificationPattern;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Concept whose extension includes perceivable objects. 
 * 
 * Examples
 * ==========
 * A perceivable object in the extension of a signifier is a token. For instance, the object 5 is a token of �the numeral five�, a signifier.
 * 
 * Explanatory notes
 * ===================
 * A signifier has the potential to refer to an object.  In this case, the referring signifier is a label.  If that object is a concept, then the referring signifier is a designation.  A label is a representation of an object by a signifier which denotes it.  For instance, the token http://www.bls.gov is a label for the web site maintained for the US Bureau of Labor Statistics. The label is also an identifier, since it is intended to dereference the BLS web site. It is a locator, since the HTTP service is associated with it. Finally, �Dan Gillman� is a name for a co-author of this paper, since the token is linguistic.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Signifier {

	/**
	 * Perceivable object in the extension of the signifier. The actual content of this value as a string.
	 */
	string content;
	/**
	 * The usual setting "collapse" states that leading and trailing white space will be removed and multiple adjacent white spaces will be treated as a single white space. When setting to "replace" all occurrences of #x9 (tab), #xA (line feed) and #xD (carriage return) are replaced with #x20 (space) but leading and trailing spaces will be retained. If the existence of any of these white spaces is critical to the understanding of the content, change the value of this attribute to "preserve".
	 */
	WhiteSpaceRule whiteSpace;

}