package ClassLibrary.SignificationPattern;

import ClassLibrary.CollectionsPattern.*;

/**
 * Definition
 * ============
 * Something that suggests the presence or existence of a fact, condition, or quality.
 * 
 * Examples
 * ==========
 * The terms “Unemployment” and “Arbeitslosigkeit”
 * 
 * Explanatory notes
 * ===================
 * It is a perceivable object used to denote a concept or an object.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Sign extends CollectionMember {

	/**
	 * A perceivable object used to denote a signified.
	 */
	Signifier representation;

}