package ClassLibrary.EnumerationsRegExp;

public enum TextDirectionValues {
	Ltr,
	Rtl,
	Auto,
	Inherit
}