package ClassLibrary.EnumerationsRegExp;

public enum PointFormat {
	DecimalDegree,
	DegreesMinutesSeconds,
	DecimalMinutes,
	Meters,
	Feet
}