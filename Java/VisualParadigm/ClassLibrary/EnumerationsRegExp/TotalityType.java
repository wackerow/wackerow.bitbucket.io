package ClassLibrary.EnumerationsRegExp;

public enum TotalityType {
	Total,
	Partial,
	Unknown
}