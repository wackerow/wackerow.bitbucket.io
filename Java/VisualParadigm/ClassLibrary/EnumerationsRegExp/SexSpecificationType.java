package ClassLibrary.EnumerationsRegExp;

public enum SexSpecificationType {
	Masculine,
	Feminine,
	GenderNeutral
}