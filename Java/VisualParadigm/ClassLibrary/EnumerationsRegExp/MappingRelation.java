package ClassLibrary.EnumerationsRegExp;

public enum MappingRelation {
	ExactMatch,
	CloseMatch,
	Disjoint
}