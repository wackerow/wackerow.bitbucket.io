package ClassLibrary.EnumerationsRegExp;

public enum SpatialObjectPairs {
	PointToPoint,
	PointToLine,
	PointToArea,
	LineToLine,
	LineToArea,
	AreaToArea
}