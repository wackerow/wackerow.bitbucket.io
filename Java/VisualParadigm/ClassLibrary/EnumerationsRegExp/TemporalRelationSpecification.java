package ClassLibrary.EnumerationsRegExp;

public enum TemporalRelationSpecification {
	TemporalMeets,
	TemporalContains,
	TemporalFinishes,
	TemporalPrecedes,
	TemporalStarts,
	TemporalOverlaps,
	TemporalEquals
}