package ClassLibrary.EnumerationsRegExp;

public enum CategoryRelationCode {
	Nominal,
	Ordinal,
	Interval,
	Ratio,
	Continuous
}