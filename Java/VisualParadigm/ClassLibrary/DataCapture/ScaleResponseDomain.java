package ClassLibrary.DataCapture;

/**
 * Definition
 * ============
 * A response domain capturing a scaled response, such as a Likert scale. 
 * Note: This item still must be modeled and is incomplete at this time.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ScaleResponseDomain extends ResponseDomain {
}