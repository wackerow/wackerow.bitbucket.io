package ClassLibrary.DataCapture;

/**
 * Definition
 * ============
 * An instance measurement instantiates a represented measurement, so that it can be used as an Act in the Process Steps that define a data capture process.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class InstanceMeasurement extends InstrumentComponent {
}