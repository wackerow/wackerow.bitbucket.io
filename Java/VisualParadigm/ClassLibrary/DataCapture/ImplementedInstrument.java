package ClassLibrary.DataCapture;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.XMLSchemaDatatypes.*;

/**
 * Definition
 * ============
 * A specific data capture tool.  ImplementedInstruments are mode and/or unit specific.
 * 
 * Examples
 * ==========
 * Two versions of a conceptual instrument, computer assisted personal interviewing (CAPI) and mail, would be separate ImplementedInstruments. These might both reference the same ConceptualInstrument. 
 * A piece of equipment used to measure blood pressure.
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * aka PhysicalInstrument
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ImplementedInstrument extends AnnotatedIdentifiable {

	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Specification of the type of instrument according to the classification system of the documentor.
	 */
	ExternalControlledVocabularyEntry typeOfInstrument;
	/**
	 * A reference to an external representation of the the data collection instrument, such as an image of a questionnaire or programming script.
	 */
	Collection<anyUri> uri;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	Collection<InternationalStructuredString> usage;

}