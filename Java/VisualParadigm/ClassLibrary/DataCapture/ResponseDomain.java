package ClassLibrary.DataCapture;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * The possible list of values that are allowed by a Capture.
 * 
 * Examples
 * ==========
 * Yes/No, Male/Female, Age in years, Open-ended text, Temperature, BP reading
 * 
 * Explanatory notes
 * ===================
 * Identifies both the sentinel and substantive value domains used for capturing the response to a question
 * 
 * Synonyms
 * ==========
 * GSIM & DDI: ResponseDomain
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ResponseDomain extends AnnotatedIdentifiable {

	/**
	 * A display label for the domain. May be expressed in multiple languages.
	 */
	LabelForDisplay displayLabel;

}