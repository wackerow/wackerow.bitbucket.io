package ClassLibrary.DataCapture;

import ClassLibrary.Utility.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Any external material used in an instrument that aids or facilitates data capture, or that is presented to a respondent and about which measurements are made. 
 * 
 * Examples
 * ==========
 * Image, link, external aid, stimulus, physical object.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:OtherMaterialType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ExternalAid extends ExternalMaterial {

	ExternalControlledVocabularyEntry stimulusType;

}