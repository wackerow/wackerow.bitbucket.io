package ClassLibrary.DataCapture;

import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * An InstrumentComponent that specifies the performance of a specific computation within the context of an instrument flow.
 * 
 * Examples
 * ==========
 * quality control, edit check, checksums, compute filler text, compute values for use in administering the instrument
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:ComputationItemType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class InstrumentCode extends InstrumentComponent {

	/**
	 * The purpose of the code (e.g., quality control, edit check, checksums, compute filler text, compute values for use in administering the instrument)
	 */
	ExternalControlledVocabularyEntry purposeOfCode;
	/**
	 * Describes the code used to execute the command using the options of inline textual description, inline code, and/or an external file.
	 */
	CommandCode usesCommandCode;

}