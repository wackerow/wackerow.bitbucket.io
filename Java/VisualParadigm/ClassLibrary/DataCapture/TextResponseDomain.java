package ClassLibrary.DataCapture;

import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A response domain capturing a textual response including the length of the text and restriction of content using a regular expression.
 * 
 * Examples
 * ==========
 * Collecting the first name on an individual in an open ended text field
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:TextDomainType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class TextResponseDomain extends ResponseDomain {

	/**
	 * The maximum number of characters allowed.
	 */
	int maximumLength;
	/**
	 * The minimum number of characters allowed.
	 */
	int minimumLength;
	/**
	 * A regular expression limiting the allowed characters or character order of the content. Use typeOfContent to specify the syntax of the regularExpression found in content.
	 */
	TypedString regularExpression;

}