package ClassLibrary.DataCapture;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Provides the content and description of data capture instructions. Contains the "how to" information for administering an instrument.
 * 
 * Examples
 * ==========
 * Completion instructions in self-administered mail questionnaire, information for administering a blood pressure measurement, interviewer instructions for a CATI questionnaire, guidance for communicating between an interviewer and a respondent (note MIDUS cognitive assessment example).
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:InstructionType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Instruction extends AnnotatedIdentifiable {

	/**
	 * The content of the Instruction text provided using DynamicText. Note that when using Dynamic Text, the full InstructionText must be repeated for multi-language versions of the content. The InstructionText may also be repeated to provide a dynamic and plain text version of the instruction. This allows for accurate rendering of the instruction in a non-dynamic environment like print.
	 */
	Collection<DynamicText> instructionText;
	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;

}