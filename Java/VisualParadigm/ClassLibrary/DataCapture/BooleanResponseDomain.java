package ClassLibrary.DataCapture;

/**
 * Definition
 * ============
 * A response domain capturing a binary response, such as selected/unselected or checked/unchecked or true/false.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Exactly what is captured may be dependent upon the Implemented Instrument, however the response being captured is either there or not there (ON/OFF, True/False)
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class BooleanResponseDomain extends ResponseDomain {
}