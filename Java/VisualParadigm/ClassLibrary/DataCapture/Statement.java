package ClassLibrary.DataCapture;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A Statement is a type of Instrument Component containing  human readable text or referred material. 
 * 
 * Examples
 * ==========
 * Introductory text; Explanations; Labels, Headers, Help screens, etc.  "Thank you for agreeing to take this survey. We will start with a brief set of demographic questions." "The following set of questions are related to your household income. Please consider all members of the household and all sources of income when answering these questions."
 * 
 * Explanatory notes
 * ===================
 * It is not directly related to another specific Instrument Component such as an InstanceQueston or InstanceMeasurement. It may be placed anywhere in a WorkflowStepSequence. 
 * 
 * Synonyms
 * ==========
 * DDI:StatementItem
 * 
 * DDI 3.2 mapping
 * =================
 * d:StatementItemType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Statement extends InstrumentComponent {

	/**
	 * Structured human-readable text that allows for dynamic content. For example, the insertion of a name or gender specific pronoun. Repeat ONLY for capturing the same content in multiple languages.
	 */
	Collection<DynamicText> statementText;
	/**
	 * Describes the use of the statement within the instrument. For example, a section divider, welcome statement, or other text.
	 */
	ExternalControlledVocabularyEntry purposeOfStatement;

}