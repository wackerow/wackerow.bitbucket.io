package ClassLibrary.DataCapture;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A measurement that describes a means of capturing data. This class can be extended to account for different specific means. Use a specific instantiation of a Capture to describe a means of capturing a measurement.
 * 
 * 
 * 
 * 
 * Examples
 * ==========
 * A survey question, blood pressure reading; MRI images; thermometer; web service; experimental observation. Classes could include InstanceQuestion, InstanceMeasurement or other class extending Capture.
 * 
 * Explanatory notes
 * ===================
 * Provides an abstract base so that current and future forms of data capture can use this as an extension base and be freely mixed and matched within conceptual instruments as needed such as capturing a GPS point (using a RepresentedMeasurement) when administering a questionnaire (using RepresentedQuestions).
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Capture extends AnnotatedIdentifiable {

	/**
	 * A name for the measurement. A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString usage;
	/**
	 * A description of the purpose or use of the Measurement. May be expressed in multiple languages and supports the use of structured content.
	 */
	InternationalStructuredString purpose;
	/**
	 * The source of a capture structure defined briefly; typically using an external controlled vocabulary
	 */
	ExternalControlledVocabularyEntry captureSource;
	/**
	 * Identifies the unit being analyzed such as a Person, Housing Unit, Enterprise, etc.
	 */
	Collection<ExternalControlledVocabularyEntry> analysisUnit;

}