package ClassLibrary.Utility;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;

/**
 * Definition
 * ============
 * Provides information about the individual, agency and/or grant(s) which funded the described entity. Lists a reference to the agency or individual as described by a DDI Agent, the role of the funder, the grant number(s) and a description of the funding activity.
 * 
 * Examples
 * ==========
 * A "millionaire grant" (funding description) from John Beresford Tipton, Jr. (individual). Exploration of the effect of sudden wealth and basis for a television episode (funder role).
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:FundingInformationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class FundingInformation extends Identifiable {

	/**
	 * Role of the funding organization or individual. Supports the use of a controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry funderRole;
	/**
	 * The identification code of the grant or other monetary award which provided funding for the described object.
	 */
	Collection<string> grantNumber;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured tex
	 */
	InternationalStructuredString purpose;

}