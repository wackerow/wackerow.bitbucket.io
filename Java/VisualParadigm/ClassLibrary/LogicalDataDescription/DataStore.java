package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * A DataStore is either a SimpleCollection or a StructuredCollection of LogicalRecords, keeping in mind that a LogicalRecords is a definition, not a "datasets".
 * 
 * LogicalRecords organized in a StructuredCollection is called a LogicalRecordRelationStructure.
 * 
 * Instances of LogicalRecords instantiated as organizations of DataPoints hosting data are described in FormatDescription.
 * 
 * A DataStore is reusable across studies. Each Study has at most one DataStore.
 * 
 * 
 * Examples
 * ==========
 * The data lineage of an individual BusinessProcess or an entire DataPipeline are both examples of  a LogicalRecordRelationStructures. In a data lineage we can observe how LogicalRecords are connected within a BusinessProcess or across BusinessProcesses.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * Schema repository, data network
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Data Set
 */
public class DataStore extends AnnotatedIdentifiable {

	/**
	 * Default character set used in the Data Store.
	 */
	string characterSet;
	/**
	 * The type of DataStore. Could be delimited file, fixed record length file, relational database, etc. Points to an external definition which can be part of a controlled vocabulary maintained by the DDI Alliance.
	 */
	ExternalControlledVocabularyEntry dataStoreType;
	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * The number of records in the Data Store.
	 */
	int recordCount;
	/**
	 * General information about missing data, e.g., that missing data have been standardized across the collection, missing data are present because of merging, etc.-  corresponds to DDI2.5 dataMsng.
	 */
	InternationalStructuredString aboutMissing;
	/**
	 * Data in the form of Data Records contained in the Data Store. Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<LogicalRecordIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}