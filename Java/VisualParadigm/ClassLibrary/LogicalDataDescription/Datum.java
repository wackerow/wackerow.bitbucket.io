package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Representations.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A Datum is a designation (a representation of a concept by a sign) with a notion of equality defined. The sign itself is a perceivable object.
 * 
 * Examples
 * ==========
 * A systolic blood pressure of 122 is measured. The Signifier for that measurement in this paragraph is the character string "122". The Datum in this case is a kind of Designation (Sign), which is the association of the underlying measured concept (a blood pressure at that level) with that Signifier.  The Datum has an association with an InstanceVariable which allows the attachment of a unit of measurement (mm Hg), a datatype, a Population, and the Act which produced the measurement.  These InstanceVariable attributes are critical for interpreting the Signifier.
 * 
 * Explanatory notes
 * ===================
 * From GSIM 1.1 "A Datum is the actual instance of data that was collected or derived. It is the value which populates one or more Data Points. A Datum is the value found in a cell of a table." (https://statswiki.unece.org/display/GSIMclick/Datum )  A Datum could be copied to one or more datasets.
 * DDI4 takes a little more formal (semiotic) description of a Datum using the Signification Pattern. See the attached example.
 * 
 * 
 * NOTE: This is NOT datum from DDI3.2 (which is quite specific).
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Datum
 */
public class Datum extends Designation {

	/**
	 * Specification of the type of Signification
	 */
	ValueString representation;

}