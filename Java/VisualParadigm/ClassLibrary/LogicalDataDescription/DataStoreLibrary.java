package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A DataStoreLibrary is a collection or, again, a "library" of DataStores.
 * 
 * An individual DataStore is associated with a Study. A collection of DataStores is associated with a StudySeries.
 * 
 * The relationships among the DataStores in the DataStoreLibrary is described by the DataStoreRelationStructure. Relations may be more or less complicated depending on the StudySeries type. A StudySeries may be ad hoc. A StudySeries may form a time series. The variety of these collections has been described using the <a href=https://www.ddialliance.org/Specification/DDI-Lifecycle/3.1/XMLSchema/FieldLevelDocumentation/">"Group"</a> in DDI 3.1.
 * 
 * Like any RelationStructure, the DataStoreRelationStructure is able to describe both part/whole relations and generalization/specialization relations. See the controlled vocabulary at <a href="http://lion.ddialliance.org/datatypes/relationspecification">RelationSpecification</a> to review all the types of relations a RelationStructure is able to describe. 
 * 
 * Examples
 * ==========
 * A StudySeries is a time series and a DataStoreRelationStructure is dedicated to tracking "supplements".
 * 
 * There is a study at T0. Perhaps it is never executed. It is the "core" study. At T1 there is a study that includes the core and Supplement A. At T2 there is a study that includes the core, Supplement A and Supplement B. At T3 there is a study that includes the core and a one off supplement that is never asked again. T4 though T6 repeats T1 through T3.
 * 
 * 
 * 
 * Explanatory notes
 * ===================
 * If we broke a study down into sub-studies where the core is a sub-study and each supplement is a sub-study, we could also use the DataStoreRelationStructure to track the generalization/specialization relationship or, again, "going deep" over time.
 * 
 * Sometimes across a StudySeries we add panels. If panels were also sub-studies, we could use the DataStoreRelationStructure to track both the core and supplements by panel over time.
 * 
 * Synonyms
 * ==========
 * Structured metadata archive
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class DataStoreLibrary extends AnnotatedIdentifiable {

	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * The DataStores in the Catalog. Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<DataStoreIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}