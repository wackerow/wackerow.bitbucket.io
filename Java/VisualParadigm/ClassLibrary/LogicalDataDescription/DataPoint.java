package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.*;

/**
 * Definition
 * ============
 * A DataPoint is a container for a Datum.
 * 
 * Examples
 * ==========
 * A cell in a spreadsheet table. Note that this could be empty. It exists independently of the value to be stored in it.
 * 
 * Explanatory notes
 * ===================
 * The DataPoint is structural and distinct from the value (the Datum) that it holds. [GSIM 1.1]
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Data Point
 */
public class DataPoint extends AnnotatedIdentifiable {
}