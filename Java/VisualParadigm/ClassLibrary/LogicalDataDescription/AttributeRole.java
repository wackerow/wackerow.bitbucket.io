package ClassLibrary.LogicalDataDescription;

/**
 * Definition
 * ============
 * An AttributeRole identifies one or more InstanceVariables as being attributes within a ViewPoint. An AttributeRole is a SimpleCollection of InstanceVariables acting in the AttributeRole.
 * 
 * 
 * Examples
 * ==========
 * A data record with four variables: "PersonId", "Systolic", "Diastolic", "Seated" might have a Viewpoint with 
 * PersonId defined as having the IdentifierRole
 * Systolic defined as having the MeasureRole
 * Diastolic defined as having the MeasureRole
 * Seated defined as having the AttributeRole
 * 
 * PersonId, Systolic, Diastolic, Seated
 * 123,122,20,yes
 * 145,130,90,no
 * 
 * 
 * 
 * Explanatory notes
 * ===================
 * See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint.
 * An InstanceVariable with an AttributeRole assigned might contain data about the conditions under which the MeausreRole InstanceVariables were collected or other paradata.
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AttributeRole extends ViewpointRole {
}