package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.*;

/**
 * Definition
 * ============
 * The assignment of measure, identifier and attribute roles to InstanceVariables
 * 
 * Each of three roles within a UnitDataViewpoint is a SimpleCollection of instance variables. 
 * 
 * Examples
 * ==========
 * Assigning 
 * PatientID the role of identifier
 * systolic the role of measure
 * diastolic the role of measure
 * device the role of attribute
 * time the role of attribute
 * position (sitting, standing) the role of attribute
 * 
 * Explanatory notes
 * ===================
 * Viewpoint is a UnitDataViewpoint to underline that the composition of a UnitDataRecord viewpoint is different from the composition of DataCube viewpoint. The DataCube viewpoint is out of scope currently. 
 * It is sometimes useful to describe a set of variables and their roles within the set. In the blood pressure example above, values for a set of variables are captured in one setting - the taking of a blood pressure. Some of the variables may be the measures of interest, for example the systolic and diastolic pressure. Other variables may take on the role of identifiers, such as the hospitalís patient ID and a government ID such as the U.S. Social Security Number. A third subgroup of variables may serve as attributes. The age of the patient, whether she is sitting or standing, the time of day. The assignment of these three roles, identifier, measure, and attribute, is the function of the Viewpoint.
 * Viewpoints are not fixed attributes of variables. This is why there is a separate Viewpoint class which maps to InstanceVariables.   In the example above, the Viewpoint is within the context of the original measurement. In a reanalysis of the data the roles might change. Time of day might be the measure of interest in a study of hospital practices.
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class UnitDataViewpoint extends AnnotatedIdentifiable {
}