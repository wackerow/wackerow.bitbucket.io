package ClassLibrary.LogicalDataDescription;

import ClassLibrary.Identification.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * The LogicalRecord is a record definition. It is abstract.
 * 
 * The actual realization of record definitions are provided by the UnitDataRecord and the DataCube. UnitDataRecords support two types of record definitions. The first is based on the SimpleCollection it inherits from LogicalRecord. The second type is a StructuredCollection that specializes the LogicalRecord. As a SimpleCollection a UnitDataRecord can, for example, provide table definitions. As a StructuredCollection, UnitDataRecord can define structures comparable to a "struct" in C or a JSON nested object. A UnitDataRecord, then, inherits SimpleCollection from LogicalRecord but adds its own StructuredCollection into the mix. The DataCube brings into play its own StructuredCollection which is distinct from the UnitDataRecord StructuredCollection.The DataCube StructuredCollection is not currently in scope.
 * 
 * The various record definitions -- simple and structured -- when they are actually instantiated with data is described by the PhysicalLayout of a LogicalRecord in the FormatDescription package. The PhysicalLayout places data into DataPoints formed at the direction of a LogicalRecord and its instance variables. At this point a LogicalRecord turn into a "dataset" that hosts unit records, A record definition is not a record. So we conflate metadata and data when we refer to a LogicalRecord as an empty table.
 * 
 * Examples
 * ==========
 * SQL Data Definition Language (DDL) traffics in record definitions. SQL queries on "system tables" discover record definitions.
 * 
 * Explanatory notes
 * ===================
 * In GSIM a DataPoint is a member of a "DataSet" and a UnitDataRecord. Since a DataPoint contains "Datum", this can lead to the conflation of a DataSet and a record definition. In the presence of a DataPoint it is difficult to be clear that a record definition does not have any rows. 
 * 
 * In DDI4 we defer the introduction of DataPoints until PhysicalLayouts are described. In this approach the instance variables that make up a LogicalRecord work downstream where they help to give each DataPoint in a PhysicalRecord its format.
 * 
 * Synonyms
 * ==========
 * Record Type, Data Structure
 * 
 * DDI 3.2 mapping
 * =================
 * l:LogicalRecordType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Data Structure
 */
public abstract class LogicalRecord extends AnnotatedIdentifiable {

	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Specifies contents of Unit Record as Instance Variable values.
	 */
	Collection<InstanceVariableIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}