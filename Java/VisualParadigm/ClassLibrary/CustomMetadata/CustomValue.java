package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * An instance of a  key, value pair for a  particular  key.
 * 
 * Examples
 * ==========
 * OMBBurden,"High"
 * 
 * openEHRBPposition, "upper arm"
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CustomValue extends Identifiable {

	/**
	 * the value associated with a particular key expressed as a string. The ultimate value representation is defined by the corresponding CustomValue.
	 */
	ValueString value;
	/**
	 * The unique String identifying the value
	 */
	string key;

}