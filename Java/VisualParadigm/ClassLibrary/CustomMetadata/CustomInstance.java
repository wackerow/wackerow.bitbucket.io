package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A set of CustomValues to be attached to some object.
 * 
 * Examples
 * ==========
 * A set of OMB required information about a question.
 * 
 * The set of openEHR protocol information to be attached to a blood pressure Capture
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CustomInstance extends Identifiable {

	/**
	 * see Collection
	 */
	Collection<ObjectName> name;
	/**
	 * see Collection
	 */
	InternationalStructuredString purpose;
	/**
	 * see Collection
	 */
	CollectionType type;
	/**
	 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<CustomValueIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}