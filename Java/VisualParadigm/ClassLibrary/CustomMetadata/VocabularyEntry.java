package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * One entry term and its definition in an ordered list comprising a controlled vocabulary.
 * 
 * Examples
 * ==========
 * aunt - The female sibling of a parent
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class VocabularyEntry extends AnnotatedIdentifiable {

	/**
	 * the term being defined
	 */
	InternationalStructuredString entryTerm;
	/**
	 * An explanation (definition) of the value
	 */
	InternationalStructuredString definition;

}