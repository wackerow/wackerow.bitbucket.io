package ClassLibrary.CustomMetadata;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Contains a set of CustomItemRelations which together define the relationships of Custom Structure.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CustomItemRelationStructure extends AnnotatedIdentifiable {

	/**
	 * Intentional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)
	 */
	InternationalStructuredString criteria;
	/**
	 * A display label for the RelationStructure. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString usage;
	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification. .
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Member relationships that comprise the relationship structure
	 */
	Collection<CustomItemRelation> hasMemberRelation;

}