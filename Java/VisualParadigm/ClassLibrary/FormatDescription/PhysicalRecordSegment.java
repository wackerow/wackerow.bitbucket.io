package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A description of each physical storage segment required to completely cover the logical record. A logical record may be stored in one or more segments housed hierarchically in a single file or in separate data files. All logical records have at least one segment.
 * 
 * Examples
 * ==========
 * The file below has four InstanceVariables: PersonId, SegmentId, AgeYr, HeightCm. The data for each person (identified by PersonId) is recorded in two segments (identified by SegmentId), "a" and "b". AgeYr is on physical segment a, and HeightCm is on segment b. These are the same data as described in the UnitSegmentLayout documentation.
 * 1 a  22
 * 1 b 183
 * 2 a 45
 * 2 b 175
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * p:PhysicalRecordSegmentType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class PhysicalRecordSegment extends AnnotatedIdentifiable {

	/**
	 * Use when each physical segment is stored in its own file.
	 */
	string physicalFileName;
	/**
	 * A PhysicalRecordSegment has a sequence of DataPoints. Think of these as the cells in the row of a (spreadsheet) table.
	 */
	Collection<DataPointIndicator> contains;
	/**
	 * The DataPointIndicators for this PhysicalRecordSegment have index values that indicate the order of the DataPoints.
	 */
	boolean isOrdered;

}