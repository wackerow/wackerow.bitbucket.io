package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * The information needed for understanding the physical structure of data coming from a file or other source. A StructureDescription also points to the Data Store it physically represents.
 * 
 * Examples
 * ==========
 * The PhysicalDataProduct is the entry point for information about a file or other source. It includes information about the name of a file, the structure of segments in a file and the layout of segments.
 * 
 * Explanatory notes
 * ===================
 * Multiple styles of structural description are supported: including describing files as unit-record (UnitSegmentLayout) files; describing cubes; and describing event-history (spell) data.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * p:PhysicalDataProductType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class PhysicalDataSet extends AnnotatedIdentifiable {

	/**
	 * Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString overview;
	/**
	 * Use when multiple physical segments are stored in a single file
	 */
	string physicalFileName;
	/**
	 * The number of segments in a Physical Data Product
	 */
	int numberOfSegments;
	/**
	 * A physical description of the data store encompasses a description of its gross record structure. This structure consists of one or more PhysicalRecordSegments and their relationships. Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<PhysicalRecordSegmentIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;
	/**
	 * Whether the dataset is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * An explanation of the purpose of the physical dataset
	 */
	InternationalStructuredString purpose;

}