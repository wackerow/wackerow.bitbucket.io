package ClassLibrary.FormatDescription;

/**
 * Definition
 * ============
 * UnitSegmentLayout supports the description of unit-record ("wide") data sets, where each row in the data set provides the same group of values for variables all relating to a single unit. Each logical column will contain data relating to the values for a single variable. 
 * 
 * Examples
 * ==========
 * A simple spreadsheet. Commonly the first row of the table will contain variable names or descriptions.
 * 
 * The following csv file has a rectangular layout and would import into a simple table in a spreadsheet:
 * PersonId,AgeYr,HeightCm
 * 1,22,183
 * 2,45,175
 * 
 * 
 * Explanatory notes
 * ===================
 * This is the classic rectangular data table used by most statistical packages, with rows/cases/observations and columns/variables/measurements. Each cell (DataPoint) in the table is the intersection of a Unit (row) and an InstanceVariable. 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class UnitSegmentLayout extends PhysicalSegmentLayout {
}