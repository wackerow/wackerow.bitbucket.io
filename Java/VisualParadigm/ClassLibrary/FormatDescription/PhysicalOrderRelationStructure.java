package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * PhysicalStructureOrder orders thePhysicalRecordSegments  which map to a LogicalRecord.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * The same LogicalRecordLayout may be the sourceMember in several adjacency lists. This can happen when PhysicalRecordSegments are also population specific. In this instance each adjacency list associated with a LogicalRecordLayout is associated with a different population.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class PhysicalOrderRelationStructure extends Identifiable {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Member relations that comprise the relation structure
	 */
	Collection<PhysicalRecordSegmentRelation> hasMemberRelation;

}