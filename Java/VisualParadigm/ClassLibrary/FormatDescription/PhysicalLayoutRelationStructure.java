package ClassLibrary.FormatDescription;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * A realization of RelationStructure that is used to describe the sequence of Value Mappings in a Physical Layout. This can be more complex than a simple sequence. 
 * 
 * Examples
 * ==========
 * The W3C Tabular Data on the Web specification allows for a list datatype. In the example below there are three top level InstanceVariables
 * PersonID � the person identifier
 * AgeYr � age in year
 * BpSys_Dia � blood pressure (a list containing Systolic and Diastolic values)
 * 
 * There are two variables at a secondary level of the hierarchy
 * Systolic � the systolic pressure
 * Diastolic � the diastolic pressure
 * 
 * The delimited file below uses the comma to separate "columns" and forward slash to separate elements of a blood pressure list.
 * 
 * PersonID, AgeYr, BpSys_Dia
 * 1,22,119/67
 * 2,68,122/70
 * 
 * The PhysicalRelationStructure in this case would describe a BpSys_Dia  list variable as containing an ordered sequence of the Systolic and Diastolic InstanceVariables.
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class PhysicalLayoutRelationStructure extends Identifiable {

	/**
	 * Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)
	 */
	InternationalStructuredString criteria;
	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Specifics of relations between InstanceVariables in the PhysicalLayout
	 */
	Collection<ValueMappingRelation> hasMemberRelation;

}