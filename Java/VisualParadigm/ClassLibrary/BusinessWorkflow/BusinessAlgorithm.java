package ClassLibrary.BusinessWorkflow;

import ClassLibrary.SimpleMethodologyOverview.*;

/**
 * Definition
 * ============
 * A Business Algorithm is used to express the generalized function of the Business Process.
 * The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.
 * 
 * Examples
 * ==========
 * There are several well-established algorithms for performing data anonymization including "local suppression", "global attribute generalization" and "k-Anonymization with suppression" which is a specific combination of the two. An anonymizing business process might implement one of these algorithms.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class BusinessAlgorithm extends AlgorithmOverview {
}