package ClassLibrary.Conceptual;

import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * The use of a Represented Variable within a Data Set.  The InstanceVariable describes actual instances of data that have been collected. 
 * 
 * Examples
 * ==========
 * 1) Gender: Dan Gillman has gender <m, male>, Arofan Gregory has gender<m, male>, etc. 
 * 2) Number of employees: Microsoft has 90,000 employees; IBM has 433,000 employees, etc. 
 * 3) Endowment: Johns Hopkins has endowment of <3, $1,000,000 and above>, 
 * Yale has endowment of <3, $1,000,000 and above>, etc.
 * 
 * Two InstanceVariables of a person's height reference the same RepresentedVariable. This indicates that they are intended to: be measured with the same unit of measurement, have the same intended data type, have the same SubstantativeValueDomain, use a SentinelValueDomain drawn from the same set of SentinelValueDomains, have the same sentinel (missing value) concepts, and draw their Population from the same Universe. In other words, the two InstanceVariables should be comparable.
 * 
 * Explanatory notes
 * ===================
 * The InstanceVariable class inherits all of the properties and relationships of the RepresentedVariable (RV) class and, in turn, the ConceptualVariable (CV) class. This means that an InstanceVariable can be completely populated without the need to create an associated RepresentedVariable or ConceptualVariable. If, however, a user wishes to indicate that a particular InstanceVariable is patterned after a particular RepresentedVariable or a particular ConceptualVariable that may be indicated by including a relationship to the RV and or CV. Including these references is an important method of indicating that multiple InstanceVariables have the same representation, measure the same concept, and are drawn from the same Universe. If two InstanceVariables of a person's height reference the same RepresentedVariable. This indicates that they are intended to: be measured with the same unit of measurement, have the same intended data type, have the same SubstantativeValueDomain, use a SentinelValueDomain drawn from the same set of SentinelValueDomains, have the same sentinel (missing value) concepts, and draw their Population from the same Universe. In other words, the two InstanceVariables should be comparable. 
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Instance Variable
 */
public class InstanceVariable extends RepresentedVariable {

	/**
	 * An Instance Variable can take different roles, e.g. Identifier, Measure and Attribute. Note that DataStructure takes care of the ordering of Identifiers.
	 */
	InternationalStructuredString variableRole;
	/**
	 * The data type of this variable. Supports the optional use of an external controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry physicalDataType;
	/**
	 * This should match a platformType of the RepresentedVariable on which the InstanceVariable is based (if one exists). It is drawn from the same external controlled vocabulary used by the RepreentedVariable's platformType.  A sample list for the external controlled vocabulary is:  A sample list would be:  � BlankString - A Blank string indicates missing. Comparison is based on lexical order.  � EmptyString - An empty string indicates missing. Use in comparisons returns missing.   � Rstyle - Codes drawn from  NA and the IEEE 754 values of  NaN  -Inf   +Inf.   Comparisons return NA.   � SASNumeric - codes drawn from . ._ .A .B .C .D .E .F .G .H .I .J .K .L .M .N .O .P .Q .R .S .T .U .V .W .X .Y .Z    Sentinel code treated as less than any substantive value   � SPSSstyle - System missing (a dot) a set of individual values drawn from the same datatype as the SubstantiveValueDomain, and a range of values  drawn from the same datatype as the SubstantiveValueDomain.  Comparisons return system missing.  Some functions substitute with valid values (e.g. SUM replaces missing values with 0)   � StataNumeric - codes drawn from . ._ .A .B .C .D .E .F .G .H .I .J .K .L .M .N .O .P .Q .R .S .T .U .V .W .X .Y .Z  Sentinel code treated as greater than any substantive value  � Unrestricted - No restrictions on codes for sentinel values. Use in comparisons is indeterminate.
	 */
	ExternalControlledVocabularyEntry platformType;

}