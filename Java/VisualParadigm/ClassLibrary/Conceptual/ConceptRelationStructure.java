package ClassLibrary.Conceptual;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * Relation structure of concepts within a collection. Allows for the specification of complex relationships among concepts.
 * 
 * Examples
 * ==========
 * A concept of vacation might be described as having sub-types of beach vacation and mountain vacation.
 * 
 * Explanatory notes
 * ===================
 * The ConceptRelationStructure employs a set of ConceptRelations to describe the relationship among concepts. Each ConceptRelation is a one to many description of connections between concepts. Together they can describe relationships as complex as a hierarchy or even a complete cyclical network as in a concept map.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ConceptRelationStructure extends Identifiable {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Restricted to ConceptRelation. ConceptRelations making up the relation structure.
	 */
	Collection<ConceptRelation> hasMemberRelation;

}