package ClassLibrary.Conceptual;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * RelationStructure for use with any mixture of Variables in the Variable Cascade (Conceptual, Represented, Instance).
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * A VariableCollection can be described as a simple list, or as having a more complex structure. All of the "administrative" variables for a study might be described as just a simple list without a VariableRelationStructure. If, however, the administrative variables needed to be described in subgroups, a VariableRelationStructure could be employed.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class VariableRelationStructure extends Identifiable {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification.
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Member relations that comprise the relation structure
	 */
	Collection<VariableRelation> hasMemberRelation;

}