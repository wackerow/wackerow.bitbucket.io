package ClassLibrary.Conceptual;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Set of specific units (people, entities, objects, events) with specification of time and geography.
 * 
 * Examples
 * ==========
 * 1. Canadian adult persons residing in Canada on 13 November 1956
 * 2. US computer companies at the end of 2012  
 * 3. Universities in Denmark 1 January 2011.
 * 
 * Explanatory notes
 * ===================
 * Population is the most specific in the conceptually narrowing hierarchy of UnitType, Universe and Population. Several Populations having differing time and or geography may specialize the same Universe.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Population
 */
public class Population extends Concept {

	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * A short natural language account of the characteristics of the object.
	 */
	InternationalStructuredString descriptiveText;
	/**
	 * The time period associated with the Population
	 */
	Collection<DateRange> timePeriodOfPopulation;

}