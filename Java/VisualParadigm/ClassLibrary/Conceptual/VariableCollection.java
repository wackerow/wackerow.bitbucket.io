package ClassLibrary.Conceptual;

import ClassLibrary.Identification.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A collection (group) of any type of Variable within the Variable Cascade (Conceptual, Represented, Instance) for purposes of management, conceptualization or anything other than organizing a logical record or physical layout.
 * 
 * Examples
 * ==========
 * Variables within a specific section of a study, Variables related to a specific subject or keyword. Variables at a specified level of development or review.
 * 
 * Explanatory notes
 * ===================
 * A simple ordered or unordered list of variables can be described via a set of VariableIndicator parameters. An optional VariableRelationStructure can describe a more complex structure for the collection. We might, for example, use the VariableRelationStructure to group variables by content within a Study or across a StudySeries.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * l:VariableGroupType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class VariableCollection extends AnnotatedIdentifiable {

	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString usage;
	/**
	 * A semantic term defining the factor used for defining this group
	 */
	ExternalControlledVocabularyEntry groupingSemantic;
	/**
	 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<VariableIndicator> contains;
	/**
	 * If members are ordered set to true, if unordered set to false.
	 */
	boolean isOrdered;

}