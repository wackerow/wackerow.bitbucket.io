package ClassLibrary.Conceptual;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Relationship between 2 or more ConceptSystems described through mapping similarity relationships between their member Concepts.
 * 
 * Examples
 * ==========
 * Correspondence between the concepts used to define the populations in the censuses of two countries with similarity mapping of Concepts "Resident Population", "Labor Force", "Housing Unit", etc. 
 * 
 * Explanatory notes
 * ===================
 * Describes correspondence with one or more Maps which identify a source and target concept and defines their commonality and difference using descriptive text and controlled vocabularies.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ConceptSystemCorrespondence extends AnnotatedIdentifiable {

	/**
	 * A display label for the CollectionCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString usage;
	/**
	 * Realization of correspondence in Comparison for mapping similar concepts.
	 */
	Collection<Map> correspondence;

}