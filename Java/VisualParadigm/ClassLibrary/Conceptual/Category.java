package ClassLibrary.Conceptual;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A Concept whose role is to define and measure a characteristic.
 * 
 * Examples
 * ==========
 * Self-identified as "Male".  "Extremely Satisfied" as a response category.
 * 
 * Explanatory notes
 * ===================
 * The Category is a Concept. It can have multiple name and display label properties as well as a definition and some descriptive text. As a "Signified" class there can be one or more "Sign" classes (e.g. a Code) that denotes it with some representation. The relationship is from the Code to the Category.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * l:CategoryType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Category extends Concept {

	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * A short natural language account of the characteristics of the object.
	 */
	InternationalStructuredString descriptiveText;

}