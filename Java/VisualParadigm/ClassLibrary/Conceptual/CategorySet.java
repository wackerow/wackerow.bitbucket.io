package ClassLibrary.Conceptual;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Specialization of a Concept System focusing on sets of categories used as specifications for a generalized concept. Can be used directly by questions to express a set of categories for a response domain or as the related source for a CodeList or StatisticalClassification
 * 
 * Examples
 * ==========
 * "Male" and "Female" categories in a CategorySet named "Gender"
 * 
 * A CategorySet for an occupational classification system like ISCO-08 could use a ClassificationRelationStructure to describe a hierarchy (Chief Executives and Administrative and Commercial Managers as subtypes of Managers) without an associated Code List.
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CategorySet extends ConceptSystem {

	/**
	 * MemberIndicator Allows for the identification of the member and optionally provides an index for the member within an ordered array
	 */
	Collection<CategoryIndicator> contains;

}