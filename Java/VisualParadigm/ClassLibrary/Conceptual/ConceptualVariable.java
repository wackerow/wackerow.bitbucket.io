package ClassLibrary.Conceptual;

import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * The use of a Concept as a characteristic of a UnitType intended to be measured
 * 
 * Examples
 * ==========
 * 1. Sex of person
 * 2. Number of employees 
 * 3. Value of endowment
 * 
 * Explanatory notes
 * ===================
 * Note that DDI varies from GSIM in the use of a UnitType rather than a Universe. "The use of a Concept as a characteristic of a Universe intended to be measured" [GSIM 1.1]
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * c:ConceptualVariableType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Variable
 */
public class ConceptualVariable extends Concept {

	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * A short natural language account of the characteristics of the object.
	 */
	InternationalStructuredString descriptiveText;

}