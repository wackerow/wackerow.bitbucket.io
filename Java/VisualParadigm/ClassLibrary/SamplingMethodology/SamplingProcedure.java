package ClassLibrary.SamplingMethodology;

import ClassLibrary.SimpleMethodologyOverview.*;

/**
 * Definition
 * ============
 * The Sampling Procedure describes the population being sampled, the sampling frame and the sampling plan including steps and sub steps by referencing the universe, sample frames and sampling plans described in schemes. Target sample sizes for each stage can be noted as well as the process for determining sample size, the date of the sample and the person or organization responsible for drawing the sample.
 * 
 * Examples
 * ==========
 * Census of Population and Housing, 1980 Summary Tape File 4, chapter on Technical Information. Overview is the Introduction generally describing the sample; there is a sample design section which provides a more detailed overview of the design. Additional external documentation provides detailed processes for sample adjustments needed for small area estimations, sample frames etc.
 * 
 * Explanatory notes
 * ===================
 * This is a summary of the methodology of sampling providing both an overview of purpose and use of a particular sampling approach and optionally providing specific details regarding the algorithm, design, and process of selecting a sample.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SamplingProcedure extends MethodologyOverview {
}