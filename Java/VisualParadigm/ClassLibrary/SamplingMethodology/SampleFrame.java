package ClassLibrary.SamplingMethodology;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * A set of information used to identify a sample population within a larger population. The sample frame will contain identifying information for individual units within the frame as well as characteristics which will support analysis and the division of the frame population into sub-groups.
 * 
 * Examples
 * ==========
 * A listing of all business enterprises by their primary office address with information on their industry classification, work staff size, and production costs. A telephone directory. An address list of housing units.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SampleFrame extends AnnotatedIdentifiable {

	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Explanation of the ways in which the sample frame is to be employed. Supports use of multiple languages and structured text.
	 */
	InternationalStructuredString usage;
	/**
	 * An explanation of the intent of the sample frame. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * An explanation of any limitations of the sample frame. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString limitations;
	/**
	 * A description of how the sample frame is updated and maintained. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString updateProcedures;
	/**
	 * The reference period for the sample frame.
	 */
	DateRange referencePeriod;
	/**
	 * The period for which the current version is valid
	 */
	DateRange validPeriod;

}