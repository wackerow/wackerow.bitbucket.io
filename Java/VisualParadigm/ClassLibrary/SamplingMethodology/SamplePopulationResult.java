package ClassLibrary.SamplingMethodology;

import ClassLibrary.Methodologies.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * The Sample Population is the result of a sampling process. It is the subset of the population selected from the Sample Frame or Frames that will be used as respondents to represent the target population. Sample Populations may occur at a number of stages in the sampling process and serve as input to a subsequent sampling stage. The Sample Population should be validated against the intended sample population as described in the goal of the design and/or process.
 * 
 * Examples
 * ==========
 * A list of phone numbers representing a random sample from a telephone book. The Counties selected from within a State from which Households will be selected using another sampling method.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SamplePopulationResult extends Result {

	/**
	 * The overall size of the sample
	 */
	int sizeOfSample;
	/**
	 * The name or number of the specific strata if the sample frame has been stratified prior to sampling within the specified stage
	 */
	string strataName;
	/**
	 * The date the sample was selected from the sample frame
	 */
	Date sampleDate;

}