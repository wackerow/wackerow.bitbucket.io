package ClassLibrary.SamplingMethodology;

import ClassLibrary.SimpleMethodologyOverview.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Generic description of a sampling algorithm expressed by a Sampling Design and implemented by a Sampling Process. May include common algebraic formula or statistical package instructions (use of specific selection models).
 * 
 * Examples
 * ==========
 * Random Sample; Stratified Random Sample
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SamplingAlgorithm extends AlgorithmOverview {

	/**
	 * The algorithm expressed as algebraic or computer code.
	 */
	CommandCode codifiedExpressionOfAlgorithm;

}