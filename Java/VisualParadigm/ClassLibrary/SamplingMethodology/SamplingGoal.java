package ClassLibrary.SamplingMethodology;

import ClassLibrary.Methodologies.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Provides general description and specific targets for sample sizes including sub-populations.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SamplingGoal extends Goal {

	/**
	 * The goal for the overall size of the sample expressed as an integer.
	 */
	int overallTargetSampleSize;
	/**
	 * The goal for the overall size of the sample expressed as a decimal (i.e. 70% entered as .7).
	 */
	Real overallTargetSamplePercent;
	/**
	 * Repeat for primary and secondary target samples. Supports identification of size, percent, unit type, and universe
	 */
	Collection<TargetSample> targetSampleSize;

}