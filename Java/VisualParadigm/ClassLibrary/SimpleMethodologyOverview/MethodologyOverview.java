package ClassLibrary.SimpleMethodologyOverview;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;

/**
 * Definition
 * ============
 * High level, descriptive, human informative methodology statement used to describe the overall methodology, identify related design, algorithm, and process information. A methodology is normally informed by earlier research and clarifies how earlier research methods were incorporated into the current work. 
 * 
 * 
 * 
 * Examples
 * ==========
 * The target of the relationship from Study: "hasMethodology"
 * 
 * 
 * Explanatory notes
 * ===================
 * This would most commonly be used in a Codebook along with an AlgorithmOverview and a DesignOverview. Note that Process may be described in more detail than a high level overview.
 * Note that the algorithm may be implemented by multiple processes which are not limited to any specific type of Process . This can be constrained by the inclusion of only specific realizations of Process within a Functional View. Note that this MethodologyOverview can be used as a collective description of specific methodologies used by a Study or other broad set of metadata.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class MethodologyOverview extends AnnotatedIdentifiable {

	/**
	 * A term describing the subject of the Methodology. Supports the use of a controlled vocabulary
	 */
	ExternalControlledVocabularyEntry subjectOfMethodology;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided, provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString usage;
	/**
	 * Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString rationale;
	/**
	 * Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString overview;

}