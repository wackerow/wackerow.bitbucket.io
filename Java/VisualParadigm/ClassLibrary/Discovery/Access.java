package ClassLibrary.Discovery;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;

/**
 * Definition
 * ============
 * Describes access to the annotated object. This item includes a confidentiality statement, descriptions of the access permissions required, restrictions to access, citation requirements, depositor requirements, conditions for access, a disclaimer, any time limits for access restrictions, and contact information regarding access.
 * 
 * Examples
 * ==========
 * A proprietary InstanceQuestion might have specific access restrictions.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:AccessType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Access extends AnnotatedIdentifiable {

	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text
	 */
	InternationalStructuredString purpose;
	/**
	 * A statement regarding the confidentiality of the related data or metadata.
	 */
	InternationalStructuredString confidentialityStatement;
	/**
	 * A link to a form used to provide access to the data or metadata including a statement of the purpose of the form.
	 */
	Collection<Form> accessPermission;
	/**
	 * A statement regarding restrictions to access. May be expressed in multiple languages and supports the use of structured content.
	 */
	InternationalStructuredString restrictions;
	/**
	 * A statement regarding the citation requirement. May be expressed in multiple languages and supports the use of structured content.
	 */
	InternationalStructuredString citationRequirement;
	/**
	 * A statement regarding depositor requirements. May be expressed in multiple languages and supports the use of structured content.
	 */
	InternationalStructuredString depositRequirement;
	/**
	 * A statement regarding conditions for access. May be expressed in multiple languages and supports the use of structured content.
	 */
	InternationalStructuredString accessConditions;
	/**
	 * A disclaimer regarding the liability of the data producers or providers. May be expressed in multiple languages and supports the use of structured content.
	 */
	InternationalStructuredString disclaimer;
	/**
	 * The agent to contact regarding access including the role of the agent.
	 */
	Collection<AgentAssociation> contactAgent;
	/**
	 * The date range or start date of the access description.
	 */
	DateRange validDates;

}