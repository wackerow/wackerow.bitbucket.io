package ClassLibrary.Discovery;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A description of spatial coverage (geographic coverage) of the annotated object. Spatial coverage is described using a number of objects that support searching by a wide range of systems (geospatial coordinates, geographic classification systems, and general systems using dcterms:spatial).
 * 
 * Examples
 * ==========
 * A country, a neighborhood, the inside of a polygon on the surface of the earth, along a street, at a particular intersection, or perhaps even in a certain orbit around the planet Mars. For a data set within a study this may be used to define the geographic restriction of the data set within the geographic coverage of the study (eg. The study may cover all of Sweden but the spatial coverage of the data set is Stockholm). 
 * 
 * Explanatory notes
 * ===================
 * Different systems support different approaches to descriptions of geographic or spatial coverage. Dublin Core has a descriptive text field that is the equivalent of SpatialCoverage/description. Geographers need to understand the spatial objects available (spatialObject, highest and lowest level geography, geographic units and unit types) so they know the building blocks they have to create a map. Spatial search engines do a first pass search by a simple bounding box (the north and south latitudes and east and west longitudes that define the spatial coverage area). Spatial Coverage provides basic support for all of these uses.
 * 
 * Synonyms
 * ==========
 * r:SpatialCoverage, place
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SpatialCoverage extends AnnotatedIdentifiable {

	/**
	 * A textual description of the spatial coverage to support general searches.
	 */
	InternationalStructuredString description;
	/**
	 * Supports the use of a standardized code such as ISO 3166-1,  the Getty Thesaurus of Geographic Names, FIPS-5, etc.
	 */
	Collection<ExternalControlledVocabularyEntry> spatialAreaCode;
	/**
	 * Indicates the most discrete spatial object type identified for a single case. Note that data can be collected at a geographic point (address) and reported as such in a protected file, and then aggregated to a polygon for a public file.
	 */
	SpatialObjectType spatialObject;

}