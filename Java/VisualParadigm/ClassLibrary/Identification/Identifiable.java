package ClassLibrary.Identification;

import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Used to identify objects for purposes of internal and/or external referencing. Elements of this type are versioned and provide administrative metadata properties. Use for First Order Classes whose content does not need to be discoverable in its own right but needs to be related to multiple classes. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Identifiable {

	/**
	 * This is the registered agency code with optional sub-agencies separated by dots. For example, diw.soep, ucl.qss, abs.essg.
	 */
	string agency;
	/**
	 * The ID of the object. This must conform to the allowed structure of the DDI Identifier and must be unique within the Agency.
	 */
	string id;
	/**
	 * The version number of the object. The version number is incremented whenever the non-administrative metadata contained by the object changes.
	 */
	string version;
	/**
	 * Contributor who has the ownership and responsibility for the current version.
	 */
	string versionResponsibility;
	/**
	 * The reason for making this version of the object.
	 */
	string versionRationale;
	/**
	 * The date and time the object was changed. Supports standard ISO date and datetime formats.
	 */
	IsoDateType versionDate;
	/**
	 * Default value is false. Usually the combination of agency and id (ignoring different versions) is unique. If isUniversallyUnique is set to true, it indicates that the id itself is universally unique (unique across systems and/or agencies) and therefore the agency part is not required to ensure uniqueness.
	 */
	boolean isUniversallyUnique;
	/**
	 * Default value is false. Usually the content of the current version is allowed to change, for example as the contributor is working on the object contents. However, when isPersistent is true, it indicates the there will be no more changes to the current version.
	 */
	boolean isPersistent;
	/**
	 * This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id. For example, localId could be a variable name in a dataset.
	 */
	Collection<LocalIdFormat> localId;
	/**
	 * The object/version that this object version is based on.
	 */
	BasedOnObjectInformation basedOnObject;

}