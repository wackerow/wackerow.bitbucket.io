package ClassLibrary.GeographicClassification;

import ClassLibrary.Representations.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Describes the classification of specific geographic units into a classification system. As a subtype of Code List it can be used directly for the description of a value domain.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class GeographicUnitClassification extends CodeList {

	/**
	 * Define the code and Unit Type that is a member of the Geographic Unit Classification. They may be unordered or ordered and assigned to a specific level.
	 */
	Collection<GeographicUnitIndicator> contains;
	/**
	 * Date the Classification was released
	 */
	Date releaseDate;
	/**
	 * The date the classification enters production use and the date on which the Classification was superseded by a successor version or otherwise ceased to be valid. (Source: GSIM Statistical Classification)
	 */
	DateRange validDates;
	/**
	 * Indicates if the Classification is currently valid.
	 */
	boolean isCurrent;
	/**
	 * Indicates if the Classification is a floating classification. In a floating classification, a validity period should be defined for all Classification Items which will allow the display of the item structure and content at different points of time. (Source: GSIM StatisticalClassification/Floating)
	 */
	boolean isFloating;
	/**
	 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
	 */
	Collection<LabelForDisplay> displayLabel;
	/**
	 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString usage;

}