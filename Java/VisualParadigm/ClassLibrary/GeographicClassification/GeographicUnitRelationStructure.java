package ClassLibrary.GeographicClassification;

import ClassLibrary.Identification.*;
import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;

/**
 * Definition
 * ============
 * Defines the relationships between Geographic Unit Types in a collection. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class GeographicUnitRelationStructure extends Identifiable {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship context for the relationship using an External Controlled Vocabulary. Examples might include "parent-child", or "immediate supervisee"
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Controlled Vocabulary to specify whether the relation is total, partial or unknown.
	 */
	TotalityType totality;
	/**
	 * MemberRelation used to define the relationship of members within the collection
	 */
	Collection<GeographicUnitRelation> hasMemberRelation;

}