package ClassLibrary.GeographicClassification;

import ClassLibrary.Conceptual.*;
import ClassLibrary.ComplexDataTypes.*;
import java.util.*;

/**
 * Definition
 * ============
 * A specific geographic unit of a defined Unit Type. A geographic unit may change its name, composition, or geographic extent over time. This may be tracked by versioning the content of the geographic unit. Normally a new version of a geographic unit would have a geographic (spatial) overlap with its previous version (a city annexing new area). A geographic unit ends when it is no longer a unit of the same unit type.
 * 
 * Examples
 * ==========
 * The State of Minnesota from 1858 to date where the Unit Type is a State as defined by the United States Census Bureau. It has a geographic extent and supersedes a portion of the Territory of Minnesota and a portion of the Territory of Wisconsin. Minnesota territory had both a broader geographic extent, different Unit Type, and earlier time period (1849-1858) as did the Territory of Wisconsin. 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:GeographicLocationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class GeographicUnit extends Unit {

	/**
	 * The period for which the Geographic Unit is valid. Note that the geographic extent, name, or composition change within the valid period of this unit.
	 */
	DateRange geographicTime;
	/**
	 * The Geographic Unit supercedes (follows immediately in time) the described geographic unit. Repeat for multiple geographic units.
	 */
	Collection<SpatialRelationship> supercedes;
	/**
	 * The Geographic Unit is superceded by (precedes immediately in time) the described geographic unit. Repeat for multiple geographic units.
	 */
	Collection<SpatialRelationship> precedes;

}