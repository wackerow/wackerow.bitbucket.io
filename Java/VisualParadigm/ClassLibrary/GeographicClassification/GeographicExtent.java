package ClassLibrary.GeographicClassification;

import ClassLibrary.Identification.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Defines the extent of a geographic unit for a specified period of time using Bounding Box, Inclusive and Exclusive Polygons, and an area coverage that notes a type of coverage (land, water, etc.) and measurement for the coverage. The same geographic extent may be used by multiple versions of a single geographic version or by different geographic units occupying the same spatial area.
 * 
 * Examples
 * ==========
 * Bounding box for Burkino Faso: (N) 15.082773; (S) 9.395691; (E) 2.397927; (W) -5.520837.  Minnesota Land area: 206207.099 sq K, Water area: 18974.589 sq K 
 * 
 * Explanatory notes
 * ===================
 * Clarifies the source of a change in terms of footprint of an area as opposed to a name or coding change. 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:GeographicBoundaryType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class GeographicExtent extends AnnotatedIdentifiable {

	/**
	 * A description of the boundaries of the polygon either in-line or by a reference to an external file containing the boundaries. Repeatable to describe non-contiguous areas such as islands or Native American Reservations in some parts of the United States.
	 */
	Collection<Polygon> boundingPolygon;
	/**
	 * A description of a the boundaries of a polygon internal to the bounding polygon which should be excluded. For example, for the bounding polygon describing the State of Brandenburg in Germany, the Excluding Polygon would describe the boundary of Berlin, creating hole within Brandenburg which is occupied by Berlin.
	 */
	Collection<Polygon> excludingPolygon;
	/**
	 * A time for which the polygon is an accurate description of the area. This may be a range (without an end date if currently still valid) or a single date when the shape was know to be valid if a range is not available.
	 */
	DateRange geographicTime;
	/**
	 * Means of describing the area covered by the geographic extent either in its entirety (total area) or specific subsets (land, water, urban, rural, etc.) providing a definition and measurement.
	 */
	Collection<AreaCoverage> hasAreaCoverage;
	/**
	 * Identifies the centroid of a polygon as a specific point
	 */
	SpatialPoint hasCentroid;
	/**
	 * The geographic extent is a single point.
	 */
	SpatialPoint locationPoint;
	/**
	 * The geographic extent is a single line
	 */
	SpatialLine isSpatialLine;

}