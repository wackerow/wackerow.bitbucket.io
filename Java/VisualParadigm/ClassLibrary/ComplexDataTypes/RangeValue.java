package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Describes a bounding value of a string.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:RangeValueType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class RangeValue extends ValueString {

	/**
	 * Set to "true" if the value is included in the range.
	 */
	boolean included;

}