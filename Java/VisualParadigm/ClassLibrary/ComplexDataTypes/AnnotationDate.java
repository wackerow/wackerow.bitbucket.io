package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * A generic date type for use in Annotation which provides the standard date structure plus a property to define the date type (Publication date,  Accepted date, Copyrighted date, Submitted date, etc.). 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * Equivalent of http://purl.org/dc/elements/1.1/date where the type of date may identify the Dublin Core refinement term.
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AnnotationDate extends Date {

	/**
	 * Use to specify the type of date. This may reflect the refinements of dc:date such as dateAccepted, dateCopyrighted, dateSubmitted, etc.
	 */
	Collection<ExternalControlledVocabularyEntry> typeOfDate;

}