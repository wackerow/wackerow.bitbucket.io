package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.*;

/**
 * Definition
 * ============
 * Allows for unstructured content which may be an entry from an externally maintained controlled vocabulary.If the content is from a controlled vocabulary provide the code value of the entry, as well as a reference to the controlled vocabulary from which the value is taken. Provide as many of the identifying attributes as needed to adequately identify the controlled vocabulary. Note that DDI has published a number of controlled vocabularies applicable to several locations using the ExternalControlledVocabularyEntry structure. If the code portion of the controlled vocabulary entry is language specific (i.e. a list of keywords or subject headings) use language to specify that language. In most cases the code portion of an entry is not language specific although the description and usage may be managed in one or more languages. Use of shared controlled vocabularies helps support interoperability and machine actionability.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CodeValueType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ExternalControlledVocabularyEntry {

	/**
	 * The ID of the code list (controlled vocabulary) that the content was taken from.
	 */
	string controlledVocabularyID;
	/**
	 * The name of the code list.
	 */
	string controlledVocabularyName;
	/**
	 * The name of the agency maintaining the code list.
	 */
	string controlledVocabularyAgencyName;
	/**
	 * The version number of the code list (default is 1.0).
	 */
	string controlledVocabularyVersionID;
	/**
	 * If the value of the string is "Other" or the equivalent from the codelist, this attribute can provide a more specific value not found in the codelist.
	 */
	string otherValue;
	/**
	 * The URN or URL of the controlled vocabulary
	 */
	anyUri uri;
	/**
	 * The value of the entry of the controlled vocabulary. If no controlled vocabulary is used the term is entered here and none of the properties defining the controlled vocabulary location are used.
	 */
	string content;
	/**
	 * Language of the content value if applicable
	 */
	language language;

}