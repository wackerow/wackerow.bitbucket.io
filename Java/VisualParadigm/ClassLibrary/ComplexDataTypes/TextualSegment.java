package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:TextualType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class TextualSegment {

	/**
	 * Specification of the line and offset for the beginning and end of the segment.
	 */
	LineParameter lineParamenter;
	/**
	 * Specification of the character offset for the beginning and end of the segment.
	 */
	CharacterOffset characterParameter;

}