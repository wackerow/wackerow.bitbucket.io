package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * A structured display label. Label provides display content of a fully human readable display for the identification of the object. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:LabelType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class LabelForDisplay extends InternationalStructuredString {

	/**
	 * Indicate the locality specification for content that is specific to a geographic area. May be a country code, sub-country code, or area name.
	 */
	ExternalControlledVocabularyEntry locationVariant;
	/**
	 * Allows for the specification of a starting date and ending date for the period that this label is valid.
	 */
	DateRange validDates;
	/**
	 * A positive integer indicating the maximum number of characters in the label.
	 */
	int maxLength;

}