package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Relation of Vocabulary Entries as defined using RelationSpecification 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class VocabularyEntryRelation {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of its totality using an enumeration list.
	 */
	TotalityType totality;

}