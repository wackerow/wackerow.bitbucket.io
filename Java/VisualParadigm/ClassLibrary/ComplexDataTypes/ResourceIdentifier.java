package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * Provides a means of identifying a related resource and provides the typeOfRelationship.  
 * 
 * 
 * 
 * Examples
 * ==========
 * Standard usage may include: describesDate, isDescribedBy,  isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires, etc.
 * 
 * Explanatory notes
 * ===================
 * Makes use of a controlled vocabulary for typing the relationship.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ResourceIdentifier extends InternationalIdentifier {

	/**
	 * The type of relationship between the annotated object and the related resource. Standard usage may include: describesDate, isDescribedBy,  isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires, etc.
	 */
	Collection<ExternalControlledVocabularyEntry> typeOfRelatedResource;

}