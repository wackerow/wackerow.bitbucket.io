package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * Structures a numeric range. Low and High values are designated. The structure identifies Low values that should be treated as bottom coded (Stated value and bellow, High values that should be treated as top coded (stated value and higher), and provides a regular expression to further define the valid content of the range.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:NumberRangeType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class NumberRange {

	/**
	 * A display label for the number range. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
	 */
	Collection<LabelForDisplay> label;
	/**
	 * High or top value on the range expressed as an xs:decimal within an isInclusive Boolean flag.
	 */
	NumberRangeValue highCode;
	/**
	 * High or top value on the range expressed as an xs:double within an isInclusive Boolean flag.
	 */
	DoubleNumberRangeValue highCodeDouble;
	/**
	 * High or top value on the range expressed as an xs:decimal within an isInclusive Boolean flag.
	 */
	NumberRangeValue lowCode;
	/**
	 * Low or bottom value on the range expressed as an xs:double within an isInclusive Boolean flag.
	 */
	DoubleNumberRangeValue lowCodeDouble;
	/**
	 * Regular expression defining the allowed syntax of the number.
	 */
	string regExp;
	/**
	 * Default value is False. The High or top code represents the value expressed and anything higher.
	 */
	boolean isTopCoded;
	/**
	 * Default value is False. The Low or bottom code represents the value expressed and anything lower.
	 */
	boolean isBottomCoded;

}