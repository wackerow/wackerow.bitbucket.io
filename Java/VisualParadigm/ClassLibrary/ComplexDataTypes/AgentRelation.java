package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Used to define the relation agents in a hierarchical structure
 * 
 * Examples
 * ==========
 * An Organization (source/parent) employing and Individual (target/child); An Individual (source/parent) supervisory to an Individual (target/child); An Organization (source/parent) overseeing a project (Organization) (target/child). Select appropriate relationship using the controlled vocabulary available through hasRelationshipSpecification. 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AgentRelation {

	/**
	 * The effective dates of the relation. A structured DateRange with start and end Date (both with the structure of Date and supporting the use of ISO and non-ISO date structures); Use to relate a period with a start and end date.
	 */
	DateRange effectiveDates;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;

}