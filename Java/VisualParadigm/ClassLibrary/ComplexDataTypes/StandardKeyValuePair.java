package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * A basic data representation for computing systems and applications expressed as a tuple (attribute key, value). Attribute keys may or may not be unique.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:StandardKeyValuePairType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class StandardKeyValuePair {

	/**
	 * This key (sometimes referred to as a name) expressed as a string. Supports the use of an external controlled vocabulary which is the recommended approach.
	 */
	ExternalControlledVocabularyEntry attributeKey;
	/**
	 * The value assigned to the named Key expressed as a string. Supports the use of an external controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry attributeValue;

}