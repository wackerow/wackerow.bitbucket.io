package ClassLibrary.ComplexDataTypes;

public enum RelationSpecification {
	Unordered,
	List,
	ParentChild,
	WholePart,
	AcyclicPrecedence,
	Equivalence,
	GeneralSpecfic
}