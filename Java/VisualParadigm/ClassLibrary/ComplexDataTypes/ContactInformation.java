package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * Contact information for the individual or organization including location specification, address, web site, phone numbers, and other means of communication access. Address, location, telephone, and other means of communication can be repeated to express multiple means of a single type or change over time. Each major piece of contact information contains the element EffectiveDates in order to date stamp the period for which the information is valid.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:ContactInformationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ContactInformation {

	/**
	 * The URL of the Agent's website
	 */
	Collection<WebLink> website;
	/**
	 * Email contact information
	 */
	Collection<Email> hasEmail;
	/**
	 * Electronic messaging other than email
	 */
	Collection<ElectronicMessageSystem> electronicMessaging;
	/**
	 * The address for contact.
	 */
	Collection<Address> hasAddress;
	/**
	 * Telephone for contact
	 */
	Collection<Telephone> hasTelephone;

}