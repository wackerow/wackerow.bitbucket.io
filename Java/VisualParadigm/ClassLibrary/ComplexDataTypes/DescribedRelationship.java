package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Relationship specification between this item and the item to which it is related. Provides a reference to any identifiable object and a description of the relationship.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:RelationshipType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class DescribedRelationship {

	/**
	 * Explanation of the reasons for relating the external material to the identified object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString rationale;

}