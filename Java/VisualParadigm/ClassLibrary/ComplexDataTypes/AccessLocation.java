package ClassLibrary.ComplexDataTypes;

import java.util.*;
import ClassLibrary.XMLSchemaDatatypes.*;

/**
 * Definition
 * ============
 * A set of access information for a Machine including URI, mime type, and physical location
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AccessLocation {

	/**
	 * A URI for access, normally expressed as a URL
	 */
	Collection<anyUri> uri;
	/**
	 * The mime type. Supports the use of an controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry mimeType;
	/**
	 * The physical location of the machine
	 */
	Collection<InternationalString> physicalLocation;

}