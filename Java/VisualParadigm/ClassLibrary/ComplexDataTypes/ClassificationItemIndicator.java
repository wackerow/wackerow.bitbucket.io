package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * A ClassificationItemIndicator realizes and extends a MemberIndicator which provides a Classification Item with an index indicating order and a level reference providing the level location of the Classification Item within a hierarchical structure.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ClassificationItemIndicator {

	/**
	 * Provides an index for the member within an ordered array
	 */
	int index;
	/**
	 * Indicates the level within which the CodeItem resides
	 */
	int hasLevel;

}