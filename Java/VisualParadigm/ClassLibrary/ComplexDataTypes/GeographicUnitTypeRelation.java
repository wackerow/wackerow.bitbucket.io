package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Describes structured relationship between Unit Types used to define Geographic Unit Types. Extended to include the ability to define a Spatial Relationship using a controlled vocabulary.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows". 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class GeographicUnitTypeRelation {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides information on the spatial relationship between the parent and child.
	 */
	SpatialRelationSpecification hasSpatialRelationSpecification;
	/**
	 * An aggregation of the units of the child Unit Type will result in exhaustive coverage of the parent Unit Type
	 */
	boolean isExhaustiveCoverage;
	/**
	 * Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of its totality using an enumeration list.
	 */
	TotalityType totality;

}