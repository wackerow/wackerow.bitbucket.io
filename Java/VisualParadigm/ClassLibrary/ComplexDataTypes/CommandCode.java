package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * Contains information on the command used for processing data. Contains a description of the command which should clarify for the user the purpose and process of the command, an in-line provision of the command itself, and a reference to an external version of the command such as a coding script. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CommandCodeType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CommandCode {

	/**
	 * A description of the purpose and use of the command code provided. Supports multiple languages.
	 */
	InternationalStructuredString description;
	/**
	 * Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script. Designates the programming language of the command file as well as the URI for the file.
	 */
	Collection<CommandFile> usesCommandFile;
	/**
	 * This is an in-line provision of the command itself. It provides the programming language used as well as the command.
	 */
	Collection<Command> usesCommand;

}