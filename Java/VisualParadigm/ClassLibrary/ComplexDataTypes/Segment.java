package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * A structure used to express explicit segments or regions within different types of external materials (Textual, Audio, Video, XML, and Image). Provides the appropriate start, stop, or region definitions for each type.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:SegmentType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Segment {

	/**
	 * Describes the type and length of the audio segment.
	 */
	Collection<AudioSegment> usesAudioSegment;
	/**
	 * Describes the type and length of the video segment.
	 */
	Collection<VideoSegment> usesVideoSegment;
	/**
	 * An X-Pointer expression identifying a node in the XML document.
	 */
	Collection<string> xml;
	/**
	 * Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment
	 */
	Collection<TextualSegment> useseTextualSegment;
	/**
	 * Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and Coordinates provides the information required to define it.
	 */
	Collection<ImageArea> usesImageArea;

}