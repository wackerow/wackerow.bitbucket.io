package ClassLibrary.ComplexDataTypes;

import java.util.*;
import ClassLibrary.XMLSchemaDatatypes.*;

/**
 * Definition
 * ============
 * Structure supporting the use of dynamic text, where portions of the textual content change depending on external information (pre-loaded data, response to an earlier query, environmental situations, etc.).
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:DynamicTextType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class DynamicText {

	/**
	 * This is the head of a substitution group and is never used directly as an element name. Instead it is replaced with either LiteralText or ConditionalText.
	 */
	Collection<DynamicTextContent> textContent;
	/**
	 * If textual structure (e.g. size, color, font, etc.) is required to understand the meaning of the content change value to "true".
	 */
	boolean isStructureRequired;
	/**
	 * Specifies the language of the intended audience. This is particularly important for clarifying the primary language of a mixed language textual string, for example when language testing and using a foreign word withing the question text. Supports the inclusion of multiple languages within a single property.
	 */
	LanguageSpecification audienceLanguage;

}