package ClassLibrary.ComplexDataTypes;

import java.util.*;
import ClassLibrary.XMLSchemaDatatypes.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Provides annotation information on the object to support citation and crediting of the creator(s) of the object.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CitationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Annotation {

	/**
	 * Full authoritative title. List any additional titles for this item as AlternativeTitle.
	 */
	InternationalString title;
	/**
	 * Secondary or explanatory title.
	 */
	Collection<InternationalString> subTitle;
	/**
	 * An alternative title by which a data collection is commonly referred, or an abbreviation  for the title.
	 */
	Collection<InternationalString> alternativeTitle;
	/**
	 * Person, corporate body, or agency responsible for the substantive and intellectual content of the described object.
	 */
	Collection<AgentAssociation> creator;
	/**
	 * Person or organization responsible for making the resource available in its present form.
	 */
	Collection<AgentAssociation> publisher;
	/**
	 * The name of a contributing author or creator, who worked in support of the primary creator given above.
	 */
	Collection<AgentAssociation> contributor;
	/**
	 * A date associated with the annotated object (not the coverage period). Use typeOfDate to specify the type of date such as Version, Publication, Submitted, Copyrighted, Accepted, etc.
	 */
	Collection<AnnotationDate> date;
	/**
	 * Language of the intellectual content of the described object. Multiple languages are supported by the structure itself as defined in the transformation to specific bindings. Use language codes supported by xs:language which include the 2 and 3 character and extended structures defined by RFC4646 or its successors. Listing of language codes: http://www-01.sil.org/iso639-3/codes.asp Rules for construction of the code: http://www.rfc-editor.org/rfc/bcp/bcp47.txt
	 */
	LanguageSpecification languageOfObject;
	/**
	 * An identifier or locator. Contains identifier and Managing agency (ISBN, ISSN, DOI, local archive). Indicates if it is a URI.
	 */
	Collection<InternationalIdentifier> identifier;
	/**
	 * The copyright statement.
	 */
	Collection<InternationalString> copyright;
	/**
	 * Provide the type of the resource. This supports the use of a controlled vocabulary. It should be appropriate to the level of the annotation.
	 */
	Collection<ExternalControlledVocabularyEntry> typeOfResource;
	/**
	 * The name or identifier of source information for the annotated object.
	 */
	Collection<InternationalString> informationSource;
	/**
	 * Means of identifying the current version of the annotated object.
	 */
	string versionIdentification;
	/**
	 * The agent responsible for the version. May have an associated role.
	 */
	Collection<AgentAssociation> versioningAgent;
	/**
	 * A summary description (abstract) of the annotated object.
	 */
	InternationalString summary;
	/**
	 * Provide the identifier, managing agency, and type of resource related to this object. Use to specify related resources similar to Dublin Core isPartOf and hasPart to indicate collection/series membership for objects where there is an identifiable record. If not an identified object use the relationship to ExternalMaterial using a type that indicates a series description.
	 */
	Collection<ResourceIdentifier> relatedResource;
	/**
	 * A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and interpretation.
	 */
	Collection<InternationalString> provenance;
	/**
	 * Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.
	 */
	Collection<InternationalString> rights;
	/**
	 * Date the record was created
	 */
	IsoDateType recordCreationDate;
	/**
	 * Date the record was last revised
	 */
	IsoDateType recordLastRevisionDate;

}