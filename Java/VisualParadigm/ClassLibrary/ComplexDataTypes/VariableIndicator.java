package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Member Indicator for use with member type ConceptualVariable, RepresentedVariable, and InstanceVariable
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class VariableIndicator {

	/**
	 * Index value of member in an ordered array
	 */
	int index;

}