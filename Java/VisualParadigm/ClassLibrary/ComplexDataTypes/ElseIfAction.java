package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * An additional condition and resulting step to take if the condition in the parent IfThenElse is false. It cannot exist without a parent IfThenElse.
 * 
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ElseIfAction {

	/**
	 * The condition to verify if the parent IfThenElse condition is false.
	 */
	CommandCode condition;

}