package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * A standard means of expressing a Name for a class object.  A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * USE in Model: In general the property name should be "name" as it is the name of the class object which contains it. Use a specific name (i.e. xxxName) only when naming something other than the class object which contains it.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:NameType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ObjectName {

	/**
	 * The expressed name of the object.
	 */
	string content;
	/**
	 * A name may be specific to a particular context, i.e., a type of software, or a section of a registry. Identify the context related to the specified name.
	 */
	ExternalControlledVocabularyEntry context;

}