package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * A key value relationship for two or more logical records where the key is one or more equivalent instance variables and the value is a defined relationship or a relationship to a set value. Correspondence type refers to the variables themselves rather than the value of the variables concerned. In this context Correspondence type will normally be set to ExactMatch.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Logical Records: Household and Person, Key: Household ID (HHID in Household Record, HHIDP in Person Record), ValueRelaitonship: Equal, Set Value: n.a.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class InstanceVariableValueMap {

	/**
	 * Relationship between the source and target InstanceVariables or to the setValue if provided
	 */
	ValueRelationshipType valueRelationship;
	/**
	 * A set value for the key source Instance Variables
	 */
	ValueString setValue;
	/**
	 * Describes the relationship between the source and target members using both controlled vocabularies and descriptive text. In this context the correspondence refers to the two instance variables, not their value. The relationship would normally be exactMatch.
	 */
	CorrespondenceType hasCorrespondenceType;

}