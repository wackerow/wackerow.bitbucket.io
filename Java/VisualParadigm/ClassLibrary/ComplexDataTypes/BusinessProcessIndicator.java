package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Allows for the identification of the BusinessProcess specifically as a member and optionally provides an index for the member within an ordered array. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class BusinessProcessIndicator {

	/**
	 * Provides an index for the member within an ordered array
	 */
	int index;

}