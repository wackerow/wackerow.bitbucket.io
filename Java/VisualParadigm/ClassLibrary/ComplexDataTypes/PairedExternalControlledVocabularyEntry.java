package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * A tightly bound pair of items from an external controlled vocabulary. The extent property describes the extent to which the parent term applies for the specific case. 
 * 
 * Examples
 * ==========
 * When used to assign a role to an actor within a specific activity this term would express the degree of contribution. Contributor with Role=Editor and extent=Lead.
 * 
 * Alternatively. the term might be a controlled vocabulary from a list of controlled vocabularies, e.g. the Generic Longitudinal Business Process Model (GLBPM) in a list that could include other business process model frameworks. In this context the extent becomes the name of a business process model task, e.g. "integrate data" from the GLBPM.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class PairedExternalControlledVocabularyEntry {

	/**
	 * The term attributed to the parent class, for example the role of a Contributor.
	 */
	ExternalControlledVocabularyEntry term;
	/**
	 * Describes the extent to which the parent term applies for the specific case using an external controlled vocabulary. When associated with a role from the CASRAI Contributor Roles Taxonomy an appropriate vocabulary should be specified as either �lead�, �equal�, or �supporting�.
	 */
	ExternalControlledVocabularyEntry extent;

}