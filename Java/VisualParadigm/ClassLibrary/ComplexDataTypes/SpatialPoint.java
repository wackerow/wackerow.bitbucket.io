package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * A geographic point consisting of an X and Y coordinate. Each coordinate value is expressed separately providing its value and format.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:PointType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SpatialPoint {

	/**
	 * An X coordinate (latitudinal equivalent) value and format expressed using the Spatial Coordinate structure.
	 */
	SpatialCoordinate xCoordinate;
	/**
	 * A Y coordinate (longitudinal equivalent) value and format expressed using the Spatial Coordinate structure.
	 */
	SpatialCoordinate yCoordinate;

}