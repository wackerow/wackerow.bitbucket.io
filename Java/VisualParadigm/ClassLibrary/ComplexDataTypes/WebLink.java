package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.*;

/**
 * Definition
 * ============
 * A web site (normally a URL) with information on type of site, privacy flag, and effective dates.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:URLType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class WebLink {

	/**
	 * Set to "true" if this is the preferred URL.
	 */
	boolean isPreferred;
	/**
	 * Normally a URL
	 */
	anyUri uri;
	/**
	 * The type of URL for example personal, project, organization, division, etc.
	 */
	ExternalControlledVocabularyEntry typeOfWebsite;
	/**
	 * The period for which this URL is valid.
	 */
	DateRange effectiveDates;
	/**
	 * Indicates the privacy level of this URL
	 */
	ExternalControlledVocabularyEntry privacy;

}