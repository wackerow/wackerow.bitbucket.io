package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Specification of the line and offset for the beginning and end of the segment.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:LineParameterType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class LineParameter {

	/**
	 * Number of lines from beginning of the document.
	 */
	int startLine;
	/**
	 * Number of characters from start of the line specified in StartLine.
	 */
	int startOffset;
	/**
	 * Number of lines from beginning of the document.
	 */
	int endLine;
	/**
	 * Number of characters from the start of the line specified in EndLine.
	 */
	int endOffset;

}