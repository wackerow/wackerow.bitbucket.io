package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Name of the location using the DDI Name structure and the ability to add an effective date.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:LocationNameType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class LocationName extends ObjectName {

	/**
	 * The time period for which this name is accurate and in use.
	 */
	DateRange effectiveDates;

}