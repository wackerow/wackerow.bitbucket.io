package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Provides ability to declare an optional sequence or index order to a Geographic Unit
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class GeographicUnitIndicator {

	/**
	 * Index number expressed as an integer. The position of the member in an ordered array. Optional for unordered Collections.
	 */
	int index;
	/**
	 * Indicates the level within which the CodeItem resides
	 */
	int isInLevel;

}