package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * A CodeIndicator realizes and extends a MemberIndicator which provides a Code with an index indicating order and a level reference providing the level location of the Code within a hierarchical structure.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CodeIndicator {

	/**
	 * Provides an index for the member within an ordered array
	 */
	int index;
	/**
	 * Indicates the level within which the CodeItem resides
	 */
	int isInLevel;

}