package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Persistent identifier for a researcher using a system like ORCID
 * 
 * Examples
 * ==========
 * ORCID
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class AgentId {

	/**
	 * The identifier for the agent.
	 */
	string agentIdValue;
	/**
	 * The identifier system in use.
	 */
	string agentIdType;

}