package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Member Indicator for use with member type LogicalRecord
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class LogicalRecordIndicator {

	/**
	 * Index value of member in an ordered array
	 */
	int index;

}