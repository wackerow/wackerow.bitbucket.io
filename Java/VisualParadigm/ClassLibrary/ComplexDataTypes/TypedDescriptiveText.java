package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * This Complex Data Type bundles a descriptiveText with an External Controlled Vocabulary Entry allowing structured content and a means of typing that content. For example specifying that the description provides a Table of Contents for a document.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class TypedDescriptiveText {

	/**
	 * Uses a controlled vocabulary entry to classify the description provided.
	 */
	ExternalControlledVocabularyEntry typeOfContent;
	/**
	 * A short natural language account of the characteristics of the object.
	 */
	InternationalStructuredString descriptiveText;

}