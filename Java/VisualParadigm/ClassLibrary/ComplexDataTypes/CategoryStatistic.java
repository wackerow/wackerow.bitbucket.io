package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * Statistics related to a specific category of an InstanceVariable within a data set.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * pi:CategoryStatisticType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CategoryStatistic {

	/**
	 * Type of category statistic. Supports the use of an external controlled vocabulary. DDI strongly recommends the use of a controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry typeOfCategoryStatistic;
	/**
	 * The value of the identified type of statistic for the category. May be repeated to provide unweighted or weighted values and different computation bases.
	 */
	Collection<Statistic> hasStatistic;
	/**
	 * Value of the category to which the statistics apply
	 */
	ValueString categoryValue;
	/**
	 * The value of the filter variable to which the category statistic is restricted
	 */
	ValueString filterValue;

}