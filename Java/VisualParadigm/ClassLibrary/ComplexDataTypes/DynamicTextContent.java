package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Abstract type existing as the head of a substitution group. May be replaced by any valid member of the substitution group TextContent. Provides the common property of purpose to all members using TextContent as an extension base.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:TextContentType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class DynamicTextContent {

	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured tex
	 */
	InternationalStructuredString purpose;
	/**
	 * Provides the relative order of TextContent objects in a dynamic text with more than one TextContent object. Uses integer value.
	 */
	int orderPosition;

}