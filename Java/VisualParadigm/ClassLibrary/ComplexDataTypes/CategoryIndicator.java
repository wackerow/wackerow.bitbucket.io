package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Member Indicator for use when the member type is restricted to Category.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CategoryIndicator {

	/**
	 * Index value of member in an ordered array
	 */
	int index;

}