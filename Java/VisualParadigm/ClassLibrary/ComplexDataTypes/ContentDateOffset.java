package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Identifies the difference between the date applied to the data as a whole and this specific item such as previous year's income or residence 5 years ago. A value of true for the attribute isNegativeOffset indicates that the offset is the specified number of declared units prior to the date of the data as a whole and false indicates information regarding a future state.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ContentDateOffsetType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ContentDateOffset extends ExternalControlledVocabularyEntry {

	/**
	 * The number of units to off-set the date for this item expressed as a decimal.
	 */
	Real numberOfUnits;
	/**
	 * If set to "true" the date is offset the number of units specified PRIOR to the default date of the data. A setting of "false" indicates a date the specified number of units in the FUTURE from the default date of the data.
	 */
	boolean isNegativeOffset;

}