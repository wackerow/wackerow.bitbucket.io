package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Allows for the identification of the InstanceVariable specifically as a member and optionally provides an index for the member within an ordered array. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Note if multiple types of Variables may be included in a collection use VariableIndicator.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class InstanceVariableIndicator {

	/**
	 * Provides an index for the member within an ordered array
	 */
	int index;

}