package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.*;

/**
 * Definition
 * ============
 * Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script. Designates the programming language of the command file, a description of the location of the file , and a URN or URL for the command file.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CommandFileType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CommandFile {

	/**
	 * Designates the programming language used for the command. Supports the use of a controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry programLanguage;
	/**
	 * A description of the location of the file. This may not be machine actionable. It supports a description expressed in multiple languages.
	 */
	InternationalString location;
	/**
	 * The URL or URN of the command file.
	 */
	anyUri uri;

}