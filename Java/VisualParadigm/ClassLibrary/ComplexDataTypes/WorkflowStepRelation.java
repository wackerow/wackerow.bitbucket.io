package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Describes structured relations between WorkflowSteps
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class WorkflowStepRelation {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Supports the use of Allan's Intervals for describing temporal relations. Allan's Intervals are specific types of analytic precedence relations and should be used to refine a RelationSpecification of analyticPrecedence.
	 */
	TemporalRelationSpecification hasTemporalRelationSpecification;

}