package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Details of a telephone number including the number, type of number, a privacy setting and an indication of whether this is the preferred contact number.
 * 
 * Examples
 * ==========
 * +12 345 67890123
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:TelephoneType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Telephone {

	/**
	 * The telephone number including country code if appropriate.
	 */
	string telephoneNumber;
	/**
	 * Indicates type of telephone number provided (home, fax, office, cell, etc.). Supports the use of a controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry typeOfTelephone;
	/**
	 * Time period during which the telephone number is valid.
	 */
	DateRange effectiveDates;
	/**
	 * Specify the level privacy for the telephone number as public, restricted, or private. Supports the use of an external controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry privacy;
	/**
	 * Set to "true" if this is the preferred telephone number for contact.
	 */
	boolean isPreferred;

}