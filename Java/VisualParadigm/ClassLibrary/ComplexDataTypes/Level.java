package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * Provides Level information for the members of the LevelStructure. levelNumber provides the level number which may or may not be associated with a category which defines level.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * ISCO-08: index='1' label of associated category 'Major',  index='2' label of associated category 'Sub-Major',  index='3' label of associated category 'Minor', 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Level {

	/**
	 * Provides an association between a level number and optional concept which defines it within an ordered array. Use is required.
	 */
	int levelNumber;
	/**
	 * A display label for the object. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
	 */
	Collection<LabelForDisplay> displayLabel;

}