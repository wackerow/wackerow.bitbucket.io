package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Any non-email means of relaying a message electronically. This would include text messaging, Skype, Twitter, ICQ, or other emerging means of electronic message conveyance. 
 * 
 * Examples
 * ==========
 * Skype account, etc.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:InstantMessagingType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class ElectronicMessageSystem {

	/**
	 * Account identification for contacting
	 */
	string contactAddress;
	/**
	 * Indicates the type of service used. Supports the use of a controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry typeOfService;
	/**
	 * Time period during which the account is valid.
	 */
	DateRange effectiveDates;
	/**
	 * Specify the level privacy for the address as public, restricted, or private. Supports the use of an external controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry privacy;
	/**
	 * Set to "true" if this is the preferred address.
	 */
	boolean isPreferred;

}