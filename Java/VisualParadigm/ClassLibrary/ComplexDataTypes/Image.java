package ClassLibrary.ComplexDataTypes;

import ClassLibrary.XMLSchemaDatatypes.*;

/**
 * Definition
 * ============
 * A reference to an image, with a description of its properties and type.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ImageType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Image {

	/**
	 * A reference to the location of the image using a URI.
	 */
	anyUri uri;
	/**
	 * Brief description of the image type. Supports the use of an external controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry typeOfImage;
	/**
	 * Provides the resolution of the image in dots per inch to assist in selecting the appropriate image for various uses.
	 */
	int dpi;
	/**
	 * Language of image. Supports the indication of multiple languages within a single property. Supports use of codes defined by the RFC 1766.
	 */
	LanguageSpecification languageOfImage;

}