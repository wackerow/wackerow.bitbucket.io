package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:PrivateImageType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class PrivateImage extends Image {

	/**
	 * The period for which this image is effective/valid.
	 */
	DateRange effectiveDates;
	/**
	 * Specify the level privacy for the image as public, restricted, or private. Supports the use of an external controlled vocabulary.
	 */
	ExternalControlledVocabularyEntry privacy;

}