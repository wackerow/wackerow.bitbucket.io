package ClassLibrary.ComplexDataTypes;

/**
 * Definition
 * ============
 * Describes a bounding value for a number range expressed as an xs:double.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:DoubleNumberRangeValueType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class DoubleNumberRangeValue {

	/**
	 * Indicates that the value is included in the range. Set to false if the range includes numbers up to but no including the designated value.
	 */
	boolean isInclusive;
	/**
	 * The bounding value expressed as an xs:double
	 */
	Real doubleValue;

}