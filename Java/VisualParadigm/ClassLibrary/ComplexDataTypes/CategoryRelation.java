package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Source target relationship between categories in a classification relation structure
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class CategoryRelation {

	/**
	 * Type of relation in terms of totality with respect to an associated collection.
	 */
	TotalityType totality;
	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provides semantic context for the relationship
	 */
	ExternalControlledVocabularyEntry semantic;

}