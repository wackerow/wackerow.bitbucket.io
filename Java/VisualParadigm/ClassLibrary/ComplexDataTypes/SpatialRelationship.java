package ClassLibrary.ComplexDataTypes;

import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Used to specify a relationship between one spatial object and another. Includes definition of the spatial object types being related, the spatial relation specification, and the event date.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class SpatialRelationship {

	/**
	 * Provides the types of the two spatial objects.
	 */
	SpatialObjectPairs hasSpatialObjectPair;
	/**
	 * Defines the relationship of the two spatial objects
	 */
	SpatialRelationSpecification hasSpatialRelationSpecification;
	/**
	 * The date of the relationship change.
	 */
	Date eventDate;

}