package ClassLibrary.ComplexDataTypes;

import java.util.*;

/**
 * Definition
 * ============
 * Location address identifying each part of the address as separate elements, identifying the type of address, the level of privacy associated with the release of the address, and a flag to identify the preferred address for contact.
 * 
 * Examples
 * ==========
 * For example:
 * 1.  OFFICE, ABS HOUSE, 45 Benjamin Way, Belconnen, Canberra, ACT, 2617, AU
 * 2.  OFFICE, Institute of Education, 20 Bedford Way, London, WC1H 0AL, UK
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:AddressType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public class Address {

	/**
	 * Indicates address type (i.e. home, office, mailing, etc.)
	 */
	ExternalControlledVocabularyEntry typeOfAddress;
	/**
	 * Number and street including office or suite number. May use multiple lines.
	 */
	Collection<string> line;
	/**
	 * City, place, or local area used as part of an address.
	 */
	string cityPlaceLocal;
	/**
	 * A major subnational division such as a state or province used to identify a major region within an address.
	 */
	string stateProvince;
	/**
	 * Postal or ZIP Code
	 */
	string postalCode;
	/**
	 * Country of the location
	 */
	ExternalControlledVocabularyEntry countryCode;
	/**
	 * Time zone of the location expressed as code.
	 */
	ExternalControlledVocabularyEntry timeZone;
	/**
	 * Clarifies when the identification information is accurate.
	 */
	DateRange effectiveDates;
	/**
	 * Specify the level privacy for the address as public, restricted, or private. Supports the use of an external controlled vocabulary
	 */
	ExternalControlledVocabularyEntry privacy;
	/**
	 * Set to "true" if this is the preferred location for contacting the organization or individual.
	 */
	boolean isPreferred;
	/**
	 * Geographic coordinates corresponding to the address.
	 */
	SpatialPoint geographicPoint;
	/**
	 * The region covered by the agent at this address
	 */
	ExternalControlledVocabularyEntry regionalCoverage;
	/**
	 * The type or purpose of the location (i.e. regional office, distribution center, home)
	 */
	ExternalControlledVocabularyEntry typeOfLocation;
	/**
	 * Name of the location if applicable.
	 */
	ObjectName locationName;

}