package ClassLibrary.CollectionsPattern;

import ClassLibrary.Identification.*;

/**
 * Definition
 * ============
 * Generic class representing members of a collection. 
 * 
 * 
 * Examples
 * ==========
 * Classification Item, Process Step.
 * 
 * Explanatory notes
 * ===================
 * Members have to belong to some Collection, except in the case of nested Collections where the top level Collection is a Member that doesn't belong to any Collection.
 * 
 * Member is not extended, it is realized (see Collection Pattern documentation).
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Node
 */
public abstract class CollectionMember extends Identifiable {
}