package ClassLibrary.CollectionsPattern;

import ClassLibrary.Identification.*;
import java.util.*;

/**
 * Definition
 * ============
 * The minimal pattern for a comparison including a mapping between members.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class Comparison extends Identifiable {

	/**
	 * A map defining the match relationship and the members in the relationship
	 */
	Collection<ComparisonMap> correspondence;

}