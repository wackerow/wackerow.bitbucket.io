package ClassLibrary.CollectionsPattern;

import ClassLibrary.ComplexDataTypes.*;
import ClassLibrary.EnumerationsRegExp.*;

/**
 * Definition
 * ============
 * Defines one kind of relationship between one member (source) and possibly several members (target).  The type of the relationship, a semantic, and totality definition may also be specified. 
 * 
 * Examples
 * ==========
 * A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows". 
 * 
 * Explanatory notes
 * ===================
 * A MemberRelation functions like an adjacency list (https://en.wikipedia.org/wiki/Adjacency_list) in graph theory, describing the relationships of one Member to multiple other members. All of these relationships must have the same RelationSpecification, Totality, and semantic. 
 * These might not be the only relations from the source Member if there are other relations with different properties.
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class MemberRelation {

	/**
	 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list
	 */
	RelationSpecification hasRelationSpecification;
	/**
	 * Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary
	 */
	ExternalControlledVocabularyEntry semantic;
	/**
	 * Type of relation in terms of its totality using an enumeration list.
	 */
	TotalityType totality;

}