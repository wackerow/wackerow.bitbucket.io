package ClassLibrary.CollectionsPattern;

import ClassLibrary.EnumerationsRegExp.*;
import java.util.*;
import ClassLibrary.ComplexDataTypes.*;

/**
 * Definition
 * ============
 * Simple Collection container (set or bag) that may be unordered or ordered using Member Indicator. This collection type is limited to expressing only unordered sets or bags, and simple sequences. Use when there is a need to limit the organization of the collection to one of these forms of organization 
 * 
 * Examples
 * ==========
 * The sequencing of questions within a questionnaire where ordering and routing is done through conditional constructs such as IfThenElse, etc.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class SimpleCollection extends CollectionMember {

	/**
	 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
	 */
	CollectionType type;
	/**
	 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
	 */
	Collection<ObjectName> name;
	/**
	 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
	 */
	InternationalStructuredString purpose;
	/**
	 * Pointers to members of the collection that are unordered or in an indexed ordered array.
	 */
	Collection<MemberIndicator> contains;
	/**
	 * Set to true if the members are in an ordered set. If unordered set to false.
	 */
	boolean isOrdered;

}