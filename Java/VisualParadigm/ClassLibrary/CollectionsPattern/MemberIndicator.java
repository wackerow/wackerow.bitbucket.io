package ClassLibrary.CollectionsPattern;

/**
 * Definition
 * ============
 * Provides ability to declare an optional sequence or index order to a Member. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
public abstract class MemberIndicator {

	/**
	 * Index number expressed as an integer. The position of the member in an ordered array. Optional for unordered Collections.
	 */
	int index;

}