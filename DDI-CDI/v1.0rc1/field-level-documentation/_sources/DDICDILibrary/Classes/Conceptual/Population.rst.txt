.. This code was generated by the Eclipse Acceleo project UCMIS M2T.
   Target language is 'reStructuredText' ('doc') generated on the basis of the model 'DDICDIModels'.

.. _DDICDIModels::DDICDILibrary::Classes::Conceptual::Population:


Population
**********

**Fully qualified class name**: DDICDIModels::DDICDILibrary::Classes::Conceptual:::index:`Population`


Definition
============
:ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe` with time and geography specified.

Examples
==========
1. Canadian adult persons residing in Canada on 13 November 1956.
2. US computer companies at the end of 2012.  
3. Universities in Denmark 1 January 2011.

Explanatory notes
===================
Population is the most specific in the conceptually narrowing hierarchy of unit type, universe and population. Several populations having differing time and or geography may specialize the same universe.


.. dropdown:: Diagram
   :open:

   .. container::
      :name: diagram

      .. uml:: /DDICDILibrary/Classes/Conceptual/Population.pu

.. dropdown:: Inheritance

   .. |_| unicode:: U+2003 .. em space

   .. role:: focus

   .. cssclass:: regular

   | :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`

   .. cssclass:: regular

   | |_| ᐊ── :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`

   .. cssclass:: regular

   | |_| |_| ᐊ── :focus:`Conceptual::Population`




.. dropdown:: Attributes

   .. list-table::
      :widths: 10 10 60 10 10 10
      :header-rows: 1
      :class: datatable-attributes

      * - Name
        - Inherited from
        - Description
        - Data Type
        - Multiplicity
        - Default value
      * - timePeriodOfPopulation
        - \- *own class* \-
        - The time period associated with the population.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::DateRange` 
        - 0..*
        - 

      * - descriptiveText
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
        - A short natural language account of the characteristics of the object.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::InternationalString` 
        - 0..1
        - 

      * - isInclusive
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
        - Default value is True. The description statement of a universe is generally stated in inclusive terms such as "All persons with university degree". Occasionally a universe is defined by what it excludes, i.e., "All persons except those with university degree". In this case the value would be changed to False.
        - Boolean 
        - 0..1
        - true

      * - catalogDetails
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
        - Bundles the information useful for a data catalog entry. Examples would be creator, contributor, title, copyright, embargo, and license information. A set of information useful for attribution, data discovery, and access. This is information that is tied to the identity of the object. If this information changes the version of the associated object changes.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::CatalogDetails` 
        - 0..1
        - 

      * - definition
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
        - Natural language statement conveying the meaning of a concept, differentiating it from other concepts. Supports the use of multiple languages and structured text. 'externalDefinition' can't be used if 'definition' is used.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::InternationalString` 
        - 0..1
        - 

      * - displayLabel
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
        - A human-readable display label for the object. Supports the use of multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::LabelForDisplay` 
        - 0..*
        - 

      * - externalDefinition
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
        - A reference to an external definition of a concept (that is, a concept which is described outside the content of the DDI-CDI metadata description). An example is a SKOS concept. The definition property is assumed to duplicate the external one referenced if externalDefinition is used. Other corresponding properties are assumed to be included unchanged if used.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::Reference` 
        - 0..1
        - 

      * - identifier
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
        - Identifier for objects requiring short- or long-lasting referencing and management.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::Identifier` 
        - 0..1
        - 

      * - name
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
        - Human understandable name (linguistic signifier, word, phrase, or mnemonic). May follow ISO/IEC 11179-5 naming principles, and have context provided to specify usage.
        - :ref:`DDICDIModels::DDICDILibrary::DataTypes::StructuredDataTypes::ObjectName` 
        - 0..*
        - 


.. dropdown:: Associations

   .. list-table::
      :widths: 5 10 30 5 10 10 5 5 15
      :header-rows: 1
      :class: datatable-associations
   
      * - Direction
        - Association
        - Description
        - Multiplicity of Population
        - Package of Other Class
        - Other Class
        - Multiplicity of other class
        - Aggregation Kind
        - Inherited from
      * - to
        - DimensionalKey correspondsTo Universe
        - Dimensional key corresponds to zero to one universe.
        - 0..1
        - :ref:`DDICDIModels::DDICDILibrary::Classes::DataDescription::Dimensional`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::DataDescription::Dimensional::DimensionalKey`
        - 0..*
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - to
        - DimensionalKeyDefinition providesSemanticTo Universe
        - Dimensional key definition provides semantic to zero to one universe.
        - 0..1
        - :ref:`DDICDIModels::DDICDILibrary::Classes::DataDescription::Dimensional`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::DataDescription::Dimensional::DimensionalKeyDefinition`
        - 0..*
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - to
        - InstanceKey correspondsTo Universe
        - Instance key corresponds to zero to one universe.
        - 0..1
        - :ref:`DDICDIModels::DDICDILibrary::Classes::DataDescription::KeyValue`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::DataDescription::KeyValue::InstanceKey`
        - 0..*
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - to
        - Key correspondsTo Universe
        - 
        - 0..1
        - :ref:`DDICDIModels::DesignPatterns::DataDescriptionPattern`
        - :ref:`DDICDIModels::DesignPatterns::DataDescriptionPattern::Key`
        - 0..*
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - to
        - Population narrows Universe
        - Reference to a universe that the population narrows.
        - 0..1
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::Population`
        - 0..*
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - to
        - RepresentedVariable measures Universe
        - The defined class of people, entities, events, or objects to be measured by the represented variable.
        - 0..1
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::RepresentedVariable`
        - 0..*
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - from
        - Universe narrows UnitType
        - Reference to the unit type that the universe definition narrows.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::UnitType`
        - 0..1
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - from
        - Universe uses Concept
        - Reference to the concept that is being used.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept`
        - 0..*
        - none
        - :ref:`Conceptual::Universe<DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe>`
      * - to
        - AgentListing isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Agents`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Agents::AgentListing`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ClassificationFamily isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations::ClassificationFamily`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ClassificationIndex isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations::ClassificationIndex`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ClassificationSeries isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations::ClassificationSeries`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ConceptMap hasSource Concept
        - Concept map has one to many source concepts.
        - 1..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::ConceptMap`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ConceptMap hasTarget Concept
        - Concept map has one to many target concepts.
        - 1..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::ConceptMap`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ConceptRelationship hasSource Concept
        - Restricts source object to concept for the relationship.
        - 1..1
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::ConceptRelationship`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ConceptRelationship hasTarget Concept
        - Restricts target object to concept for the relationship.
        - 1..1
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::ConceptRelationship`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ConceptSystem has Concept
        - Concept system has zero to many concepts.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::ConceptSystem`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ConceptSystem isDefinedBy Concept
        - Concept system is defined by zero to many concepts. The conceptual basis for the collection of members.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::ConceptSystem`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - ConceptualVariable uses Concept
        - Reference to a concept that is being used.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::ConceptualVariable`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - DataStore isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription::DataStore`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - EnumerationDomain isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations::EnumerationDomain`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - Level isDefinedBy Concept
        - A concept or concept sub-type which describes the level.
        - 0..1
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Representations::Level`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - LogicalRecord isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription::LogicalRecord`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - PhysicalDataSet isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription::PhysicalDataSet`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - PhysicalRecordSegment isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription::PhysicalRecordSegment`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - PhysicalSegmentLayout isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription::PhysicalSegmentLayout`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - UnitType uses Concept
        - Reference to the concept that is being used.
        - 0..1
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::UnitType`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - Universe uses Concept
        - Reference to the concept that is being used.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - VariableCollection isDefinedBy Concept
        - The conceptual basis for the collection of members.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::VariableCollection`
        - 0..*
        - none
        - :ref:`Conceptual::Concept<DDICDIModels::DDICDILibrary::Classes::Conceptual::Concept>`
      * - to
        - InstanceVariable measures Population
        - Set of specific units (people, entities, objects, events), usually in a given time and geography, being measured. Can be a specialization of the universe measured by a related represented variable.
        - 0..1
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::InstanceVariable`
        - 0..*
        - none
        - \- *own class* \-
      * - to
        - PhysicalRecordSegment represents Population
        - A record segment may represent a specific population or sub-population within a larger set of segments. Allows for the identification of this filter for membership in the segment.
        - 0..1
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription`
        - :ref:`DDICDIModels::DDICDILibrary::Classes::FormatDescription::PhysicalRecordSegment`
        - 0..*
        - none
        - \- *own class* \-
      * - from
        - Population isComposedOf Unit
        - A unit in the population.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::Unit`
        - 0..*
        - shared
        - \- *own class* \-
      * - from
        - Population narrows Universe
        - Reference to a universe that the population narrows.
        - 0..*
        - \- *own package* \-
        - :ref:`DDICDIModels::DDICDILibrary::Classes::Conceptual::Universe`
        - 0..1
        - none
        - \- *own class* \-

.. dropdown:: Syntax representations / encodings

   All syntax representations except the Canonical XMI are provided as reference points
   for specific implementations, or for use as defaults if sufficient in the form presented.

   .. tab-set::

      .. tab-item:: Canonical XMI
         :class-content: encoding

         Fragment for the class **Population** (`entire model as XMI </encoding/xmi/ddi-cdi_canonical-unique-names.xmi>`_)

         .. literalinclude:: /xmi/Population.xmi
            :lines: 2-
            :language: xml
            :linenos:
            :emphasize-lines: 4

      .. tab-item:: XML Schema
         :class-content: encoding

         Fragment for the class **Population** (`entire XML Schema </encoding/xml-schema/ddi-cdi.xsd>`_)

         .. literalinclude:: /DDICDILibrary/Classes/Conceptual/Population_xsd.txt 
            :language: xml
            :linenos:
            :emphasize-lines: 1
            :dedent: 2

      .. tab-item:: Ontology (Turtle)
         :class-content: encoding

         Fragment for the class **Population** (`main ontology </encoding/ontology/ddi-cdi.onto.ttl>`_)

         .. literalinclude:: /DDICDILibrary/Classes/Conceptual/Population.onto.ttl 
            :language: turtle
            :linenos:
            :emphasize-lines: 3

      .. tab-item:: JSON-LD
         :class-content: encoding

         Fragment for the class **Population** (`main JSON-LD </encoding/json-ld/ddi-cdi.jsonld>`_)

         .. literalinclude:: /DDICDILibrary/Classes/Conceptual/Population.jsonld 
            :language: JSON-LD
            :linenos:
            :emphasize-lines: 5

