using ClassLibrary.Agents;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Agents
{
	public class Machine : Agent
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfMachine;

		public readonly ObjectName[] name;

		public readonly AccessLocation[] hasAccessLocation;

		public readonly ExternalControlledVocabularyEntry[] function;

		public readonly ExternalControlledVocabularyEntry[] machineInterface;

		public readonly ContactInformation[] ownerOperatorContact;

	}

}

