using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.Agents;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Agents
{
	public class AgentRelationStructure : Identifiable
	{
		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] privacy;

		public readonly InternationalStructuredString[] purpose;

		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly AgentRelation[] hasMemberRelation;

		public AgentListing[] agentListing;

		public RelationStructure[] relationStructure;

	}

}

