using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Agents;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Agents
{
	public class AgentListing : Identifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly AgentIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public AgentRelationStructure[] agentRelationStructure;

		public Concept[] concept;

		public StructuredCollection[] structuredCollection;

		public Agent[] agent;

	}

}

