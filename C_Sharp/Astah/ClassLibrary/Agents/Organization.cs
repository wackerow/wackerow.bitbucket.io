using ClassLibrary.Agents;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Representations;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.Agents
{
	public class Organization : Agent
	{
		public readonly OrganizationName[] hasOrganizationName;

		public readonly String[] ddiId;

		public readonly ContactInformation[] hasContactInformation;

		public StatisticalClassification[] statisticalClassification;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

		public GeographicUnitClassification[] geographicUnitClassification;

	}

}

