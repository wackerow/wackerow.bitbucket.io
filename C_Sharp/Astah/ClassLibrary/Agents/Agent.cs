using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;
using ClassLibrary.StudyRelated;
using ClassLibrary.Utility;
using ClassLibrary.SamplingMethodology;
using ClassLibrary.ProcessPattern;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Agents;
using ClassLibrary.Workflows;

namespace ClassLibrary.Agents
{
	public abstract class Agent : AnnotatedIdentifiable
	{
		public readonly AgentId[] hasAgentId;

		public readonly InternationalStructuredString[] purpose;

		public readonly PrivateImage[] image;

		public ClassificationIndex[] classificationIndex;

		public ClassificationIndex[] classificationIndex;

		public ClassificationSeries[] classificationSeries;

		public Embargo embargo;

		public FundingInformation[] fundingInformation;

		public SampleFrame sampleFrame;

		public AuthorizationSource[] authorizationSource;

		public CorrespondenceTable[] correspondenceTable;

		public CorrespondenceTable[] correspondenceTable;

		public CorrespondenceTable correspondenceTable;

		public Service[] service;

		public ExPostEvaluation[] exPostEvaluation;

		public Embargo embargo;

		public AgentRelation agentRelation;

		public AgentRelation agentRelation;

		public AgentIndicator agentIndicator;

		public CollectionMember[] collectionMember;

		public AgentListing agentListing;

		public WorkflowService[] workflowService;

		public AgentAssociation agentAssociation;

	}

}

