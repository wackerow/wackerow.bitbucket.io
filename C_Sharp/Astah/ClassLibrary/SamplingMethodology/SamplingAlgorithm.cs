using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.SamplingMethodology
{
	public class SamplingAlgorithm : AlgorithmOverview
	{
		public readonly CommandCode[] codifiedExpressionOfAlgorithm;

		public SamplingProcedure[] samplingProcedure;

		public SamplingProcess[] samplingProcess;

		public SamplingDesign[] samplingDesign;

	}

}

