using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.SamplingMethodology
{
	public class SamplingDesign : DesignOverview
	{
		public SamplingProcedure[] samplingProcedure;

		public SamplingProcess[] samplingProcess;

		public SamplingGoal[] samplingGoal;

		public SamplingAlgorithm[] samplingAlgorithm;

	}

}

