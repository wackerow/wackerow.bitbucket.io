using ClassLibrary.Methodologies;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.SamplingMethodology
{
	public class SamplePopulationResult : Result
	{
		public readonly Integer[] sizeOfSample;

		public readonly String[] strataName;

		public readonly Date[] sampleDate;

		public Population[] population;

		public UnitType[] unitType;

		public SamplingProcess[] samplingProcess;

	}

}

