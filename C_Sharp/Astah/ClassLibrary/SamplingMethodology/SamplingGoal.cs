using ClassLibrary.Methodologies;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.SamplingMethodology
{
	public class SamplingGoal : Goal
	{
		public readonly Integer[] overallTargetSampleSize;

		public readonly Real[] overallTargetSamplePercent;

		public readonly TargetSample[] targetSampleSize;

		public SamplingDesign[] samplingDesign;

	}

}

