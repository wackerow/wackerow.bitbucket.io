using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.SamplingMethodology;
using ClassLibrary.ProcessPattern;
using ClassLibrary.Workflows;
using ClassLibrary.Utility;

namespace ClassLibrary.SamplingMethodology
{
	public class SamplingProcess : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public SamplingDesign[] samplingDesign;

		public Process[] process;

		public WorkflowStepSequence workflowStepSequence;

		public SamplePopulationResult[] samplePopulationResult;

		public ExternalMaterial[] externalMaterial;

		public SamplingAlgorithm[] samplingAlgorithm;

		public SamplingProcedure[] samplingProcedure;

	}

}

