using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Agents;
using ClassLibrary.Conceptual;
using ClassLibrary.Workflows;
using ClassLibrary.Discovery;

namespace ClassLibrary.SamplingMethodology
{
	public class SampleFrame : AnnotatedIdentifiable
	{
		public readonly ObjectName[] name;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public readonly InternationalStructuredString[] purpose;

		public readonly InternationalStructuredString[] limitations;

		public readonly InternationalStructuredString[] updateProcedures;

		public readonly DateRange[] referencePeriod;

		public readonly DateRange[] validPeriod;

		public SampleFrame[] sampleFrame;

		public SampleFrame[] sampleFrame;

		public Agent[] agent;

		public UnitType unitType;

		public Population population;

		public Parameter[] parameter;

		public UnitType[] unitType;

		public Access[] access;

		public Parameter[] parameter;

	}

}

