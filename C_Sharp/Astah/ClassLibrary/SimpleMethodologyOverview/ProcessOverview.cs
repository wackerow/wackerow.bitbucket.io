using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.ProcessPattern;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.SimpleMethodologyOverview
{
	public class ProcessOverview : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public AlgorithmOverview[] algorithmOverview;

		public Process[] process;

		public ProcessSequence processSequence;

		public Study[] study;

	}

}

