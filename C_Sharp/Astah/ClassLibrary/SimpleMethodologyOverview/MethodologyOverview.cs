using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.Utility;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.SimpleMethodologyOverview
{
	public class MethodologyOverview : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] subjectOfMethodology;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] usage;

		public readonly InternationalStructuredString[] rationale;

		public readonly InternationalStructuredString[] overview;

		public MethodologyOverview[] methodologyOverview;

		public MethodologyOverview[] methodologyOverview;

		public AlgorithmOverview[] algorithmOverview;

		public Methodology[] methodology;

		public ExternalMaterial[] externalMaterial;

		public Study[] study;

		public DesignOverview[] designOverview;

	}

}

