using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Workflows;
using ClassLibrary.Utility;
using ClassLibrary.Methodologies;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.StudyRelated;
using ClassLibrary.MethodologyPattern;

namespace ClassLibrary.SimpleMethodologyOverview
{
	public class DesignOverview : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] subectOfDesign;

		public readonly InternationalStructuredString[] overview;

		public WorkflowProcess[] workflowProcess;

		public ExternalMaterial[] externalMaterial;

		public Precondition[] precondition;

		public AlgorithmOverview[] algorithmOverview;

		public Study study;

		public Goal[] goal;

		public Design[] design;

		public MethodologyOverview[] methodologyOverview;

	}

}

