using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.Utility;
using ClassLibrary.Workflows;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.SimpleMethodologyOverview
{
	public class AlgorithmOverview : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] subjectOfAlgorithm;

		public readonly InternationalStructuredString[] overview;

		public Algorithm[] algorithm;

		public ExternalMaterial[] externalMaterial;

		public WorkflowProcess[] workflowProcess;

		public DesignOverview[] designOverview;

		public ProcessOverview[] processOverview;

		public MethodologyOverview[] methodologyOverview;

		public Study[] study;

	}

}

