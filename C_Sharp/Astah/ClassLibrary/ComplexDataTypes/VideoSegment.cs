using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class VideoSegment
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfVideoClip;

		public readonly String[] videoClipBegin;

		public readonly String[] videoClipEnd;

	}

}

