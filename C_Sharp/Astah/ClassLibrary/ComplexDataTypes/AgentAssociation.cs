using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Agents;

namespace ClassLibrary.ComplexDataTypes
{
	public class AgentAssociation
	{
		public readonly BibliographicName[] agentName;

		public readonly PairedExternalControlledVocabularyEntry[] role;

		public Agent[] agent;

	}

}

