using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class ClassificationSeriesRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public ClassificationSeries[] classificationSeries;

		public ClassificationSeries[] classificationSeries;

		public MemberRelation[] memberRelation;

	}

}

