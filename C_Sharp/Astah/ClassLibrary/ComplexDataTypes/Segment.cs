using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class Segment
	{
		public readonly AudioSegment[] usesAudioSegment;

		public readonly VideoSegment[] usesVideoSegment;

		public readonly String[] xml;

		public readonly TextualSegment[] useseTextualSegment;

		public readonly ImageArea[] usesImageArea;

	}

}

