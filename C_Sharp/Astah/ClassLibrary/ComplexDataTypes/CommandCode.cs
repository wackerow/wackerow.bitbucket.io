using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class CommandCode
	{
		public readonly InternationalStructuredString[] description;

		public readonly CommandFile[] usesCommandFile;

		public readonly Command[] usesCommand;

	}

}

