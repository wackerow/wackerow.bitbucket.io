using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class NumberRange
	{
		public readonly LabelForDisplay[] label;

		public readonly NumberRangeValue[] highCode;

		public readonly DoubleNumberRangeValue[] highCodeDouble;

		public readonly NumberRangeValue[] lowCode;

		public readonly DoubleNumberRangeValue[] lowCodeDouble;

		public readonly String[] regExp;

		public readonly Boolean isTopCoded;

		public readonly Boolean isBottomCoded;

	}

}

