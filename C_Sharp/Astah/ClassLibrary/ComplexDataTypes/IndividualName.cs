using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;

namespace ClassLibrary.ComplexDataTypes
{
	public class IndividualName
	{
		public readonly String[] prefix;

		public readonly String[] firstGiven;

		public readonly String[] middle;

		public readonly String[] lastFamily;

		public readonly String[] suffix;

		public readonly InternationalString[] fullName;

		public readonly DateRange[] effectiveDates;

		public readonly InternationalString[] abbreviation;

		public readonly ExternalControlledVocabularyEntry[] typeOfIndividualName;

		public readonly SexSpecificationType[] sex;

		public readonly Boolean[] isPreferred;

		public readonly ExternalControlledVocabularyEntry[] context;

		public readonly Boolean[] isFormal;

	}

}

