using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.FormatDescription;

namespace ClassLibrary.ComplexDataTypes
{
	public class PhysicalRecordSegmentRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public PhysicalRecordSegment[] physicalRecordSegment;

		public PhysicalRecordSegment[] physicalRecordSegment;

	}

}

