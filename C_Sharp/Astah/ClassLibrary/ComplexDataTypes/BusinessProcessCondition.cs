using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.ComplexDataTypes
{
	public class BusinessProcessCondition
	{
		public readonly String[] sql;

		public readonly CommandCode[] rejectionCriteria;

		public readonly InternationalStructuredString[] dataDescription;

		public LogicalRecord[] logicalRecord;

	}

}

