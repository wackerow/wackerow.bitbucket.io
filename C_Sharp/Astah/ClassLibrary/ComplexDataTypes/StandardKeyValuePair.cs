using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class StandardKeyValuePair
	{
		public readonly ExternalControlledVocabularyEntry[] attributeKey;

		public readonly ExternalControlledVocabularyEntry[] attributeValue;

	}

}

