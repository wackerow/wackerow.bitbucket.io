using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class InstanceVariableValueMap
	{
		public readonly ValueRelationshipType valueRelationship;

		public readonly ValueString[] setValue;

		public readonly CorrespondenceType hasCorrespondenceType;

		public ComparisonMap[] comparisonMap;

		public InstanceVariable[] instanceVariable;

		public InstanceVariable[] instanceVariable;

	}

}

