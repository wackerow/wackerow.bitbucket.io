using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class InternationalIdentifier
	{
		public readonly String[] identifierContent;

		public readonly ExternalControlledVocabularyEntry[] managingAgency;

		public readonly Boolean[] isURI;

	}

}

