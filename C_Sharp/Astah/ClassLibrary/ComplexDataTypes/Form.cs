using Primitive Package;
using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class Form
	{
		public readonly String[] formNumber;

		public readonly anyUri[] uri;

		public readonly InternationalString[] statement;

		public readonly Boolean[] isRequired;

	}

}

