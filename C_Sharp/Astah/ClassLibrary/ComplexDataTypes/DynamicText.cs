using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.XMLSchemaDatatypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class DynamicText
	{
		public readonly DynamicTextContent[] textContent;

		public readonly Boolean[] isStructureRequired;

		public readonly LanguageSpecification[] audienceLanguage;

	}

}

