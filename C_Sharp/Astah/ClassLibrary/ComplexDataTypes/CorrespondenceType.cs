using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;

namespace ClassLibrary.ComplexDataTypes
{
	public class CorrespondenceType
	{
		public readonly InternationalStructuredString[] commonality;

		public readonly InternationalStructuredString[] difference;

		public readonly ExternalControlledVocabularyEntry[] commonalityTypeCode;

		public readonly MappingRelation[] hasMappingRelation;

	}

}

