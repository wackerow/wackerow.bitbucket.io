using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class LogicalRecordRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public LogicalRecord[] logicalRecord;

		public MemberRelation[] memberRelation;

		public LogicalRecord[] logicalRecord;

	}

}

