using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class SpatialPoint
	{
		public readonly SpatialCoordinate[] xCoordinate;

		public readonly SpatialCoordinate[] yCoordinate;

	}

}

