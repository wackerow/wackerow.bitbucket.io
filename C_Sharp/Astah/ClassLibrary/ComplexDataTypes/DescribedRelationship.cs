using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Identification;

namespace ClassLibrary.ComplexDataTypes
{
	public class DescribedRelationship
	{
		public readonly InternationalStructuredString[] rationale;

		public Identifiable[] identifiable;

	}

}

