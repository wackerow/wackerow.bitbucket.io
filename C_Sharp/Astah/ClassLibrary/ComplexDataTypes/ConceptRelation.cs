using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class ConceptRelation
	{
		public readonly RelationSpecification hasRelationSepcification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public Concept[] concept;

		public Concept[] concept;

	}

}

