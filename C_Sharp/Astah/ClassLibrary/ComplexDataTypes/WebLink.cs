using Primitive Package;
using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class WebLink
	{
		public readonly Boolean[] isPreferred;

		public readonly anyUri[] uri;

		public readonly ExternalControlledVocabularyEntry[] typeOfWebsite;

		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] privacy;

	}

}

