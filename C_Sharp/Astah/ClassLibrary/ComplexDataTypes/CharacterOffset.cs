using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class CharacterOffset
	{
		public readonly Integer[] startCharOffset;

		public readonly Integer[] endCharOffset;

		public readonly Integer[] characterLength;

	}

}

