using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class AudioSegment
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfAudioClip;

		public readonly String[] audioClipBegin;

		public readonly String[] audioClipEnd;

	}

}

