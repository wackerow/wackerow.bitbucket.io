using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class InstanceVariableIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public InstanceVariable[] instanceVariable;

	}

}

