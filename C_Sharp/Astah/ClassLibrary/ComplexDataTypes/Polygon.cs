using ClassLibrary.XMLSchemaDatatypes;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class Polygon
	{
		public readonly anyUri[] uri;

		public readonly String[] polygonLinkCode;

		public readonly ExternalControlledVocabularyEntry[] shapeFileFormat;

		public readonly SpatialPoint[] point;

	}

}

