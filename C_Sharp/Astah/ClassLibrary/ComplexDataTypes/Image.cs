using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class Image
	{
		public readonly anyUri[] uri;

		public readonly ExternalControlledVocabularyEntry[] typeOfImage;

		public readonly Integer[] dpi;

		public readonly LanguageSpecification[] languageOfImage;

	}

}

