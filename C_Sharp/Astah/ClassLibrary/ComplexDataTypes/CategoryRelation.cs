using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class CategoryRelation
	{
		public readonly TotalityType[] totality;

		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public Category[] category;

		public MemberRelation[] memberRelation;

		public Category[] category;

	}

}

