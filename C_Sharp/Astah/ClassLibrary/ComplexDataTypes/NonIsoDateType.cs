using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class NonIsoDateType
	{
		public readonly String dateContent;

		public readonly ExternalControlledVocabularyEntry[] nonIsoDateFormat;

		public readonly ExternalControlledVocabularyEntry[] calendar;

	}

}

