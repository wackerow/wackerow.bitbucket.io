using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class ContactInformation
	{
		public readonly WebLink[] website;

		public readonly Email[] hasEmail;

		public readonly ElectronicMessageSystem[] electronicMessaging;

		public readonly Address[] hasAddress;

		public readonly Telephone[] hasTelephone;

	}

}

