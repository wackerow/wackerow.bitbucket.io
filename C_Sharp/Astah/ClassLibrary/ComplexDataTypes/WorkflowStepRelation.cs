using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Workflows;

namespace ClassLibrary.ComplexDataTypes
{
	public class WorkflowStepRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly TemporalRelationSpecification[] hasTemporalRelationSpecification;

		public MemberRelation[] memberRelation;

		public WorkflowStep[] workflowStep;

		public WorkflowStep[] workflowStep;

	}

}

