using Primitive Package;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class ConceptIndicator
	{
		public readonly Integer[] index;

		public Concept[] concept;

		public MemberIndicator[] memberIndicator;

	}

}

