using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.ComplexDataTypes
{
	public class VocabularyEntryRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public VocabularyEntry[] vocabularyEntry;

		public VocabularyEntry[] vocabularyEntry;

	}

}

