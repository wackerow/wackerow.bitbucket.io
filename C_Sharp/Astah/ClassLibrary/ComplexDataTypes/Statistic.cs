using Primitive Package;
using ClassLibrary.EnumerationsRegExp;

namespace ClassLibrary.ComplexDataTypes
{
	public class Statistic
	{
		public readonly Boolean[] isWeighted;

		public readonly ComputationBaseList[] computationBase;

		public readonly Real[] decimalValue;

		public readonly Real[] doubleValue;

	}

}

