using Primitive Package;
using ClassLibrary.XMLSchemaDatatypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class LanguageSpecificStructuredStringType
	{
		public readonly String[] formattedContent;

		public readonly language[] language;

		public readonly Boolean[] isTranslated;

		public readonly Boolean[] isTranslatable;

		public readonly language[] translationSourceLanguage;

		public readonly UnlimitedNatural[] translationDate;

		public readonly Boolean[] isPlainText;

	}

}

