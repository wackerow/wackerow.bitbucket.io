using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.ComplexDataTypes
{
	public class CustomValueRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public CustomValue[] customValue;

		public CustomValue[] customValue;

	}

}

