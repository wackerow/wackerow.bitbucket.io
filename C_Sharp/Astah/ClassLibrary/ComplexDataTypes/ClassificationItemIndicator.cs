using Primitive Package;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class ClassificationItemIndicator
	{
		public readonly Integer[] index;

		public readonly Integer[] hasLevel;

		public ClassificationItem[] classificationItem;

		public MemberIndicator[] memberIndicator;

	}

}

