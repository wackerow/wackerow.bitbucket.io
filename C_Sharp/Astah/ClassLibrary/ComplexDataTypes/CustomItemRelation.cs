using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.ComplexDataTypes
{
	public class CustomItemRelation
	{
		public readonly RelationSpecification hasRelationSepcification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public CustomItem[] customItem;

		public CustomItem[] customItem;

	}

}

