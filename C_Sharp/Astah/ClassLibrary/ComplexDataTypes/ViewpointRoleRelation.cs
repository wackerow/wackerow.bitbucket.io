using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class ViewpointRoleRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public InstanceVariable[] instanceVariable;

		public InstanceVariable[] instanceVariable;

	}

}

