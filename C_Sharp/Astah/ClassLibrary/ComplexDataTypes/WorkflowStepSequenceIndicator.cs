using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Workflows;

namespace ClassLibrary.ComplexDataTypes
{
	public class WorkflowStepSequenceIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public WorkflowStepSequence[] workflowStepSequence;

	}

}

