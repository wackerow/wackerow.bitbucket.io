using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.FormatDescription;

namespace ClassLibrary.ComplexDataTypes
{
	public class PhysicalRecordSegmentIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public PhysicalRecordSegment[] physicalRecordSegment;

	}

}

