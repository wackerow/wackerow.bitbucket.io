using Primitive Package;
using ClassLibrary.ProcessPattern;
using ClassLibrary.Workflows;

namespace ClassLibrary.ComplexDataTypes
{
	public class WorkflowStepIndicator
	{
		public readonly Integer[] index;

		public ProcessStepIndicator[] processStepIndicator;

		public WorkflowStep[] workflowStep;

	}

}

