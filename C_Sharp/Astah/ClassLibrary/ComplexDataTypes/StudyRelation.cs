using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.ComplexDataTypes
{
	public class StudyRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public Study[] study;

		public Study[] study;

	}

}

