using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class GeographicUnitTypeIndicator
	{
		public readonly Integer[] index;

		public readonly Integer[] isInLevel;

		public MemberIndicator[] memberIndicator;

		public UnitType[] unitType;

	}

}

