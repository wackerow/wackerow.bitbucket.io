using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class ReferenceDate : AnnotationDate
	{
		public readonly ExternalControlledVocabularyEntry[] subject;

		public readonly ExternalControlledVocabularyEntry[] keyword;

	}

}

