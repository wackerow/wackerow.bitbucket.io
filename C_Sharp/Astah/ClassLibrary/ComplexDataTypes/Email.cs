using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class Email
	{
		public readonly String[] internetEmail;

		public readonly ExternalControlledVocabularyEntry[] typeOfEmail;

		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] privacy;

		public readonly Boolean[] isPreferred;

	}

}

