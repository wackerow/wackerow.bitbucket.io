using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class Date
	{
		public readonly IsoDateType[] isoDate;

		public readonly NonIsoDateType[] nonIsoDate;

	}

}

