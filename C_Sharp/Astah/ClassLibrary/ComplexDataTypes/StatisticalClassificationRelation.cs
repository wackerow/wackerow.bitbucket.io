using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;

namespace ClassLibrary.ComplexDataTypes
{
	public class StatisticalClassificationRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public StatisticalClassification[] statisticalClassification;

		public StatisticalClassification[] statisticalClassification;

	}

}

