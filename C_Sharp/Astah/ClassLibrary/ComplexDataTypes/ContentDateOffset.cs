using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class ContentDateOffset : ExternalControlledVocabularyEntry
	{
		public readonly Real[] numberOfUnits;

		public readonly Boolean[] isNegativeOffset;

	}

}

