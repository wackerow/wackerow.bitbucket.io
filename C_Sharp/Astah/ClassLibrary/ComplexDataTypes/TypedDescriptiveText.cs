using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class TypedDescriptiveText
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfContent;

		public readonly InternationalStructuredString[] descriptiveText;

	}

}

