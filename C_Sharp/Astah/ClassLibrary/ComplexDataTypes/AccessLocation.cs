using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class AccessLocation
	{
		public readonly anyUri[] uri;

		public readonly ExternalControlledVocabularyEntry[] mimeType;

		public readonly InternationalString[] physicalLocation;

	}

}

