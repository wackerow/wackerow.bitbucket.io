using Primitive Package;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class DataStoreIndicator
	{
		public readonly Integer[] index;

		public DataStore[] dataStore;

		public MemberIndicator[] memberIndicator;

	}

}

