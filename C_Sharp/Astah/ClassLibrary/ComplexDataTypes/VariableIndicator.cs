using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class VariableIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public ConceptualVariable[] conceptualVariable;

	}

}

