using Primitive Package;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class ClassificationSeriesIndicator
	{
		public readonly Integer[] index;

		public ClassificationSeries[] classificationSeries;

		public MemberIndicator[] memberIndicator;

	}

}

