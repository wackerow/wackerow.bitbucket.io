using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class DateRange
	{
		public readonly Date[] startDate;

		public readonly Date[] endDate;

	}

}

