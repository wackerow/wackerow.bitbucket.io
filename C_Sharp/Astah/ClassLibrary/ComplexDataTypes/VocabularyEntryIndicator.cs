using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.ComplexDataTypes
{
	public class VocabularyEntryIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public VocabularyEntry[] vocabularyEntry;

	}

}

