using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.ComplexDataTypes
{
	public class StudyIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public Study[] study;

	}

}

