using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public abstract class DynamicTextContent
	{
		public readonly InternationalStructuredString[] purpose;

		public readonly Integer[] orderPosition;

	}

}

