using Primitive Package;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class ClassificationIndexEntryIndicator
	{
		public readonly Integer[] index;

		public ClassificationIndexEntry[] classificationIndexEntry;

		public MemberIndicator[] memberIndicator;

	}

}

