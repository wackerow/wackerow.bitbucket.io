using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Identification;

namespace ClassLibrary.ComplexDataTypes
{
	public class BasedOnObjectInformation
	{
		public readonly InternationalString[] basedOnRationaleDescription;

		public readonly ExternalControlledVocabularyEntry[] basedOnRationaleCode;

		public Identifiable[] identifiable;

	}

}

