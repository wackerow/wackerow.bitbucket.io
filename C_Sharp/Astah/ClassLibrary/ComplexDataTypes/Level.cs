using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class Level
	{
		public readonly Integer levelNumber;

		public readonly LabelForDisplay[] displayLabel;

		public Concept[] concept;

	}

}

