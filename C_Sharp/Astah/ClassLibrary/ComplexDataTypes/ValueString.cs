using Primitive Package;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.SignificationPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class ValueString
	{
		public readonly String[] content;

		public readonly WhiteSpaceRule[] whiteSpace;

		public Signifier[] signifier;

	}

}

