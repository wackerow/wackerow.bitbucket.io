using Primitive Package;
using ClassLibrary.XMLSchemaDatatypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class LanguageSpecificStringType
	{
		public readonly String content;

		public readonly language[] language;

		public readonly Boolean[] isTranslated;

		public readonly Boolean[] isTranslatable;

		public readonly language[] translationSourceLanguage;

		public readonly UnlimitedNatural[] translationDate;

	}

}

