using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class ElectronicMessageSystem
	{
		public readonly String[] contactAddress;

		public readonly ExternalControlledVocabularyEntry[] typeOfService;

		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] privacy;

		public readonly Boolean[] isPreferred;

	}

}

