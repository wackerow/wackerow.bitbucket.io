using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class PairedExternalControlledVocabularyEntry
	{
		public readonly ExternalControlledVocabularyEntry term;

		public readonly ExternalControlledVocabularyEntry[] extent;

	}

}

