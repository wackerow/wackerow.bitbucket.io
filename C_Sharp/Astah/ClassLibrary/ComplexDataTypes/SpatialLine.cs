using ClassLibrary.XMLSchemaDatatypes;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class SpatialLine
	{
		public readonly anyUri[] uri;

		public readonly String[] lineLinkCode;

		public readonly ExternalControlledVocabularyEntry[] shapeFileFormat;

		public readonly SpatialPoint[] point;

	}

}

