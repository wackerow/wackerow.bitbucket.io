using ClassLibrary.ComplexDataTypes;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class Map
	{
		public readonly DateRange[] validDates;

		public readonly CorrespondenceType[] hasCorrespondenceType;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public ComparisonMap[] comparisonMap;

		public Concept[] concept;

		public Concept[] concept;

	}

}

