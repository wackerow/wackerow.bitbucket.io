using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class AreaCoverage
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfArea;

		public readonly ExternalControlledVocabularyEntry[] measurementUnit;

		public readonly Real[] areaMeasure;

	}

}

