using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Agents;

namespace ClassLibrary.ComplexDataTypes
{
	public class AgentRelation
	{
		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly RelationSpecification hasRelationSpecification;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public Agent[] agent;

		public Agent[] agent;

	}

}

