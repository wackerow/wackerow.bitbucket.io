using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.FormatDescription;

namespace ClassLibrary.ComplexDataTypes
{
	public class ValueMappingIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public ValueMapping[] valueMapping;

	}

}

