using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using Primitive Package;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class GeographicUnitTypeRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly SpatialRelationSpecification[] hasSpatialRelationSpecification;

		public readonly Boolean[] isExhaustiveCoverage;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public UnitType[] unitType;

		public UnitType[] unitType;

		public MemberRelation[] memberRelation;

	}

}

