using ClassLibrary.ComplexDataTypes;
using ClassLibrary.XMLSchemaDatatypes;
using Primitive Package;
using ClassLibrary.EnumerationsRegExp;

namespace ClassLibrary.ComplexDataTypes
{
	public class Annotation
	{
		public readonly InternationalString[] title;

		public readonly InternationalString[] subTitle;

		public readonly InternationalString[] alternativeTitle;

		public readonly AgentAssociation[] creator;

		public readonly AgentAssociation[] publisher;

		public readonly AgentAssociation[] contributor;

		public readonly AnnotationDate[] date;

		public readonly LanguageSpecification[] languageOfObject;

		public readonly InternationalIdentifier[] identifier;

		public readonly InternationalString[] copyright;

		public readonly ExternalControlledVocabularyEntry[] typeOfResource;

		public readonly InternationalString[] informationSource;

		public readonly String[] versionIdentification;

		public readonly AgentAssociation[] versioningAgent;

		public readonly InternationalString[] summary;

		public readonly ResourceIdentifier[] relatedResource;

		public readonly InternationalString[] provenance;

		public readonly InternationalString[] rights;

		public readonly IsoDateType[] recordCreationDate;

		public readonly IsoDateType[] recordLastRevisionDate;

	}

}

