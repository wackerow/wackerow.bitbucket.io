using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class ResourceIdentifier : InternationalIdentifier
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfRelatedResource;

	}

}

