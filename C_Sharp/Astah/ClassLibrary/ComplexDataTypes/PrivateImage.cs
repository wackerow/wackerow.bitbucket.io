using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class PrivateImage : Image
	{
		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] privacy;

	}

}

