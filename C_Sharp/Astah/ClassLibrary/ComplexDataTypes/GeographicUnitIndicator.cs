using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.ComplexDataTypes
{
	public class GeographicUnitIndicator
	{
		public readonly Integer[] index;

		public readonly Integer[] isInLevel;

		public MemberIndicator[] memberIndicator;

		public GeographicUnit[] geographicUnit;

	}

}

