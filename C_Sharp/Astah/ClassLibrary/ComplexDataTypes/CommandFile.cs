using ClassLibrary.ComplexDataTypes;
using ClassLibrary.XMLSchemaDatatypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class CommandFile
	{
		public readonly ExternalControlledVocabularyEntry[] programLanguage;

		public readonly InternationalString[] location;

		public readonly anyUri[] uri;

	}

}

