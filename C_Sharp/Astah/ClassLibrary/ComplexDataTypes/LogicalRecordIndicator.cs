using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.ComplexDataTypes
{
	public class LogicalRecordIndicator
	{
		public readonly Integer[] index;

		public MemberIndicator[] memberIndicator;

		public LogicalRecord[] logicalRecord;

	}

}

