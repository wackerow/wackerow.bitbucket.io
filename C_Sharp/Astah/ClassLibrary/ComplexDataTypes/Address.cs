using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class Address
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfAddress;

		public readonly String[] line;

		public readonly String[] cityPlaceLocal;

		public readonly String[] stateProvince;

		public readonly String[] postalCode;

		public readonly ExternalControlledVocabularyEntry[] countryCode;

		public readonly ExternalControlledVocabularyEntry[] timeZone;

		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] privacy;

		public readonly Boolean[] isPreferred;

		public readonly SpatialPoint[] geographicPoint;

		public readonly ExternalControlledVocabularyEntry[] regionalCoverage;

		public readonly ExternalControlledVocabularyEntry[] typeOfLocation;

		public readonly ObjectName[] locationName;

	}

}

