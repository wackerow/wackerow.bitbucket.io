using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class LabelForDisplay : InternationalStructuredString
	{
		public readonly ExternalControlledVocabularyEntry[] locationVariant;

		public readonly DateRange[] validDates;

		public readonly Integer[] maxLength;

	}

}

