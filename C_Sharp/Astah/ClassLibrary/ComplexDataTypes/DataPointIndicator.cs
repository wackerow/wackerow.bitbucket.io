using Primitive Package;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class DataPointIndicator
	{
		public readonly Integer[] index;

		public DataPoint[] dataPoint;

		public MemberIndicator[] memberIndicator;

	}

}

