using Primitive Package;
using ClassLibrary.XMLSchemaDatatypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class ExternalControlledVocabularyEntry
	{
		public readonly String[] controlledVocabularyID;

		public readonly String[] controlledVocabularyName;

		public readonly String[] controlledVocabularyAgencyName;

		public readonly String[] controlledVocabularyVersionID;

		public readonly String[] otherValue;

		public readonly anyUri[] uri;

		public readonly String[] content;

		public readonly language[] language;

	}

}

