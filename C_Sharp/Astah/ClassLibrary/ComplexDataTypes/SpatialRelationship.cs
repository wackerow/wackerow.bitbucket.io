using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.ComplexDataTypes
{
	public class SpatialRelationship
	{
		public readonly SpatialObjectPairs[] hasSpatialObjectPair;

		public readonly SpatialRelationSpecification[] hasSpatialRelationSpecification;

		public readonly Date[] eventDate;

		public GeographicUnit[] geographicUnit;

	}

}

