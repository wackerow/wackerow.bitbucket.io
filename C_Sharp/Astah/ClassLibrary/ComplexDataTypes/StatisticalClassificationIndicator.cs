using Primitive Package;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class StatisticalClassificationIndicator
	{
		public readonly Integer[] index;

		public StatisticalClassification[] statisticalClassification;

		public MemberIndicator[] memberIndicator;

	}

}

