using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class Telephone
	{
		public readonly String[] telephoneNumber;

		public readonly ExternalControlledVocabularyEntry[] typeOfTelephone;

		public readonly DateRange[] effectiveDates;

		public readonly ExternalControlledVocabularyEntry[] privacy;

		public readonly Boolean[] isPreferred;

	}

}

