using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ComplexDataTypes
{
	public class CategoryStatistic
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfCategoryStatistic;

		public readonly Statistic[] hasStatistic;

		public readonly ValueString[] categoryValue;

		public readonly ValueString[] filterValue;

		public CodeIndicator[] codeIndicator;

	}

}

