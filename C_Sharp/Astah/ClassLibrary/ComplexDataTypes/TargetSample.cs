using Primitive Package;
using ClassLibrary.Conceptual;

namespace ClassLibrary.ComplexDataTypes
{
	public class TargetSample
	{
		public readonly Boolean isPrimary;

		public readonly Integer[] targetSize;

		public readonly Real[] targetPercent;

		public UnitType unitType;

		public Universe universe;

	}

}

