using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.ComplexDataTypes
{
	public class GeographicUnitRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly SpatialRelationSpecification[] hasSpatialRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public MemberRelation[] memberRelation;

		public GeographicUnit[] geographicUnit;

		public GeographicUnit[] geographicUnit;

	}

}

