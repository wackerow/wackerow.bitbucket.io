using ClassLibrary.ComplexDataTypes;
using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class OrganizationName : ObjectName
	{
		public readonly InternationalString[] abbreviation;

		public readonly ExternalControlledVocabularyEntry[] typeOfOrganizationName;

		public readonly DateRange[] effectiveDates;

		public readonly Boolean[] isFormal;

	}

}

