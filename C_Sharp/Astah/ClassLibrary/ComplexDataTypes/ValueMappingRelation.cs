using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.FormatDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class ValueMappingRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public ValueMapping[] valueMapping;

		public MemberRelation[] memberRelation;

		public ValueMapping[] valueMapping;

	}

}

