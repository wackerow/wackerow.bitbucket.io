using Primitive Package;

namespace ClassLibrary.ComplexDataTypes
{
	public class LineParameter
	{
		public readonly Integer[] startLine;

		public readonly Integer[] startOffset;

		public readonly Integer[] endLine;

		public readonly Integer[] endOffset;

	}

}

