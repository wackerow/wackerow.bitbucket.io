using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class InstanceVariableRelation
	{
		public readonly TotalityType[] totality;

		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public InstanceVariable[] instanceVariable;

		public InstanceVariable[] instanceVariable;

		public MemberRelation[] memberRelation;

	}

}

