using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.ComplexDataTypes
{
	public class CodeIndicator
	{
		public readonly Integer[] index;

		public readonly Integer[] isInLevel;

		public CategoryStatistic categoryStatistic;

		public Code[] code;

		public MemberIndicator[] memberIndicator;

	}

}

