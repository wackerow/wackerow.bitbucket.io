using ClassLibrary.Identification;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.FormatDescription;
using ClassLibrary.SimpleCodebook;
using ClassLibrary.Conceptual;

namespace ClassLibrary.SimpleCodebook
{
	public class VariableStatistics : Identifiable
	{
		public readonly Integer[] totalResponses;

		public readonly SummaryStatistic[] hasSummaryStatistic;

		public readonly CategoryStatistic[] hasCategoryStatistic;

		public CollectionMember[] collectionMember;

		public PhysicalDataSet[] physicalDataSet;

		public StandardWeight[] standardWeight;

		public InstanceVariable[] instanceVariable;

		public InstanceVariable[] instanceVariable;

		public InstanceVariable[] instanceVariable;

	}

}

