using ClassLibrary.Identification;
using Primitive Package;
using ClassLibrary.SimpleCodebook;

namespace ClassLibrary.SimpleCodebook
{
	public class StandardWeight : AnnotatedIdentifiable
	{
		public readonly Real[] standardWeightValue;

		public VariableStatistics variableStatistics;

	}

}

