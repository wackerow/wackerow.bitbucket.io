using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.GeographicClassification
{
	public class GeographicUnitRelationStructure : Identifiable
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly GeographicUnitRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public GeographicUnitClassification geographicUnitClassification;

	}

}

