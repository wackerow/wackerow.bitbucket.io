using ClassLibrary.Representations;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Discovery;
using ClassLibrary.Conceptual;
using ClassLibrary.GeographicClassification;
using ClassLibrary.Agents;

namespace ClassLibrary.GeographicClassification
{
	public class GeographicUnitTypeClassification : CodeList
	{
		public readonly GeographicUnitTypeIndicator[] contains;

		public readonly Date[] releaseDate;

		public readonly DateRange[] validDates;

		public readonly Boolean[] isCurrent;

		public readonly Boolean[] isFloating;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public SpatialCoverage[] spatialCoverage;

		public ConceptSystem[] conceptSystem;

		public GeographicUnitTypeRelationStructure[] geographicUnitTypeRelationStructure;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

		public Organization[] organization;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

		public GeographicUnitTypeClassification geographicUnitTypeClassification;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

	}

}

