using ClassLibrary.Conceptual;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Discovery;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.GeographicClassification
{
	public class GeographicUnit : Unit
	{
		public readonly DateRange[] geographicTime;

		public readonly SpatialRelationship[] supercedes;

		public readonly SpatialRelationship[] precedes;

		public SpatialRelationship spatialRelationship;

		public SpatialCoverage[] spatialCoverage;

		public GeographicUnitIndicator geographicUnitIndicator;

		public GeographicUnitRelation[] geographicUnitRelation;

		public GeographicUnitRelation geographicUnitRelation;

		public Population[] population;

		public GeographicExtent[] geographicExtent;

	}

}

