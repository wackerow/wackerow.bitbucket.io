using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Discovery;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.GeographicClassification
{
	public class GeographicExtent : AnnotatedIdentifiable
	{
		public readonly Polygon[] boundingPolygon;

		public readonly Polygon[] excludingPolygon;

		public readonly DateRange[] geographicTime;

		public readonly AreaCoverage[] hasAreaCoverage;

		public readonly SpatialPoint[] hasCentroid;

		public readonly SpatialPoint[] locationPoint;

		public readonly SpatialLine[] isSpatialLine;

		public BoundingBox[] boundingBox;

		public GeographicUnit[] geographicUnit;

	}

}

