using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.GeographicClassification;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.GeographicClassification
{
	public class GeographicUnitTypeRelationStructure : Identifiable
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly GeographicUnitTypeRelation[] hasMemberRelation;

		public GeographicUnitTypeClassification geographicUnitTypeClassification;

		public RelationStructure[] relationStructure;

	}

}

