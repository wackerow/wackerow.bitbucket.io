using ClassLibrary.Representations;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.GeographicClassification;
using ClassLibrary.Conceptual;
using ClassLibrary.Agents;
using ClassLibrary.Discovery;

namespace ClassLibrary.GeographicClassification
{
	public class GeographicUnitClassification : CodeList
	{
		public readonly GeographicUnitIndicator[] contains;

		public readonly Date[] releaseDate;

		public readonly DateRange[] validDates;

		public readonly Boolean[] isCurrent;

		public readonly Boolean[] isFloating;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public GeographicUnitClassification[] geographicUnitClassification;

		public GeographicUnitClassification[] geographicUnitClassification;

		public GeographicUnitClassification[] geographicUnitClassification;

		public GeographicUnitClassification[] geographicUnitClassification;

		public GeographicUnitRelationStructure[] geographicUnitRelationStructure;

		public ConceptSystem[] conceptSystem;

		public GeographicUnitClassification[] geographicUnitClassification;

		public GeographicUnitClassification geographicUnitClassification;

		public Organization[] organization;

		public SpatialCoverage[] spatialCoverage;

	}

}

