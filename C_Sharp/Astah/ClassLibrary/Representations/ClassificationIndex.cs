using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.EnumerationsRegExp;
using Primitive Package;
using ClassLibrary.Agents;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;
using ClassLibrary.Utility;

namespace ClassLibrary.Representations
{
	public class ClassificationIndex : AnnotatedIdentifiable
	{
		public readonly Date[] releaseDate;

		public readonly LanguageSpecification[] availableLanguage;

		public readonly InternationalString[] corrections;

		public readonly CommandCode[] codingInstruction;

		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly ClassificationIndexEntryIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public Agent agent;

		public Agent[] agent;

		public Concept[] concept;

		public StructuredCollection[] structuredCollection;

		public IndexEntryRelationStructure[] indexEntryRelationStructure;

		public ExternalMaterial[] externalMaterial;

		public StatisticalClassification[] statisticalClassification;

		public ClassificationFamily[] classificationFamily;

	}

}

