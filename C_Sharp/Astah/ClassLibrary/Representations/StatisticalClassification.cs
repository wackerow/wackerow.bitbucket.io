using ClassLibrary.Representations;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.Utility;
using ClassLibrary.Agents;

namespace ClassLibrary.Representations
{
	public class StatisticalClassification : CodeList
	{
		public readonly Date[] releaseDate;

		public readonly DateRange[] validDates;

		public readonly Boolean[] isCurrent;

		public readonly Boolean[] isFloating;

		public readonly InternationalStructuredString[] changeFromBase;

		public readonly InternationalStructuredString[] purposeOfVariant;

		public readonly InternationalString[] copyright;

		public readonly InternationalStructuredString[] updateChanges;

		public readonly LanguageSpecification[] availableLanguage;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] rationale;

		public readonly InternationalStructuredString[] usage;

		public readonly ClassificationItemIndicator[] contains;

		public StatisticalClassification[] statisticalClassification;

		public StatisticalClassification[] statisticalClassification;

		public StatisticalClassification[] statisticalClassification;

		public StatisticalClassification statisticalClassification;

		public CorrespondenceTable[] correspondenceTable;

		public ExternalMaterial[] externalMaterial;

		public ClassificationIndex[] classificationIndex;

		public StatisticalClassificationIndicator statisticalClassificationIndicator;

		public StatisticalClassificationRelation[] statisticalClassificationRelation;

		public StatisticalClassificationRelation[] statisticalClassificationRelation;

		public Organization[] organization;

		public StatisticalClassification[] statisticalClassification;

		public StatisticalClassification[] statisticalClassification;

	}

}

