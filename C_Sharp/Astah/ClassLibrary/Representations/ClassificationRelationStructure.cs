using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;

namespace ClassLibrary.Representations
{
	public class ClassificationRelationStructure : Identifiable
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly LabelForDisplay[] displayLabel;

		public readonly CategoryRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public CodeList[] codeList;

	}

}

