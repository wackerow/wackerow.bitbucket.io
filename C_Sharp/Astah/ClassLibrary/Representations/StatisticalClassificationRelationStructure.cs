using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Representations
{
	public class StatisticalClassificationRelationStructure : Identifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly StatisticalClassificationRelation[] hasMemberRelation;

		public ClassificationSeries[] classificationSeries;

		public RelationStructure[] relationStructure;

	}

}

