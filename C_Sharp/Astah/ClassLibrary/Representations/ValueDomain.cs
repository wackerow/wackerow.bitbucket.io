using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Workflows;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.Representations
{
	public abstract class ValueDomain : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly ExternalControlledVocabularyEntry[] recommendedDataType;

		public Parameter parameter;

		public CustomItem customItem;

	}

}

