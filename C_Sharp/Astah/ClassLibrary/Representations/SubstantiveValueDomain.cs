using ClassLibrary.Representations;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Representations
{
	public class SubstantiveValueDomain : ValueDomain
	{
		public RepresentedVariable representedVariable;

		public ValueAndConceptDescription[] valueAndConceptDescription;

		public CodeList[] codeList;

		public SubstantiveConceptualDomain[] substantiveConceptualDomain;

	}

}

