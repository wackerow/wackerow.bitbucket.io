using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;
using ClassLibrary.Agents;
using ClassLibrary.Utility;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Representations
{
	public class CorrespondenceTable : AnnotatedIdentifiable
	{
		public readonly DateRange[] effectiveDates;

		public readonly Map[] correspondence;

		public LevelStructure[] levelStructure;

		public LevelStructure[] levelStructure;

		public StatisticalClassification[] statisticalClassification;

		public Agent[] agent;

		public ExternalMaterial[] externalMaterial;

		public Comparison[] comparison;

		public Agent[] agent;

		public Agent[] agent;

	}

}

