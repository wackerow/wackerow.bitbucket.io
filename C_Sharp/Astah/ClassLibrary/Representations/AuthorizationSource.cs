using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.StudyRelated;
using ClassLibrary.Agents;
using ClassLibrary.Representations;

namespace ClassLibrary.Representations
{
	public class AuthorizationSource : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] statementOfAuthorization;

		public readonly InternationalString[] legalMandate;

		public readonly Date[] authorizationDate;

		public readonly InternationalStructuredString[] purpose;

		public Study[] study;

		public Agent[] agent;

		public ClassificationItem[] classificationItem;

	}

}

