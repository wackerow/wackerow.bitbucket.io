using ClassLibrary.Identification;
using ClassLibrary.SignificationPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Representations
{
	public abstract class Designation : Identifiable
	{
		public readonly Signifier[] representation;

		public Concept[] concept;

		public Sign[] sign;

	}

}

