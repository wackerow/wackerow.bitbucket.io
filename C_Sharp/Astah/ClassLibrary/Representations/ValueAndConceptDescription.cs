using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.Conceptual;
using ClassLibrary.Representations;

namespace ClassLibrary.Representations
{
	public class ValueAndConceptDescription : Identifiable
	{
		public readonly InternationalStructuredString[] description;

		public readonly ExternalControlledVocabularyEntry[] logicalExpression;

		public readonly TypedString[] regularExpression;

		public readonly String[] minimumValueInclusive;

		public readonly String[] maximumValueInclusive;

		public readonly String[] minimumValueExclusive;

		public readonly String[] maximumValueExclusive;

		public readonly CategoryRelationCode[] classificationLevel;

		public readonly ExternalControlledVocabularyEntry[] formatPattern;

		public ConceptualDomain conceptualDomain;

		public SentinelValueDomain sentinelValueDomain;

		public SubstantiveValueDomain substantiveValueDomain;

	}

}

