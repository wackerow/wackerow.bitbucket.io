using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.Representations;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Representations
{
	public class IndexEntryRelationStructure : AnnotatedIdentifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly IndexEntryRelation[] hasMemberRelation;

		public ClassificationIndex[] classificationIndex;

		public RelationStructure[] relationStructure;

	}

}

