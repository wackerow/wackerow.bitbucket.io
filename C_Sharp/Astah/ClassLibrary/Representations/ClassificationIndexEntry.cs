using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Representations
{
	public class ClassificationIndexEntry : AnnotatedIdentifiable
	{
		public readonly InternationalString[] entry;

		public readonly DateRange[] validDates;

		public readonly CommandCode[] codingInstruction;

		public CollectionMember[] collectionMember;

		public ClassificationIndexEntryIndicator classificationIndexEntryIndicator;

		public IndexEntryRelation indexEntryRelation;

		public IndexEntryRelation indexEntryRelation;

	}

}

