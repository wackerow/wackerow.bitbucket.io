using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;
using ClassLibrary.Agents;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Representations
{
	public class ClassificationSeries : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] context;

		public readonly ExternalControlledVocabularyEntry[] objectsOrUnitsClassified;

		public readonly ExternalControlledVocabularyEntry[] subject;

		public readonly ExternalControlledVocabularyEntry[] keyword;

		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly StatisticalClassificationIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public StructuredCollection[] structuredCollection;

		public StatisticalClassificationRelationStructure[] statisticalClassificationRelationStructure;

		public Agent[] agent;

		public Concept[] concept;

		public ClassificationSeriesIndicator classificationSeriesIndicator;

		public ClassificationSeriesRelation classificationSeriesRelation;

		public ClassificationSeriesRelation[] classificationSeriesRelation;

	}

}

