using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;

namespace ClassLibrary.Representations
{
	public class LevelStructure : AnnotatedIdentifiable
	{
		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] usage;

		public readonly Level[] containsLevel;

		public readonly DateRange[] validDateRange;

		public CorrespondenceTable correspondenceTable;

		public CorrespondenceTable correspondenceTable;

		public CodeList codeList;

	}

}

