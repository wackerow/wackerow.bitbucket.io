using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;

namespace ClassLibrary.Representations
{
	public class ClassificationSeriesRelationStructure : Identifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly ClassificationSeriesRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public ClassificationFamily[] classificationFamily;

	}

}

