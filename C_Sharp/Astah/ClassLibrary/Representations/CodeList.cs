using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Representations;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.DataCapture;

namespace ClassLibrary.Representations
{
	public class CodeList : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly CodeIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public LevelStructure[] levelStructure;

		public ClassificationRelationStructure classificationRelationStructure;

		public Concept[] concept;

		public StructuredCollection[] structuredCollection;

		public SentinelValueDomain sentinelValueDomain;

		public CategorySet[] categorySet;

		public SubstantiveValueDomain substantiveValueDomain;

		public CodeListResponseDomain codeListResponseDomain;

	}

}

