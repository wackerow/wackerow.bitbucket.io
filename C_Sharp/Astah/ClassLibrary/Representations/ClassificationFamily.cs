using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Representations
{
	public class ClassificationFamily : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly ClassificationSeriesIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public StructuredCollection[] structuredCollection;

		public ClassificationSeriesRelationStructure[] classificationSeriesRelationStructure;

		public Concept[] concept;

		public ClassificationIndex[] classificationIndex;

	}

}

