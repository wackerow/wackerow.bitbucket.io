using ClassLibrary.Representations;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Representations
{
	public class ClassificationItem : Code
	{
		public readonly Boolean[] isValid;

		public readonly Boolean[] isGenerated;

		public readonly InternationalStructuredString[] explanatoryNotes;

		public readonly InternationalString[] futureNotes;

		public readonly InternationalString[] changeLog;

		public readonly InternationalString[] changeFromPreviousVersion;

		public readonly DateRange[] validDates;

		public readonly ObjectName[] name;

		public AuthorizationSource[] authorizationSource;

		public ClassificationItemIndicator classificationItemIndicator;

		public ClassificationItem[] classificationItem;

		public ClassificationItem[] classificationItem;

	}

}

