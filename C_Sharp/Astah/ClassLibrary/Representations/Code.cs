using ClassLibrary.Representations;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.DataCapture;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Representations
{
	public class Code : Designation
	{
		public readonly ValueString representation;

		public CodeListResponseDomain codeListResponseDomain;

		public Category[] category;

		public CodeIndicator codeIndicator;

	}

}

