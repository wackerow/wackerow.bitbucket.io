using ClassLibrary.Representations;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Representations
{
	public class SentinelValueDomain : ValueDomain
	{
		public readonly ExternalControlledVocabularyEntry[] platformType;

		public SentinelConceptualDomain[] sentinelConceptualDomain;

		public ValueAndConceptDescription[] valueAndConceptDescription;

		public CodeList[] codeList;

		public InstanceVariable instanceVariable;

		public RepresentedVariable[] representedVariable;

	}

}

