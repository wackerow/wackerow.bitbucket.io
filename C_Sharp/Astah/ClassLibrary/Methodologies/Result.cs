using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Workflows;
using ClassLibrary.Methodologies;

namespace ClassLibrary.Methodologies
{
	public abstract class Result : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public readonly Binding[] hasBinding;

		public Parameter[] parameter;

		public Precondition[] precondition;

		public AppliedUse[] appliedUse;

		public Goal[] goal;

		public Parameter[] parameter;

	}

}

