using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Methodologies;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Methodologies
{
	public class AppliedUse : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public AnnotatedIdentifiable[] annotatedIdentifiable;

		public Guide[] guide;

		public UnitType[] unitType;

		public Result[] result;

	}

}

