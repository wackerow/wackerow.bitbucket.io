using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Utility;
using ClassLibrary.Methodologies;

namespace ClassLibrary.Methodologies
{
	public class Guide : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public ExternalMaterial[] externalMaterial;

		public AppliedUse[] appliedUse;

	}

}

