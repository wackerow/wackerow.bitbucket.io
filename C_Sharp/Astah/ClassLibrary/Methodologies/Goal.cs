using ClassLibrary.Methodologies;
using ClassLibrary.Utility;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.MethodologyPattern;

namespace ClassLibrary.Methodologies
{
	public class Goal : BusinessFunction
	{
		public ExternalMaterial[] externalMaterial;

		public DesignOverview[] designOverview;

		public Result[] result;

		public Design[] design;

	}

}

