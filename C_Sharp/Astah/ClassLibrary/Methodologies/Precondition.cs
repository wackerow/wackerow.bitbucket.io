using ClassLibrary.Methodologies;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.Utility;

namespace ClassLibrary.Methodologies
{
	public class Precondition : BusinessFunction
	{
		public DesignOverview[] designOverview;

		public Design[] design;

		public Result[] result;

		public ExternalMaterial[] externalMaterial;

	}

}

