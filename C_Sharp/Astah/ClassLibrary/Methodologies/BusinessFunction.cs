using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Methodologies
{
	public abstract class BusinessFunction : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

	}

}

