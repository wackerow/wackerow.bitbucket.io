using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Agents;
using ClassLibrary.StudyRelated;
using ClassLibrary.Utility;

namespace ClassLibrary.Utility
{
	public class FundingInformation : Identifiable
	{
		public readonly ExternalControlledVocabularyEntry[] funderRole;

		public readonly String[] grantNumber;

		public readonly InternationalStructuredString[] purpose;

		public Agent[] agent;

		public Study[] study;

		public DocumentInformation[] documentInformation;

	}

}

