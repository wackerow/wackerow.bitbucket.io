using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Utility;
using ClassLibrary.Discovery;

namespace ClassLibrary.Utility
{
	public class DocumentInformation : AnnotatedIdentifiable
	{
		public readonly TypedDescriptiveText[] contentCoverage;

		public readonly Boolean isPublished;

		public readonly UnlimitedNatural[] hasPrimaryContent;

		public readonly DDI4Version ofType;

		public Access[] access;

		public Coverage[] coverage;

		public FundingInformation[] fundingInformation;

		public ExternalMaterial[] externalMaterial;

		public Access[] access;

	}

}

