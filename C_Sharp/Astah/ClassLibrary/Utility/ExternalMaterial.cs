using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.StudyRelated;
using ClassLibrary.Methodologies;
using ClassLibrary.DataCapture;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.Representations;
using ClassLibrary.Utility;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.Utility
{
	public class ExternalMaterial : Identifiable
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfMaterial;

		public readonly InternationalStructuredString[] descriptiveText;

		public readonly anyUri[] uri;

		public readonly InternationalStructuredString[] relationshipDescription;

		public readonly ExternalControlledVocabularyEntry[] mimeType;

		public readonly Segment[] usesSegment;

		public readonly Annotation[] citationOfExternalMaterial;

		public DesignOverview[] designOverview;

		public Standard[] standard;

		public AlgorithmOverview[] algorithmOverview;

		public Guide[] guide;

		public Instruction[] instruction;

		public Goal[] goal;

		public Algorithm[] algorithm;

		public Design[] design;

		public ClassificationIndex[] classificationIndex;

		public Budget[] budget;

		public StatisticalClassification[] statisticalClassification;

		public CorrespondenceTable[] correspondenceTable;

		public DocumentInformation[] documentInformation;

		public MethodologyOverview[] methodologyOverview;

		public SamplingProcess[] samplingProcess;

		public AnnotatedIdentifiable[] annotatedIdentifiable;

		public Precondition[] precondition;

	}

}

