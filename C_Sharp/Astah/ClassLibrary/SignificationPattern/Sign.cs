using ClassLibrary.CollectionsPattern;
using ClassLibrary.SignificationPattern;
using ClassLibrary.Representations;

namespace ClassLibrary.SignificationPattern
{
	public abstract class Sign : CollectionMember
	{
		public readonly Signifier representation;

		public Designation designation;

		public Signified[] signified;

	}

}

