using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;
using ClassLibrary.SignificationPattern;

namespace ClassLibrary.SignificationPattern
{
	public abstract class Signified : CollectionMember
	{
		public Concept concept;

		public Sign sign;

	}

}

