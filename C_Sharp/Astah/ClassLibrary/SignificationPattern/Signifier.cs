using Primitive Package;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.SignificationPattern
{
	public abstract class Signifier
	{
		public readonly String[] content;

		public readonly WhiteSpaceRule[] whiteSpace;

		public ValueString valueString;

	}

}

