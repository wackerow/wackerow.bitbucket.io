using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.ProcessPattern;

namespace ClassLibrary.MethodologyPattern
{
	public abstract class Methodology : Identifiable
	{
		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] usage;

		public readonly InternationalStructuredString[] rationale;

		public readonly InternationalStructuredString[] overview;

		public Design[] design;

		public MethodologyOverview methodologyOverview;

		public Methodology[] methodology;

		public Methodology[] methodology;

		public Algorithm[] algorithm;

		public Process[] process;

	}

}

