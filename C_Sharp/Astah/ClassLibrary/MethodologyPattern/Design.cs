using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Methodologies;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.Utility;
using ClassLibrary.ProcessPattern;
using ClassLibrary.SimpleMethodologyOverview;

namespace ClassLibrary.MethodologyPattern
{
	public abstract class Design : Identifiable
	{
		public readonly InternationalStructuredString[] overview;

		public Precondition[] precondition;

		public Algorithm[] algorithm;

		public ExternalMaterial[] externalMaterial;

		public Process[] process;

		public Methodology[] methodology;

		public DesignOverview designOverview;

		public Goal[] goal;

	}

}

