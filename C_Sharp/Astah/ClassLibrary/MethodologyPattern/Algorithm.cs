using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.Utility;
using ClassLibrary.ProcessPattern;

namespace ClassLibrary.MethodologyPattern
{
	public abstract class Algorithm : Identifiable
	{
		public readonly InternationalStructuredString[] overview;

		public AlgorithmOverview algorithmOverview;

		public Design[] design;

		public ExternalMaterial[] externalMaterial;

		public Process[] process;

		public Methodology[] methodology;

	}

}

