using ClassLibrary.Workflows;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.BusinessWorkflow
{
	public class BusinessProcess : WorkflowProcess
	{
		public readonly PairedExternalControlledVocabularyEntry[] standardModelUsed;

		public readonly BusinessProcessCondition[] preCondition;

		public readonly BusinessProcessCondition[] postCondition;

		public BusinessProcessIndicator businessProcessIndicator;

	}

}

