using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.BusinessWorkflow
{
	public class DataPipeline : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly BusinessProcessIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public SimpleCollection[] simpleCollection;

		public Concept[] concept;

		public Study[] study;

	}

}

