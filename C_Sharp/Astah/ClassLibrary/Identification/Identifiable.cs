using Primitive Package;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Identification
{
	public abstract class Identifiable
	{
		public readonly String agency;

		public readonly String id;

		public readonly String version;

		public readonly String[] versionResponsibility;

		public readonly String[] versionRationale;

		public readonly IsoDateType[] versionDate;

		public readonly Boolean isUniversallyUnique;

		public readonly Boolean isPersistent;

		public readonly LocalIdFormat[] localId;

		public readonly BasedOnObjectInformation[] basedOnObject;

		public BasedOnObjectInformation[] basedOnObjectInformation;

		public DescribedRelationship describedRelationship;

	}

}

