using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Methodologies;
using ClassLibrary.Utility;

namespace ClassLibrary.Identification
{
	public abstract class AnnotatedIdentifiable : Identifiable
	{
		public readonly Annotation[] hasAnnotation;

		public AppliedUse[] appliedUse;

		public ExternalMaterial[] externalMaterial;

	}

}

