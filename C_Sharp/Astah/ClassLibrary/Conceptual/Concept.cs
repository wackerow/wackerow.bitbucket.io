using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;
using ClassLibrary.BusinessWorkflow;
using ClassLibrary.Workflows;
using ClassLibrary.SignificationPattern;
using ClassLibrary.CustomMetadata;
using ClassLibrary.StudyRelated;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Conceptual;
using ClassLibrary.FormatDescription;
using ClassLibrary.DataCapture;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Agents;

namespace ClassLibrary.Conceptual
{
	public class Concept : AnnotatedIdentifiable
	{
		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] definition;

		public ClassificationIndex[] classificationIndex;

		public ClassificationSeries[] classificationSeries;

		public DataPipeline[] dataPipeline;

		public WorkflowStepSequence[] workflowStepSequence;

		public ClassificationFamily[] classificationFamily;

		public Signified[] signified;

		public CustomStructure[] customStructure;

		public ComplianceStatement[] complianceStatement;

		public CodeList[] codeList;

		public CustomItem customItem;

		public Designation designation;

		public ViewpointRole[] viewpointRole;

		public Universe[] universe;

		public VariableCollection[] variableCollection;

		public Map[] map;

		public Map[] map;

		public PhysicalDataSet[] physicalDataSet;

		public PhysicalRecordSegment[] physicalRecordSegment;

		public ConceptIndicator conceptIndicator;

		public Capture[] capture;

		public ConceptRelation conceptRelation;

		public ConceptRelation conceptRelation;

		public Study[] study;

		public SimpleCollection[] simpleCollection;

		public ConceptSystem[] conceptSystem;

		public DataStore[] dataStore;

		public DataStoreLibrary[] dataStoreLibrary;

		public ConceptualVariable[] conceptualVariable;

		public Population[] population;

		public LogicalRecord[] logicalRecord;

		public AgentListing[] agentListing;

		public UnitType[] unitType;

		public UnitDataRecord[] unitDataRecord;

		public CustomInstance[] customInstance;

		public ControlledVocabulary[] controlledVocabulary;

		public StudySeries[] studySeries;

		public PhysicalSegmentLayout[] physicalSegmentLayout;

		public Level level;

	}

}

