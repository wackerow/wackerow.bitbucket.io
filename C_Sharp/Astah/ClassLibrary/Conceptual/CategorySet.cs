using ClassLibrary.Conceptual;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;

namespace ClassLibrary.Conceptual
{
	public class CategorySet : ConceptSystem
	{
		public readonly CategoryIndicator[] contains;

		public CategoryRelationStructure[] categoryRelationStructure;

		public CodeList codeList;

	}

}

