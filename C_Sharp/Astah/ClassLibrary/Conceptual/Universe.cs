using ClassLibrary.Conceptual;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.Conceptual
{
	public class Universe : Concept
	{
		public readonly Boolean[] isInclusive;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] descriptiveText;

		public Population population;

		public Concept[] concept;

		public TargetSample[] targetSample;

		public UnitType[] unitType;

		public Study[] study;

		public RepresentedVariable representedVariable;

	}

}

