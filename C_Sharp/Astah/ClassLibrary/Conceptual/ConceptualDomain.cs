using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Conceptual
{
	public abstract class ConceptualDomain : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public ValueAndConceptDescription[] valueAndConceptDescription;

		public ConceptSystem[] conceptSystem;

	}

}

