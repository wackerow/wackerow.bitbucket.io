using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Conceptual
{
	public class ConceptSystemCorrespondence : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] purpose;

		public readonly InternationalStructuredString[] usage;

		public readonly Map[] correspondence;

		public ConceptSystem[] conceptSystem;

		public Comparison[] comparison;

	}

}

