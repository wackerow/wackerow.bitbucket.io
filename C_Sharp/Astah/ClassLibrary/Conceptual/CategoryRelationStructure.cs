using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Conceptual
{
	public class CategoryRelationStructure : Identifiable
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType totality;

		public readonly CategoryRelation[] hasMemberRelation;

		public CategorySet[] categorySet;

		public RelationStructure[] relationStructure;

	}

}

