using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Conceptual
{
	public class Unit : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly ObjectName[] name;

		public Population[] population;

		public UnitType[] unitType;

	}

}

