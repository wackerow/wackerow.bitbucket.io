using ClassLibrary.Conceptual;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Discovery;
using ClassLibrary.Methodologies;
using ClassLibrary.SamplingMethodology;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.Conceptual
{
	public class UnitType : Concept
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] descriptiveText;

		public TargetSample[] targetSample;

		public SpatialCoverage[] spatialCoverage;

		public SpatialCoverage[] spatialCoverage;

		public AppliedUse[] appliedUse;

		public SpatialCoverage[] spatialCoverage;

		public Unit[] unit;

		public SampleFrame[] sampleFrame;

		public Universe universe;

		public SamplePopulationResult samplePopulationResult;

		public SampleFrame[] sampleFrame;

		public GeographicUnitTypeRelation[] geographicUnitTypeRelation;

		public GeographicUnitTypeRelation geographicUnitTypeRelation;

		public Concept concept;

		public GeographicUnitTypeIndicator geographicUnitTypeIndicator;

		public ConceptualVariable conceptualVariable;

		public Study[] study;

	}

}

