using ClassLibrary.Conceptual;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Conceptual
{
	public class ConceptualVariable : Concept
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] descriptiveText;

		public SentinelConceptualDomain[] sentinelConceptualDomain;

		public InstanceVariable instanceVariable;

		public Concept[] concept;

		public RepresentedVariable representedVariable;

		public VariableRelation[] variableRelation;

		public VariableRelation[] variableRelation;

		public UnitType[] unitType;

		public VariableIndicator variableIndicator;

		public SubstantiveConceptualDomain[] substantiveConceptualDomain;

	}

}

