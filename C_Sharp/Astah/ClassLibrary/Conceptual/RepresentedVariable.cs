using ClassLibrary.Conceptual;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;
using ClassLibrary.CustomMetadata;
using ClassLibrary.DataCapture;

namespace ClassLibrary.Conceptual
{
	public class RepresentedVariable : ConceptualVariable
	{
		public readonly String[] unitOfMeasurement;

		public readonly ExternalControlledVocabularyEntry[] hasIntendedDataType;

		public SubstantiveValueDomain[] substantiveValueDomain;

		public CustomItem customItem;

		public ResponseDomain responseDomain;

		public Universe[] universe;

		public RepresentedQuestion[] representedQuestion;

		public RepresentedMeasurement representedMeasurement;

		public ConceptualVariable[] conceptualVariable;

		public SentinelValueDomain[] sentinelValueDomain;

		public InstanceVariable instanceVariable;

	}

}

