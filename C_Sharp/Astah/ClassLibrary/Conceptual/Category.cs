using ClassLibrary.Conceptual;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Representations;
using ClassLibrary.DataCapture;

namespace ClassLibrary.Conceptual
{
	public class Category : Concept
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] descriptiveText;

		public Code code;

		public CategoryRelation[] categoryRelation;

		public CategoryRelation categoryRelation;

		public BooleanResponseDomain booleanResponseDomain;

		public CategoryIndicator categoryIndicator;

	}

}

