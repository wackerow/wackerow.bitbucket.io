using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.Conceptual
{
	public class VariableCollection : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public readonly ExternalControlledVocabularyEntry[] groupingSemantic;

		public readonly VariableIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public Concept[] concept;

		public VariableRelationStructure[] variableRelationStructure;

		public StructuredCollection[] structuredCollection;

		public Study[] study;

	}

}

