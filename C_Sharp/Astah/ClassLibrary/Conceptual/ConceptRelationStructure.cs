using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Conceptual
{
	public class ConceptRelationStructure : Identifiable
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly ConceptRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public ConceptSystem[] conceptSystem;

	}

}

