using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.Conceptual
{
	public class ConceptSystem : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly ConceptIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public ConceptualDomain conceptualDomain;

		public ConceptSystemCorrespondence[] conceptSystemCorrespondence;

		public StructuredCollection[] structuredCollection;

		public Concept[] concept;

		public ConceptRelationStructure[] conceptRelationStructure;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

		public GeographicUnitClassification[] geographicUnitClassification;

	}

}

