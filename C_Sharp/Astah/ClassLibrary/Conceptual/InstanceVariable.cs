using ClassLibrary.Conceptual;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.StudyRelated;
using ClassLibrary.CustomMetadata;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.SimpleCodebook;
using ClassLibrary.Representations;
using ClassLibrary.Workflows;
using ClassLibrary.DataCapture;

namespace ClassLibrary.Conceptual
{
	public class InstanceVariable : RepresentedVariable
	{
		public readonly InternationalStructuredString[] variableRole;

		public readonly ExternalControlledVocabularyEntry[] physicalDataType;

		public readonly ExternalControlledVocabularyEntry[] platformType;

		public Study[] study;

		public CustomValue customValue;

		public Datum datum;

		public VariableStatistics variableStatistics;

		public VariableStatistics variableStatistics;

		public DataPoint dataPoint;

		public InstanceVariableIndicator instanceVariableIndicator;

		public InstanceVariableRelation[] instanceVariableRelation;

		public InstanceVariableRelation instanceVariableRelation;

		public Population[] population;

		public SentinelValueDomain[] sentinelValueDomain;

		public ConceptualVariable[] conceptualVariable;

		public InstanceVariableValueMap[] instanceVariableValueMap;

		public InstanceVariableValueMap[] instanceVariableValueMap;

		public ViewpointRoleRelation[] viewpointRoleRelation;

		public ViewpointRoleRelation viewpointRoleRelation;

		public Act[] act;

		public VariableStatistics variableStatistics;

		public RepresentedVariable[] representedVariable;

		public Capture[] capture;

	}

}

