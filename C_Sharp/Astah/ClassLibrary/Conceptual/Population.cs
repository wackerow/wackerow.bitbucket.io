using ClassLibrary.Conceptual;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.StudyRelated;
using ClassLibrary.FormatDescription;
using ClassLibrary.SamplingMethodology;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.Conceptual
{
	public class Population : Concept
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] descriptiveText;

		public readonly DateRange[] timePeriodOfPopulation;

		public Study[] study;

		public PhysicalRecordSegment physicalRecordSegment;

		public Universe[] universe;

		public Unit[] unit;

		public SamplePopulationResult samplePopulationResult;

		public SampleFrame[] sampleFrame;

		public InstanceVariable instanceVariable;

		public Concept[] concept;

		public GeographicUnit[] geographicUnit;

	}

}

