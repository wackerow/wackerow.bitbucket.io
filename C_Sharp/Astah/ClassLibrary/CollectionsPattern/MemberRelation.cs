using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.CollectionsPattern
{
	public abstract class MemberRelation
	{
		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public CollectionMember[] collectionMember;

		public CollectionMember[] collectionMember;

		public CustomValueRelation customValueRelation;

		public DataPointRelation dataPointRelation;

		public DataStoreRelation dataStoreRelation;

		public PhysicalRecordSegmentRelation physicalRecordSegmentRelation;

		public CategoryRelation categoryRelation;

		public ClassificationSeriesRelation classificationSeriesRelation;

		public LogicalRecordRelation logicalRecordRelation;

		public StatisticalClassificationRelation statisticalClassificationRelation;

		public ConceptRelation conceptRelation;

		public CustomItemRelation customItemRelation;

		public InstanceVariableRelation instanceVariableRelation;

		public IndexEntryRelation indexEntryRelation;

		public GeographicUnitTypeRelation geographicUnitTypeRelation;

		public WorkflowStepRelation workflowStepRelation;

		public VocabularyEntryRelation vocabularyEntryRelation;

		public AgentRelation agentRelation;

		public ValueMappingRelation valueMappingRelation;

		public StudyRelation studyRelation;

		public ViewpointRoleRelation viewpointRoleRelation;

		public GeographicUnitRelation geographicUnitRelation;

		public VariableRelation variableRelation;

	}

}

