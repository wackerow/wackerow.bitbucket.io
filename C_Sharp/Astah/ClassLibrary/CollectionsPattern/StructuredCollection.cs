using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;
using ClassLibrary.StudyRelated;
using ClassLibrary.CustomMetadata;
using ClassLibrary.Workflows;
using ClassLibrary.Conceptual;
using ClassLibrary.FormatDescription;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Agents;

namespace ClassLibrary.CollectionsPattern
{
	public abstract class StructuredCollection : SimpleCollection
	{
		public ClassificationIndex classificationIndex;

		public ClassificationSeries classificationSeries;

		public StudySeries studySeries;

		public ClassificationFamily classificationFamily;

		public CustomStructure customStructure;

		public StructuredWorkflowSteps structuredWorkflowSteps;

		public CodeList codeList;

		public RelationStructure[] relationStructure;

		public VariableCollection variableCollection;

		public PhysicalDataSet physicalDataSet;

		public PhysicalRecordSegment physicalRecordSegment;

		public ConceptSystem conceptSystem;

		public DataStoreLibrary dataStoreLibrary;

		public UnitDataRecord unitDataRecord;

		public ControlledVocabulary controlledVocabulary;

		public AgentListing agentListing;

		public DataStore dataStore;

		public PhysicalSegmentLayout physicalSegmentLayout;

	}

}

