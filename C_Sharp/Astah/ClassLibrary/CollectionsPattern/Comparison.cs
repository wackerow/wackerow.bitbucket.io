using ClassLibrary.Identification;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;
using ClassLibrary.Conceptual;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.CollectionsPattern
{
	public abstract class Comparison : Identifiable
	{
		public readonly ComparisonMap[] correspondence;

		public CorrespondenceTable correspondenceTable;

		public ConceptSystemCorrespondence conceptSystemCorrespondence;

		public RecordRelation recordRelation;

		public SimpleCollection[] simpleCollection;

	}

}

