using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;
using ClassLibrary.StudyRelated;
using ClassLibrary.Conceptual;
using ClassLibrary.CustomMetadata;
using ClassLibrary.Workflows;
using ClassLibrary.FormatDescription;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Agents;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.CollectionsPattern
{
	public abstract class RelationStructure : Identifiable
	{
		public readonly TotalityType totality;

		public readonly RelationSpecification hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly MemberRelation[] hasMemberRelation;

		public ClassificationRelationStructure classificationRelationStructure;

		public ClassificationSeriesRelationStructure classificationSeriesRelationStructure;

		public StudyRelationStructure studyRelationStructure;

		public CategoryRelationStructure categoryRelationStructure;

		public ConceptRelationStructure conceptRelationStructure;

		public CustomItemRelationStructure customItemRelationStructure;

		public VocabularyRelationStructure vocabularyRelationStructure;

		public WorkflowStepRelationStructure workflowStepRelationStructure;

		public IndexEntryRelationStructure indexEntryRelationStructure;

		public StatisticalClassificationRelationStructure statisticalClassificationRelationStructure;

		public StructuredCollection[] structuredCollection;

		public VariableRelationStructure variableRelationStructure;

		public DataPointRelationStructure dataPointRelationStructure;

		public PhysicalOrderRelationStructure physicalOrderRelationStructure;

		public PhysicalLayoutRelationStructure physicalLayoutRelationStructure;

		public DataStoreRelationStructure dataStoreRelationStructure;

		public InstanceVariableRelationStructure instanceVariableRelationStructure;

		public LogicalRecordRelationStructure logicalRecordRelationStructure;

		public AgentRelationStructure agentRelationStructure;

		public GeographicUnitRelationStructure geographicUnitRelationStructure;

		public GeographicUnitTypeRelationStructure geographicUnitTypeRelationStructure;

	}

}

