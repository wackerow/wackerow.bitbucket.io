using ClassLibrary.Identification;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Representations;
using ClassLibrary.StudyRelated;
using ClassLibrary.SimpleCodebook;
using ClassLibrary.CustomMetadata;
using ClassLibrary.Agents;
using ClassLibrary.FormatDescription;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.CollectionsPattern
{
	public abstract class CollectionMember : Identifiable
	{
		public MemberRelation[] memberRelation;

		public MemberRelation memberRelation;

		public ClassificationIndexEntry classificationIndexEntry;

		public Study study;

		public VariableStatistics variableStatistics;

		public CustomItem customItem;

		public CustomValue customValue;

		public VocabularyEntry vocabularyEntry;

		public MemberIndicator memberIndicator;

		public ComparisonMap[] comparisonMap;

		public ComparisonMap[] comparisonMap;

		public Agent agent;

		public ValueMapping valueMapping;

		public DataPoint dataPoint;

	}

}

