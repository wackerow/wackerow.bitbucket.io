using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.CollectionsPattern
{
	public abstract class MemberIndicator
	{
		public readonly Integer[] index;

		public CollectionMember[] collectionMember;

		public CustomValueIndicator customValueIndicator;

		public DataPointIndicator dataPointIndicator;

		public DataStoreIndicator dataStoreIndicator;

		public PhysicalRecordSegmentIndicator physicalRecordSegmentIndicator;

		public ClassificationIndexEntryIndicator classificationIndexEntryIndicator;

		public ClassificationSeriesIndicator classificationSeriesIndicator;

		public ClassificationItemIndicator classificationItemIndicator;

		public CodeIndicator codeIndicator;

		public ConceptIndicator conceptIndicator;

		public StatisticalClassificationIndicator statisticalClassificationIndicator;

		public CustomItemIndicator customItemIndicator;

		public InstanceVariableIndicator instanceVariableIndicator;

		public WorkflowStepSequenceIndicator workflowStepSequenceIndicator;

		public GeographicUnitTypeIndicator geographicUnitTypeIndicator;

		public LogicalRecordIndicator logicalRecordIndicator;

		public AgentIndicator agentIndicator;

		public BusinessProcessIndicator businessProcessIndicator;

		public CategoryIndicator categoryIndicator;

		public ValueMappingIndicator valueMappingIndicator;

		public StudyIndicator studyIndicator;

		public GeographicUnitIndicator geographicUnitIndicator;

		public VocabularyEntryIndicator vocabularyEntryIndicator;

		public VariableIndicator variableIndicator;

	}

}

