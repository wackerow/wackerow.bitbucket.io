using ClassLibrary.CollectionsPattern;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.BusinessWorkflow;
using ClassLibrary.Workflows;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Conceptual;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.CollectionsPattern
{
	public abstract class SimpleCollection : CollectionMember
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly MemberIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public DataPipeline dataPipeline;

		public WorkflowStepSequence workflowStepSequence;

		public ViewpointRole viewpointRole;

		public Concept[] concept;

		public LogicalRecord logicalRecord;

		public CustomInstance customInstance;

		public Comparison[] comparison;

	}

}

