using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Utility;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.StudyRelated
{
	public class Budget : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public readonly ObjectName[] name;

		public ExternalMaterial[] externalMaterial;

		public Study[] study;

	}

}

