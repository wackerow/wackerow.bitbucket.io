using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using Primitive Package;
using ClassLibrary.StudyRelated;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Conceptual;

namespace ClassLibrary.StudyRelated
{
	public class StudySeries : AnnotatedIdentifiable
	{
		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] overview;

		public readonly CollectionType[] type;

		public readonly InternationalStructuredString[] purpose;

		public readonly StudyIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public StudyRelationStructure[] studyRelationStructure;

		public StructuredCollection[] structuredCollection;

		public Study[] study;

		public DataStoreLibrary dataStoreLibrary;

		public Concept[] concept;

	}

}

