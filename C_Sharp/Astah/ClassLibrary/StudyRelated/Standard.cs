using ClassLibrary.Identification;
using ClassLibrary.Utility;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.StudyRelated
{
	public class Standard : AnnotatedIdentifiable
	{
		public ExternalMaterial[] externalMaterial;

		public ComplianceStatement complianceStatement;

		public QualityStatement qualityStatement;

	}

}

