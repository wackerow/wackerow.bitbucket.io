using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.StudyRelated
{
	public class ComplianceStatement : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] externalComplianceCode;

		public readonly InternationalStructuredString[] usage;

		public Concept concept;

		public Standard[] standard;

	}

}

