using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.StudyRelated
{
	public class QualityStatement : AnnotatedIdentifiable
	{
		public readonly ObjectName[] name;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] overview;

		public readonly InternationalStructuredString[] rationale;

		public readonly InternationalStructuredString[] usage;

		public Study study;

		public Standard[] standard;

	}

}

