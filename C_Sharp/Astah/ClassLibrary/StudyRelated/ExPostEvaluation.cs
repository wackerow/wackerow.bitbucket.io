using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.StudyRelated;
using ClassLibrary.Agents;

namespace ClassLibrary.StudyRelated
{
	public class ExPostEvaluation : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfEvaluation;

		public readonly InternationalStructuredString[] evaluationProcess;

		public readonly InternationalStructuredString[] outcomes;

		public readonly Date[] completionDate;

		public Study[] study;

		public Agent[] agent;

	}

}

