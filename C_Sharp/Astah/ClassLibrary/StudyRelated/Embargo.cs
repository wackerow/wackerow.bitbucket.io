using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Agents;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.StudyRelated
{
	public class Embargo : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly DateRange[] embargoDates;

		public readonly InternationalStructuredString[] rationale;

		public Agent[] agent;

		public Study study;

		public Agent[] agent;

	}

}

