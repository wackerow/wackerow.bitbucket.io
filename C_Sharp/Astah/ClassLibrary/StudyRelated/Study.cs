using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.StudyRelated;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;
using ClassLibrary.Utility;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.Representations;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Discovery;
using ClassLibrary.DataCapture;
using ClassLibrary.SamplingMethodology;
using ClassLibrary.BusinessWorkflow;

namespace ClassLibrary.StudyRelated
{
	public class Study : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] kindOfData;

		public readonly InternationalStructuredString[] overview;

		public readonly InternationalStructuredString[] bibliographicCitation;

		public StudyControl[] studyControl;

		public CollectionMember[] collectionMember;

		public InstanceVariable[] instanceVariable;

		public Population[] population;

		public FundingInformation[] fundingInformation;

		public DesignOverview[] designOverview;

		public AuthorizationSource[] authorizationSource;

		public QualityStatement[] qualityStatement;

		public DataStore[] dataStore;

		public StudySeries[] studySeries;

		public Access[] access;

		public ImplementedInstrument[] implementedInstrument;

		public SamplingProcedure[] samplingProcedure;

		public ExPostEvaluation exPostEvaluation;

		public Universe[] universe;

		public Embargo[] embargo;

		public Concept[] concept;

		public MethodologyOverview[] methodologyOverview;

		public Budget[] budget;

		public StudyIndicator studyIndicator;

		public DataPipeline dataPipeline;

		public StudyRelation[] studyRelation;

		public StudyRelation studyRelation;

		public VariableCollection[] variableCollection;

		public AlgorithmOverview[] algorithmOverview;

		public ProcessOverview[] processOverview;

		public Coverage[] coverage;

		public UnitType[] unitType;

	}

}

