using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.StudyRelated;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.StudyRelated
{
	public class StudyRelationStructure : Identifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly StudyRelation[] hasMemberRelation;

		public StudySeries[] studySeries;

		public RelationStructure[] relationStructure;

	}

}

