using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Workflows;

namespace ClassLibrary.Workflows
{
	public class WorkflowStepRelationStructure : AnnotatedIdentifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly WorkflowStepRelation[] hasMemberRelation;

		public readonly TemporalRelationSpecification[] hasTemporalRelationSpecification;

		public RelationStructure[] relationStructure;

		public StructuredWorkflowSteps[] structuredWorkflowSteps;

	}

}

