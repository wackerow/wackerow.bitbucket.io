using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Workflows;
using ClassLibrary.ProcessPattern;

namespace ClassLibrary.Workflows
{
	public abstract class WorkflowStep : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly InternationalStructuredString[] usage;

		public readonly InternationalStructuredString[] overview;

		public readonly Binding[] hasInformationFlow;

		public Parameter[] parameter;

		public Parameter[] parameter;

		public WorkflowService[] workflowService;

		public Split[] split;

		public SplitJoin[] splitJoin;

		public ProcessStep[] processStep;

		public WorkflowStepIndicator workflowStepIndicator;

		public WorkflowStepRelation[] workflowStepRelation;

		public WorkflowStepRelation workflowStepRelation;

	}

}

