using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using Primitive Package;
using ClassLibrary.Workflows;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.Workflows
{
	public class WorkflowStepSequence : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] typeOfWorkflowStepSequence;

		public readonly CollectionType[] type;

		public readonly WorkflowStepIndicator[] contains;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly Boolean[] isOrdered;

		public IfThenElse[] ifThenElse;

		public Concept[] concept;

		public SimpleCollection[] simpleCollection;

		public WorkflowStepSequenceIndicator workflowStepSequenceIndicator;

		public SamplingProcess[] samplingProcess;

		public ConditionalControlConstruct[] conditionalControlConstruct;

		public ElseIfAction elseIfAction;

		public WorkflowProcess[] workflowProcess;

	}

}

