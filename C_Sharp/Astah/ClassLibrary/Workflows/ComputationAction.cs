using ClassLibrary.Workflows;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Workflows
{
	public class ComputationAction : Act
	{
		public readonly InternationalStructuredString[] activityDescription;

		public readonly CommandCode[] usesCommandCode;

		public readonly ExternalControlledVocabularyEntry[] typeOfComputation;

	}

}

