using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Workflows;
using ClassLibrary.ProcessPattern;
using ClassLibrary.Agents;

namespace ClassLibrary.Workflows
{
	public class WorkflowService : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] serviceInterface;

		public readonly ExternalControlledVocabularyEntry[] serviceLocation;

		public readonly Date[] estimatedDuration;

		public WorkflowStep workflowStep;

		public Service[] service;

		public Agent[] agent;

	}

}

