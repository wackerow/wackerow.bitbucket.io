using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.ProcessPattern;
using ClassLibrary.Workflows;

namespace ClassLibrary.Workflows
{
	public class WorkflowProcess : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly InternationalStructuredString[] usage;

		public DesignOverview[] designOverview;

		public AlgorithmOverview[] algorithmOverview;

		public Process[] process;

		public WorkflowStepSequence workflowStepSequence;

	}

}

