using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Methodologies;
using ClassLibrary.Workflows;
using ClassLibrary.DataCapture;
using ClassLibrary.Representations;
using ClassLibrary.ProcessPattern;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.Workflows
{
	public class Parameter
	{
		public readonly UnlimitedNatural[] alias;

		public readonly ValueString[] defaultValue;

		public readonly Boolean[] isArray;

		public readonly UnlimitedNatural[] limitArrayIndex;

		public readonly String agency;

		public readonly String id;

		public readonly String version;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public Result[] result;

		public WorkflowStep workflowStep;

		public ResponseDomain responseDomain;

		public WorkflowStep workflowStep;

		public ValueDomain[] valueDomain;

		public Binding binding;

		public ProcessStep processStep;

		public ProcessStep processStep;

		public SampleFrame sampleFrame;

		public Binding binding;

		public SampleFrame sampleFrame;

		public Result[] result;

	}

}

