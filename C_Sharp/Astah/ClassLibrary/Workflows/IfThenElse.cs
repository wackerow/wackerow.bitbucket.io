using ClassLibrary.Workflows;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Workflows
{
	public class IfThenElse : ConditionalControlConstruct
	{
		public readonly ElseIfAction[] elseIf;

		public WorkflowStepSequence workflowStepSequence;

	}

}

