using ClassLibrary.Workflows;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.Workflows
{
	public class MetadataDrivenAction : Act
	{
		public readonly InternationalStructuredString[] activityDescription;

		public readonly ExternalControlledVocabularyEntry[] typeOfMetadataDrivenAction;

		public readonly InternationalStructuredString[] quasiVTL;

	}

}

