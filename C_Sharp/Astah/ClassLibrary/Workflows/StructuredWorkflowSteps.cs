using ClassLibrary.Workflows;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.Workflows
{
	public class StructuredWorkflowSteps : WorkflowStepSequence
	{
		public StructuredCollection[] structuredCollection;

		public WorkflowStepRelationStructure[] workflowStepRelationStructure;

	}

}

