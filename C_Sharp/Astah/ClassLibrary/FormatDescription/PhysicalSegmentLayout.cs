using ClassLibrary.Identification;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.FormatDescription;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.FormatDescription
{
	public abstract class PhysicalSegmentLayout : AnnotatedIdentifiable
	{
		public readonly Boolean isDelimited;

		public readonly String[] delimiter;

		public readonly Boolean isFixedWidth;

		public readonly String[] escapeCharacter;

		public readonly String[] lineTerminator;

		public readonly String[] quoteCharacter;

		public readonly String[] commentPrefix;

		public readonly ExternalControlledVocabularyEntry[] encoding;

		public readonly Boolean[] hasHeader;

		public readonly Integer[] headerRowCount;

		public readonly Boolean[] skipBlankRows;

		public readonly Integer[] skipDataColumns;

		public readonly Boolean[] skipInitialSpace;

		public readonly Integer[] skipRows;

		public readonly TrimValues[] trim;

		public readonly String[] nullSequence;

		public readonly Boolean[] headerIsCaseSensitive;

		public readonly Integer[] arrayBase;

		public readonly Boolean[] treatConsecutiveDelimitersAsOne;

		public readonly InternationalStructuredString[] overview;

		public readonly TableDirectionValues[] tableDirection;

		public readonly TextDirectionValues[] textDirection;

		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly ValueMappingIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public PhysicalRecordSegment physicalRecordSegment;

		public LogicalRecord[] logicalRecord;

		public PhysicalLayoutRelationStructure[] physicalLayoutRelationStructure;

		public Concept[] concept;

		public StructuredCollection[] structuredCollection;

	}

}

