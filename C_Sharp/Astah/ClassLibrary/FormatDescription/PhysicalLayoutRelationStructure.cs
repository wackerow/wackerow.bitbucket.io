using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.FormatDescription;

namespace ClassLibrary.FormatDescription
{
	public class PhysicalLayoutRelationStructure : Identifiable
	{
		public readonly InternationalStructuredString[] criteria;

		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly ValueMappingRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public PhysicalSegmentLayout[] physicalSegmentLayout;

	}

}

