using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.FormatDescription;

namespace ClassLibrary.FormatDescription
{
	public class DataPointRelationStructure : Identifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly DataPointRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public PhysicalRecordSegment[] physicalRecordSegment;

	}

}

