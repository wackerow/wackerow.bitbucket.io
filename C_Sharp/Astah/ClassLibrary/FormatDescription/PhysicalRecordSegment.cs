using ClassLibrary.Identification;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.FormatDescription;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.FormatDescription
{
	public class PhysicalRecordSegment : AnnotatedIdentifiable
	{
		public readonly String[] physicalFileName;

		public readonly DataPointIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public PhysicalSegmentLayout[] physicalSegmentLayout;

		public Population[] population;

		public PhysicalRecordSegmentRelation[] physicalRecordSegmentRelation;

		public PhysicalRecordSegmentRelation physicalRecordSegmentRelation;

		public PhysicalRecordSegmentIndicator physicalRecordSegmentIndicator;

		public Concept[] concept;

		public DataPointRelationStructure[] dataPointRelationStructure;

		public StructuredCollection[] structuredCollection;

		public LogicalRecord[] logicalRecord;

	}

}

