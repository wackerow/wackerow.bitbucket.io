using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.FormatDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.FormatDescription
{
	public class PhysicalOrderRelationStructure : Identifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly PhysicalRecordSegmentRelation[] hasMemberRelation;

		public PhysicalDataSet[] physicalDataSet;

		public RelationStructure[] relationStructure;

	}

}

