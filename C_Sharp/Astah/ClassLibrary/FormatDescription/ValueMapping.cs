using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using Primitive Package;
using ClassLibrary.FormatDescription;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.FormatDescription
{
	public class ValueMapping : Identifiable
	{
		public readonly ExternalControlledVocabularyEntry[] physicalDataType;

		public readonly OneCharString[] defaultDecimalSeparator;

		public readonly OneCharString[] defaultDigitGroupSeparator;

		public readonly String[] numberPattern;

		public readonly ValueString[] defaultValue;

		public readonly String[] nullSequence;

		public readonly ExternalControlledVocabularyEntry[] format;

		public readonly Integer[] length;

		public readonly Integer[] minimumLength;

		public readonly Integer[] maximumLength;

		public readonly Integer[] scale;

		public readonly Integer[] decimalPositions;

		public readonly Boolean[] isRequired;

		public PhysicalSegmentLocation[] physicalSegmentLocation;

		public ValueMappingRelation valueMappingRelation;

		public ValueMappingIndicator valueMappingIndicator;

		public DataPoint[] dataPoint;

		public ValueMappingRelation valueMappingRelation;

		public CollectionMember[] collectionMember;

	}

}

