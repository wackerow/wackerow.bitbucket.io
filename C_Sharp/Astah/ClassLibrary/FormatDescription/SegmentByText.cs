using ClassLibrary.FormatDescription;
using Primitive Package;

namespace ClassLibrary.FormatDescription
{
	public class SegmentByText : PhysicalSegmentLocation
	{
		public readonly Integer[] startLine;

		public readonly Integer[] endLine;

		public readonly Integer[] startCharacterPosition;

		public readonly Integer[] endCharacterPosition;

		public readonly Integer[] characterLength;

	}

}

