using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.SimpleCodebook;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.FormatDescription;

namespace ClassLibrary.FormatDescription
{
	public class PhysicalDataSet : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public readonly String[] physicalFileName;

		public readonly Integer[] numberOfSegments;

		public readonly PhysicalRecordSegmentIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public readonly CollectionType[] type;

		public readonly InternationalStructuredString[] purpose;

		public DataStore[] dataStore;

		public VariableStatistics[] variableStatistics;

		public Concept[] concept;

		public StructuredCollection[] structuredCollection;

		public PhysicalOrderRelationStructure[] physicalOrderRelationStructure;

	}

}

