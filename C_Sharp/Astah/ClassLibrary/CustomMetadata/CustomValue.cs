using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Conceptual;
using ClassLibrary.CustomMetadata;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.CustomMetadata
{
	public class CustomValue : Identifiable
	{
		public readonly ValueString[] value;

		public readonly String[] key;

		public InstanceVariable[] instanceVariable;

		public CustomItem[] customItem;

		public CollectionMember[] collectionMember;

		public CustomValueIndicator customValueIndicator;

		public CustomValueRelation customValueRelation;

		public CustomValueRelation[] customValueRelation;

	}

}

