using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.CustomMetadata;
using ClassLibrary.Conceptual;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.CustomMetadata
{
	public class ControlledVocabulary : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly VocabularyEntryIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public VocabularyRelationStructure[] vocabularyRelationStructure;

		public Concept[] concept;

		public StructuredCollection[] structuredCollection;

	}

}

