using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.CustomMetadata
{
	public class CustomStructure : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly CustomItemIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public StructuredCollection[] structuredCollection;

		public Concept[] concept;

		public CustomItemRelationStructure[] customItemRelationStructure;

		public CustomInstance customInstance;

	}

}

