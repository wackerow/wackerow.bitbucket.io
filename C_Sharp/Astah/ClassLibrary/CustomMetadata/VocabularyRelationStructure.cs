using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.CustomMetadata;

namespace ClassLibrary.CustomMetadata
{
	public class VocabularyRelationStructure : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] criteria;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly VocabularyEntryRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public ControlledVocabulary[] controlledVocabulary;

	}

}

