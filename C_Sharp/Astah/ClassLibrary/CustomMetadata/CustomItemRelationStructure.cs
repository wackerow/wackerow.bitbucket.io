using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CustomMetadata;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.CustomMetadata
{
	public class CustomItemRelationStructure : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] criteria;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly CustomItemRelation[] hasMemberRelation;

		public CustomStructure[] customStructure;

		public RelationStructure[] relationStructure;

	}

}

