using ClassLibrary.Identification;
using Primitive Package;
using ClassLibrary.CustomMetadata;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;
using ClassLibrary.Representations;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.CustomMetadata
{
	public class CustomItem : AnnotatedIdentifiable
	{
		public readonly Integer[] maxOccurs;

		public readonly Integer[] minOccurs;

		public readonly String[] key;

		public CustomValue customValue;

		public CollectionMember[] collectionMember;

		public RepresentedVariable[] representedVariable;

		public ValueDomain[] valueDomain;

		public Concept[] concept;

		public CustomItemIndicator customItemIndicator;

		public CustomItemRelation customItemRelation;

		public CustomItemRelation[] customItemRelation;

	}

}

