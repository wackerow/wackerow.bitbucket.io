using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.CustomMetadata
{
	public class VocabularyEntry : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] entryTerm;

		public readonly InternationalStructuredString[] definition;

		public CollectionMember[] collectionMember;

		public VocabularyEntryRelation vocabularyEntryRelation;

		public VocabularyEntryRelation vocabularyEntryRelation;

		public VocabularyEntryIndicator vocabularyEntryIndicator;

	}

}

