using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using Primitive Package;
using ClassLibrary.Conceptual;
using ClassLibrary.CustomMetadata;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.CustomMetadata
{
	public class CustomInstance : Identifiable
	{
		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly CollectionType[] type;

		public readonly CustomValueIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public Concept[] concept;

		public CustomStructure[] customStructure;

		public SimpleCollection[] simpleCollection;

	}

}

