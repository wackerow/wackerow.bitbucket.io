using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Utility;
using ClassLibrary.StudyRelated;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.Discovery
{
	public class Access : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] purpose;

		public readonly InternationalStructuredString[] confidentialityStatement;

		public readonly Form[] accessPermission;

		public readonly InternationalStructuredString[] restrictions;

		public readonly InternationalStructuredString[] citationRequirement;

		public readonly InternationalStructuredString[] depositRequirement;

		public readonly InternationalStructuredString[] accessConditions;

		public readonly InternationalStructuredString[] disclaimer;

		public readonly AgentAssociation[] contactAgent;

		public readonly DateRange[] validDates;

		public DocumentInformation[] documentInformation;

		public Study study;

		public SampleFrame sampleFrame;

		public DocumentInformation[] documentInformation;

	}

}

