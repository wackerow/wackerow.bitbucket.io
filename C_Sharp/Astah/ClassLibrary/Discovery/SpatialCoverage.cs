using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.Discovery;
using ClassLibrary.GeographicClassification;
using ClassLibrary.Conceptual;

namespace ClassLibrary.Discovery
{
	public class SpatialCoverage : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] description;

		public readonly ExternalControlledVocabularyEntry[] spatialAreaCode;

		public readonly SpatialObjectType[] spatialObject;

		public BoundingBox[] boundingBox;

		public GeographicUnit[] geographicUnit;

		public UnitType[] unitType;

		public UnitType[] unitType;

		public UnitType[] unitType;

		public Coverage[] coverage;

		public GeographicUnitTypeClassification[] geographicUnitTypeClassification;

		public GeographicUnitClassification[] geographicUnitClassification;

	}

}

