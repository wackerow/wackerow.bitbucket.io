using ClassLibrary.Identification;
using Primitive Package;
using ClassLibrary.Discovery;
using ClassLibrary.GeographicClassification;

namespace ClassLibrary.Discovery
{
	public class BoundingBox : AnnotatedIdentifiable
	{
		public readonly Real eastLongitude;

		public readonly Real westLongitude;

		public readonly Real northLatitude;

		public readonly Real southLatitude;

		public SpatialCoverage[] spatialCoverage;

		public GeographicExtent geographicExtent;

	}

}

