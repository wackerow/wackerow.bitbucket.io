using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Discovery;

namespace ClassLibrary.Discovery
{
	public class TopicalCoverage : AnnotatedIdentifiable
	{
		public readonly ExternalControlledVocabularyEntry[] subject;

		public readonly ExternalControlledVocabularyEntry[] keyword;

		public Coverage[] coverage;

	}

}

