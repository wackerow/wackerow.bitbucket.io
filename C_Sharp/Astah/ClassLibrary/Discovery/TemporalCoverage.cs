using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Discovery;

namespace ClassLibrary.Discovery
{
	public class TemporalCoverage : AnnotatedIdentifiable
	{
		public readonly ReferenceDate[] coverageDate;

		public Coverage[] coverage;

	}

}

