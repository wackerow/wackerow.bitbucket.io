using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Discovery;
using ClassLibrary.Utility;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.Discovery
{
	public class Coverage : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] overview;

		public TemporalCoverage temporalCoverage;

		public SpatialCoverage spatialCoverage;

		public DocumentInformation documentInformation;

		public TopicalCoverage topicalCoverage;

		public Study[] study;

	}

}

