using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.LogicalDataDescription
{
	public class InstanceVariableRelationStructure : AnnotatedIdentifiable
	{
		public readonly InternationalStructuredString[] criteria;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType totality;

		public readonly RelationSpecification hasRelationSpecification;

		public readonly InstanceVariableRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public UnitDataRecord[] unitDataRecord;

	}

}

