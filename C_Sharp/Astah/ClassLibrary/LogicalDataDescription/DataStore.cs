using ClassLibrary.Identification;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.FormatDescription;
using ClassLibrary.StudyRelated;
using ClassLibrary.Conceptual;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.LogicalDataDescription
{
	public class DataStore : AnnotatedIdentifiable
	{
		public readonly String[] characterSet;

		public readonly ExternalControlledVocabularyEntry[] dataStoreType;

		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly Integer[] recordCount;

		public readonly InternationalStructuredString[] aboutMissing;

		public readonly LogicalRecordIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public PhysicalDataSet[] physicalDataSet;

		public Study study;

		public DataStoreRelation[] dataStoreRelation;

		public DataStoreIndicator dataStoreIndicator;

		public DataStoreRelation[] dataStoreRelation;

		public Concept[] concept;

		public RecordRelation recordRelation;

		public StructuredCollection[] structuredCollection;

		public LogicalRecordRelationStructure[] logicalRecordRelationStructure;

	}

}

