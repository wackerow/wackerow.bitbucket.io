using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.LogicalDataDescription
{
	public class DataStoreLibrary : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly DataStoreIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public DataStoreRelationStructure[] dataStoreRelationStructure;

		public StructuredCollection[] structuredCollection;

		public Concept[] concept;

		public StudySeries[] studySeries;

	}

}

