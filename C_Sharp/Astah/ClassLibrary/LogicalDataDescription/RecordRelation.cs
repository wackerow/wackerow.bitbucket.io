using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.LogicalDataDescription
{
	public class RecordRelation : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] purpose;

		public readonly InternationalStructuredString[] usage;

		public readonly InstanceVariableValueMap[] correspondence;

		public UnitDataRecord[] unitDataRecord;

		public Comparison[] comparison;

		public DataStore[] dataStore;

	}

}

