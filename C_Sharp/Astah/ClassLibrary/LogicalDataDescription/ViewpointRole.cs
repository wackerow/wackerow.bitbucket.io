using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.LogicalDataDescription
{
	public abstract class ViewpointRole : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly InstanceVariableIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public SimpleCollection[] simpleCollection;

		public Concept[] concept;

	}

}

