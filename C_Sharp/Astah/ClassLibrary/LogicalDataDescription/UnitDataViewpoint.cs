using ClassLibrary.Identification;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.LogicalDataDescription
{
	public class UnitDataViewpoint : AnnotatedIdentifiable
	{
		public AttributeRole[] attributeRole;

		public IdentifierRole[] identifierRole;

		public MeasureRole[] measureRole;

		public UnitDataRecord[] unitDataRecord;

	}

}

