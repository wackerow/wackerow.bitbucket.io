using ClassLibrary.Representations;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.Conceptual;

namespace ClassLibrary.LogicalDataDescription
{
	public class Datum : Designation
	{
		public readonly ValueString representation;

		public DataPoint[] dataPoint;

		public InstanceVariable[] instanceVariable;

	}

}

