using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.LogicalDataDescription
{
	public class DataStoreRelationStructure : Identifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly DataStoreRelation[] hasMemberRelation;

		public DataStoreLibrary[] dataStoreLibrary;

		public RelationStructure[] relationStructure;

	}

}

