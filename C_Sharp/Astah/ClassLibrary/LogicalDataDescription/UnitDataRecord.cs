using ClassLibrary.LogicalDataDescription;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.LogicalDataDescription
{
	public class UnitDataRecord : LogicalRecord
	{
		public RecordRelation[] recordRelation;

		public UnitDataViewpoint[] unitDataViewpoint;

		public InstanceVariableRelationStructure[] instanceVariableRelationStructure;

		public StructuredCollection[] structuredCollection;

		public Concept[] concept;

	}

}

