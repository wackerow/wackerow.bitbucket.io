using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.LogicalDataDescription;

namespace ClassLibrary.LogicalDataDescription
{
	public class LogicalRecordRelationStructure : Identifiable
	{
		public readonly RelationSpecification[] hasRelationSpecification;

		public readonly ExternalControlledVocabularyEntry[] semantic;

		public readonly TotalityType[] totality;

		public readonly LogicalRecordRelation[] hasMemberRelation;

		public RelationStructure[] relationStructure;

		public DataStore[] dataStore;

	}

}

