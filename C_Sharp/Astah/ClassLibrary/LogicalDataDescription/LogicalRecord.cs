using ClassLibrary.Identification;
using ClassLibrary.EnumerationsRegExp;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.FormatDescription;
using ClassLibrary.CollectionsPattern;
using ClassLibrary.Conceptual;

namespace ClassLibrary.LogicalDataDescription
{
	public abstract class LogicalRecord : AnnotatedIdentifiable
	{
		public readonly CollectionType[] type;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] purpose;

		public readonly InstanceVariableIndicator[] contains;

		public readonly Boolean[] isOrdered;

		public BusinessProcessCondition businessProcessCondition;

		public PhysicalSegmentLayout physicalSegmentLayout;

		public LogicalRecordRelation[] logicalRecordRelation;

		public PhysicalRecordSegment physicalRecordSegment;

		public SimpleCollection[] simpleCollection;

		public LogicalRecordIndicator logicalRecordIndicator;

		public Concept[] concept;

		public LogicalRecordRelation logicalRecordRelation;

	}

}

