using ClassLibrary.Identification;
using ClassLibrary.LogicalDataDescription;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;
using ClassLibrary.FormatDescription;
using ClassLibrary.CollectionsPattern;

namespace ClassLibrary.LogicalDataDescription
{
	public class DataPoint : AnnotatedIdentifiable
	{
		public Datum datum;

		public DataPointRelation[] dataPointRelation;

		public DataPointIndicator dataPointIndicator;

		public DataPointRelation[] dataPointRelation;

		public InstanceVariable[] instanceVariable;

		public ValueMapping valueMapping;

		public CollectionMember[] collectionMember;

	}

}

