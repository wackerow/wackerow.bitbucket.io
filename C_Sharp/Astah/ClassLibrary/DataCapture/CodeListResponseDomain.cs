using ClassLibrary.DataCapture;
using ClassLibrary.Representations;

namespace ClassLibrary.DataCapture
{
	public class CodeListResponseDomain : ResponseDomain
	{
		public Code[] code;

		public CodeList[] codeList;

		public ResponseDomain[] responseDomain;

	}

}

