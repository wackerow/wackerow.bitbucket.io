using ClassLibrary.DataCapture;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.DataCapture
{
	public class InstrumentCode : InstrumentComponent
	{
		public readonly ExternalControlledVocabularyEntry[] purposeOfCode;

		public readonly CommandCode[] usesCommandCode;

	}

}

