using ClassLibrary.Workflows;
using ClassLibrary.DataCapture;

namespace ClassLibrary.DataCapture
{
	public abstract class InstrumentComponent : Act
	{
		public ExternalAid[] externalAid;

		public Instruction[] instruction;

	}

}

