using ClassLibrary.DataCapture;
using Primitive Package;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.DataCapture
{
	public class TextResponseDomain : ResponseDomain
	{
		public readonly Integer[] maximumLength;

		public readonly Integer[] minimumLength;

		public readonly TypedString[] regularExpression;

	}

}

