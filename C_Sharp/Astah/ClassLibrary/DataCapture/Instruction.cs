using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Utility;
using ClassLibrary.DataCapture;

namespace ClassLibrary.DataCapture
{
	public class Instruction : AnnotatedIdentifiable
	{
		public readonly DynamicText[] instructionText;

		public readonly LabelForDisplay[] displayLabel;

		public readonly ObjectName[] name;

		public ExternalMaterial[] externalMaterial;

		public Capture[] capture;

		public InstrumentComponent[] instrumentComponent;

	}

}

