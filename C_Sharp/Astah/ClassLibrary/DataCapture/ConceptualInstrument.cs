using ClassLibrary.Workflows;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.DataCapture;

namespace ClassLibrary.DataCapture
{
	public class ConceptualInstrument : WorkflowProcess
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] description;

		public readonly InternationalStructuredString[] usage;

		public ImplementedInstrument[] implementedInstrument;

		public Capture[] capture;

	}

}

