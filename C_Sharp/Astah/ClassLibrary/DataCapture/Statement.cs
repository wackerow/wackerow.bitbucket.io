using ClassLibrary.DataCapture;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.DataCapture
{
	public class Statement : InstrumentComponent
	{
		public readonly DynamicText[] statementText;

		public readonly ExternalControlledVocabularyEntry[] purposeOfStatement;

	}

}

