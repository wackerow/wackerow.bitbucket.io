using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.DataCapture;
using ClassLibrary.Conceptual;

namespace ClassLibrary.DataCapture
{
	public abstract class Capture : AnnotatedIdentifiable
	{
		public readonly ObjectName[] name;

		public readonly LabelForDisplay[] displayLabel;

		public readonly InternationalStructuredString[] usage;

		public readonly InternationalStructuredString[] purpose;

		public readonly ExternalControlledVocabularyEntry[] captureSource;

		public readonly ExternalControlledVocabularyEntry[] analysisUnit;

		public ResponseDomain[] responseDomain;

		public Instruction[] instruction;

		public ExternalAid[] externalAid;

		public Concept[] concept;

		public ConceptualInstrument conceptualInstrument;

		public InstanceVariable instanceVariable;

	}

}

