using ClassLibrary.Utility;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.DataCapture;

namespace ClassLibrary.DataCapture
{
	public class ExternalAid : ExternalMaterial
	{
		public readonly ExternalControlledVocabularyEntry[] stimulusType;

		public InstrumentComponent[] instrumentComponent;

		public Capture[] capture;

	}

}

