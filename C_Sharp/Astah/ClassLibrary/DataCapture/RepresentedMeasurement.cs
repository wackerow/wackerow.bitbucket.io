using ClassLibrary.DataCapture;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Conceptual;

namespace ClassLibrary.DataCapture
{
	public class RepresentedMeasurement : Capture
	{
		public readonly ExternalControlledVocabularyEntry[] measurementType;

		public InstanceMeasurement instanceMeasurement;

		public RepresentedVariable[] representedVariable;

	}

}

