using ClassLibrary.DataCapture;
using ClassLibrary.ComplexDataTypes;
using Primitive Package;
using ClassLibrary.Conceptual;

namespace ClassLibrary.DataCapture
{
	public class RepresentedQuestion : Capture
	{
		public readonly DynamicText[] questionText;

		public readonly InternationalStructuredString[] questionIntent;

		public readonly Real[] estimatedResponseTimeInSeconds;

		public InstanceQuestion instanceQuestion;

		public RepresentedVariable[] representedVariable;

	}

}

