using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Workflows;
using ClassLibrary.DataCapture;
using ClassLibrary.Conceptual;

namespace ClassLibrary.DataCapture
{
	public class ResponseDomain : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public Parameter[] parameter;

		public Capture[] capture;

		public RepresentedVariable[] representedVariable;

		public CodeListResponseDomain codeListResponseDomain;

	}

}

