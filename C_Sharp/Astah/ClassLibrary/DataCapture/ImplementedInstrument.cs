using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.XMLSchemaDatatypes;
using ClassLibrary.DataCapture;
using ClassLibrary.StudyRelated;

namespace ClassLibrary.DataCapture
{
	public class ImplementedInstrument : AnnotatedIdentifiable
	{
		public readonly LabelForDisplay[] displayLabel;

		public readonly ExternalControlledVocabularyEntry[] typeOfInstrument;

		public readonly anyUri[] uri;

		public readonly ObjectName[] name;

		public readonly InternationalStructuredString[] usage;

		public ConceptualInstrument conceptualInstrument;

		public Study[] study;

	}

}

