using ClassLibrary.DataCapture;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.DataCapture
{
	public class NumericResponseDomain : ResponseDomain
	{
		public readonly ExternalControlledVocabularyEntry[] numericTypeCode;

		public readonly ExternalControlledVocabularyEntry[] unit;

		public readonly NumberRange[] usesNumberRange;

	}

}

