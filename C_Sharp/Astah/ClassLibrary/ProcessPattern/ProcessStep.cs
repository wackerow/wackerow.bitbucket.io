using ClassLibrary.CollectionsPattern;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.ProcessPattern;
using ClassLibrary.Workflows;

namespace ClassLibrary.ProcessPattern
{
	public abstract class ProcessStep : CollectionMember
	{
		public readonly Binding[] hasInformationFlow;

		public ProcessStepRelation processStepRelation;

		public Service[] service;

		public ProcessStepRelation processStepRelation;

		public Parameter[] parameter;

		public ProcessStepIndicator processStepIndicator;

		public WorkflowStep workflowStep;

		public Parameter[] parameter;

	}

}

