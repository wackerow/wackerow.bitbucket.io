using ClassLibrary.CollectionsPattern;
using ClassLibrary.ProcessPattern;
using ClassLibrary.SimpleMethodologyOverview;

namespace ClassLibrary.ProcessPattern
{
	public abstract class ProcessSequence : SimpleCollection
	{
		public readonly ProcessStepIndicator[] hasMemberIndicator;

		public Process[] process;

		public ProcessOverview[] processOverview;

		public ProcessControlStep[] processControlStep;

	}

}

