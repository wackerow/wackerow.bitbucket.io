using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.ProcessPattern;
using ClassLibrary.Workflows;
using ClassLibrary.Agents;

namespace ClassLibrary.ProcessPattern
{
	public abstract class Service : Identifiable
	{
		public readonly ExternalControlledVocabularyEntry[] serviceLocation;

		public ProcessStep[] processStep;

		public WorkflowService workflowService;

		public Agent[] agent;

	}

}

