using ClassLibrary.ProcessPattern;

namespace ClassLibrary.ProcessPattern
{
	public abstract class ProcessControlStep : ProcessStep
	{
		public ProcessSequence processSequence;

	}

}

