using ClassLibrary.CollectionsPattern;
using ClassLibrary.ProcessPattern;
using ClassLibrary.ComplexDataTypes;

namespace ClassLibrary.ProcessPattern
{
	public abstract class ProcessStepIndicator : MemberIndicator
	{
		public ProcessStep[] processStep;

		public WorkflowStepIndicator workflowStepIndicator;

	}

}

