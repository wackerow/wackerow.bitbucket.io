using ClassLibrary.Identification;
using ClassLibrary.ComplexDataTypes;
using ClassLibrary.Workflows;
using ClassLibrary.MethodologyPattern;
using ClassLibrary.ProcessPattern;
using ClassLibrary.SimpleMethodologyOverview;
using ClassLibrary.SamplingMethodology;

namespace ClassLibrary.ProcessPattern
{
	public abstract class Process : Identifiable
	{
		public readonly InternationalStructuredString[] overview;

		public readonly ObjectName[] name;

		public WorkflowProcess workflowProcess;

		public Design[] design;

		public ProcessSequence processSequence;

		public Algorithm[] algorithm;

		public ProcessOverview processOverview;

		public SamplingProcess samplingProcess;

		public Methodology[] methodology;

	}

}

