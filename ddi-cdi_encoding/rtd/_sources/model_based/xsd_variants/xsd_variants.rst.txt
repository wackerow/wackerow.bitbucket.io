DDI-CDI XML Schema Variants
===========================

.. container:: revealjs-font75p

   UML associations should be represented in the XML Schema in a way that the class on the other association end is referenced by a distinct reference mechanism.

   DDI-CDI has a data type Reference for generic purposes. The reference type for associations needs only the attributes 'ddiReference' and 'validType'.
   ddiReference for referring to the other class by the DDI identifier mechanism and validType for limiting the allowed reference types.

   There are various conceivable ways of accomplishing this using XML Schema means.

Various Approaches
------------------

.. container:: revealjs-font50p revealjs-width-130

   1. The Dagstuhl discussion showed that the current solution in the schema is not good. The same Reference type (derived from the model) is used for generic reference purposes and for referencing the associated class/element. This can be error prone for implementations. Furthermore the association reference needs only the class attributes ddiReference and validType not the other attributes.

   2. A XML-like solution where the associated element is represented by a child element (works for simple models) doesn't work for a complex UML model. The DDI-CDI model is a graph. Nested associations could be very deep or even circular.

   3. With XLink, it doesn't seem to be possible to control the allowed list of values for the associated element. The Python generator generateDS doesn't mention XLink in the documentation but there is a small code section which does something with XLink. Furthermore it is not clear if other XSD data binding generators implement XLink. With this background, XLink doesn't seem to be an alternative.

   4. A type for association reference purposes which is derived from Reference would be possible. The requirement would be that the derived type has some indication that it is used for association reference and that it uses only the attributes ddiReference and validType. validType needs to be constrained according to the allowed values of the specific association.

   Though there are some limitations of defining the XML Schema with restriction and extension of types. Restriction and extension cannot be combined. The requirements need an extension for the additional indicator of an association reference, a restriction to the attributes ddiReference and validType, and a restriction to the allowed values of validType. With Restriction, it is necessary to repeat all allowed elements (the implications regarding the generators are not clear).

Approach 4.1
------------

.. container:: revealjs-font75p

   Add attribute isAssociationReference (default value false) to an extended type Reference. Restrict this type for associations to ddiReference and validType (with a set of allowed values) and set isAssociationReference to true.

   * Pro: clear definition in XML Schema. Only one 'root' Reference type.
   * Con: for generic references, isAssociationReference is set to false only by default. This could be error prone.

.. container:: revealjs-font50p

   XML Schema `ddi-cdi_41.xsd <https://bitbucket.org/wackerow/ddi-cdi_encoding/src/master/encoding/model_based/xsd_variants/ddi-cdi_41.xsd>`_

Approach 4.2
------------

.. container:: revealjs-font75p

   Add attribute isAssociationReference to an extended type Reference. Restrict this type for associations to ddiReference and validType (with a set of allowed values) and set isAssociationReference to true. Restrict this type by setting isAssociationReference to false

   * Pro: clear distinction of references. Only one 'root' Reference type.
   * Con: for each generic reference all attributes/elements need to be repeated in the restriction. The data binding generators might generate additional code for this. The impact is not clear.

.. container:: revealjs-font50p

   XML Schema `ddi-cdi_42.xsd <https://bitbucket.org/wackerow/ddi-cdi_encoding/src/master/encoding/model_based/xsd_variants/ddi-cdi_42.xsd>`_

Approach 4.3
------------

.. container:: revealjs-font75p

   Use Reference for generic references. Derive AssociationReference (with fixed attribute isAssociationReference set to true) by extension. Restrict this type for associations to ddiReference and validType (with a set of allowed values).

   * Pro: Clean solution
   * Con: generators might not 'see' the relationship between Reference and AssociationReference and generate separate code. Might generate unnecessary code for AssociationReference (all Reference attributes).

.. container:: revealjs-font50p

   XML Schema `ddi-cdi_43.xsd <https://bitbucket.org/wackerow/ddi-cdi_encoding/src/master/encoding/model_based/xsd_variants/ddi-cdi_43.xsd>`_

Approach 4.4
------------

.. container:: revealjs-font75p

   Similar to 4.3. Use Reference for generic references. Derive IntermediateAssociationReference (with fixed attribute isAssociationReference set to true) by extension. Derive AssociationReference from IntermediateAssociationReference by restriction to a type with only necessary attributes ddiReference and validType. Restrict this type for associations to allowed values for validType.

   * Pro: Clean solution. No issue with unnecessary elements in AssociationReference definition.
   * Con: generators might not 'see' the relationship between Reference and AssociationReference and generate separate code. Two types for AssociationReference necessary.

.. container:: revealjs-font50p

   XML Schema `ddi-cdi_44.xsd <https://bitbucket.org/wackerow/ddi-cdi_encoding/src/master/encoding/model_based/xsd_variants/ddi-cdi_44.xsd>`_

Approach 4.5
------------

.. container:: revealjs-font75p

   Use Reference for generic references. Hard code AssociationReference in the model-to-text generator for XSD and Restrict this type for associations to allowed values for validType.

   * Pro: straight forward approach
   * Con: total separation of Reference and AssociationReference

.. container:: revealjs-font50p

   XML Schema `ddi-cdi_45.xsd <https://bitbucket.org/wackerow/ddi-cdi_encoding/src/master/encoding/model_based/xsd_variants/ddi-cdi_45.xsd>`_
