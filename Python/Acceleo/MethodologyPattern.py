



















class Design(Identifiable):
	"""
	 Definition
	   ============
	   The design pattern class may be used to specify or, again, defines how a process will be performed in general. The design informs a specific or implemented process as to its general parameters.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Process Design.
	"""
	def __init__(self)
	    self.overview = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class Methodology(Identifiable):
	"""
	 Definition
	   ============
	   Methodology brings together the design, algorithm, and process as a logically structured approach to an activity such as sampling, weighting, harmonization, imputation, collection management, etc. A methodology in normally informed by earlier research and clarifies how earlier research methods were incorporated into the current work.
	   
	   
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.name = []
	    self.usage = None
	    self.rationale = None
	    self.overview = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class Algorithm(Identifiable):
	"""
	 Definition
	   ============
	   An algorithm is an effective method that can be expressed within a finite amount of space and time and in a well-defined formal language for calculating a function. Starting from an initial state and initial input (perhaps empty), the instructions describe a computation that, when executed, proceeds through a finite number of well-defined successive states, eventually producing "output" and terminating at a final ending state. The transition from one state to the next is not necessarily deterministic; some algorithms, known as randomized algorithms, incorporate random input. [from www.wikipedia.org 15 April 2016]
	   
	   The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self. = []
