"""
 Purpose
   Support the description of business processing such as data transformation, harmonization, ingest processing, creation of OAIS packages, etc.
   
   Restriction: Study has been included in the view so that it can be included in describing a Data Pipeline. The relationships of Study are not supported by this view.
"""

