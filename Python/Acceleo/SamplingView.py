"""
 Purpose:
   The Sampling View covers the methodology and process for obtaining a sample from a described sample frame. It provides descriptive information of the methodologies used, design of the sample, algorithms to support the processing of the sample, goals, sample frames used, and guidance on the use of the resulting sample (for defining weights, limitations, etc.). This includes the use of the Workflows which can define the process of creating simple or complex samples on a step-by-step basis. Can be used as metadata to drive sample selection.
   
   Use Cases:
   Defining Sample Frames for prospective use
   Describing the process of simple or complex sampling procedures to inform the automated creation of a sample from a sample frame.
   Describe the processing of sampling for the purpose of informing the user of limitations or strengths of a particular sampling procedure and to provide guidance on the use of the procedure or the result of the procedure.
   
   Target Audience:
   Organizations defining sample frames and sampling procedures for execution.
   Users requiring information on the sampling process and its limitations.
   
   List of Constrained Classes:
   none
   
   Documentation of specialized use of classes:
   none
   
   General documentation:.
"""

