











class Agent(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   An actor that performs a role in relation to a process or product.
	   
	   Examples
	   ==========
	   Analyst performing edits on data, interviewer conducting an interview, a relational database management system managing data, organization publishing data on a regular basis, creator or contributor of a publication.
	   
	   Explanatory notes
	   ===================
	   foaf:Agent is: An agent (eg. person, group, software or physical artifact)
	   prov:Agent is An agent is something that bears some form of responsibility for an activity taking place, for the existence of an entity, or for another agent's activity.
	   
	   Synonyms
	   ==========
	   Agent
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasAgentId = []
	    self.purpose = None
	    self.image = []
	    self. = []

class Individual(Agent):
	"""
	 Definition
	   ============
	   A person who may have a relationship to another Agent and who may be specified as being associated with an act.
	   
	   Examples
	   ==========
	   Analyst performing edits on data, interviewer conducting an interview, a creator.
	   
	   Explanatory notes
	   ===================
	   A set of information describing an individual and means of unique identification and/or contact. Information may be repeated and provided effective date ranges to retain a history of the individual within a metadata record. Actions and relationships are specified by the use of the Individual as the target of a relationship (Creator) or within a collection of Agents in an AgentListing (employee of an Organization).
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasIndividualName = []
	    self.ddiId = []
	    self.hasContactInformation = None

class AgentRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Defines the relationships between Agents in a collection. Clarifies valid period and the purpose the relationship serve
	   
	   Examples
	   ==========
	   An individual employed by an Organization. A unit or project (organization) within another Organization.
	   
	   Explanatory notes
	   ===================
	   Describes relations between agents not roles within a project or in relationship to a product. Roles are defined by the parent class and relationship name that uses an Agent as a target.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.effectiveDates = None
	    self.privacy = None
	    self.purpose = None
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class Organization(Agent):
	"""
	 Definition
	   ============
	   A framework of authority designated to act toward some purpose.
	   
	   Examples
	   ==========
	   U.S. Census Bureau, University of Michigan/ISR, Norwegian Social Data Archive 
	   
	   Explanatory notes
	   ===================
	   related to org:Organization which is described as "Represents a collection of people organized together into a community or other..."
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasOrganizationName = []
	    self.ddiId = []
	    self.hasContactInformation = None

class AgentListing(Identifiable):
	"""
	 Definition
	   ============
	   A listing of Agents of any type. The AgentList may be organized to describe relationships between members using AgentRelationStructure.
	   
	   Examples
	   ==========
	   Organizations contributing to a project. Individuals within an agency. All organizations, indivduals, and machines identified within the collections of an archive.
	   
	   Explanatory notes
	   ===================
	   Relationships between agents are fluid and reflect effective dates of the relationship. An agent may have multiple relationships which may be sequencial or concurrent. Relationships may or may not be hierarchical in nature. All Agents are serialized individually and brought into relationships as appropriate.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class Machine(Agent):
	"""
	 Definition
	   ============
	   Mechanism or computer program used to implement a process.
	   
	   Examples
	   ==========
	   SAS program, photocopier
	   
	   Explanatory notes
	   ===================
	   May be used as the target to describe how an action was performed. Relevent to data capture and data processing or wherever a role is performed by a Machine.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfMachine = None
	    self.name = []
	    self.hasAccessLocation = None
	    self.function = []
	    self.machineInterface = []
	    self.ownerOperatorContact = None
