





































class NumericResponseDomain(ResponseDomain):
	"""
	 Definition
	   ============
	   A response domain capturing a numeric response (the intent is to analyze the response as a number) for a question.
	   
	   Examples
	   ==========
	   Systolic blood pressure level, as an integer from 0 to 200
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:NumericDomainType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.numericTypeCode = None
	    self.unit = None
	    self.usesNumberRange = []

class Capture(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A measurement that describes a means of capturing data. This class can be extended to account for different specific means. Use a specific instantiation of a Capture to describe a means of capturing a measurement.
	   
	   
	   
	   
	   Examples
	   ==========
	   A survey question, blood pressure reading; MRI images; thermometer; web service; experimental observation. Classes could include InstanceQuestion, InstanceMeasurement or other class extending Capture.
	   
	   Explanatory notes
	   ===================
	   Provides an abstract base so that current and future forms of data capture can use this as an extension base and be freely mixed and matched within conceptual instruments as needed such as capturing a GPS point (using a RepresentedMeasurement) when administering a questionnaire (using RepresentedQuestions).
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.name = []
	    self.displayLabel = []
	    self.usage = None
	    self.purpose = None
	    self.captureSource = None
	    self.analysisUnit = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class CodeListResponseDomain(ResponseDomain):
	"""
	 Definition
	   ============
	   A response domain capturing a coded response (where both codes and their related category value are displayed) for a question. This response domain allows the single selection of one coded response.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:CodeDomainType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []
	    self. = []

class BooleanResponseDomain(ResponseDomain):
	"""
	 Definition
	   ============
	   A response domain capturing a binary response, such as selected/unselected or checked/unchecked or true/false.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Exactly what is captured may be dependent upon the Implemented Instrument, however the response being captured is either there or not there (ON/OFF, True/False)
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []

class ConceptualInstrument(WorkflowProcess):
	"""
	 Definition
	   ============
	   Design plan for creating a data capture tool. 
	   
	   Examples
	   ==========
	   Design of a questionnaire regardless of mode; Design of an experiment
	   
	   Explanatory notes
	   ===================
	   A single ConceptualInstrument would contain workflow sequences of the intended flow logic of the instrument, which could be implemented as a web survey or paper survey, utilizing the same sequence and questions.
	   Similarly a single ConceptualInstrument would contain workflow sequences of the intended flow logic of the instrument, which could be implemented as a protocol delivered by a nurse or by self-administration.
	   
	   Synonyms
	   ==========
	   Idea; Intention; Theory; Design; LogicalInstrument
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.name = []
	    self.description = None
	    self.usage = None
	    self. = []

class ScaleResponseDomain(ResponseDomain):
	"""
	 Definition
	   ============
	   A response domain capturing a scaled response, such as a Likert scale. 
	   Note: This item still must be modeled and is incomplete at this time.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class InstanceQuestion(InstrumentComponent):
	"""
	 Definition
	   ============
	   An instance question is an instantiation of a represented question,to be used as an Act in the process steps that define a survey questionnaire.
	   
	   Examples
	   ==========
	   “How old are you?”
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.name = []
	    self. = []

class Statement(InstrumentComponent):
	"""
	 Definition
	   ============
	   A Statement is a type of Instrument Component containing  human readable text or referred material. 
	   
	   Examples
	   ==========
	   Introductory text; Explanations; Labels, Headers, Help screens, etc.  "Thank you for agreeing to take this survey. We will start with a brief set of demographic questions." "The following set of questions are related to your household income. Please consider all members of the household and all sources of income when answering these questions."
	   
	   Explanatory notes
	   ===================
	   It is not directly related to another specific Instrument Component such as an InstanceQueston or InstanceMeasurement. It may be placed anywhere in a WorkflowStepSequence. 
	   
	   Synonyms
	   ==========
	   DDI:StatementItem
	   
	   DDI 3.2 mapping
	   =================
	   d:StatementItemType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.statementText = []
	    self.purposeOfStatement = None

class ImplementedInstrument(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A specific data capture tool.  ImplementedInstruments are mode and/or unit specific.
	   
	   Examples
	   ==========
	   Two versions of a conceptual instrument, computer assisted personal interviewing (CAPI) and mail, would be separate ImplementedInstruments. These might both reference the same ConceptualInstrument. 
	   A piece of equipment used to measure blood pressure.
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   aka PhysicalInstrument
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.typeOfInstrument = None
	    self.uri = []
	    self.name = []
	    self.usage = []
	    self. = None

class RepresentedMeasurement(Capture):
	"""
	 Definition
	   ============
	   The description of a reusable non-question measurement that can be used as a template that describes the components of a measurement. A type code can be used to describe the type of measure that was used.
	   
	   Examples
	   ==========
	   A measure providing a geographic point; A blood pressure protocol; A protocol for measuring an ocean current
	   
	   Explanatory notes
	   ===================
	   Non-question measurements often involve the use of specific machines or protocols to ensure consistency and comparability of the measure. The RepresentedMeasurement allows the reusable format and protocol to be housed in a repository for use by multiple studies or repeated studies.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.measurementType = None
	    self. = []

class ResponseDomain(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The possible list of values that are allowed by a Capture.
	   
	   Examples
	   ==========
	   Yes/No, Male/Female, Age in years, Open-ended text, Temperature, BP reading
	   
	   Explanatory notes
	   ===================
	   Identifies both the sentinel and substantive value domains used for capturing the response to a question
	   
	   Synonyms
	   ==========
	   GSIM & DDI: ResponseDomain
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = None
	    self. = []
	    self. = []

class TextResponseDomain(ResponseDomain):
	"""
	 Definition
	   ============
	   A response domain capturing a textual response including the length of the text and restriction of content using a regular expression.
	   
	   Examples
	   ==========
	   Collecting the first name on an individual in an open ended text field
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:TextDomainType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.maximumLength = None
	    self.minimumLength = None
	    self.regularExpression = None

class RankingResponseDomain(ResponseDomain):
	"""
	 Definition
	   ============
	   A response domain capturing a ranking response which supports a "ranking" or "Ordering" of provided categories. 
	   Note: This item still must be modeled and is incomplete at this time.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class RepresentedQuestion(Capture):
	"""
	 Definition
	   ============
	   The description of a reusable question, that can be used as a template that describes the components of a question
	   
	   Examples
	   ==========
	   A question in a question bank such as the content and format for a question on Race for all survey/censuses fielded by U.S. agencies as prescribed by the U.S. Office of Management and Budget (OMB)
	   
	   Explanatory notes
	   ===================
	   A question that is repeated across waves of a panel study can be reused and also allow reference to a RepresentedVariable. A question that has been tested for consistency of response and is used by multiple studies.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:QuestionItemType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.questionText = []
	    self.questionIntent = None
	    self.estimatedResponseTimeInSeconds = None
	    self. = []

class Instruction(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Provides the content and description of data capture instructions. Contains the "how to" information for administering an instrument.
	   
	   Examples
	   ==========
	   Completion instructions in self-administered mail questionnaire, information for administering a blood pressure measurement, interviewer instructions for a CATI questionnaire, guidance for communicating between an interviewer and a respondent (note MIDUS cognitive assessment example).
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:InstructionType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.instructionText = []
	    self.displayLabel = []
	    self.name = []
	    self. = []

class InstanceMeasurement(InstrumentComponent):
	"""
	 Definition
	   ============
	   An instance measurement instantiates a represented measurement, so that it can be used as an Act in the Process Steps that define a data capture process.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []

class ExternalAid(ExternalMaterial):
	"""
	 Definition
	   ============
	   Any external material used in an instrument that aids or facilitates data capture, or that is presented to a respondent and about which measurements are made. 
	   
	   Examples
	   ==========
	   Image, link, external aid, stimulus, physical object.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:OtherMaterialType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.stimulusType = None

class InstrumentComponent(Act):
	"""
	 Definition
	   ============
	   InstrumentComponent is an abstract object which extends an Act (a type of Process Step). The purpose of InstrumentComponent is to provide a common parent for Capture (e.g., Question, Measure), Statement, and Instructions.
	   
	   
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   InstrumentComponent acts as a substitution head (abstract) for semantically meaningful for objects that extend it and exist in an instrument.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []

class InstrumentCode(InstrumentComponent):
	"""
	 Definition
	   ============
	   An InstrumentComponent that specifies the performance of a specific computation within the context of an instrument flow.
	   
	   Examples
	   ==========
	   quality control, edit check, checksums, compute filler text, compute values for use in administering the instrument
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:ComputationItemType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.purposeOfCode = None
	    self.usesCommandCode = None
