"""
 Purpose:
   To review the conceptual concept structures and to manage concept systems (other than statistical classifications). The coverage go down to a RepresentedVariable. InstanceVariable has been included in the collection for use as a member of a VariableCollection. However it does not go into the detailed content of an InstanceVariable.
   
   Restriction: 
   InstanceVariable has been included for the sole purpose of membership in a VariableCollection. The classes required to completely describe an InstanceVariable have not been included.
"""

