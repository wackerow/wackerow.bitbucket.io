



















class ProcessControlStep(ProcessStep):
	"""
	 Definition
	   ============
	   A Process Step that controls the execution flow of the Process by determining the next Process Step in its scope.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = None

class ProcessSequence(SimpleCollection):
	"""
	 Definition
	   ============
	   A collection of Process Steps organized as a sequence.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasMemberIndicator = []

class Process(Identifiable):
	"""
	 Definition
	   ============
	   Process is an implementation of an algorithm. It is the set of process steps taken as a whole. It is decomposable into ProcessSteps organized by a Process Sequence or more complex Process Relation Structure, but this decomposition is not necessary. 
	   
	   Examples
	   ==========
	   The Generic Longitudinal Business Process Model (GLBPM) identifies a series of processes like data integration, anonymization, analysis variable construction and so forth in the data lifecycle.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Process Design.
	"""
	def __init__(self)
	    self.overview = None
	    self.name = []
	    self. = []
	    self. = None

class ProcessStepRelation(MemberRelation):
	"""
	 Definition
	   ============
	   Specifies MemberRelation to ProcessSteps for Process in Process Pattern
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []

class ProcessStep(CollectionMember):
	"""
	 Definition
	   ============
	   One of the constituents of a Process. It can be a composition or atomic and might be performed by a Service.
	   
	   Examples
	   ==========
	   Each step and substep in the Generic Longitudinal Business Process Model is a ProcessStep. 
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasInformationFlow = []
	    self. = []
	    self. = []
	    self. = []

class Service(Identifiable):
	"""
	 Definition
	   ============
	   A means of performing a Process Step as part of a Business Function (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve). 
	   
	   
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Business Service.
	"""
	def __init__(self)
	    self.serviceLocation = None
	    self. = []

class ProcessStepIndicator(MemberIndicator):
	"""
	 Definition
	   ============
	   Member Indicator limited to ProcessStep
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
