from datetime import datetime

































class GeographicUnit(Unit):
	"""
	 Definition
	   ============
	   A specific geographic unit of a defined Unit Type. A geographic unit may change its name, composition, or geographic extent over time. This may be tracked by versioning the content of the geographic unit. Normally a new version of a geographic unit would have a geographic (spatial) overlap with its previous version (a city annexing new area). A geographic unit ends when it is no longer a unit of the same unit type.
	   
	   Examples
	   ==========
	   The State of Minnesota from 1858 to date where the Unit Type is a State as defined by the United States Census Bureau. It has a geographic extent and supersedes a portion of the Territory of Minnesota and a portion of the Territory of Wisconsin. Minnesota territory had both a broader geographic extent, different Unit Type, and earlier time period (1849-1858) as did the Territory of Wisconsin. 
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:GeographicLocationType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.geographicTime = None
	    self.supercedes = []
	    self.precedes = []
	    self. = []

class GeographicUnitTypeClassification(CodeList):
	"""
	 Definition
	   ============
	   A structured collection of Unit Types defining a geographic structure. As a subtype of CodeList it may be used directly to describe a value domain.
	   
	   Examples
	   ==========
	   Country--State--County
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.contains = []
	    self.releaseDate = datetime()
	    self.validDates = None
	    self.isCurrent = None
	    self.isFloating = None
	    self.displayLabel = []
	    self.usage = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class GeographicUnitTypeRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Defines the relationships between Geographic Unit Types in a collection.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class GeographicUnitRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Defines the relationships between Geographic Unit Types in a collection. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class GeographicUnitClassification(CodeList):
	"""
	 Definition
	   ============
	   Describes the classification of specific geographic units into a classification system. As a subtype of Code List it can be used directly for the description of a value domain.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.contains = []
	    self.releaseDate = datetime()
	    self.validDates = None
	    self.isCurrent = None
	    self.isFloating = None
	    self.displayLabel = []
	    self.usage = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class GeographicExtent(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Defines the extent of a geographic unit for a specified period of time using Bounding Box, Inclusive and Exclusive Polygons, and an area coverage that notes a type of coverage (land, water, etc.) and measurement for the coverage. The same geographic extent may be used by multiple versions of a single geographic version or by different geographic units occupying the same spatial area.
	   
	   Examples
	   ==========
	   Bounding box for Burkino Faso: (N) 15.082773; (S) 9.395691; (E) 2.397927; (W) -5.520837.  Minnesota Land area: 206207.099 sq K, Water area: 18974.589 sq K 
	   
	   Explanatory notes
	   ===================
	   Clarifies the source of a change in terms of footprint of an area as opposed to a name or coding change. 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:GeographicBoundaryType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.boundingPolygon = []
	    self.excludingPolygon = []
	    self.geographicTime = None
	    self.hasAreaCoverage = []
	    self.hasCentroid = None
	    self.locationPoint = None
	    self.isSpatialLine = None
	    self. = []
