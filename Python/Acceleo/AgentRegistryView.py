"""
 Purpose:
   The Agent Registry View supports the creation of a listing of Agents (Organization, Individual, Machine) for the purpose of maintenance and reuse. The listing of Agents can be organized in pair-wise descriptions within Agent Relationships. These may be typed and provided with effective date ranges either as a set of relationships of a specified type or as individual pairs.
   
   Use Cases:
   Maintaining a listing of all Organizations and Individuals related to a project or statistical activities, i.e. Individuals and Organizations related to the IPUMS projects at the Minnesota Population Center or the Departments and Individuals involved in the production of the Australian Census within the Australian Bureau of Statistics.
   
   Target Audience:
   Organizations working with a number of Individuals and Organizations whose descriptive information and interrelationships change over time.
   
   Included Classes: Agent Listing, Agent Relation Structure, Organization, Individual, Machine, and External Material. 
   
   Restricted Classes:
   All classes are included in their entirety, there are no restrictions.
   
   General Documentation:
   The primary class is the Agent List which can be structured by Agent Relation Structure. Agents may be of any type supported by DDI at the time of this release. Agent relationships can be defined using the pair-wise relationship Agent Relation within Agent Relation Structure specifying the Relationship Specification (Equality, Parent/Child, Whole/Part, etc.), a relevant semantic such as a specific role, and effective time span for the relationship.
"""

