









class StandardWeight(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Provides an identified value for a standard weight expressed as an xs:float. This object may be referenced by a variable or statistic and used as a weight for analysis.
	   
	   Examples
	   ==========
	   A simple sample survey without an individual weight variable will have a StandardWeight to be used on all cases: 1% sample has a sample weight of 100
	   
	   Explanatory notes
	   ===================
	   A simple random sample survey without an individual weight variable will have a StandardWeight to be used on all cases. This is common when the sample is a simple random sample of a population with no oversampling to provide larger case counts for small populations.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:StandardWeightType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.standardWeightValue = None

class VariableStatistics(Identifiable):
	"""
	 Definition
	   ============
	   Contains summary and category level statistics for the referenced variable.
	   
	   Examples
	   ==========
	   A mean of values for a variable "Height". Counts for each level of variable "Gender" (male and female)
	   
	   Explanatory notes
	   ===================
	    Includes information on the total number of responses, the weights in calculating the statistics, variable level summary statistics, and category statistics. The category statistics may be provided as unfiltered values or filtered through a single variable. For example the category statistics for Sex filtered by the variable Country for a multi-national data file. Note that if no weighting factor is identified, all of the statistics provided are unweighted.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   pi:VariableStatisticsType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.totalResponses = None
	    self.hasSummaryStatistic = []
	    self.hasCategoryStatistic = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
