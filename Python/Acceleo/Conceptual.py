









































































class ConceptualVariable(Concept):
	"""
	 Definition
	   ============
	   The use of a Concept as a characteristic of a UnitType intended to be measured
	   
	   Examples
	   ==========
	   1. Sex of person
	   2. Number of employees 
	   3. Value of endowment
	   
	   Explanatory notes
	   ===================
	   Note that DDI varies from GSIM in the use of a UnitType rather than a Universe. "The use of a Concept as a characteristic of a Universe intended to be measured" [GSIM 1.1]
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   c:ConceptualVariableType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Variable.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.descriptiveText = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class Unit(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The object of interest in a process step related to the collection or use of observational data.
	   
	   Examples
	   ==========
	   Here are 3 examples - 1. Individual US person (i.e., Arofan Gregory, Dan Gillman, Barack Obama, etc.) 2. Individual US computer companies (i.e., Microsoft, Apple, IBM, etc.) 3. Individual US universities (i.e., Johns Hopkins, University of Maryland, Yale, etc.) [GSIM 1.1]
	   
	   Explanatory notes
	   ===================
	   In a traditional data table each column might represent some variable (measurement). Each row would represent the entity (Unit)  to which those variables relate. Height measurements might be made on persons (UnitType) of primary school age (Universe) at Pinckney Elementary School on September 1, 2005 (Population). The height for Mary Roe (Unit)  might be 139 cm.
	   The Unit is not invariably tied to some value. A median income might be calculated for a block in the U.S. but then used as an attribute of a person residing on that block. For the initial measurement the Unit was the block. In the reuse the unit would be that specific person to which the value was applied.
	   In a big data table each row represents a unit/variable double. Together a unit identifier and a variable identifier define the key. And for each key there is just one value – the measure of the unit  on the variable. 
	   A big data table is sometimes referred to as a column-oriented data store whereas a traditional database is sometimes referred to as a row-oriented data store. The unit plays an identifier role in both types of data stores.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Unit.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.name = []
	    self. = []

class Population(Concept):
	"""
	 Definition
	   ============
	   Set of specific units (people, entities, objects, events) with specification of time and geography.
	   
	   Examples
	   ==========
	   1. Canadian adult persons residing in Canada on 13 November 1956
	   2. US computer companies at the end of 2012  
	   3. Universities in Denmark 1 January 2011.
	   
	   Explanatory notes
	   ===================
	   Population is the most specific in the conceptually narrowing hierarchy of UnitType, Universe and Population. Several Populations having differing time and or geography may specialize the same Universe.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Population.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.descriptiveText = None
	    self.timePeriodOfPopulation = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class UnitType(Concept):
	"""
	 Definition
	   ============
	   A Unit Type is a class of objects of interest.
	   
	   Examples
	   ==========
	   Person, Establishment, Household, State, Country, Dog, Automobile
	   
	   Explanatory notes
	   ===================
	   UnitType is the most general in the hierarchy of UnitType, Universe, and Population. It is a description of the basic characteristic for a general set of Units.  A Universe is a set of entities defined by a more narrow specification than that of an underlying UnitType. A Population further narrows the specification to a specific time and geography.
	   A Unit Type is used to describe a class or group of Units based on a single characteristic with no specification of time and geography.  For example, the Unit Type of “Person” groups together a set of Units based on the characteristic that they are ‘Persons’.
	   It concerns not only Unit Types used in dissemination, but anywhere in the statistical process. E.g. using administrative data might involve the use of a fiscal unit. [GSIM 1.1]
	   
	   Synonyms
	   ==========
	   Object Class [ISO11179]
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Unit Type.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.descriptiveText = None
	    self. = None

class InstanceVariable(RepresentedVariable):
	"""
	 Definition
	   ============
	   The use of a Represented Variable within a Data Set.  The InstanceVariable describes actual instances of data that have been collected. 
	   
	   Examples
	   ==========
	   1) Gender: Dan Gillman has gender <m, male>, Arofan Gregory has gender<m, male>, etc. 
	   2) Number of employees: Microsoft has 90,000 employees; IBM has 433,000 employees, etc. 
	   3) Endowment: Johns Hopkins has endowment of <3, $1,000,000 and above>, 
	   Yale has endowment of <3, $1,000,000 and above>, etc.
	   
	   Two InstanceVariables of a person's height reference the same RepresentedVariable. This indicates that they are intended to: be measured with the same unit of measurement, have the same intended data type, have the same SubstantativeValueDomain, use a SentinelValueDomain drawn from the same set of SentinelValueDomains, have the same sentinel (missing value) concepts, and draw their Population from the same Universe. In other words, the two InstanceVariables should be comparable.
	   
	   Explanatory notes
	   ===================
	   The InstanceVariable class inherits all of the properties and relationships of the RepresentedVariable (RV) class and, in turn, the ConceptualVariable (CV) class. This means that an InstanceVariable can be completely populated without the need to create an associated RepresentedVariable or ConceptualVariable. If, however, a user wishes to indicate that a particular InstanceVariable is patterned after a particular RepresentedVariable or a particular ConceptualVariable that may be indicated by including a relationship to the RV and or CV. Including these references is an important method of indicating that multiple InstanceVariables have the same representation, measure the same concept, and are drawn from the same Universe. If two InstanceVariables of a person's height reference the same RepresentedVariable. This indicates that they are intended to: be measured with the same unit of measurement, have the same intended data type, have the same SubstantativeValueDomain, use a SentinelValueDomain drawn from the same set of SentinelValueDomains, have the same sentinel (missing value) concepts, and draw their Population from the same Universe. In other words, the two InstanceVariables should be comparable. 
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Instance Variable.
	"""
	def __init__(self)
	    self.variableRole = None
	    self.physicalDataType = None
	    self.platformType = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class VariableRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   RelationStructure for use with any mixture of Variables in the Variable Cascade (Conceptual, Represented, Instance).
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   A VariableCollection can be described as a simple list, or as having a more complex structure. All of the "administrative" variables for a study might be described as just a simple list without a VariableRelationStructure. If, however, the administrative variables needed to be described in subgroups, a VariableRelationStructure could be employed.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class ConceptRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Relation structure of concepts within a collection. Allows for the specification of complex relationships among concepts.
	   
	   Examples
	   ==========
	   A concept of vacation might be described as having sub-types of beach vacation and mountain vacation.
	   
	   Explanatory notes
	   ===================
	   The ConceptRelationStructure employs a set of ConceptRelations to describe the relationship among concepts. Each ConceptRelation is a one to many description of connections between concepts. Together they can describe relationships as complex as a hierarchy or even a complete cyclical network as in a concept map.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class ConceptualDomain(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Set of Concepts, both sentinel and substantive, that can be described by either enumeration or by an expression.
	   
	   Examples
	   ==========
	   Substantive: Housing Unit Tenure - Owned, Rented, Vacant; Sentinel: Non-response - Refused, Don't Know, Not Applicable 
	   
	   Explanatory notes
	   ===================
	   Intent of a Conceptual Domain is defining a set of concepts used to measure a broader concept. For effective use they should be discrete (non-overlapping) and provide exhaustive coverage of the broader concept.  
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self. = []
	    self. = []

class ConceptSystemCorrespondence(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Relationship between 2 or more ConceptSystems described through mapping similarity relationships between their member Concepts.
	   
	   Examples
	   ==========
	   Correspondence between the concepts used to define the populations in the censuses of two countries with similarity mapping of Concepts "Resident Population", "Labor Force", "Housing Unit", etc. 
	   
	   Explanatory notes
	   ===================
	   Describes correspondence with one or more Maps which identify a source and target concept and defines their commonality and difference using descriptive text and controlled vocabularies.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.purpose = None
	    self.usage = None
	    self.correspondence = []
	    self. = []
	    self. = []

class VariableCollection(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A collection (group) of any type of Variable within the Variable Cascade (Conceptual, Represented, Instance) for purposes of management, conceptualization or anything other than organizing a logical record or physical layout.
	   
	   Examples
	   ==========
	   Variables within a specific section of a study, Variables related to a specific subject or keyword. Variables at a specified level of development or review.
	   
	   Explanatory notes
	   ===================
	   A simple ordered or unordered list of variables can be described via a set of VariableIndicator parameters. An optional VariableRelationStructure can describe a more complex structure for the collection. We might, for example, use the VariableRelationStructure to group variables by content within a Study or across a StudySeries.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   l:VariableGroupType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.displayLabel = []
	    self.usage = None
	    self.groupingSemantic = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []

class CategoryRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Relation structure of categories within a collection. Allows for the specification of complex relationships among categories.
	   
	   Examples
	   ==========
	   The category of student might be described as having sub-types of primary school student and high school student.
	   
	   Explanatory notes
	   ===================
	   The CategoryRelationStructure employs a set of CategoryRelations to describe the relationship among concepts. Each CategoryRelation is a one to many description of connections between categories. Together they might commonly describe relationships as complex as a hierarchy. 
	   This is a kind of a ConceptRelationStructure restricted to categories (which are concepts).
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class ConceptSystem(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A set of Concepts structured by the relations among them. [GSIM 1.1] 
	   
	   Examples
	   ==========
	   1) Concept of Sex: Male, Female, Other 2) Concept of Household Relationship: Household Head, Spouse of Household Head, Child of Household Head, Unrelated Household Member, etc.  
	   
	   Explanatory notes
	   ===================
	   Note that this structure can be used to structure Concept, Classification, Universe, Population, Unit Type and any other class that extends from Concept.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   c:ConceptSchemeType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Concept System.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []

class Category(Concept):
	"""
	 Definition
	   ============
	   A Concept whose role is to define and measure a characteristic.
	   
	   Examples
	   ==========
	   Self-identified as "Male".  "Extremely Satisfied" as a response category.
	   
	   Explanatory notes
	   ===================
	   The Category is a Concept. It can have multiple name and display label properties as well as a definition and some descriptive text. As a "Signified" class there can be one or more "Sign" classes (e.g. a Code) that denotes it with some representation. The relationship is from the Code to the Category.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   l:CategoryType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.descriptiveText = None

class RepresentedVariable(ConceptualVariable):
	"""
	 Definition
	   ============
	   A combination of a characteristic of a universe to be measured and how that measure will be represented.
	   
	   
	   Examples
	   ==========
	   The pair (Number of Employees, Integer), where "Number of Employees" is the characteristic of the population (Variable) and "Integer" is how that measure will be represented (Value Domain).
	   
	   Explanatory notes
	   ===================
	   Extends from ConceptualVariable and can contain all descriptive fields without creating a ConceptualVariable. By referencing a ConceptualVariable it can indicate a common relationship with RepresentedVariables expressing the same characteristic of a universe measured in another way, such as Age of Persons in hours rather than years. RepresentedVariable constrains the coverage of the UnitType to a specific Universe. In the above case the Universe with the measurement of Age in hours may be constrained to Persons under 5 days (120 hours old). RepresentedVariable can define sentinel values for multiple storage systems which have the same conceptual domain but specialized value domains.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   l:RepresentedVariableType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.unitOfMeasurement = None
	    self.hasIntendedDataType = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class Universe(Concept):
	"""
	 Definition
	   ============
	   A defined class of people, entities, events, or objects, with no specification of time and geography, contextualizing a Unit Type
	   
	   Examples
	   ==========
	   1. Canadian adults (not limited to those residing in Canada)
	   2. Computer companies 
	   3. Universities
	   
	   Explanatory notes
	   ===================
	   Universe sits in a hierarchy between UnitType and Population, with UnitType being most general and Population most specific. A Universe is a set of entities defined by a more narrow specification than that of an underlying UnitType. A Population further narrows the specification to a specific time and geography.
	   
	   If the Universe consist of people a number of factors may be used in defining membership in the Universe, such as age, sex, race, residence, income, veteran status, criminal convictions, etc. The universe may consist of elements other than persons, such as housing units, court cases, deaths, countries, etc. A universe may be described as "inclusive" or "exclusive". 
	   
	   Not to be confused with Population, which includes the specification of time and geography.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   c:UniverseType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Population.
	"""
	def __init__(self)
	    self.isInclusive = None
	    self.displayLabel = []
	    self.descriptiveText = None
	    self. = []
	    self. = []

class Concept(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Unit of thought differentiated by characteristics [GSIM 1.1]
	   
	   Examples
	   ==========
	   Poverty, Income, Household relationship, Family, Gender, Business Establishment, Satisfaction, etc.
	   
	   Explanatory notes
	   ===================
	   Many DDI4 classes are subtypes of the Concept class including Category, Universe, UnitType, ConceptualVariable. This class realizes the pattern class Signified and as such a Concept can be denoted by a Sign.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   c:ConceptType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Concept.
	"""
	def __init__(self)
	    self.name = []
	    self.definition = None
	    self. = []

class SubstantiveConceptualDomain(ConceptualDomain):
	"""
	 Definition
	   ============
	   Set of valid Concepts. The Concepts can be described by either enumeration or by an expression.
	   
	   Examples
	   ==========
	   An enumeration of concepts for a categorical variable like "male" and "female" for gender.
	   
	   Explanatory notes
	   ===================
	   A ConceptualVariable links a UnitType to a SubstantiveConceptualDomain. The latter can be an enumeration or description of the values (Signified) that the variable may take on. In the enumerated case these are the categories in a CategorySet that can be values, not the codes that represent the values. 
	   An example might be the conceptual domain for a variable representing self-identified gender. An enumeration might include the concept of “male” and the concept of “female”. These, in turn, would be represented in a SubstantiveValueDomain by codes in a CodeList like “m” and “f”, or “0” and “1”. 
	   A conceptual domain might be described through a ValueAndConceptDescription’s description property of “a real number greater than 0” or through a more formal logicalExpression of “(all reals x such that x >0) “. Even in the described case, what is being described are conceptual, not the symbols used to represent the values. This may be a subtle distinction, but allows specifying that the same numeric value might be represented by 32 bits or by 64 bits or by an Arabic numeral or a Roman numeral.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Conceptual Domain.
	"""
	    pass

class SentinelConceptualDomain(ConceptualDomain):
	"""
	 Definition
	   ============
	   Description or list of possible sentinel concepts , e.g. missing values.
	   
	   Examples
	   ==========
	   Refused, 
	   Don't know, 
	   Lost in processing
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   missing categories
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class CategorySet(ConceptSystem):
	"""
	 Definition
	   ============
	   Specialization of a Concept System focusing on sets of categories used as specifications for a generalized concept. Can be used directly by questions to express a set of categories for a response domain or as the related source for a CodeList or StatisticalClassification
	   
	   Examples
	   ==========
	   "Male" and "Female" categories in a CategorySet named "Gender"
	   
	   A CategorySet for an occupational classification system like ISCO-08 could use a ClassificationRelationStructure to describe a hierarchy (Chief Executives and Administrative and Commercial Managers as subtypes of Managers) without an associated Code List.
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.contains = []
	    self. = []
