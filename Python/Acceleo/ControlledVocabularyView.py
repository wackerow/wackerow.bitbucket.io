"""
 PurposeControlled Vocabularies in DDI4
   The representation of a controlled vocabulary in DDI4 consists of three classes:
   ControlledVocabulary – The structured collection of vocabulary entries
   VocabularyEntry – Assigns a definition to a term
   VocabularyRelationStructure – Allows for the description of a complex structure among the terms, such as a hierarchy
   
   The ControlledVocabulary uses a set of  VocabularyEntryIndicators to delineate the entries in the vocabulary. These may be ordered with an index.
   The VocabularyRelationStructure uses a set of VocabularyEntryRelations to describe relationships among the vocabulary entries. A "source" entry might have a relationship to several "target" entries, for example one entry (e.g. Vehicle) might be a more general term for several other terms (e.g. car, bike, skateboard). This relationship may be described with a semantic, a relationSpecification (e.g. GeneralSpecific), and whether the relation is total or not.
"""

