class CategoryRelationCode:
	Nominal, Ordinal, Interval, Ratio, Continuous = range(5)

class ComputationBaseList:
	Total, ValidOnly, MissingOnly = range(3)

class TemporalRelationSpecification:
	TemporalMeets, TemporalContains, TemporalFinishes, TemporalPrecedes, TemporalStarts, TemporalOverlaps, TemporalEquals = range(7)

class WhiteSpaceRule:
	Preserve, Replace, collapse = range(3)

class MappingRelation:
	ExactMatch, CloseMatch, Disjoint = range(3)

class ShapeCoded:
	Rectangle, Circle, Polygon, LinearRing = range(4)

class SpatialObjectPairs:
	PointToPoint, PointToLine, PointToArea, LineToLine, LineToArea, AreaToArea = range(6)

class CollectionType:
	Bag, Set = range(2)

class SpatialObjectType:
	Point, Polygon, Line, LinearRing, Raster = range(5)

class SexSpecificationType:
	Masculine, Feminine, GenderNeutral = range(3)

class ValueRelationshipType:
	Equal, NotEqual, GreaterThan, GreaterThanOrEqualTo, LessThan, LessThanOrEqualTo = range(6)

class TotalityType:
	Total, Partial, Unknown = range(3)



class SpatialRelationSpecification:
	Equals, Disjoint, Intersects, Contains, Touches = range(5)



class PointFormat:
	DecimalDegree, DegreesMinutesSeconds, DecimalMinutes, Meters, Feet = range(5)

class TableDirectionValues:
	Ltr, Rtl, Auto = range(3)

class TrimValues:
	Start, End, Both, Neither = range(4)

class TextDirectionValues:
	Ltr, Rtl, Auto, Inherit = range(4)
