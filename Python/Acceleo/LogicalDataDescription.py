

























































class DataStoreRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   A structure for describing a complex relation of DataStores within a DataStoreLibrary
	   
	   Examples
	   ==========
	   If the succession of DataStores are created by a StudySeries that is a time series, from one DataStore to the next part/whole relations can obtain as supplements are added and subtracted.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class DataStoreLibrary(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A DataStoreLibrary is a collection or, again, a "library" of DataStores.
	   
	   An individual DataStore is associated with a Study. A collection of DataStores is associated with a StudySeries.
	   
	   The relationships among the DataStores in the DataStoreLibrary is described by the DataStoreRelationStructure. Relations may be more or less complicated depending on the StudySeries type. A StudySeries may be ad hoc. A StudySeries may form a time series. The variety of these collections has been described using the <a href=https://www.ddialliance.org/Specification/DDI-Lifecycle/3.1/XMLSchema/FieldLevelDocumentation/">"Group"</a> in DDI 3.1.
	   
	   Like any RelationStructure, the DataStoreRelationStructure is able to describe both part/whole relations and generalization/specialization relations. See the controlled vocabulary at <a href="http://lion.ddialliance.org/datatypes/relationspecification">RelationSpecification</a> to review all the types of relations a RelationStructure is able to describe. 
	   
	   Examples
	   ==========
	   A StudySeries is a time series and a DataStoreRelationStructure is dedicated to tracking "supplements".
	   
	   There is a study at T0. Perhaps it is never executed. It is the "core" study. At T1 there is a study that includes the core and Supplement A. At T2 there is a study that includes the core, Supplement A and Supplement B. At T3 there is a study that includes the core and a one off supplement that is never asked again. T4 though T6 repeats T1 through T3.
	   
	   
	   
	   Explanatory notes
	   ===================
	   If we broke a study down into sub-studies where the core is a sub-study and each supplement is a sub-study, we could also use the DataStoreRelationStructure to track the generalization/specialization relationship or, again, "going deep" over time.
	   
	   Sometimes across a StudySeries we add panels. If panels were also sub-studies, we could use the DataStoreRelationStructure to track both the core and supplements by panel over time.
	   
	   Synonyms
	   ==========
	   Structured metadata archive
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class UnitDataViewpoint(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The assignment of measure, identifier and attribute roles to InstanceVariables
	   
	   Each of three roles within a UnitDataViewpoint is a SimpleCollection of instance variables. 
	   
	   Examples
	   ==========
	   Assigning 
	   PatientID the role of identifier
	   systolic the role of measure
	   diastolic the role of measure
	   device the role of attribute
	   time the role of attribute
	   position (sitting, standing) the role of attribute
	   
	   Explanatory notes
	   ===================
	   Viewpoint is a UnitDataViewpoint to underline that the composition of a UnitDataRecord viewpoint is different from the composition of DataCube viewpoint. The DataCube viewpoint is out of scope currently. 
	   It is sometimes useful to describe a set of variables and their roles within the set. In the blood pressure example above, values for a set of variables are captured in one setting - the taking of a blood pressure. Some of the variables may be the measures of interest, for example the systolic and diastolic pressure. Other variables may take on the role of identifiers, such as the hospital’s patient ID and a government ID such as the U.S. Social Security Number. A third subgroup of variables may serve as attributes. The age of the patient, whether she is sitting or standing, the time of day. The assignment of these three roles, identifier, measure, and attribute, is the function of the Viewpoint.
	   Viewpoints are not fixed attributes of variables. This is why there is a separate Viewpoint class which maps to InstanceVariables.   In the example above, the Viewpoint is within the context of the original measurement. In a reanalysis of the data the roles might change. Time of day might be the measure of interest in a study of hospital practices.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []
	    self. = []

class IdentifierRole(ViewpointRole):
	"""
	 Definition
	   ============
	   An IdentifierRole identifies one or more InstanceVariables as being identifiers within a ViewPoint. An IdentifierRole is a SimpleCollection of InstanceVariables acting in the IdentifierRole.
	   
	   
	   Examples
	   ==========
	   A data record with four variables: "PersonId", "Systolic", "Diastolic", "Seated" might have a Viewpoint with 
	   PersonId defined as having the IdentifierRole
	   Systolic defined as having the MeasureRole
	   Diastolic defined as having the MeasureRole
	   Seated defined as having the AttributeRole
	   
	   PersonId, Systolic, Diastolic, Seated
	   123,122,20,yes
	   145,130,90,no
	   
	   
	   Explanatory notes
	   ===================
	   See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class InstanceVariableRelationStructure(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A realization of RelationStructure that is used to describe the order of InstanceVariables in a record for those cases where the variables have a more complicated logical order than a simple sequence.
	   
	   
	   Examples
	   ==========
	   The HL7 FHIR (Fast Healthcare Interoperability Resources) EHR is a non-homogeneous, hierarchical, recursive aggregate datatype.
	   
	   The openEHR archetypes that are one component in an EHR are non-homogeneous, hierarchical, non-recursive aggregate datatypes.
	   
	   Explanatory notes
	   ===================
	   For a simple sequence the order can be defined by the index values of the LogicalRecord's InstanceVariableIndicators that a UnitDataRecord inherits. Alternatively, the InstanceVariableRelationStructure can be used by a UnitDataRecord to describe a StructuredCollection as needed. 
	   
	   In terms of ISO-11404 an InstanceVariableRelationStructure is able to create "aggregate datatypes". These aggregate datatypes may be homogeneous if all the component datatypes are the same datatype or non-homogeneous. Aggregate datatypes may be hierarchical or not and they may be recursive or not. Under IS0-11404 the InstanceVariableRelationStructure qualifies as a "datatype generator".
	   
	   Synonyms
	   ==========
	   An information model, a struct in C, a nested object in JSON
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.criteria = None
	    self.displayLabel = []
	    self.usage = None
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.hasMemberRelation = []
	    self. = []

class MeasureRole(ViewpointRole):
	"""
	 Definition
	   ============
	   A MeasureRole identifies one or more InstanceVariables as being measures within a ViewPoint. A MeasureRole is a SimpleCollection of InstanceVariables acting in the MeasureRole.
	   
	   
	   
	   Examples
	   ==========
	   A data record with four variables: "PersonId", "Systolic", "Diastolic", "Seated" might have a Viewpoint with 
	   PersonId defined as having the IdentifierRole
	   Systolic defined as having the MeasureRole
	   Diastolic defined as having the MeasureRole
	   Seated defined as having the AttributeRole
	   
	   PersonId, Systolic, Diastolic, Seated
	   123,122,20,yes
	   145,130,90,no
	   
	   
	   Explanatory notes
	   ===================
	   See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class Datum(Designation):
	"""
	 Definition
	   ============
	   A Datum is a designation (a representation of a concept by a sign) with a notion of equality defined. The sign itself is a perceivable object.
	   
	   Examples
	   ==========
	   A systolic blood pressure of 122 is measured. The Signifier for that measurement in this paragraph is the character string "122". The Datum in this case is a kind of Designation (Sign), which is the association of the underlying measured concept (a blood pressure at that level) with that Signifier.  The Datum has an association with an InstanceVariable which allows the attachment of a unit of measurement (mm Hg), a datatype, a Population, and the Act which produced the measurement.  These InstanceVariable attributes are critical for interpreting the Signifier.
	   
	   Explanatory notes
	   ===================
	   From GSIM 1.1 "A Datum is the actual instance of data that was collected or derived. It is the value which populates one or more Data Points. A Datum is the value found in a cell of a table." (https://statswiki.unece.org/display/GSIMclick/Datum )  A Datum could be copied to one or more datasets.
	   DDI4 takes a little more formal (semiotic) description of a Datum using the Signification Pattern. See the attached example.
	   
	   
	   NOTE: This is NOT datum from DDI3.2 (which is quite specific).
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Datum.
	"""
	def __init__(self)
	    self.representation = None
	    self. = []

class LogicalRecord(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The LogicalRecord is a record definition. It is abstract.
	   
	   The actual realization of record definitions are provided by the UnitDataRecord and the DataCube. UnitDataRecords support two types of record definitions. The first is based on the SimpleCollection it inherits from LogicalRecord. The second type is a StructuredCollection that specializes the LogicalRecord. As a SimpleCollection a UnitDataRecord can, for example, provide table definitions. As a StructuredCollection, UnitDataRecord can define structures comparable to a "struct" in C or a JSON nested object. A UnitDataRecord, then, inherits SimpleCollection from LogicalRecord but adds its own StructuredCollection into the mix. The DataCube brings into play its own StructuredCollection which is distinct from the UnitDataRecord StructuredCollection.The DataCube StructuredCollection is not currently in scope.
	   
	   The various record definitions -- simple and structured -- when they are actually instantiated with data is described by the PhysicalLayout of a LogicalRecord in the FormatDescription package. The PhysicalLayout places data into DataPoints formed at the direction of a LogicalRecord and its instance variables. At this point a LogicalRecord turn into a "dataset" that hosts unit records, A record definition is not a record. So we conflate metadata and data when we refer to a LogicalRecord as an empty table.
	   
	   Examples
	   ==========
	   SQL Data Definition Language (DDL) traffics in record definitions. SQL queries on "system tables" discover record definitions.
	   
	   Explanatory notes
	   ===================
	   In GSIM a DataPoint is a member of a "DataSet" and a UnitDataRecord. Since a DataPoint contains "Datum", this can lead to the conflation of a DataSet and a record definition. In the presence of a DataPoint it is difficult to be clear that a record definition does not have any rows. 
	   
	   In DDI4 we defer the introduction of DataPoints until PhysicalLayouts are described. In this approach the instance variables that make up a LogicalRecord work downstream where they help to give each DataPoint in a PhysicalRecord its format.
	   
	   Synonyms
	   ==========
	   Record Type, Data Structure
	   
	   DDI 3.2 mapping
	   =================
	   l:LogicalRecordType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Data Structure.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []

class DataStore(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A DataStore is either a SimpleCollection or a StructuredCollection of LogicalRecords, keeping in mind that a LogicalRecords is a definition, not a "datasets".
	   
	   LogicalRecords organized in a StructuredCollection is called a LogicalRecordRelationStructure.
	   
	   Instances of LogicalRecords instantiated as organizations of DataPoints hosting data are described in FormatDescription.
	   
	   A DataStore is reusable across studies. Each Study has at most one DataStore.
	   
	   
	   Examples
	   ==========
	   The data lineage of an individual BusinessProcess or an entire DataPipeline are both examples of  a LogicalRecordRelationStructures. In a data lineage we can observe how LogicalRecords are connected within a BusinessProcess or across BusinessProcesses.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   Schema repository, data network
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Data Set.
	"""
	def __init__(self)
	    self.characterSet = None
	    self.dataStoreType = None
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.recordCount = None
	    self.aboutMissing = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = None
	    self. = None
	    self. = []

class AttributeRole(ViewpointRole):
	"""
	 Definition
	   ============
	   An AttributeRole identifies one or more InstanceVariables as being attributes within a ViewPoint. An AttributeRole is a SimpleCollection of InstanceVariables acting in the AttributeRole.
	   
	   
	   Examples
	   ==========
	   A data record with four variables: "PersonId", "Systolic", "Diastolic", "Seated" might have a Viewpoint with 
	   PersonId defined as having the IdentifierRole
	   Systolic defined as having the MeasureRole
	   Diastolic defined as having the MeasureRole
	   Seated defined as having the AttributeRole
	   
	   PersonId, Systolic, Diastolic, Seated
	   123,122,20,yes
	   145,130,90,no
	   
	   
	   
	   Explanatory notes
	   ===================
	   See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint.
	   An InstanceVariable with an AttributeRole assigned might contain data about the conditions under which the MeausreRole InstanceVariables were collected or other paradata.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class RecordRelation(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The RecordRelation object is used to indicate relationships among record types within and between LogicalRecords. For InstanceVariables existing in a LogicalRecord with multiple record layouts, pairs of InstanceVariables may function as paired keys to permit the expression of hierarchical links between records of different types. These links between keys in different record types could also be used to link records in a relational structure.
	   
	   Examples
	   ==========
	   One LogicalRecord containing a PersonIdentifier and a PersonName and another LogicalRecord containing a MeasurementID, a PersonID, a SystolicPressure, and a DiastolicPressure could be linked by a RecordRelation. The RecordRelation could employ an InstanceVariableValueMap to describe the linkage between  PersonIdentifier and PersonID.
	   
	   Explanatory notes
	   ===================
	   A household-level LogicalRecord might contain an InstanceVariable called HouseholdID and a person-level LogicalRecord might contain an InstanceVariable called HID. Describing a link between HouseholdID and HID would allow the linking of a person-level LogicalRecord to their corresponding household-level LogicalRecord.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.purpose = None
	    self.usage = None
	    self.correspondence = []
	    self. = []
	    self. = []

class UnitDataRecord(LogicalRecord):
	"""
	 Definition
	   ============
	   Gives a UnitDataRecord structure to a Logical Record. UnitDataRecord structures a LogicalRecord as either a SimpleCollection or a StructuredCollection (InstanceVariableRelationStructure) of instance variables.
	   
	   The UnitDataRecord also organizes instance variables in a UnitDataViewpoint. The UnitDataViewpoint assigns roles to Instance Variables. In any one UnitDataViewpoint UnitDataRecord instance variables play one of three roles: identifier, measure or attribute. The same UnitDataRecord may have many UnitDataViewpoints.
	   
	   The UnitDataViewPoint can work in conjunction with the InstanceVariableRelationStructure. Together they can break out instance variables into complex (hierarchical) identifier, measure and attribute groups. 
	   
	   Examples
	   ==========
	   The UnitDataRecord can be used to specify the structure of a table in a relational database. It can be used to specify the structure of both the fact and dimension tables in a data warehouse. It can be used to specify the structure of a big data table. Likewise it can be used to specify the structures of "column-based" and "row-based" tables.
	   
	   The UnitDataRecord may also form an information model if it is a StructuredCollection. Blood Pressure as described by an openEHR archetype is an information model. So is a collection of FHIR (HL7) resources that form an Electronic Health Record (EHR). 
	   
	   Explanatory notes
	   ===================
	   The <a href="http://www.openehr.org/ckm/">openEHR archetype</a> is a use case that motivates the use of the InstanceVariableRelationStructure together with a UnitDataViewPoint. In the Blood Pressure archetype as well as most other archetypes attributes are not a SimpleCollection. Instead they are part of a StructuredCollection that breaks out attributes into "Protocol", "State" and "Events". In other archetypes the measure may be structured. For example body composition data includes a "base model" (fat mass, fat percentage, fat free mass), an "atomic level" (chemical elements, hydrogen, carbon, oxygen), a "molecular level" (minerals, protein, fat, water) and so forth.
	   
	   It is, however, possible to break out attributes into not one but many SimpleCollections in the event attributes and/or data do not form StructuredCollections. That is because the UnitDataViewpoint supports not just one but many attribute roles within the same viewpoint.
	   
	   Synonyms
	   ==========
	   Table Definition if the UnitDataRecord is a SimpleCollection, Information Model if the UnitDataRecord is a StructuredCollection
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class DataPoint(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A DataPoint is a container for a Datum.
	   
	   Examples
	   ==========
	   A cell in a spreadsheet table. Note that this could be empty. It exists independently of the value to be stored in it.
	   
	   Explanatory notes
	   ===================
	   The DataPoint is structural and distinct from the value (the Datum) that it holds. [GSIM 1.1]
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Data Point.
	"""
	def __init__(self)
	    self. = []
	    self. = None
	    self. = []

class LogicalRecordRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Allows for the complex structuring of relationships between LogicalRecords in a DataStore
	   
	   Examples
	   ==========
	   A DataStore with a Household, Family, and Person LogicalRecord type. Allows for describing parent/child, whole/part, or other relationships as appropriate
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class ViewpointRole(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A ViewpointRole designates the function an InstanceVariable performs in the context of the Viewpoint. (IdentifierRole, AttributeRole, or MeasureRole of interest).
	   
	   Each of three roles within a Viewpoint may be a collection. This happens when a role is mapped to multiple instance variables. In this event a role forms a SimpleCollection. There are SimpleCollections of instance variables in each role.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
