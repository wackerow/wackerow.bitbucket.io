









































class PhysicalSegmentLayout(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The PhysicalSegmentLayout is an abstract class used as an extension point in the description of the different layout styles of data structure description.
	   
	   Examples
	   ==========
	   Examples include UnitSegmentLayouts, event data layouts, and cube layouts (e.g. summary data).
	   
	   Explanatory notes
	   ===================
	   A PhysicalLayout is a physical description (EventLayout, UnitSegmentLayout, or CubeLayout) of the associated Logical Record Layout consisting of a Collection of Value Mappings describing the physical representation of each related Instance Variable.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.isDelimited = None
	    self.delimiter = None
	    self.isFixedWidth = None
	    self.escapeCharacter = None
	    self.lineTerminator = []
	    self.quoteCharacter = None
	    self.commentPrefix = None
	    self.encoding = None
	    self.hasHeader = None
	    self.headerRowCount = None
	    self.skipBlankRows = None
	    self.skipDataColumns = None
	    self.skipInitialSpace = None
	    self.skipRows = None
	    self.trim = TrimValues.Start
	    self.nullSequence = None
	    self.headerIsCaseSensitive = None
	    self.arrayBase = None
	    self.treatConsecutiveDelimitersAsOne = None
	    self.overview = None
	    self.tableDirection = TableDirectionValues.Ltr
	    self.textDirection = TextDirectionValues.Ltr
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class SegmentByText(PhysicalSegmentLocation):
	"""
	 Definition
	   ============
	   Defines the location of a segment of text through character, and optionally line, counts. An adequate description will always include a startCharacterPosition and then may include an endCharacterPosition or a characterLength.
	   If StartLine is specified, the character counts begin within that line.
	   An endCharacterPosition of 0 indicates that whole lines are specified
	   
	   
	   Examples
	   ==========
	   The segment beginning at line 3, character 4 and ending at line 27 character 13.
	   Alternatively the segment beginning at character 257 and ending at character 1350 of the whole body of text.
	   StartLine of 10, endLine of 12, startCharacterPosition of 1, endCharacterPosition of 0 means all of lines 10,11, and 12. 
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.startLine = None
	    self.endLine = None
	    self.startCharacterPosition = None
	    self.endCharacterPosition = None
	    self.characterLength = None

class UnitSegmentLayout(PhysicalSegmentLayout):
	"""
	 Definition
	   ============
	   UnitSegmentLayout supports the description of unit-record ("wide") data sets, where each row in the data set provides the same group of values for variables all relating to a single unit. Each logical column will contain data relating to the values for a single variable. 
	   
	   Examples
	   ==========
	   A simple spreadsheet. Commonly the first row of the table will contain variable names or descriptions.
	   
	   The following csv file has a rectangular layout and would import into a simple table in a spreadsheet:
	   PersonId,AgeYr,HeightCm
	   1,22,183
	   2,45,175
	   
	   
	   Explanatory notes
	   ===================
	   This is the classic rectangular data table used by most statistical packages, with rows/cases/observations and columns/variables/measurements. Each cell (DataPoint) in the table is the intersection of a Unit (row) and an InstanceVariable. 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class PhysicalDataSet(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The information needed for understanding the physical structure of data coming from a file or other source. A StructureDescription also points to the Data Store it physically represents.
	   
	   Examples
	   ==========
	   The PhysicalDataProduct is the entry point for information about a file or other source. It includes information about the name of a file, the structure of segments in a file and the layout of segments.
	   
	   Explanatory notes
	   ===================
	   Multiple styles of structural description are supported: including describing files as unit-record (UnitSegmentLayout) files; describing cubes; and describing event-history (spell) data.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   p:PhysicalDataProductType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self.physicalFileName = None
	    self.numberOfSegments = None
	    self.contains = []
	    self.isOrdered = None
	    self.type = CollectionType.Bag
	    self.purpose = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class DataPointRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   A means for describing the complex relational structure of Data Points in a Logical Record 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class PhysicalSegmentLocation(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Defines the location of a segment in a DataStore (e.g. a text file). This is abstract since there are many different ways to describe the location of a segment - character counts, start and end times, etc.
	   
	   Examples
	   ==========
	   A segment of text in a plain text file beginning at character 3 and ending at character 123.
	   
	   Explanatory notes
	   ===================
	   While this has no properties or relationships of its own, it is useful as a target of relationships where its extensions may serve.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class ValueMapping(Identifiable):
	"""
	 Definition
	   ============
	   Provides physical characteristics for an InstanceVariable as part of a PhysicalSegmentLayout
	   
	   Examples
	   ==========
	   A variable "age" might be represented in a file as a string with a maximum length of 5 characters and a number pattern of ##0.0
	   
	   Explanatory notes
	   ===================
	   An InstanceVariable has details of value domain and datatype, but will not have the final details of how a value is physically represented in a data file. A variable for height, for example, may be represented as a real number, but may be represented as a string in multiple ways. The decimal separator might be, for example a period or a comma. The string representing the value of a payment might be preceded by a currency symbol. The same numeric value might be written as “1,234,567” or “1.234567 E6”. A missing value might be written as “.”, “NA”, “.R” or as “R”. 
	   The ValueMapping describes how the value of an InstanceVariable is physically expressed. The properties of the ValueMapping as intended to be compatible with the W3C Metadata Vocabulary for Tabular Data (https://www.w3.org/TR/tabular-metadata/ ) as well as common programming languages and statistical packages. The “format” property, for example can draw from an external controlled vocabulary such as the set of formats for Stata, SPSS, or SAS.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.physicalDataType = None
	    self.defaultDecimalSeparator = None
	    self.defaultDigitGroupSeparator = None
	    self.numberPattern = None
	    self.defaultValue = None
	    self.nullSequence = None
	    self.format = None
	    self.length = None
	    self.minimumLength = None
	    self.maximumLength = None
	    self.scale = None
	    self.decimalPositions = None
	    self.isRequired = None
	    self. = []
	    self. = []
	    self. = []

class PhysicalRecordSegment(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A description of each physical storage segment required to completely cover the logical record. A logical record may be stored in one or more segments housed hierarchically in a single file or in separate data files. All logical records have at least one segment.
	   
	   Examples
	   ==========
	   The file below has four InstanceVariables: PersonId, SegmentId, AgeYr, HeightCm. The data for each person (identified by PersonId) is recorded in two segments (identified by SegmentId), "a" and "b". AgeYr is on physical segment a, and HeightCm is on segment b. These are the same data as described in the UnitSegmentLayout documentation.
	   1 a  22
	   1 b 183
	   2 a 45
	   2 b 175
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   p:PhysicalRecordSegmentType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.physicalFileName = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class PhysicalLayoutRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   A realization of RelationStructure that is used to describe the sequence of Value Mappings in a Physical Layout. This can be more complex than a simple sequence. 
	   
	   Examples
	   ==========
	   The W3C Tabular Data on the Web specification allows for a list datatype. In the example below there are three top level InstanceVariables
	   PersonID – the person identifier
	   AgeYr – age in year
	   BpSys_Dia – blood pressure (a list containing Systolic and Diastolic values)
	   
	   There are two variables at a secondary level of the hierarchy
	   Systolic – the systolic pressure
	   Diastolic – the diastolic pressure
	   
	   The delimited file below uses the comma to separate "columns" and forward slash to separate elements of a blood pressure list.
	   
	   PersonID, AgeYr, BpSys_Dia
	   1,22,119/67
	   2,68,122/70
	   
	   The PhysicalRelationStructure in this case would describe a BpSys_Dia  list variable as containing an ordered sequence of the Systolic and Diastolic InstanceVariables.
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.criteria = None
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class PhysicalOrderRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   PhysicalStructureOrder orders thePhysicalRecordSegments  which map to a LogicalRecord.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   The same LogicalRecordLayout may be the sourceMember in several adjacency lists. This can happen when PhysicalRecordSegments are also population specific. In this instance each adjacency list associated with a LogicalRecordLayout is associated with a different population.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []
