













class FundingInformation(Identifiable):
	"""
	 Definition
	   ============
	   Provides information about the individual, agency and/or grant(s) which funded the described entity. Lists a reference to the agency or individual as described by a DDI Agent, the role of the funder, the grant number(s) and a description of the funding activity.
	   
	   Examples
	   ==========
	   A "millionaire grant" (funding description) from John Beresford Tipton, Jr. (individual). Exploration of the effect of sudden wealth and basis for a television episode (funder role).
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:FundingInformationType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.funderRole = None
	    self.grantNumber = []
	    self.purpose = None
	    self. = []

class ExternalMaterial(Identifiable):
	"""
	 Definition
	   ============
	   ExternalMaterial describes the location, structure, and relationship to the DDI metadata instance for any material held external to that instance. This includes citations to such material, an external reference to a URL (or other URI), and a statement about the relationship between the cited ExternalMaterial the contents of the DDI instance. It should be used as follows:as a target object from a relationship which clarifies its role within a class; or as the target of a relatedResource within an annotation.
	   
	   
	   Examples
	   ==========
	   ExternalMaterial is used to identify material and optionally specific sections of the material that have a relationship to a class. There is a generic relatedMaterial on AnnotatedIdentifiable. This should be used to attach any additional material that the user identifies as important to the class. The properties typeOfMaterial, descriptiveText, and relationshipDescription should be used to clarify the purpose and coverage of the related material.
	   
	   Explanatory notes
	   ===================
	   Within the DDI model, ExternalMaterial is used as an extension base for specific external materials found such as an External Aid. It is used as a base for specifically related material (e.g. ExternalAid) by creating a relationship whose name clarifies the purpose of the related material.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:OtherMaterialType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfMaterial = None
	    self.descriptiveText = None
	    self.uri = []
	    self.relationshipDescription = []
	    self.mimeType = None
	    self.usesSegment = []
	    self.citationOfExternalMaterial = None

class DocumentInformation(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Provides a consistent path to identifying information regarding the DDI Document and is automatically available for all Functional Views (/DocumentInformation/Annotation etc.). It covers annotation information, coverage (general, plus specific spatial, temporal, and topical coverage), access information from the producer (persistent access control), access information from the local distributor (Local Access Control), information on related series (i.e. a study done as a series of data capture events over time, qualitative material that is part of a larger set, one of a series of funded data capture activities, etc.), funding information, and other document level information used to identify and discover information about a DDI document regardless of its Functional View type. Use the Annotation at this level to provide the full annotation of the DDI Document. Note that related materials can be entered within the Annotation fields.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Annotation associated with a specific class is the annotation of the metadata class itself, such as the Study, InstanceVariable, Concept, etc. The annotation associated with Document Information is to refer to the Document itself. Note that not all DDI instances are intended to be persistent documents. For those that are DocumentInformation provides a consistent set of discovery level information on the content of the whole rather than specific parts of the instance. 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.contentCoverage = []
	    self.isPublished = None
	    self.hasPrimaryContent = None
	    self.ofType = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
