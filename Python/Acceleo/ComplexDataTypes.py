from datetime import datetime

class Map(object):
	"""
	 Definition
	   ============
	   Describes the correspondence between concepts in a correspondence table related to one or more Statistical Classifications.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   A Map is the pairing of similar Concepts. Each Concept in the Map belongs to a different Collection. The collection of maps for all the Concepts in corresponding Collections is a Correspondence Table.
	   
	   A simple example might map the following 2 martial status category sets:
	   MS1 -
	   single
	   married
	   widowed
	   divorced
	   
	   MS2 -
	   single
	   married
	   
	   So, a correspondence table between these 2 category sets might look like this:
	   MS1                                               MS2
	   single                                             single
	   widowed                                          "
	   divorced                                          "
	   married                                            married
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Map.
	"""
	def __init__(self)
	    self.validDates = None
	    self.hasCorrespondenceType = None
	    self.displayLabel = None
	    self.usage = None
	    self. = []
	    self. = []
	    self. = []





class StatisticalClassificationIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type StatisticalClassification
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class CustomItemIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type CustomItem
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []

class LanguageSpecificStructuredStringType(object):
	"""
	 Definition
	   ============
	   Supports the optional use of XHTML formatting tags within the string structure. XHTML tag content is controlled by the schema, see http://www.w3.org/1999/xhtml/ for a detailed list of available tags. Language of the string is defined by the attribute language. The content can be identified as translated (isTranslated), subject to translation (isTranslatable), the result of translation from one or more languages (translationSourceLanguages), and carry an indication whether or not it should be treated as plain text (isPlain).
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ContentType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.formattedContent = []
	    self.language = None
	    self.isTranslated = None
	    self.isTranslatable = None
	    self.translationSourceLanguage = []
	    self.translationDate = None
	    self.isPlainText = None





class RelationSpecification:
	Unordered, List, ParentChild, WholePart, AcyclicPrecedence, Equivalence, GeneralSpecfic = range(7)

class SpatialLine(object):
	"""
	 Definition
	   ============
	   Defines a geographic line (minimum of 2 points)
	   
	   Examples
	   ==========
	   Street, River
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.uri = None
	    self.lineLinkCode = None
	    self.shapeFileFormat = None
	    self.point = []

class CustomValueRelation(object):
	"""
	 Definition
	   ============
	   Defines the relation between CustomValues
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class StandardKeyValuePair(object):
	"""
	 Definition
	   ============
	   A basic data representation for computing systems and applications expressed as a tuple (attribute key, value). Attribute keys may or may not be unique.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:StandardKeyValuePairType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.attributeKey = None
	    self.attributeValue = None









class Date(object):
	"""
	 Definition
	   ============
	   Provides the structure of a single Date expressed in an ISO date structure along with equivalent expression in any number of non-ISO formats. While it supports the use of the ISO time interval structure this should only be used when the exact date is unclear (i.e. occurring at some point in time between the two specified dates) or in specified applications. Ranges with specified start and end dates should use the DateRange as a datatype. Commonly uses property names include: eventDate, issueDate, and releaseDate.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Date allows one of a set of date-time (YYYY-MM-DDThh:mm:ss), date (YYYY-MM-DD), year-month (YYYY-MM), year (YYYY), time (hh:mm:ss) and duration (PnYnMnDnHnMnS), or time interval (YYYY-MM-DDThh:mm:ss/YYYY-MM-DDThh:mm:ss, YYYY-MM-DDThh:mm:ss/PnYnMnDnHnMnS, PnYnMnDnHnMnS/ YYYY-MM-DDThh:mm:ss) which is formatted according to ISO 8601 and backed supported by regular expressions in the BaseDateType. Time Zone designation and negative/positive prefixes are allowed as are dates before and after 0000 through 9999.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:DateType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.isoDate = None
	    self.nonIsoDate = []



class CommandCode(object):
	"""
	 Definition
	   ============
	   Contains information on the command used for processing data. Contains a description of the command which should clarify for the user the purpose and process of the command, an in-line provision of the command itself, and a reference to an external version of the command such as a coding script. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:CommandCodeType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.description = None
	    self.usesCommandFile = []
	    self.usesCommand = []









class DataStoreRelation(object):
	"""
	 Definition
	   ============
	   Defines the complex relationship between 2 or more DataStores in a DataLibrary
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []





class VariableIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type ConceptualVariable, RepresentedVariable, and InstanceVariable
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class StudyIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type Study
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []

class Telephone(object):
	"""
	 Definition
	   ============
	   Details of a telephone number including the number, type of number, a privacy setting and an indication of whether this is the preferred contact number.
	   
	   Examples
	   ==========
	   +12 345 67890123
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:TelephoneType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.telephoneNumber = None
	    self.typeOfTelephone = None
	    self.effectiveDates = None
	    self.privacy = None
	    self.isPreferred = None

class DoubleNumberRangeValue(object):
	"""
	 Definition
	   ============
	   Describes a bounding value for a number range expressed as an xs:double.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:DoubleNumberRangeValueType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.isInclusive = None
	    self.doubleValue = None

class PhysicalRecordSegmentRelation(object):
	"""
	 Definition
	   ============
	   Defines structured relationship between PhysicalRecordSegments.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []



class Address(object):
	"""
	 Definition
	   ============
	   Location address identifying each part of the address as separate elements, identifying the type of address, the level of privacy associated with the release of the address, and a flag to identify the preferred address for contact.
	   
	   Examples
	   ==========
	   For example:
	   1.  OFFICE, ABS HOUSE, 45 Benjamin Way, Belconnen, Canberra, ACT, 2617, AU
	   2.  OFFICE, Institute of Education, 20 Bedford Way, London, WC1H 0AL, UK
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:AddressType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfAddress = None
	    self.line = []
	    self.cityPlaceLocal = None
	    self.stateProvince = None
	    self.postalCode = None
	    self.countryCode = None
	    self.timeZone = None
	    self.effectiveDates = None
	    self.privacy = None
	    self.isPreferred = None
	    self.geographicPoint = None
	    self.regionalCoverage = None
	    self.typeOfLocation = None
	    self.locationName = None



class Command(object):
	"""
	 Definition
	   ============
	   Provides the following information on the command. The content of the command and the programming language used.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:CommandType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.programLanguage = None
	    self.commandContent = None







class NumberRange(object):
	"""
	 Definition
	   ============
	   Structures a numeric range. Low and High values are designated. The structure identifies Low values that should be treated as bottom coded (Stated value and bellow, High values that should be treated as top coded (stated value and higher), and provides a regular expression to further define the valid content of the range.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:NumberRangeType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.label = []
	    self.highCode = None
	    self.highCodeDouble = None
	    self.lowCode = None
	    self.lowCodeDouble = None
	    self.regExp = None
	    self.isTopCoded = None
	    self.isBottomCoded = None



class Binding(object):
	"""
	 Definition
	   ============
	   Binds two parameters together to direct flow of data through a process
	   
	   Examples
	   ==========
	   From the output of an InstanceQuestion to the input of a ComputationAction
	   
	   Explanatory notes
	   ===================
	   Binding is used to define the flow of data into, out of, and within a process. It is separate from the flow of a process. When used in the workflow of a data capture process most data may go from a capture to an instance variable, but when needed as a check sum in a loop, a recoding process, or conditional content for DynamicText, Binding provides the means for explicitly directing the movement of data from one point to another in the process.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []



class Annotation(object):
	"""
	 Definition
	   ============
	   Provides annotation information on the object to support citation and crediting of the creator(s) of the object.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:CitationType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.title = None
	    self.subTitle = []
	    self.alternativeTitle = []
	    self.creator = []
	    self.publisher = []
	    self.contributor = []
	    self.date = []
	    self.languageOfObject = None
	    self.identifier = []
	    self.copyright = []
	    self.typeOfResource = []
	    self.informationSource = []
	    self.versionIdentification = None
	    self.versioningAgent = []
	    self.summary = None
	    self.relatedResource = []
	    self.provenance = []
	    self.rights = []
	    self.recordCreationDate = None
	    self.recordLastRevisionDate = None



class InternationalStructuredString(object):
	"""
	 Definition
	   ============
	   Packaging structure for multiple language versions of the same string content, for objects that allow for internal formatting using XHTML tags. Where an element of this type is repeatable, the expectation is that each repetition contains different content, each of which can be expressed in multiple languages.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:StructuredStringType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.languageSpecificStructuredString = []







class BusinessProcessCondition(object):
	"""
	 Definition
	   ============
	   A BusinessProcess precondition or post condition which describes the condition which must be met to begin (pre) or exit (post) a process. It may use a specified LogicalRecord. The Logical Record has SQL that describes it, rejectionCriteria against which its adequacy may be tested and an optional annotation that describes its provenance.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.sql = None
	    self.rejectionCriteria = None
	    self.dataDescription = []
	    self. = []





class ContactInformation(object):
	"""
	 Definition
	   ============
	   Contact information for the individual or organization including location specification, address, web site, phone numbers, and other means of communication access. Address, location, telephone, and other means of communication can be repeated to express multiple means of a single type or change over time. Each major piece of contact information contains the element EffectiveDates in order to date stamp the period for which the information is valid.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:ContactInformationType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.website = []
	    self.hasEmail = []
	    self.electronicMessaging = []
	    self.hasAddress = []
	    self.hasTelephone = []



class WorkflowStepIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type WorkflowStep, Act, and ConditionalControlConstruct or any of their subtypes
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []







class WorkflowStepSequenceIndicator(object):
	"""
	 Definition
	   ============
	   Identify WorkflowStepSequence's that are organized into a master sequence for executing a WorkflowProcess.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []









class Image(object):
	"""
	 Definition
	   ============
	   A reference to an image, with a description of its properties and type.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ImageType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.uri = None
	    self.typeOfImage = None
	    self.dpi = None
	    self.languageOfImage = None

class AgentId(object):
	"""
	 Definition
	   ============
	   Persistent identifier for a researcher using a system like ORCID
	   
	   Examples
	   ==========
	   ORCID
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.agentIdValue = None
	    self.agentIdType = None

class ValueString(object):
	"""
	 Definition
	   ============
	   The Value expressed as an xs:string with the ability to preserve whitespace if critical to the understanding of the content. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ValueType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.content = None
	    self.whiteSpace = WhiteSpaceRule.Preserve
	    self. = []





class IndividualName(object):
	"""
	 Definition
	   ============
	   The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix. The preferred compilation of the name parts may also be provided. The legal or formal name of the individual should have the isFormal attribute set to true. The preferred name should be noted with the isPreferred attribute. The attribute sex provides information to assist in the appropriate use of pronouns.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:IndividualNameType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.prefix = None
	    self.firstGiven = None
	    self.middle = []
	    self.lastFamily = None
	    self.suffix = None
	    self.fullName = None
	    self.effectiveDates = None
	    self.abbreviation = None
	    self.typeOfIndividualName = None
	    self.sex = SexSpecificationType.Masculine
	    self.isPreferred = None
	    self.context = None
	    self.isFormal = None

class DataPointIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type DataPoint
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []





class LogicalRecordRelation(object):
	"""
	 Definition
	   ============
	   Defines complex relationship between LogicalRecords. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class CategoryRelation(object):
	"""
	 Definition
	   ============
	   Source target relationship between categories in a classification relation structure
	   
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.totality = TotalityType.Total
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self. = []
	    self. = []
	    self. = []

class InstanceVariableRelation(object):
	"""
	 Definition
	   ============
	   Defines the relationship between 2 or more InstanceVariables.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Use when relationships are limited to InstanceVariables only. Use VariableRelation when mixing relationships between various levels in the Variable Cascade. Use ConceptRelation when any subtype of Concept may be part of the relationship.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.totality = TotalityType.Total
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self. = []
	    self. = []
	    self. = []

class WebLink(object):
	"""
	 Definition
	   ============
	   A web site (normally a URL) with information on type of site, privacy flag, and effective dates.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:URLType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.isPreferred = None
	    self.uri = None
	    self.typeOfWebsite = None
	    self.effectiveDates = None
	    self.privacy = None





class CommandFile(object):
	"""
	 Definition
	   ============
	   Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script. Designates the programming language of the command file, a description of the location of the file , and a URN or URL for the command file.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:CommandFileType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.programLanguage = None
	    self.location = None
	    self.uri = None

class CharacterOffset(object):
	"""
	 Definition
	   ============
	   Specification of the character offset for the beginning and end of the segment, or beginning and length.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:CharacterParameterType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.startCharOffset = None
	    self.endCharOffset = None
	    self.characterLength = None

class Polygon(object):
	"""
	 Definition
	   ============
	   A closed plane figure bounded by three or more line segments, representing a geographic area. Contains either the URI of the file containing the polygon, a specific link code for the shape within the file, and a file format, or a minimum of 4 points to describe the polygon in-line. Note that the first and last point must be identical in order to close the polygon. A triangle has 4 points. A geographic time designating the time period that the shape is valid should be included. If the date range is unknown use a SingleDate indicating a date that the shape was known to be valid.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:PolygonType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.uri = None
	    self.polygonLinkCode = None
	    self.shapeFileFormat = None
	    self.point = []

class Form(object):
	"""
	 Definition
	   ============
	   A link to a form used by the metadata containing the form number, a statement regarding the contents of the form, a statement as to the mandatory nature of the form and a privacy level designation.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:FormType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.formNumber = None
	    self.uri = None
	    self.statement = None
	    self.isRequired = None



class PhysicalRecordSegmentIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type PhysicalRecordSegment
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []













class WorkflowStepRelation(object):
	"""
	 Definition
	   ============
	   Describes structured relations between WorkflowSteps
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasTemporalRelationSpecification = TemporalRelationSpecification.TemporalMeets
	    self. = []
	    self. = []
	    self. = []





class DynamicTextContent(object):
	"""
	 Definition
	   ============
	   Abstract type existing as the head of a substitution group. May be replaced by any valid member of the substitution group TextContent. Provides the common property of purpose to all members using TextContent as an extension base.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:TextContentType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.purpose = None
	    self.orderPosition = None



class Level(object):
	"""
	 Definition
	   ============
	   Provides Level information for the members of the LevelStructure. levelNumber provides the level number which may or may not be associated with a category which defines level.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   ISCO-08: index='1' label of associated category 'Major',  index='2' label of associated category 'Sub-Major',  index='3' label of associated category 'Minor', 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.levelNumber = None
	    self.displayLabel = []
	    self. = []

class NumberRangeValue(object):
	"""
	 Definition
	   ============
	   Describes a bounding value for a number range expressed as an xs:demical.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:NumberRangeValueType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.isInclusive = None
	    self.decimalValue = None

class AgentAssociation(object):
	"""
	 Definition
	   ============
	   A basic structure for declaring the name of an Agent inline, reference to an Agent, and role specification. This object is used primarily within Annotation.
	   
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ContributorType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.agentName = None
	    self.role = []
	    self. = []

class TargetSample(object):
	"""
	 Definition
	   ============
	   Specifies details of target sample size, percentage, type, and universe
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.isPrimary = None
	    self.targetSize = None
	    self.targetPercent = None
	    self. = None
	    self. = None

class LineParameter(object):
	"""
	 Definition
	   ============
	   Specification of the line and offset for the beginning and end of the segment.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:LineParameterType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.startLine = None
	    self.startOffset = None
	    self.endLine = None
	    self.endOffset = None



class ValueMappingRelation(object):
	"""
	 Definition
	   ============
	   Variable relationships in PhysicalLayout as defined using ValueMappingRelation
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class TextualSegment(object):
	"""
	 Definition
	   ============
	   Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:TextualType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.lineParamenter = None
	    self.characterParameter = None









class DataPointRelation(object):
	"""
	 Definition
	   ============
	   Data Point relations used by the Data Point Relation Structure of a Logical Record to describe specific source target Data Points and their relationship
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []





class GeographicUnitIndicator(object):
	"""
	 Definition
	   ============
	   Provides ability to declare an optional sequence or index order to a Geographic Unit
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self.isInLevel = None
	    self. = []
	    self. = []



class BasedOnObjectInformation(object):
	"""
	 Definition
	   ============
	   Use when creating an object that is based on an existing object or objects that are managed by a different agency or when the new object is NOT simply a version change but you wish to maintain a reference to the object that served as a basis for the new object. BasedOnObject may contain references to any number of objects which serve as a basis for this object, a BasedOnRationaleDescription of how the content of the referenced object was incorporated or altered, and a BasedOnRationaleCode to allow for specific typing of the BasedOnReference according to an external controlled vocabulary.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:BasedOnObjectType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.basedOnRationaleDescription = None
	    self.basedOnRationaleCode = None
	    self. = []

class AccessLocation(object):
	"""
	 Definition
	   ============
	   A set of access information for a Machine including URI, mime type, and physical location
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.uri = []
	    self.mimeType = None
	    self.physicalLocation = []



class VocabularyEntryRelation(object):
	"""
	 Definition
	   ============
	   Relation of Vocabulary Entries as defined using RelationSpecification 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []



class LogicalRecordIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type LogicalRecord
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []









class AgentRelation(object):
	"""
	 Definition
	   ============
	   Used to define the relation agents in a hierarchical structure
	   
	   Examples
	   ==========
	   An Organization (source/parent) employing and Individual (target/child); An Individual (source/parent) supervisory to an Individual (target/child); An Organization (source/parent) overseeing a project (Organization) (target/child). Select appropriate relationship using the controlled vocabulary available through hasRelationshipSpecification. 
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.effectiveDates = None
	    self.semantic = None
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class LocalIdFormat(object):
	"""
	 Definition
	   ============
	   This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id.
	   
	   Examples
	   ==========
	   The name of a variable within a dataset is a localId. Software systems might use their own identifier systems for performance reasons (e.g. incrementing integers).
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.localIdValue = None
	    self.localIdType = None
	    self.localIdVersion = None



class ObjectName(object):
	"""
	 Definition
	   ============
	   A standard means of expressing a Name for a class object.  A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   USE in Model: In general the property name should be "name" as it is the name of the class object which contains it. Use a specific name (i.e. xxxName) only when naming something other than the class object which contains it.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:NameType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.content = None
	    self.context = None



class InstanceVariableValueMap(object):
	"""
	 Definition
	   ============
	   A key value relationship for two or more logical records where the key is one or more equivalent instance variables and the value is a defined relationship or a relationship to a set value. Correspondence type refers to the variables themselves rather than the value of the variables concerned. In this context Correspondence type will normally be set to ExactMatch.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Logical Records: Household and Person, Key: Household ID (HHID in Household Record, HHIDP in Person Record), ValueRelaitonship: Equal, Set Value: n.a.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.valueRelationship = ValueRelationshipType.Equal
	    self.setValue = None
	    self.hasCorrespondenceType = None
	    self. = []
	    self. = []
	    self. = []

class ClassificationIndexEntryIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type ClassificationIndexEntry
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []





class BusinessProcessIndicator(object):
	"""
	 Definition
	   ============
	   Allows for the identification of the BusinessProcess specifically as a member and optionally provides an index for the member within an ordered array. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class ClassificationSeriesRelation(object):
	"""
	 Definition
	   ============
	   Defines the Classification Series in a complex relation and specifies the relationshi
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []



class InstanceVariableIndicator(object):
	"""
	 Definition
	   ============
	   Allows for the identification of the InstanceVariable specifically as a member and optionally provides an index for the member within an ordered array. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Note if multiple types of Variables may be included in a collection use VariableIndicator.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []

class VideoSegment(object):
	"""
	 Definition
	   ============
	   Describes the type and length of the video segment.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:VideoType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfVideoClip = None
	    self.videoClipBegin = None
	    self.videoClipEnd = None

class Email(object):
	"""
	 Definition
	   ============
	   An e-mail address which conforms to the internet format (RFC 822) including its type and time period for which it is valid.
	   
	   Examples
	   ==========
	   info@ddialliance.org; ex.ample@somewhere.org
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   e-mail address; internet email address
	   
	   DDI 3.2 mapping
	   =================
	   r:EmailType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.internetEmail = None
	    self.typeOfEmail = None
	    self.effectiveDates = None
	    self.privacy = None
	    self.isPreferred = None



class InternationalString(object):
	"""
	 Definition
	   ============
	   Packaging structure for multiple language versions of the same string content. Where an element of this type is repeatable, the expectation is that each repetition contains different content, each of which can be expressed in multiple languages. The language designation goes on the individual String.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:InternationalStringType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.languageSpecificString = []

class ViewpointRoleRelation(object):
	"""
	 Definition
	   ============
	   Defines complex relationship between ViewpointRoles
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class DescribedRelationship(object):
	"""
	 Definition
	   ============
	   Relationship specification between this item and the item to which it is related. Provides a reference to any identifiable object and a description of the relationship.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:RelationshipType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.rationale = None
	    self. = []



class TypedString(object):
	"""
	 Definition
	   ============
	   TypedString combines a typeOfContent with a content defined as a simple string. May be used wherever a simple string needs to support a type definition to clarify its content
	   
	   Examples
	   ==========
	   content is a regular expression and typeOfContent is used to define the syntax used.
	   
	   Explanatory notes
	   ===================
	   This is a generic type + string where property name and documentation should be used to define any specification for the content. If international structured string content is required use TypedStructuredString 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfContent = None
	    self.content = None















class GeographicUnitTypeIndicator(object):
	"""
	 Definition
	   ============
	   Provides ability to declare an optional sequence or index order to a Unit Type describing a Geographic Unit Type. Extended to support membership in a specific level.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self.isInLevel = None
	    self. = []
	    self. = []

class InternationalIdentifier(object):
	"""
	 Definition
	   ============
	   An identifier whose scope of uniqueness is broader than the local archive. Common forms of an international identifier are ISBN, ISSN, DOI or similar designator. Provides both the value of the identifier and the agency who manages it.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   For use in Annotation or other citation format. 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:InternationalIdentifierType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.identifierContent = None
	    self.managingAgency = None
	    self.isURI = None







class VariableRelation(object):
	"""
	 Definition
	   ============
	   Ordered relations between any Variables in the Variable Cascade (Conceptual, Represented, Instance)
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []







class Statistic(object):
	"""
	 Definition
	   ============
	   The value of the statistic expressed as an xs:decimal and/or xs:double. Indicates whether it is weighted value and the computation base.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   pi:StatisticType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.isWeighted = None
	    self.computationBase = ComputationBaseList.Total
	    self.decimalValue = None
	    self.doubleValue = None

class GeographicUnitTypeRelation(object):
	"""
	 Definition
	   ============
	   Describes structured relationship between Unit Types used to define Geographic Unit Types. Extended to include the ability to define a Spatial Relationship using a controlled vocabulary.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows". 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.hasSpatialRelationSpecification = SpatialRelationSpecification.Equals
	    self.isExhaustiveCoverage = None
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

















class ExternalControlledVocabularyEntry(object):
	"""
	 Definition
	   ============
	   Allows for unstructured content which may be an entry from an externally maintained controlled vocabulary.If the content is from a controlled vocabulary provide the code value of the entry, as well as a reference to the controlled vocabulary from which the value is taken. Provide as many of the identifying attributes as needed to adequately identify the controlled vocabulary. Note that DDI has published a number of controlled vocabularies applicable to several locations using the ExternalControlledVocabularyEntry structure. If the code portion of the controlled vocabulary entry is language specific (i.e. a list of keywords or subject headings) use language to specify that language. In most cases the code portion of an entry is not language specific although the description and usage may be managed in one or more languages. Use of shared controlled vocabularies helps support interoperability and machine actionability.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:CodeValueType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.controlledVocabularyID = None
	    self.controlledVocabularyName = None
	    self.controlledVocabularyAgencyName = None
	    self.controlledVocabularyVersionID = None
	    self.otherValue = None
	    self.uri = None
	    self.content = None
	    self.language = None



class VocabularyEntryIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type VocabularyEntry
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class ClassificationItemIndicator(object):
	"""
	 Definition
	   ============
	   A ClassificationItemIndicator realizes and extends a MemberIndicator which provides a Classification Item with an index indicating order and a level reference providing the level location of the Classification Item within a hierarchical structure.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self.hasLevel = None
	    self. = []
	    self. = []



class StatisticalClassificationRelation(object):
	"""
	 Definition
	   ============
	   Specifies the Statistical Classifications for the source and target of the complex relationship and defines the relationship.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []







class DateRange(object):
	"""
	 Definition
	   ============
	   Expresses a date/time range using a start date and end date (both with the structure of Date and supporting the use of ISO and non-ISO date structures). Use in all locations where a range of dates is required, i.e. validFor, embargoPeriod, collectionPeriod, etc.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:DateType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.startDate = datetime()
	    self.endDate = datetime()





class PairedExternalControlledVocabularyEntry(object):
	"""
	 Definition
	   ============
	   A tightly bound pair of items from an external controlled vocabulary. The extent property describes the extent to which the parent term applies for the specific case. 
	   
	   Examples
	   ==========
	   When used to assign a role to an actor within a specific activity this term would express the degree of contribution. Contributor with Role=Editor and extent=Lead.
	   
	   Alternatively. the term might be a controlled vocabulary from a list of controlled vocabularies, e.g. the Generic Longitudinal Business Process Model (GLBPM) in a list that could include other business process model frameworks. In this context the extent becomes the name of a business process model task, e.g. "integrate data" from the GLBPM.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.term = None
	    self.extent = None



class ConceptIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type Concept and all subtypes of Concept: Category, Universe, Population, Unit Type
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []

class SpatialRelationship(object):
	"""
	 Definition
	   ============
	   Used to specify a relationship between one spatial object and another. Includes definition of the spatial object types being related, the spatial relation specification, and the event date.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasSpatialObjectPair = SpatialObjectPairs.PointToPoint
	    self.hasSpatialRelationSpecification = SpatialRelationSpecification.Equals
	    self.eventDate = datetime()
	    self. = []



class LanguageSpecificStringType(object):
	"""
	 Definition
	   ============
	   Allows for non-formatted language specific strings that may be translations from other languages, or that may be translatable into other languages. Only one string per language/location type is allowed. LanguageSpecificString contains the following attributes, xmlang to designate the language, isTranslated with a default value of false to designate if an object is a translation of another language, isTranslatable with a default value of true to designate if the content can be translated, translationSourceLanguage to indicate the source languages used in creating this translation, and translationDate.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:StringType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.content = None
	    self.language = None
	    self.isTranslated = None
	    self.isTranslatable = None
	    self.translationSourceLanguage = []
	    self.translationDate = None

class TypedDescriptiveText(object):
	"""
	 Definition
	   ============
	   This Complex Data Type bundles a descriptiveText with an External Controlled Vocabulary Entry allowing structured content and a means of typing that content. For example specifying that the description provides a Table of Contents for a document.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfContent = None
	    self.descriptiveText = None

class DynamicText(object):
	"""
	 Definition
	   ============
	   Structure supporting the use of dynamic text, where portions of the textual content change depending on external information (pre-loaded data, response to an earlier query, environmental situations, etc.).
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:DynamicTextType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.textContent = []
	    self.isStructureRequired = None
	    self.audienceLanguage = None



class SpatialPoint(object):
	"""
	 Definition
	   ============
	   A geographic point consisting of an X and Y coordinate. Each coordinate value is expressed separately providing its value and format.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:PointType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.xCoordinate = None
	    self.yCoordinate = None











class Segment(object):
	"""
	 Definition
	   ============
	   A structure used to express explicit segments or regions within different types of external materials (Textual, Audio, Video, XML, and Image). Provides the appropriate start, stop, or region definitions for each type.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:SegmentType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.usesAudioSegment = []
	    self.usesVideoSegment = []
	    self.xml = []
	    self.useseTextualSegment = []
	    self.usesImageArea = []

class CategoryIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use when the member type is restricted to Category.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class SpatialCoordinate(object):
	"""
	 Definition
	   ============
	   Lists the value and format type for the coordinate value. Note that this is a single value (X coordinate or Y coordinate) rather than a coordinate pair.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:SpatialCoordinateType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.coordinateValue = None
	    self.coordinateType = PointFormat.DecimalDegree

class AreaCoverage(object):
	"""
	 Definition
	   ============
	   Use to specify the area of land, water, total or other area coverage in terms of square miles/kilometers or other measure as part of a Geographic Extent.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:AreaCoverageType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfArea = None
	    self.measurementUnit = None
	    self.areaMeasure = None



class CodeIndicator(object):
	"""
	 Definition
	   ============
	   A CodeIndicator realizes and extends a MemberIndicator which provides a Code with an index indicating order and a level reference providing the level location of the Code within a hierarchical structure.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self.isInLevel = None
	    self. = []
	    self. = []

class CustomValueIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type CustomValue
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class DataStoreIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type DataStore
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []

class SummaryStatistic(object):
	"""
	 Definition
	   ============
	   Describes summary statistics for a variable.
	   
	   Examples
	   ==========
	   Mean, standard deviation
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   pi:SummaryStatisticType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfSummaryStatistic = None
	    self.hasStatistic = []

class ValueMappingIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type ValueMapping
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []







class StudyRelation(object):
	"""
	 Definition
	   ============
	   Relation specification between source and target studies in a relation structure
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class ClassificationSeriesIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type ClassificationSeries
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class ElectronicMessageSystem(object):
	"""
	 Definition
	   ============
	   Any non-email means of relaying a message electronically. This would include text messaging, Skype, Twitter, ICQ, or other emerging means of electronic message conveyance. 
	   
	   Examples
	   ==========
	   Skype account, etc.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:InstantMessagingType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.contactAddress = None
	    self.typeOfService = None
	    self.effectiveDates = None
	    self.privacy = None
	    self.isPreferred = None

class GeographicUnitRelation(object):
	"""
	 Definition
	   ============
	   Describes structured relationship between Geographic Units.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows". 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.hasSpatialRelationSpecification = SpatialRelationSpecification.Equals
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class NonIsoDateType(object):
	"""
	 Definition
	   ============
	   Used to preserve an historical date, formatted in a non-ISO fashion.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:HistoricalDateType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.dateContent = None
	    self.nonIsoDateFormat = None
	    self.calendar = None

class AudioSegment(object):
	"""
	 Definition
	   ============
	   Describes the type and length of the audio segment.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:AudioType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfAudioClip = None
	    self.audioClipBegin = None
	    self.audioClipEnd = None







class AgentIndicator(object):
	"""
	 Definition
	   ============
	   Member Indicator for use with member type Agent
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []
	    self. = []



class ImageArea(object):
	"""
	 Definition
	   ============
	   Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and Coordinates provides the information required to define it.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ImageAreaType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.coordinates = None
	    self.shape = ShapeCoded.Rectangle





class CategoryStatistic(object):
	"""
	 Definition
	   ============
	   Statistics related to a specific category of an InstanceVariable within a data set.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   pi:CategoryStatisticType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfCategoryStatistic = None
	    self.hasStatistic = []
	    self.categoryValue = None
	    self.filterValue = None
	    self. = []

class CustomItemRelation(object):
	"""
	 Definition
	   ============
	   Defines a relationship between CustomItems .
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSepcification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []





class ElseIfAction(object):
	"""
	 Definition
	   ============
	   An additional condition and resulting step to take if the condition in the parent IfThenElse is false. It cannot exist without a parent IfThenElse.
	   
	   
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.condition = None
	    self. = []

class CorrespondenceType(object):
	"""
	 Definition
	   ============
	   Describes the commonalities and differences between two members using a textual description of both commonalities and differences plus an optional coding of the type of commonality.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   cm:CorrespondenceType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.commonality = None
	    self.difference = None
	    self.commonalityTypeCode = []
	    self.hasMappingRelation = MappingRelation.ExactMatch

class IndexEntryRelation(object):
	"""
	 Definition
	   ============
	   Relationship between Index Entries defined by RelationSpecification.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []

class ConceptRelation(object):
	"""
	 Definition
	   ============
	   Specifies the relationship between concepts in a collection of concepts. Use controlled vocabulary provided in hasRelationSpecification to identify the type of relationship (e.g. ParentChild, WholePart, etc.)
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSepcification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []
	    self. = []





class ConditionalText(DynamicTextContent):
	"""
	 Definition
	   ============
	   Text which has a changeable value depending on a stated condition, response to earlier questions, or as input from a set of metrics (pre-supplied data).
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:ConditionalTextType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.expression = None

class ResourceIdentifier(InternationalIdentifier):
	"""
	 Definition
	   ============
	   Provides a means of identifying a related resource and provides the typeOfRelationship.  
	   
	   
	   
	   Examples
	   ==========
	   Standard usage may include: describesDate, isDescribedBy,  isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires, etc.
	   
	   Explanatory notes
	   ===================
	   Makes use of a controlled vocabulary for typing the relationship.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfRelatedResource = []

class LabelForDisplay(InternationalStructuredString):
	"""
	 Definition
	   ============
	   A structured display label. Label provides display content of a fully human readable display for the identification of the object. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:LabelType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.locationVariant = None
	    self.validDates = None
	    self.maxLength = None

class AnnotationDate(Date):
	"""
	 Definition
	   ============
	   A generic date type for use in Annotation which provides the standard date structure plus a property to define the date type (Publication date,  Accepted date, Copyrighted date, Submitted date, etc.). 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   Equivalent of http://purl.org/dc/elements/1.1/date where the type of date may identify the Dublin Core refinement term.
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfDate = []

class RangeValue(ValueString):
	"""
	 Definition
	   ============
	   Describes a bounding value of a string.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:RangeValueType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.included = None

class LocationName(ObjectName):
	"""
	 Definition
	   ============
	   Name of the location using the DDI Name structure and the ability to add an effective date.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:LocationNameType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.effectiveDates = None

class ReferenceDate(AnnotationDate):
	"""
	 Definition
	   ============
	   The date covered by the annotated object. In addition to specifying a type of date (e.g. collection period, census year, etc.) the date or time span may be associated with a particular subject or keyword. This allows for the expression of a referent date associated with specific subjects or keywords. 
	   
	   Examples
	   ==========
	   A set of date items on income and labor force status may have a referent date for the year prior to the collection date. To express a duration the preference is for Start and End dates expressing two time points separated by a "/". 
	   
	   Explanatory notes
	   ===================
	   Note that if needed you may use Start and Duration or Duration and End. These options may not be recognizable by all systems.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ReferenceDateType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.subject = []
	    self.keyword = []

class FixedText(LanguageSpecificStructuredStringType):
	"""
	 Definition
	   ============
	   The static portion of the text expressed as a StructuredString with the ability to preserve whitespace if critical to the understanding of the content.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:TextType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.whiteSpace = WhiteSpaceRule.Preserve

class OrganizationName(ObjectName):
	"""
	 Definition
	   ============
	   Names by which the organization is known. Use the attribute isFormal="true" to designate the legal or formal name of the Organization. The preferred name should be noted with the isPreferred attribute. Names may be typed with TypeOfOrganizationName to indicate their appropriate usage.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:OrganizationNameType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.abbreviation = None
	    self.typeOfOrganizationName = None
	    self.effectiveDates = None
	    self.isFormal = None

class ContentDateOffset(ExternalControlledVocabularyEntry):
	"""
	 Definition
	   ============
	   Identifies the difference between the date applied to the data as a whole and this specific item such as previous year's income or residence 5 years ago. A value of true for the attribute isNegativeOffset indicates that the offset is the specified number of declared units prior to the date of the data as a whole and false indicates information regarding a future state.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ContentDateOffsetType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.numberOfUnits = None
	    self.isNegativeOffset = None

class LiteralText(DynamicTextContent):
	"""
	 Definition
	   ============
	   Literal (static) text to be used in the instrument using the StructuredString structure plus an attribute allowing for the specification of white space to be preserved.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:LiteralTextType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.text = None

class PrivateImage(Image):
	"""
	 Definition
	   ============
	   References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:PrivateImageType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.effectiveDates = None
	    self.privacy = None

class BibliographicName(InternationalString):
	"""
	 Definition
	   ============
	   Personal names should be listed surname or family name first, followed by forename or given name. When in doubt, give the name as it appears, and do not invert. In the case of organizations where there is clearly a hierarchy present, list the parts of the hierarchy from largest to smallest, separated by full stops and a space. If it is not clear whether there is a hierarchy present, or unclear which is the larger or smaller portion of the body, give the name as it appears in the item. The name may be provided in one or more languages.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:BibliographicNameType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.affiliation = None
