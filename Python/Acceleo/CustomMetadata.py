





































class CustomItem(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A custom item description. This allows the definition of an item which is a member of a CustomStructure.
	   It defines the minimum and maximum number of occurrences and representation of a CustomValue.
	   
	   Examples
	   ==========
	   DegreeOfBurden for a question as required by the U.S. OMB.
	   
	   BloodPressureCuffSize - the size of a blood pressure cuff specified for a particular protocol.
	   
	   Explanatory notes
	   ===================
	   This definition will be referenced by a CustomValue when recording a key,value instance
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.maxOccurs = None
	    self.minOccurs = None
	    self.key = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class ControlledVocabulary(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The specification of a controlled vocabulary defines a set of values and their definitions together with the order relationships among those entries.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []

class CustomStructure(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A Collection containing a set of item descriptions defining a structure for a set of key,value pairs
	   
	   Examples
	   ==========
	   The set of CustomItems which are to be used to document the OMB required information for a question.
	   
	   The set of CustomItems used to describe an openEHR blood pressure protocol.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []

class VocabularyRelationStructure(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Contains the Vocabulary Relations used to structure the ControlledVocabulary
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.criteria = None
	    self.displayLabel = []
	    self.usage = None
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class CustomInstance(Identifiable):
	"""
	 Definition
	   ============
	   A set of CustomValues to be attached to some object.
	   
	   Examples
	   ==========
	   A set of OMB required information about a question.
	   
	   The set of openEHR protocol information to be attached to a blood pressure Capture
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.name = []
	    self.purpose = None
	    self.type = CollectionType.Bag
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []

class VocabularyEntry(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   One entry term and its definition in an ordered list comprising a controlled vocabulary.
	   
	   Examples
	   ==========
	   aunt - The female sibling of a parent
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.entryTerm = None
	    self.definition = None
	    self. = []

class CustomValue(Identifiable):
	"""
	 Definition
	   ============
	   An instance of a  key, value pair for a  particular  key.
	   
	   Examples
	   ==========
	   OMBBurden,"High"
	   
	   openEHRBPposition, "upper arm"
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.value = None
	    self.key = None
	    self. = []
	    self. = []
	    self. = []

class CustomItemRelationStructure(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Contains a set of CustomItemRelations which together define the relationships of Custom Structure.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.criteria = None
	    self.displayLabel = []
	    self.usage = None
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []
