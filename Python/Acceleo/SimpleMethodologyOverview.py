































class MethodologyOverview(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   High level, descriptive, human informative methodology statement used to describe the overall methodology, identify related design, algorithm, and process information. A methodology is normally informed by earlier research and clarifies how earlier research methods were incorporated into the current work. 
	   
	   
	   
	   Examples
	   ==========
	   The target of the relationship from Study: "hasMethodology"
	   
	   
	   Explanatory notes
	   ===================
	   This would most commonly be used in a Codebook along with an AlgorithmOverview and a DesignOverview. Note that Process may be described in more detail than a high level overview.
	   Note that the algorithm may be implemented by multiple processes which are not limited to any specific type of Process . This can be constrained by the inclusion of only specific realizations of Process within a Functional View. Note that this MethodologyOverview can be used as a collective description of specific methodologies used by a Study or other broad set of metadata.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.subjectOfMethodology = None
	    self.name = []
	    self.usage = None
	    self.rationale = None
	    self.overview = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class ProcessOverview(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Process is an implementation of an algorithm. It is the series of steps taken as a whole. It is decomposable into ProcessSteps, but this decomposition is not necessary. 
	   
	   Examples
	   ==========
	   In descriptive document regarding ingest of a study into a repository: "Validation processes were run comparing all data against the provided documentation to check for out-of-range and invalid content."
	   
	   Explanatory notes
	   ===================
	   In a descriptive document, Process Overview provides a means of presenting the reader with a clear view of an overall process without going into the details of the process steps. Process Overview should be used when a description of the process in general is all that is desired.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self. = None
	    self. = []
	    self. = []

class DesignOverview(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   High level, descriptive, human informative, design statement The design may be used to specify how a process will be performed in general. This would most commonly be used in a Codebook along with an AlgorithmOverview and a MethodologyOverview. The design informs a specific or implemented process as to its general parameters. Supports specification of any realization of Goal. 
	   
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Allows for the use of any realized Process. The methodology, design, and algorithm of a specific realized process should be used if available. The use of a generic Process such as a WorkflowProcess containing an Act would be appropriate here. Restriction would be done by inclusion of the appropriate realized process class(es) in a Functional View.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.subectOfDesign = None
	    self.overview = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class AlgorithmOverview(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   High level, descriptive, human informative, algorithm statement used to describe the overall methodology. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   This would most commonly be used in a Codebook along with a MethodologyOverview and a DesignOverview.The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.subjectOfAlgorithm = None
	    self.overview = None
	    self. = []
	    self. = []
