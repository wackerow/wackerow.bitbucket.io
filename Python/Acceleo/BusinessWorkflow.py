





class BusinessAlgorithm(AlgorithmOverview):
	"""
	 Definition
	   ============
	   A Business Algorithm is used to express the generalized function of the Business Process.
	   The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.
	   
	   Examples
	   ==========
	   There are several well-established algorithms for performing data anonymization including "local suppression", "global attribute generalization" and "k-Anonymization with suppression" which is a specific combination of the two. An anonymizing business process might implement one of these algorithms.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class BusinessProcess(WorkflowProcess):
	"""
	 Definition
	   ============
	   BusinessProcesses could be Generic Longitudinal Business Process Model (GLBPM) and/or Generic Statistical Business Process Model (GSBPM) steps and/or sub-steps. BusinessProcesses participate in a DataPipeline by virtue of their preconditions and postconditions. 
	   
	   A BusinessProcess differs from a WorkflowStep by virtue of its granularity. Higher level operations are the subject of BusinessProcesses. A BusinessProcess has a WorkflowStepSequence through which these higher levels operations may optionally be decomposed into WorkflowSteps.
	   
	   BusinessProcess preconditions and postconditions differ from input and output parameters because they don't take variables and values. Instead the subject of preconditions and postconditions are typically whole datasets.
	   
	   Given this level of granularity it is possible to view a BusinessProcess through the Open Archival Information System (OAIS) Reference Model. So a BusinessProcess may optionally be described as a component of a Submission Information Package (SIP), an Archival Information Package (AIP) or a Dissemination Information Package (DIP).
	   
	   Examples
	   ==========
	   In a single BusinessProcess we might receive data from a data source or merge data from different data sources or create new variables and recodes or format data for data dissemination
	   
	   Explanatory notes
	   ===================
	   One or more PairedExternalControlledVocabularyEntry objects may be used to identify and characterize controlled vocabularies like the GLBPM, GSBPM and OAIS. The PairedExternalControlledVocabularyEntry both identifies both the model (term) and the step/sub-step of the model (extent). If a BusinessProcess combines two steps from the same model (GLBPM, GSBPM, OAIS, etc.) or from different models, multiple PairedExternalControlledVocabularyEntry objects can be specified.
	   
	   Synonyms
	   ==========
	   Task
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.standardModelUsed = []
	    self.preCondition = []
	    self.postCondition = []

class DataPipeline(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A DataPipeline is a single traversal of the Generic Longitudinal Business Model (GLBPM) and/or the Generic Statistical Business process Model (GSBPM) in the course of a study where a study is either one off or a wave in a StudySeries.
	   
	   Examples
	   ==========
	   In a study where the study is a wave in a StudySeries, we do a single traversal of the Generic Longitudinal Business Process Model. From one wave to the next the traversal may be different. Each traversal is described in a DataPipeline.
	   
	   Extract Transform and Load (ETL) platforms support data pipelines to move data between systems. Using an ETL platform, data engineers create data pipelines to orchestrate the movement, transformation, validation, and loading of data, from source to final destination. The DataPipeline describes this "orchestration".
	   
	   A prospective DataPipeline gives guidance to data engineers. It is a design pattern. A retrospective DataPipeline documents an ETL data pipeline after the fact.
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   Traversal, Data Lifecycle
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
