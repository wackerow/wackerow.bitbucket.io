from datetime import datetime







































class Parameter(object):
	"""
	 Definition
	   ============
	   An Input or Output to a Process Step defined by as a unique identifier. It may be a single value or an array. 
	   
	   Examples
	   ==========
	   A question might have dynamic text or, again, "fills". Depending on the gender of the subject, a question might say "he" or "she" and/or "him" or "her". A gender variable is passed into the question, and question code resolves the dynamic text.
	   A computation action may be expecting a numeric array of values [result of looping an "Age in years" question through every member of a household]. isArray = "true"; valueRepresentation would link to a SubstantiveValueDomain defining a numeric value with a possible valid range; and because the parameter occurs in the context of a computation action it is necessary to specify an alias (e.g. AGE). The alias links the parameter to the code in the computation.
	   
	   Explanatory notes
	   ===================
	   When used as an hasInputParameter it defines a value being used by Process Step as a direct input or as an alias value within an equation. When used as an hasOutputParameter it defines itself as the value of the variable, capture, or explicit value of the parent class or as a specific value in a computation output by assigning the related Alias and/or defining the item within an array. Providing a defaultValue ensures that when bound to another Parameter that some value will be passed or received. A Value Domain expressed as a SubstantiveValueDomain or a SentinalValueDomain may be designated to define the datatype and range( of the expected value.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.alias = None
	    self.defaultValue = None
	    self.isArray = None
	    self.limitArrayIndex = None
	    self.agency = None
	    self.id = None
	    self.version = None
	    self.name = []
	    self.purpose = None
	    self. = []

class WorkflowProcess(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A Workflow Process is a realization of Process which identifies the Workflow Step Sequence which contains the WorkflowSteps and their order. It may identify the algorithm that it implements.
	   
	   Examples
	   ==========
	   Overall description of steps taken when ingesting a dataset into an archive; A Sampling Process; A step or sub-step of the Generic Longitudinal Business Process Model (GLBPM).
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self.name = []
	    self.purpose = None
	    self.usage = None
	    self. = []
	    self. = []
	    self. = None

class WorkflowService(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A means of performing a Workflow Step as part of a concrete implementation of a Business Function  (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve).
	   
	   Examples
	   ==========
	   A specification for an air flow monitor used to capture a measure for an ImplementedMeasure (a subtype of Act).
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.serviceInterface = []
	    self.serviceLocation = None
	    self.estimatedDuration = datetime()
	    self. = []
	    self. = []

class WorkflowStepRelationStructure(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   In the context of data management a WorkflowStepRelationStructure describes a network of data transformations and data products. Like all RelationStructures, a WorkflowStepRelationStructure is a Directed Acyclic Graph (DAG). A forward looking WorkflowStepRelationStructure describes the succession of data transformations and data products. A backward looking WorkflowStepRelationStructure describes the antecedents of one or a combination of data transformations and/or data products.
	   
	   Examples
	   ==========
	   WorkflowStepRelationStructures are useful in the context of data transformations and data products. Imagine a network of data transformations. One WorkflowStepRelationStructure could describe this network in a forward direction, Another WorkflowStepRelationStructure could describe this network in a backward direction.
	   
	   Looking backward, we can use this network to identify the antecedents of a particular data product. Looking forward, we can use this network to determine the effect(s) of a single or a combination of data transformations.
	   
	   If we had rendered these networks using XML, we might use xpath queries to look upstream or downstream in a network. Looking upstream we might document provenance or, again data lineage.
	   
	   If we render these networks in RDF, then we can write comparable SPARQL queries.
	   
	   Explanatory notes
	   ===================
	   The WorkflowStepRelationStructure was introduced specifically to support the data management use case. So far in the context of data capture and questionnaires there has not been a need for the WorkflowStepRelationStructure.
	   
	   Synonyms
	   ==========
	   Dataflow, Data Provenance, Data Lineage
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self.hasTemporalRelationSpecification = TemporalRelationSpecification.TemporalMeets
	    self. = []

class Loop(ConditionalControlConstruct):
	"""
	 Definition
	   ============
	   Iterative control structure to be repeated a specified number of times based on one or more conditions. Inside the loop, one or more Workflow Steps are evaluated and processed in the order they appear.
	   
	   Examples
	   ==========
	   A loop is set to repeat a fixed number of times (initialValue). A stepValue is initially specified. At the end of the loop the current value is decremented by the stepValue. The current value might be the initialValue or it might be the current value after the initialValue has been decremented one or more times.
	   
	   Explanatory notes
	   ===================
	   The Workflow Sequence contained in the Loop is executed until the condition is met, and then control is handed back to the containing control construct.
	   
	   Synonyms
	   ==========
	   ForLoop
	   
	   DDI 3.2 mapping
	   =================
	   d:LoopType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.initialValue = None
	    self.stepValue = None

class RepeatUntil(ConditionalControlConstruct):
	"""
	 Definition
	   ============
	   Iterative control structure to be repeated until a specified condition is met. After each iteration the condition is tested. If the condition is not met, the associated Workflow Sequence in contains (inherited from Conditional Control Construct) is triggered. When the condition is met, control passes back to the containing Workflow Step.
	   
	   Examples
	   ==========
	   A RepeatUntil loop is similar to a RepeatWhile loop, except that it tests the Boolean expression after the loop rather than before. Thus, a RepeatUntil loop always executes the loop once, whereas if the Boolean expression is initially False, a RepeatWhile loop does not execute the loop at all. For example, in a household there is at last one person and a loop might contain a block of person questions. After the first person the last question in the block might be "Anyone else in the household"? The RepeatUntil would continue iterating until there was no-one else in the household.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:RepeatUntilType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class ConditionalControlConstruct(WorkflowStep):
	"""
	 Definition
	   ============
	   Type of WorkflowStep in which the execution flow is determined by one or more conditions.
	   
	   
	   Examples
	   ==========
	   If-then-else and iterative structures.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.condition = None
	    self. = None

class MetadataDrivenAction(Act):
	"""
	 Definition
	   ============
	   Whereas ComputationActions are used in statistical packages like SPSS, Stata, SAS and R to perform data management and data transformations, MetadataDrivenActions are used by ETL (Extract Transform Load) platforms along with ComputationActions.
	   
	   In ETLs the user is presented with a menu of MetadataDrivenActions that are captured here in an external controlled vocabulary.
	   
	   In ETLs users enter into a dialog with the platform through which they customize the MetadataDrivenAction. The user writes no code. The dialog is saved.
	   
	   In the course of this dialog the user might specify a data source, specify which variables to keep or drop, rename variables, specify join specifics, create a value map between two variables and so forth.
	   
	   MetadataDrivenActions represent this dialog in a Standard Data Transformation Language (SDTL) called Variable Transformation Language (VTL). To be more precise MetaDataDrivenAction uses VTL with certain "extensions". Together they form quasi-VTL. quasi-VTL in turn conforms to a known schema. Using this schema users can render the quasi-VTL as XHTML and record it in the MetadataDrivenAction quasiVTL property. quasiVTL supports structured text.
	   
	   Examples
	   ==========
	   Some data management systems present users with a menu of actions they can use in a sequence to perform a specific task. The system engages the user in a dialog driven by an action template. The dialog completes the template which the system then uses to perform the action. The user doesn't write any code.
	   
	   Explanatory notes
	   ===================
	   We would like NOT to create specific subtypes of MetadataDrivenAction -- one for each transformation. In fact there are a dozen or more such actions that will be captured in the MetadataDrivenAction types external controlled vocabulary. As an alternative, users can optionally write structured text based on a known schema. In this approach we are able to "evolve" the Standard Data Transformation Language (SDTL) as needed without changing our model. Instead, as our language "evolves", we one have to update its schema.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.activityDescription = None
	    self.typeOfMetadataDrivenAction = None
	    self.quasiVTL = None

class Act(WorkflowStep):
	"""
	 Definition
	   ============
	   An Act is an indivisible, atomic step, i.e. not composed of other steps. An Act can also be viewed as a terminal node in a hierarchy of Workflow Steps. 
	   
	   Examples
	   ==========
	   QuestionConstructType from DDI 3.x is an example of an Act.
	   GenerationInstructionType from DDI 3..x is an example of an Act extended to include a CommandCode used to create a derivation or recode for an InstanceVariable or other similar use 
	   
	   Explanatory notes
	   ===================
	   Act is named after the act in the HL7 RIM. This act can take many forms including an observation, a procedure, a referral, a prescription, a consent and so forth.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class ComputationAction(Act):
	"""
	 Definition
	   ============
	   Provides an extensible framework for specific computation or transformation objects.
	   
	   Examples
	   ==========
	   In data processing, a ComputationAction might be a code statement in a code sequence.
	   
	   In data capture, a ComputationAction might takes the form of a question, a measurement or instrument code.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:ComputationItemType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.activityDescription = None
	    self.usesCommandCode = None
	    self.typeOfComputation = None

class Split(WorkflowStep):
	"""
	 Definition
	   ============
	   The components of a Split consists of a number of process steps to be executed concurrently with partial synchronization. Split completes as soon as all of its component process steps have been scheduled for execution.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Supports parallel processing that does not require completion to exit. 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []

class WorkflowMasterSequence(WorkflowStepSequence):
	"""
	 Definition
	   ============
	   Allows for the organizing WorkflowStepSequences into a larger MasterSequence. The WorkflowMasterSequence is a succession of WorkflowStepSequences in which one WorkflowStepSequence precedes the next.
	   
	   Examples
	   ==========
	   A common example is of a Questionnaire that has multiple blocks (sequences) of InstrumentComponents (InstanceQuestion, Statement, etc.) that are reused in various orders. The U.S. Current Population Survey has a block of standard demographic questions as well as specialized blocks of questions that are fielded periodically generally in a given month (ex. School Enrollment fielded in September).
	   In a data capture multiple instruments might be administered in a certain order. A WorkflowMasterSequence describes this order.  Note that in this example successive instruments cannot begin before a previous instrument ends: this would NOT be a SimpleSucession. In a Demographic Surveillance Business Process Model there is BusinessProcess that identifies data quality issues. This BusinessProcess has a WorkflowMasterSequence that includes a succession of WorkflowStepSequences. Each WorkflowStepSequence performs the work of identifying a specific data quality issue. The WorkflowMasterSequence determines the order in which each data quality issue is identified. When the order is consequential the WorkflowMasterSequence isOrdered property is set to true.
	   
	   Explanatory notes
	   ===================
	   A WorkflowMasterSequence is a SimpleCollection that is normally ordered. It allows for blocks WorkflowSteps expressed as a WorkflowStepSequence to be ordered sequentially thereby creating a master sequence that is identified by the WorkflowProcess as the overall ordering for the process.  
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.contains = []

class StructuredWorkflowSteps(WorkflowStepSequence):
	"""
	 Definition
	   ============
	   This extends the WorkflowStepSequence allowing for a graph structuring of WorkflowSteps. It adds a WorkflowStepRelationStructure which describes the order of Workflow steps in StructuredWorkflowSteps that has multiple entry points and/or multiple exit points and/or, alternatively, a WorkflowStep can have multiple predecessors and/or multiple successors.
	   
	   Examples
	   ==========
	   Data Integration might combine multiple data source and make multiple data products. This sequence would have several starting points and terminate in multiple endpoints.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []

class WorkflowStep(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   One of the constituents of a Workflow. It can be a composition or atomic and might be performed by a Service.
	   
	   Examples
	   ==========
	   A ControlConstruct that orchestrates steps and substeps in a Workflow is a WorkflowStep. An Act is a WorkflowStep.
	   
	   Explanatory notes
	   ===================
	   An atomic Workflow Step has no Control Constructs -- it's an Act. A composition consists of a tree of Control Constructs. In this tree Acts are associated with the leaf nodes.
	   
	   Furthermore, a composition might be a glass box or a black box from the perspective of a service. If a service performs a WorkflowStep, it might invoke just the topmost ControlConstruct -- for example a WorkflowSequence. It may know nothing about the internal workings of the sequence. In this case the WorkflowStep is a black box.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Process Step.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.name = []
	    self.purpose = None
	    self.usage = []
	    self.overview = None
	    self.hasInformationFlow = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class WorkflowStepSequence(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A WorkflowStepSequence is an enumerated (simple) collection of WorkflowSteps.
	   
	   WorkflowSteps that may be used in a WorkflowStepSequence may be of many subtypes, covering simple or computational acts, conditional steps such as an IfThenElse, specific concurrent processing defined by Split and SplitJoin, or designed for various specializations of the WorkflowProcess. .
	   
	   Members in a WorkflowStepSequence exchange variables and values by way of output parameters, input parameters and their bindings.
	   
	   
	   Examples
	   ==========
	   A WorkflowStepSequence can be used to describe the flow of questions in a questionnaire. 
	   
	   A WorkflowStepSequence can be used to decompose a BusinessProcess like data integration or anonymization into a series of steps.
	   
	   Explanatory notes
	   ===================
	   If more complex temporal or graph ordering is required use the subtype StructuredWorkflowSteps.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:SequenceType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfWorkflowStepSequence = []
	    self.type = CollectionType.Bag
	    self.contains = []
	    self.name = []
	    self.purpose = None
	    self.isOrdered = None
	    self. = []
	    self. = []

class SplitJoin(WorkflowStep):
	"""
	 Definition
	   ============
	   SplitJoin consists of process steps that are executed concurrently (execution with barrier synchronization). That is, SplitJoin completes when all of its components processes have completed. 
	   
	   Examples
	   ==========
	   A GANTT chart where a number of processes running parallel to each other are all prerequisites of a subsequent step.
	   
	   Explanatory notes
	   ===================
	   Supports parallel processing that requires completion of all included process steps to exit.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []

class RepeatWhile(ConditionalControlConstruct):
	"""
	 Definition
	   ============
	   Iterative control structure to be repeated while a specified condition is met. Before each iteration the condition is tested. If the condition is met, the associated Workflow Sequence in contains (inherited from Conditional Control Construct) is triggered. When the condition is not met, control passes back to the containing Workflow Step.
	   
	   Examples
	   ==========
	   A RepeatWhile loop is similar to a RepeatUntil loop, except that it tests the Boolean expression before the loop rather than after. Thus, a RepeatUntil loop always executes the loop once, whereas if the Boolean expression is initially False, a RepeatWhile loop does not execute the loop at all. For example, RepeatWhile may be used to pre-test a certain condition like if "the number of new people in a household is greater than 0" in which case a block of questions might be asked for each new person.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:RepeatWhileType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass

class IfThenElse(ConditionalControlConstruct):
	"""
	 Definition
	   ============
	   IfThenElse describes an if-then-else decision type of control construct. If the stated condition is met, then the associated Workflow Sequence in containsSubSeqence is triggered, otherwise the Workflow Sequence that is triggered is the one associated via elseContains.
	   
	   
	   Examples
	   ==========
	   An IfThenElse object describes the conditional logic in the flow of a questionnaire or other data collection instrument, where if a stated condition is met, one path is followed through the flow, and if the stated condition is met, another path is taken.
	   
	   Explanatory notes
	   ===================
	   Contains a condition and two associations:
	   - one to the Workflow Sequence that is triggered when the condition is true (containsSubSequence), and 
	   - another to the Workflow Sequence that is triggered when the condition is false (elseContains). 
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   d:IfThenElseType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.elseIf = []
	    self. = None
