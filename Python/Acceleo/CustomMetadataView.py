"""
 Purpose
   Custom Metadata allows for the creation of specialized key value pairs and values with DDI classes. This view was created to demonstrate the use of this set of classes. 
   
   The DDI4 CustomMetadataView allows for the definition of a set of keys and their interrelationships. This structure can be shared and reused by a community. 
   These keys can be used in key-value pairs to enter metadata that is more actionable than would be if an uncontrolled set of keys were employed.
   
   The keys and their interrelationships are defined in a CustomStructure which is a collection of CustomItems. Each CustomItem defines a key, giving it a cardinality and associating it with a RepresentedVariable and a ValueDomain. The set of CustomItems can be arranged into a structure, for example a hierarchy, by a CustomItemRelationStructure.
   
   Once the structure is defined, it can be used in a CustomInstance. This is a collection of CustomValues. A CustomValue contains a key-value pair where the key corresponds to a previously defined CustomItem. A CustomValue also may be associated with an InstanceVariable.  The CustomValues may also be structured by a CustomValueRelationStructure.
"""

