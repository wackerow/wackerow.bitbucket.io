from datetime import datetime













































class SamplePopulationResult(Result):
	"""
	 Definition
	   ============
	   The Sample Population is the result of a sampling process. It is the subset of the population selected from the Sample Frame or Frames that will be used as respondents to represent the target population. Sample Populations may occur at a number of stages in the sampling process and serve as input to a subsequent sampling stage. The Sample Population should be validated against the intended sample population as described in the goal of the design and/or process.
	   
	   Examples
	   ==========
	   A list of phone numbers representing a random sample from a telephone book. The Counties selected from within a State from which Households will be selected using another sampling method.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.sizeOfSample = None
	    self.strataName = None
	    self.sampleDate = datetime()
	    self. = []
	    self. = []

class SamplingGoal(Goal):
	"""
	 Definition
	   ============
	   Provides general description and specific targets for sample sizes including sub-populations.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overallTargetSampleSize = None
	    self.overallTargetSamplePercent = None
	    self.targetSampleSize = []

class SamplingAlgorithm(AlgorithmOverview):
	"""
	 Definition
	   ============
	   Generic description of a sampling algorithm expressed by a Sampling Design and implemented by a Sampling Process. May include common algebraic formula or statistical package instructions (use of specific selection models).
	   
	   Examples
	   ==========
	   Random Sample; Stratified Random Sample
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.codifiedExpressionOfAlgorithm = None

class SamplingProcedure(MethodologyOverview):
	"""
	 Definition
	   ============
	   The Sampling Procedure describes the population being sampled, the sampling frame and the sampling plan including steps and sub steps by referencing the universe, sample frames and sampling plans described in schemes. Target sample sizes for each stage can be noted as well as the process for determining sample size, the date of the sample and the person or organization responsible for drawing the sample.
	   
	   Examples
	   ==========
	   Census of Population and Housing, 1980 Summary Tape File 4, chapter on Technical Information. Overview is the Introduction generally describing the sample; there is a sample design section which provides a more detailed overview of the design. Additional external documentation provides detailed processes for sample adjustments needed for small area estimations, sample frames etc.
	   
	   Explanatory notes
	   ===================
	   This is a summary of the methodology of sampling providing both an overview of purpose and use of a particular sampling approach and optionally providing specific details regarding the algorithm, design, and process of selecting a sample.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class SamplingDesign(DesignOverview):
	"""
	 Definition
	   ============
	   Sampling plan defines the how the sample is to be obtained through the description of methodology or a model based approach, defining the intended target population, stratification or split procedures and recommended sample frames. The content of a sampling plan is intended to be reusable. The application of the sample plan is captured in Sampling Process.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []
	    self. = []

class SampleFrame(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A set of information used to identify a sample population within a larger population. The sample frame will contain identifying information for individual units within the frame as well as characteristics which will support analysis and the division of the frame population into sub-groups.
	   
	   Examples
	   ==========
	   A listing of all business enterprises by their primary office address with information on their industry classification, work staff size, and production costs. A telephone directory. An address list of housing units.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.name = []
	    self.displayLabel = []
	    self.usage = None
	    self.purpose = None
	    self.limitations = None
	    self.updateProcedures = None
	    self.referencePeriod = None
	    self.validPeriod = None
	    self. = []
	    self. = []
	    self. = None
	    self. = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class SamplingProcess(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Provides the details of the process used to apply the sampling design and obtain the sample. Sampling Process contains a Workflow Step which provides the constituents of a Workflow. It can be a composition (a set of Control Constructs) or atomic (an Act) and may be performed by a service. This allows the use of existing Workflow models to express various sampling process actions. Sampling Frames and Sample Population can be used as designated Inputs or Outputs using Parameters.
	   
	   Examples
	   ==========
	   A Split which takes a sampling stage and divides the sample frame into different subsets. A different sampling technique is applied to each subset. Once a split occurs each subset can have stages underneath, and the number of states under each split subset may differ.
	   A Stage is the application of a single sampling algorithm applied to a sampling frame. For instance, the US Current Population Survey samples geographic areas first before identifying household to contact within each of those areas.
	   A Stratification of a stage into multiple subsets. Each stratified group will be sampled using the same sampling approach. For example stratifying a state by ZIP Code areas in each of 5 mean income quintiles and then doing a random sample of the households in a set of Zip Codes. Allows for oversampling of identified subpopulations.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self. = []
	    self. = None
	    self. = []
	    self. = []
	    self. = []
