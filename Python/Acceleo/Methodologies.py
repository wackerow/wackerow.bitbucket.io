





















class AppliedUse(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Links the guidance instructions to specific unit types.
	   
	   Examples
	   ==========
	   Links a guide for the use of a sample (result) obtained in the first stage of complex sample with the unit type. A sample of Counties from which households will be selected in the next sample stage.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self. = []
	    self. = []
	    self. = []

class Precondition(BusinessFunction):
	"""
	 Definition
	   ============
	   A precondition is a state. The state includes one or more goals that were previously achieved. This state is the necessary condition before a process can begin.
	   
	   Examples
	   ==========
	   Using goals and preconditions processes can be chained to form work flows. Note that these work flows do not include data flows. Instead data flows consisting ouf outputs, inputs and bindings are a separate swim lane. 
	   
	   Explanatory notes
	   ===================
	   A precondition related to a design defines the state that must exist in order for a design being applied. For example in applying a Sampling Design there may be a precondition for the existance of a sampling frame meeting certain specifications.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []

class BusinessFunction(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Something an enterprise does, or needs to do, in order to achieve its objectives.
	   
	   A Business Function delivers added value from a business point of view. It is delivered by bringing together people, processes and technology (resources), for a specific business purpose. 
	   
	   Business Functions answer in a generic sense "What business purpose does this Business Service or Process Step serve?" Through identifying the Business Function associated with each Business Service or Process Step it increases the documentation of the use of the associated Business Services and Process Steps, to enable future reuse.
	   
	   Examples
	   ==========
	   A Business Function may be defined directly with descriptive text and/or through reference to an existing catalogue of Business Functions. 
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Business Function.
	"""
	def __init__(self)
	    self.overview = None

class Result(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Describes the results of a process for the purpose of linking these results to guidance for future usage in specified situations. Result is abstract and serves as a substitution base for the specified result of a specific instantiation of a methodology process.
	   
	   Examples
	   ==========
	   The use of a weight resulting from a sampling process by an analyst within the context of a specific set of variables. The class containing the weight would use the extension base Result, adding any additional required properties and relationships to describe it.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self.hasBinding = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class Goal(BusinessFunction):
	"""
	 Definition
	   ============
	   Goals are the "things" a method aims to achieve. A goal may be a business function (GSIM) corresponding to a function in a catalog of functions such as GSBPM or GLBMN. However, goals may be specified more broadly. For example, conducting a clinical trial might be the goal of a method. Machine learning might be the goal of a method. 
	   
	   
	   
	   Examples
	   ==========
	   To further distinguish a "goal" from an "output", consider the construction of a statistic. A goal may be the construction of the statistic. The output is the statistic. The output is data. The statistic by itself -- the output -- is arguably meaningless even if we capture the process of its construction. 
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Business Function.
	"""
	def __init__(self)
	    self. = []

class Guide(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Provides a guide for the usage of a result within a specified application
	   
	   Examples
	   ==========
	   The applied use of a weight determined by a weighting process in analyzing a data set
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self. = []
