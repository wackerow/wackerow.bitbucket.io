"""
 Purpose:
   A basic set of objects that can support structured searching for discovery and ingest to statistical software, and provide sufficient information for the intelligent use and understanding of the associated data. Making data independently understandable so people can reuse it. 
   
   Use Cases:
   This Functional View supports three major uses
   * Basic search to find data (general search, known item search). Users are looking to use data.
   * Machine actionable to allow users to analyze the data (ingest to statistical software)
   * Informational content to support Intelligent use and understanding the data
   
   Target Audience:
   Individual researchers, research groups, National Statistical Organizations focusing on stand-alone codebooks, students, instructors, archives, and libraries.
   
   General Usage Instructions:
   Study is the conceptual anchor for an individual collection activity resulting in a data set or data file. It can be both a part of a StudySeries as a wave etc. or an unrelated activity like a single survey project.
"""

