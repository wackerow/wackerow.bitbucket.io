











class ComparisonMap(object):
	"""
	 Definition
	   ============
	   Provides a basic pattern for a comparison map which identifies source and target members and details about their match. Source and target were retained as in describing differences and commonalities the direction of the comparison may be important.
	   
	   Examples
	   ==========
	   Comparing the Classification Items between Statistical Classifications; describing the comparative relationship between InstanceVariables in two LogicalRecords for use in linking records
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasCorrespondenceType = None
	    self. = []
	    self. = []

class MemberIndicator(object):
	"""
	 Definition
	   ============
	   Provides ability to declare an optional sequence or index order to a Member. 
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.index = None
	    self. = []





class MemberRelation(object):
	"""
	 Definition
	   ============
	   Defines one kind of relationship between one member (source) and possibly several members (target).  The type of the relationship, a semantic, and totality definition may also be specified. 
	   
	   Examples
	   ==========
	   A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows". 
	   
	   Explanatory notes
	   ===================
	   A MemberRelation functions like an adjacency list (https://en.wikipedia.org/wiki/Adjacency_list) in graph theory, describing the relationships of one Member to multiple other members. All of these relationships must have the same RelationSpecification, Totality, and semantic. 
	   These might not be the only relations from the source Member if there are other relations with different properties.
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self. = []
	    self. = []

class StructuredCollection(SimpleCollection):
	"""
	 Definition
	   ============
	   Structured Collection container extends a Simple Collection allowing it to optionally have an RelationshpStructure to model hierarchies and nesting or more complex network structures. Like Simple Collection it is also a subtype of Member to allow for nested collections. A Collection can be described directly as having unordered members or an indexed array of members through a set of MemberIndicators. 
	   
	   Examples
	   ==========
	   Node Sets, Schemes, Groups, Concept Systems are all types of Collections.
	   
	   Explanatory notes
	   ===================
	   Members have to belong to some Collection, except in the case of nested Collections where the top level Collection is a Member that doesn't belong to any Collection.
	   If a Collection is ordered as a simple list that ordering can be indicated by an index property of each MemberIndicator. If the Collection is unordered, the index is not necessary.
	   Collection is not extended, it is realized (see Collection Pattern documentation).
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Node Set.
	"""
	def __init__(self)
	    self. = []

class Comparison(Identifiable):
	"""
	 Definition
	   ============
	   The minimal pattern for a comparison including a mapping between members.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.correspondence = []
	    self. = []

class SimpleCollection(CollectionMember):
	"""
	 Definition
	   ============
	   Simple Collection container (set or bag) that may be unordered or ordered using Member Indicator. This collection type is limited to expressing only unordered sets or bags, and simple sequences. Use when there is a need to limit the organization of the collection to one of these forms of organization 
	   
	   Examples
	   ==========
	   The sequencing of questions within a questionnaire where ordering and routing is done through conditional constructs such as IfThenElse, etc.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []

class RelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   The set of MemberRelations used to structure a Collection
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   The optional RelationStructure is used to describe relations among Members of the Collection that are more complex than a simple list. There can be multiple types of relation structures for a Collection and these in turn may each have a different semantic. These differences can be described by the properties of the Relation Structure, or in the case of hybrid structures described by the individual MemberRelations.
	   Each MemberRelations functions like an adjacency list (https://en.wikipedia.org/wiki/Adjacency_list) in graph theory, describing the relationships of one Member to multiple other members.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.totality = TotalityType.Total
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.hasMemberRelation = []

class CollectionMember(Identifiable):
	"""
	 Definition
	   ============
	   Generic class representing members of a collection. 
	   
	   
	   Examples
	   ==========
	   Classification Item, Process Step.
	   
	   Explanatory notes
	   ===================
	   Members have to belong to some Collection, except in the case of nested Collections where the top level Collection is a Member that doesn't belong to any Collection.
	   
	   Member is not extended, it is realized (see Collection Pattern documentation).
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Node.
	"""
	    pass
