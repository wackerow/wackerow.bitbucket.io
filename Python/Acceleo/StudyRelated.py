from datetime import datetime







































































class Budget(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A description of the budget for any of the main publication types that can contain a reference to an external budget document.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:BudgetType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self.name = []
	    self. = []

class StudyControl(Act):
	"""
	 Definition
	   ============
	   StudyControl references a Study. StudyControl enables the representation of a research protocol at the Study level. [If the only purpose of the object is to carry a Study, it should be seriously considered for melting with another object. Modeling rules state that any object needs to have a reality of its own. Carrying another object is not a reality. If I misunderstood, please clarify the description instead]
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []

class Embargo(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Provides information about data that are not currently available because of policies established by the principal investigators and/or data producers. Embargo provides a name and label of the embargo, the dates covered by the embargo, the rationale or reason for the embargo, a reference to the agency establishing the embargo, and a reference to the agency enforcing the embargo.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:EmbargoType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.embargoDates = None
	    self.rationale = None
	    self. = []
	    self. = []

class StudyRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Allows for a complex nested structure for a Study Series
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class Study(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Study is the conceptual anchor for an individual collection activity resulting in a data set or data file. It can be both a part of a StudySeries as a wave etc. or an unrelated activity like a single survey project.
	   
	   Examples
	   ==========
	   ICPSR study 35575 Americans and the Arts [1973 - 1992] (ICPSR 35575). https://www.icpsr.umich.edu/icpsrweb/ICPSR/studies/35575?dataFormat%5B0%5D=SAS&keyword%5B0%5D=public+opinion&geography%5B0%5D=United+States&searchSource=revise  
	   
	   Explanatory notes
	   ===================
	   The Study class brings together many properties and relationships describing a set of data – coverage, kind of data, methodology, citation information, access information and more.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   s:StudyUnitType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.kindOfData = []
	    self.overview = None
	    self.bibliographicCitation = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class QualityStatement(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A statement of quality which may be related to an external standard or contain a simple overview, rationale, and usage statement. When relating to an external standard information on compliance may be added providing a reference to a ComplianceConcept, an ExternalComplianceCode, as well as a description.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:QualityStatementType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.name = []
	    self.displayLabel = []
	    self.overview = None
	    self.rationale = None
	    self.usage = None
	    self. = []

class Standard(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Identifies the external standard used and describes the level of compliance with the standard in terms specific aspects of the standard's content.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:StandardType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = None
	    self. = []

class ComplianceStatement(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Allows for a quality statement based on frameworks to be described using itemized properties. A reference to a concept, a coded value, or both can be used to specify the property from the standard framework identified in StandardUsed. Usage can provide further details or a general description of compliance with a standard.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ComplianceType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.externalComplianceCode = None
	    self.usage = None
	    self. = None

class ExPostEvaluation(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Evaluation for the purpose of reviewing the study, data collection, data processing, or management processes. Results may feed into a revision process for future data collection or management. Identifies the type of evaluation undertaken, who did the evaluation, the evaluation process, outcomes and completion date.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:ExPostEvaluationType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.typeOfEvaluation = []
	    self.evaluationProcess = []
	    self.outcomes = []
	    self.completionDate = datetime()
	    self. = []

class StudySeries(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A collection of studies which can be structured as a simple sequence, or with a more complex structure.
	   
	   Examples
	   ==========
	   An annual series of surveys.
	   
	   Explanatory notes
	   ===================
	   A set of studies may be defined in many ways. A study may be repeated over time. One or more studies may attempt to replicate an earlier study. The StudySeries allows for the description of the relationships among a set of studies.
	   
	   A simple ordered or unordered sequence of studies can be described via the "contains StudyIndicator" property. More complex relationships among studies may also be described using the optional "isStructuredBy StudyRelationsStructure".
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.name = []
	    self.overview = None
	    self.type = CollectionType.Bag
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
