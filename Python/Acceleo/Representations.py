from datetime import datetime





































































































class ClassificationIndexEntry(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A Classification Index Entry is a word or a short text (e.g. the name of a locality, an economic activity or an occupational title) describing a type of object/unit or object property to which a Classification Item applies, together with the code of the corresponding Classification Item. Each Classification Index Entry typically refers to one item of the Statistical Classification. Although a Classification Index Entry may be associated with a Classification Item at any Level of a Statistical Classification, Classification Index Entries are normally associated with items at the lowest Level.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Classification Index Entry.
	"""
	def __init__(self)
	    self.entry = None
	    self.validDates = None
	    self.codingInstruction = None
	    self. = []

class ClassificationSeries(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A Classification Series is an ensemble of one or more Statistical Classifications, based on the same concept, and related to each other as versions or updates. Typically, these Statistical Classifications have the same name.
	   
	   Examples
	   ==========
	   ISIC or ISCO
	   SIC (with different published versions related to the publication year)
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Classification Series.
	"""
	def __init__(self)
	    self.context = None
	    self.objectsOrUnitsClassified = None
	    self.subject = []
	    self.keyword = []
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class ClassificationItem(Code):
	"""
	 Definition
	   ============
	   A Classification Item represents a Category at a certain Level within a Statistical Classification.
	   
	   
	   Examples
	   ==========
	   In the  2012 NAICS (North American Industry Classification System) one Classification Item has the Code 23 and the Category construction.
	   
	   Explanatory notes
	   ===================
	   A Classification Item defines the content and the borders of the Category. A Unit can be classified to one and only one item at each Level of a Statistical Classification. As such a Classification Item is a placeholder for a position in a StatisitcalClassification. It contains a Designation, for which Code is a kind; a Category; and other things. 
	   
	   This differentiates it from Code which is a kind of Designation, in particular it is an alphanumeric string assigned to stand in place of a category. For example, the letter M might stand for the category Male in the CodeList called Gender.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Classification Item.
	"""
	def __init__(self)
	    self.isValid = None
	    self.isGenerated = None
	    self.explanatoryNotes = []
	    self.futureNotes = []
	    self.changeLog = None
	    self.changeFromPreviousVersion = None
	    self.validDates = None
	    self.name = []
	    self. = []
	    self. = []

class Code(Designation):
	"""
	 Definition
	   ============
	   A type of Designation that relates a representation expressed as a string with or without meaningful white space to a specific classification. For use in a Code List. The representation property (Value) is expressed as it would be found in a data file. Multiple representations may relate to the same Category but should be expressed as separate Codes.
	   
	   Examples
	   ==========
	   The letter M might stand for the category Male in the CodeList called Gender.
	   
	   Explanatory notes
	   ===================
	   A Code is a kind of Designation, in particular it is the assignment of an alphanumeric string to stand in place of a category. 
	   
	   It should not be confused with a ClassificationItem which is a placeholder for a position in a StatisticalClassification. It contains a Designation, for which Code is a kind; a Category; and other things. For example, in 2012 NAICS (North American Industry Classification System) one Classification Item has the Code 23 and the Category construction.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   l:CodeType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Code.
	"""
	def __init__(self)
	    self.representation = None
	    self. = []

class Designation(Identifiable):
	"""
	 Definition
	   ============
	   A sign denoting a concept.
	   
	   Examples
	   ==========
	   A linking of the term “Unemployment” to an particular underlying concept.
	   
	   Explanatory notes
	   ===================
	   The representation of a concept by a sign (e.g., string, pictogram, bitmap) which denotes it. An example is the code M designating the marital status Married, which is a concept. In this context, M means Married.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Designation.
	"""
	def __init__(self)
	    self.representation = None
	    self. = []
	    self. = []

class StatisticalClassification(CodeList):
	"""
	 Definition
	   ============
	   A Statistical Classification is a set of Categories which may be assigned to one or more variables registered in statistical surveys or administrative files, and used in the production and dissemination of statistics. The Categories at each Level of the classification structure must be mutually exclusive and jointly exhaustive of all objects/units in the population of interest. (Source: GSIM StatisticalClassification)
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   The Categories are defined with reference to one or more characteristics of a particular universe of units of observation. A Statistical Classification may have a flat, linear structure or may be hierarchically structured, such that all Categories at lower Levels are sub-Categories of Categories at the next Level up.Categories in Statistical Classifications are represented in the information model as Classification Items. (Source: GSIM StatisticalClassification
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Statistical Classification.
	"""
	def __init__(self)
	    self.releaseDate = datetime()
	    self.validDates = None
	    self.isCurrent = None
	    self.isFloating = None
	    self.changeFromBase = None
	    self.purposeOfVariant = None
	    self.copyright = []
	    self.updateChanges = []
	    self.availableLanguage = None
	    self.displayLabel = []
	    self.rationale = None
	    self.usage = None
	    self.contains = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class LevelStructure(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The LevelStructure describes the nesting structure of a hierarchical collection. The levels within the structure begin at the root level '1' and continue as an ordered array through each level of nesting.
	   
	   
	   
	   Examples
	   ==========
	   ISCO-08 (Major, Sub-Major, and Minor) or NAICS (2 digit sector codes, 3 digit subsector code list, 4 digit industry group code list, and 5 digit industry code list)
	   
	   
	   Explanatory notes
	   ===================
	   Levels are used to organize a hierarchy. Usually, a hierarchy often contains one root member at the top, though it could contain several. These are the first Level. All members directly related to those  in the first Level compose the second Level. The third and subsequent Levels are defined similarly. 
	   
	   A Level often is associated with a Concept, which defines it. These correspond to kinds of aggregates. For example, in the US Standard Occupational Classification (2010), the Level below the top is called Major Occupation Groups, and the next Level is called Minor Occupational Groups. These ideas convey the structure. In particular, Health Care Practitioners (a Major Group) can be broken into Chiropractors, Dentists, Physicians, Vets, Therapists, etc. (Minor Groups) The Categories in the Nodes at the lower Level aggregate to the Category in Node above them.
	   
	   "Classification schemes are frequently organized in nested levels of increasing detail. ISCO-08, for example, has four levels: at the top level are ten major groups, each of which contain sub-major groups, which in turn are subdivided in minor groups, which contain unit groups. Even when a classification is not structured in levels ("flat classification"), the usual convention, which is adopted here, is to consider that it contains one unique level." (http://rdf-vocabulary.ddialliance.org/xkos.html#) Individual classification items organized in a hierarchy may be associated with a specific level.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   l:LevelType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Level.
	"""
	def __init__(self)
	    self.name = []
	    self.usage = None
	    self.containsLevel = []
	    self.validDateRange = None

class StatisticalClassificationRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   A structure for describing the complex relationship between statistical classifications in a classification series
	   
	   Examples
	   ==========
	   A classification series that branches into separately versioned classifications
	   
	   Explanatory notes
	   ===================
	   Can use relation specification information to more fully describe the relationship between members such as parent/child, whole/part, general/specific, equivalence, etc.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class ClassificationSeriesRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   Describes the complex relation structure of a classification family.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class CodeList(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A list of Codes and associated Categories. May be flat or hierarchical. A hierarchical structure may have an indexed order for intended presentation even though the content within levels of the hierarchy are conceptually unordered. For hierarchical structures ClassificationRelationStructure is used to provide additional information on the structure and organization of the categories. Note that a CategorySet can be structured by a ClassificationRelationStructure without the need for associating any Codes with the Categories. This allows for the creation of a CategorySet, for example for a response domain, without an associated CodeList.
	   
	   Examples
	   ==========
	   The codes "M" and "F" could point to "Male" and "Female" categories respectively.
	   
	   A CodeList for an occupational classification system like ISCO-08 could use a ClassificationRelationStructure to describe a hierarchy (Chief Executives and Administrative and Commercial Managers as subtypes of Managers)
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   l:CodeListType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Code List.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = None
	    self. = []
	    self. = []

class AuthorizationSource(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Identifies the authorizing agency and allows for the full text of the authorization (law, regulation, or other form of authorization).
	   
	   Examples
	   ==========
	    May be used to list authorizations from oversight committees and other regulatory agencies.
	   
	   Explanatory notes
	   ===================
	   Supports requirements for some statistical offices to identify the agency or law authorizing the collection or management of data or metadata.
	   
	   Synonyms
	   ==========
	   Use for the Case Law, Case Law Description, and Case Law Date properties in ClassificationItem
	   
	   DDI 3.2 mapping
	   =================
	   r:AuthorizationSourceType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.statementOfAuthorization = None
	    self.legalMandate = None
	    self.authorizationDate = datetime()
	    self.purpose = None
	    self. = []

class IndexEntryRelationStructure(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Structures relationship of Classification Index Entries in a Classification Index.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.hasMemberRelation = []
	    self. = []

class SentinelValueDomain(ValueDomain):
	"""
	 Definition
	   ============
	   The Value Domain for a sentinel conceptual domain. Sentinel values are defined in ISO 11404 as
	   "element of a value space that is not completely consistent with a datatype's properties and characterizing operations...". A common example would be codes for missing values.
	   
	   Examples
	   ==========
	   Missing categories expressed as Codes <-9, refused>; <-8, Don't Know> for a numeric variable with values greater than zero.  
	   
	   Explanatory notes
	   ===================
	   Sentinel values are used for processing, not to describe subject matter. Typical examples include missing values or invalid entry codes. Sentinel Value Domains are typically of the enumerated type, but they can be the described type, too.
	   
	   This is not to say that sentinel values carry no information. Data on gender might be enumerated by "0, male" and "1, female". These are the substantive values (see Substantive Value Domain). However, there may be the need to include missing values along with that data, such as "m, missing" and "r, refused". These are sentinel values.
	   
	   ISO/IEC 11404 - General purpose datatypes, defines sentinel values in terms of how that standard defines datatypes. But, the fact that the sentinels don't fit in terms of the calculations and statistics one would perform on the "clean" data is a distinguishing characteristic. In the example above, one would not include missing or refused data in calculating a ratio of females to the total population.
	   
	   Sentinel values may be described rather than enumerated. For instance, there might be a range of values, each representing an out of range value, but there could be too many to enumerate. It is easier to describe the range.
	   
	   In some software missing values are represented as values not in the datatype of the valid values. R has values of NA, NaN, Inf, and -Inf. SAS and Stata have values displayed as ".", ".A" through ".Z", and "._"
	   
	   Other software might use values like 999 for missing that would otherwise be the same datatype as valid values but outside the parameters of the domain.
	   
	   In the gender example above:
	   For SPSS the sentinel values might be represented as:
	   998 = "refused"
	   999 = "not recorded"
	   
	   For SAS or Stata the sentinel values might be represented as:
	   .R = "refused"
	   .N = "not recorded"
	   
	   Sentinel values can also be used for other purposes beyond missing. For a numeric variable "A" might represent a value somewhere in a defined range to prevent disclosure of information about an individual. This might be considered a "semi-missing value".
	   In SAS or Stata for example:
	   .A = "greater than or equal to 100 and less than 1000 "
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Enumerated Value Domain.
	"""
	def __init__(self)
	    self.platformType = None
	    self. = []
	    self. = []
	    self. = []

class CorrespondenceTable(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A Correspondence Table expresses relationships between the members within or between StatisticalClassifications.
	   
	   Examples
	   ==========
	   Correspondence between the U.S. Standard Industrial Classification (SIC) and North American Industrial Classification System (NAICS)
	   
	   Explanatory notes
	   ===================
	   CorrespondenceTables are used with Statistical Classifications. For instance, it can relate two versions from the same Classification Series; Statistical Classifications from different Classification Series; a variant and the version on which it is based; or, different versions of a variant. In the first and last examples, the Correspondence Table facilitates comparability over time.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Correspondence Table.
	"""
	def __init__(self)
	    self.effectiveDates = None
	    self.correspondence = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class ValueAndConceptDescription(Identifiable):
	"""
	 Definition
	   ============
	   A formal description of a set of values. 
	   
	   Examples
	   ==========
	   Example 1) 
	   The integers between 1 and 10 inclusive.  
	   The values of x satisfying the logicalExpression property:
	   " (1 <=x <= 10) AND mod(x,10)=0"
	   Also described with minimumValueInclusive = 1 and maximumValueInclusive = 10 (and datatype of integer)
	   
	   Example 2)
	   The upper case letters A through C and X
	   described with the regularExpression "/[A-CX]/"
	   
	   
	   Example 3)
	   A date-time described with the Unicode Locale Data Markup Language pattern yyyy.MM.dd G 'at' HH:mm:ss zzz
	   
	   
	   Explanatory notes
	   ===================
	   The ValueAndConceptDescription may be used to describe either a value domain or a conceptual domain. For a value domain, the ValueAndConceptDescription contains the details for a “described” domain (as opposed to an enumerated domain). There are a number of properties which can be used for the description. The description could be just text such as: “an even number greater than zero”. Or a more formal logical expression like “x>0 and mod(x,2)=0”. A regular expression might be useful for describing a textual domain. It could also employ a format pattern from the Unicode Locale Data Markup Language (LDML) (http://www.unicode.org/reports/tr35/tr35.html.
	   Some Conceptual Domains might be described with just a narrative. Others, though, might be described in much the same way as a value domain depending on the specificity of the concept.
	   
	   In ISO 11404 a value domain may be described either through enumeration or description, or both. This class provides the facility for that description. It may be just a text description, but a description through a logical expression is machine actionable and might, for example, be rendered as an integrity constraint. If both text and a logical expression are provided the logical expression is to be taken as the canonical description.
	   The logical expression should conform to the expression syntax of VTL. https://sdmx.org/?page_id=5096 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.description = None
	    self.logicalExpression = None
	    self.regularExpression = None
	    self.minimumValueInclusive = None
	    self.maximumValueInclusive = None
	    self.minimumValueExclusive = None
	    self.maximumValueExclusive = None
	    self.classificationLevel = CategoryRelationCode.Nominal
	    self.formatPattern = None

class ClassificationIndex(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A Classification Index is an ordered list (alphabetical, in code order etc) of Classification Index Entries. A Classification Index can relate to one particular or to several Statistical Classifications. [GSIM Statistical Classification Model]
	   
	   Examples
	   ==========
	   An alphabetical index of a topically ordered Statistical Classification
	   
	   Explanatory notes
	   ===================
	   A Classification Index shows the relationship between text found in statistical data sources (responses to survey questionnaires, administrative records) and one or more Statistical Classifications.  A Classification Index may be used to assign the codes for Classification Items to observations in statistical collections. 
	   A Statistical Classification is a subtype of Node Set. The relationship between Statistical Classification and Classification Index can also be extended to include the other Node Set types - Code List and Category Set.  [GSIM Statistical Classification Model]  Note that a GSIM Node is the equivalent of a DDI Member and a GSIM Node Set is a DDI Collection.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Classification Index.
	"""
	def __init__(self)
	    self.releaseDate = datetime()
	    self.availableLanguage = None
	    self.corrections = []
	    self.codingInstruction = []
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
	    self. = None
	    self. = []
	    self. = []

class ClassificationRelationStructure(Identifiable):
	"""
	 Definition
	   ============
	   A complex RelationStructure for use with various classification collections ( CodeList, Statistical Classification, etc.)
	   
	   Examples
	   ==========
	   A ClassificationRelationStructure for ISCO-08 would describe each of the major classifications as a parent of its sub-classifications.   1 Managers, for example would be listed as a parent of four sub groups: 11 Chief Executives, Senior Officials and Legislators;  12 Chief Executives, Senior Officials and Legislators;  13 Production and Specialized Services Managers; and  14 Hospitality, Retail and Other Services Managers. 
	   
	   Explanatory notes
	   ===================
	   The ClassificationRelationStructure has a set of CategoryRelations which are basically adjacency lists. A source Category has a described relationship to a target list of Categories. The semantic might be, for example, "parentOf", or "contains", etc.. 
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.hasRelationSpecification = RelationSpecification.Unordered
	    self.semantic = None
	    self.totality = TotalityType.Total
	    self.displayLabel = []
	    self.hasMemberRelation = []
	    self. = []

class ValueDomain(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   The permitted range of values for a characteristic of a variable. [GSIM 1.1]
	   
	   Examples
	   ==========
	   Age categories with a numeric code list; Age in years; Young, Middle-aged and Old
	   
	   Explanatory notes
	   ===================
	   The values can be described by enumeration or by an expression. Value domains can be either substantive/sentinel, or described/enumeration
	   
	   Synonyms
	   ==========
	   BUT NOT Grid/Numeric/Code/*_ResponseDomain [DDI-L/Questions] - this is "ResponseDomain"
	   
	   DDI 3.2 mapping
	   =================
	   r:RepresentationType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Value Domain.
	"""
	def __init__(self)
	    self.displayLabel = []
	    self.recommendedDataType = []

class ClassificationFamily(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A Classification Family is a collection of Classification Series related from a particular point of view. The Classification Family is related by being based on a common Concept (e.g. economic activity).[GSIM1.1]
	   
	   Examples
	   ==========
	   A family of industrial classifications each a separate series (i.e. U.S. Standard Industrial Classification (SIC) and North American Industrial Classification System (NAICS)
	   
	   Explanatory notes
	   ===================
	   Different classification databases may use different types of Classification Families and have different names for the families, as no standard has been agreed upon. [GSIM1.1]
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============
	   Classification Family.
	"""
	def __init__(self)
	    self.type = CollectionType.Bag
	    self.name = []
	    self.purpose = None
	    self.contains = []
	    self.isOrdered = None
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class SubstantiveValueDomain(ValueDomain):
	"""
	 Definition
	   ============
	   The Value Domain for a substantive conceptual domain. 
	   
	   Examples
	   ==========
	   All real decimal numbers relating to the subject matter of interest between 0 and 1 specified in Arabic numerals. [GSIM 1.1].
	   The codes "M" male and "F" for female .
	   
	   
	   Explanatory notes
	   ===================
	   In DDI4 the value domain for a variable is separated into “substantive” and “sentinel” values. Substantive values are the values of primary interest. Sentinel values are additional values that may carry supplementary information, such as reasons for missing. This duality is described in ISO 11404. Substantive values for height might be real numbers expressed in meters. The full value domain might also include codes for different kinds of missing values - one code for “refused” and another for “don’t know”. Sentinel Values may also convey some substantive information and at the same time represent missing values. An example might be where a numeric variable like number of employees be sometimes a count and sometimes a code representing a range of counts in order to avoid disclosure of information about a specific entity. 
	   
	   The SubstantiveValueDomain may use either a ValueDescription, for described values,  or a CodeList for enumerated values, or both. 
	   
	   A value domain may consist of substantive values or sentinel values. Substantive values are those associated directly with some subject matter area. They do not address concerns around processing, such as missing values. Substantive values are called "regular values" in ISO/IEC 11404 - General purpose datatypes.
	   
	   The enumerated case is one where all values are listed. An example is the categorical values for gender:
	   The conceptual domain could consist of the concept of male and the concept of female. These might be represented in codes and associated labels as
	   1 (="Male")
	   2 (="Female")
	   
	   The described case is one where some description is needed to define the set of values.
	   Take the following description for height in meters:
	   "a real number between 0 and 3, represented to two Arabic decimal places". This description might be structured in some way to be machine actionable (datatype="double", min=0, max=3, decimals=2).
	   
	   
	   Synonyms
	   ==========
	   value domain [ISO11179]
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self. = []
	    self. = []
	    self. = []
