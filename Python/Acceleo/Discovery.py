



















class BoundingBox(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A type of Spatial coverage describing a rectangular area within which the actual range of location fits. A BoundingBox is described by 4 numbers - the maxima of the north, south, east, and west coordinates found in the area.
	   
	   Examples
	   ==========
	   Burkino Faso: (N) 15.082773; (S) 9.395691; (E) 2.397927; (W) -5.520837
	   
	   Explanatory notes
	   ===================
	   A BoundingBox is often described by two x,y coordinates where the x coordinates are used for the North and South Latitudes and y coordinates for the West and East Longitudes
	   
	   Synonyms
	   ==========
	   r:BoundingBox
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.eastLongitude = None
	    self.westLongitude = None
	    self.northLatitude = None
	    self.southLatitude = None

class Access(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Describes access to the annotated object. This item includes a confidentiality statement, descriptions of the access permissions required, restrictions to access, citation requirements, depositor requirements, conditions for access, a disclaimer, any time limits for access restrictions, and contact information regarding access.
	   
	   Examples
	   ==========
	   A proprietary InstanceQuestion might have specific access restrictions.
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   a:AccessType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.purpose = None
	    self.confidentialityStatement = None
	    self.accessPermission = []
	    self.restrictions = None
	    self.citationRequirement = None
	    self.depositRequirement = None
	    self.accessConditions = None
	    self.disclaimer = None
	    self.contactAgent = []
	    self.validDates = None

class Coverage(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Coverage information for an annotated object. Includes coverage information for temporal, topical, and spatial coverage. 
	   
	   Examples
	   ==========
	   A survey might have ask people about motels they stayed in (topical coverage) in the year 2015 (temporal coverage), while they were travelling in Kansas (spatial coverage). This is different than the temporal, and spatial attributes of the population studied – (international travelers to the US surveyed in 2017).
	   
	   Explanatory notes
	   ===================
	   Coverage is a container for the more specific temporal, spatial, and topical coverages to which it refers.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:CoverageType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.overview = None
	    self. = None
	    self. = None
	    self. = None

class SpatialCoverage(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   A description of spatial coverage (geographic coverage) of the annotated object. Spatial coverage is described using a number of objects that support searching by a wide range of systems (geospatial coordinates, geographic classification systems, and general systems using dcterms:spatial).
	   
	   Examples
	   ==========
	   A country, a neighborhood, the inside of a polygon on the surface of the earth, along a street, at a particular intersection, or perhaps even in a certain orbit around the planet Mars. For a data set within a study this may be used to define the geographic restriction of the data set within the geographic coverage of the study (eg. The study may cover all of Sweden but the spatial coverage of the data set is Stockholm). 
	   
	   Explanatory notes
	   ===================
	   Different systems support different approaches to descriptions of geographic or spatial coverage. Dublin Core has a descriptive text field that is the equivalent of SpatialCoverage/description. Geographers need to understand the spatial objects available (spatialObject, highest and lowest level geography, geographic units and unit types) so they know the building blocks they have to create a map. Spatial search engines do a first pass search by a simple bounding box (the north and south latitudes and east and west longitudes that define the spatial coverage area). Spatial Coverage provides basic support for all of these uses.
	   
	   Synonyms
	   ==========
	   r:SpatialCoverage, place
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.description = None
	    self.spatialAreaCode = []
	    self.spatialObject = SpatialObjectType.Point
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []
	    self. = []

class TopicalCoverage(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Describes the topical coverage of the module using Subject and Keyword. Subjects are members of structured classification systems such as formal subject headings in libraries. Keywords may be structured (e.g. TheSoz thesauri) or unstructured and reflect the terminology found in the document and other related (broader or similar) terms.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   r:TopicalCoverageType
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.subject = []
	    self.keyword = []

class TemporalCoverage(AnnotatedIdentifiable):
	"""
	 Definition
	   ============
	   Describes the temporal coverage of the annotated object by specifying dates and optionally associating them with Subject and Keyword. The date itself may be expressed as either and ISO and/or NonISO date. Dates may be catagorized by use of optional typeOfDate using and external controlled vocabulary entry.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   The association of subjects or keywords with the date periods allows for describing the meaning of the temporal period. A survey, for example might be administered in one time period, but ask questions about the preceding decade. Temporal coverage might then include the data collection coverage as well as the referenced time period.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.coverageDate = []
