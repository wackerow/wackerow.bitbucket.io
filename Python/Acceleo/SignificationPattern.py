

class Signifier(object):
	"""
	 Definition
	   ============
	   Concept whose extension includes perceivable objects. 
	   
	   Examples
	   ==========
	   A perceivable object in the extension of a signifier is a token. For instance, the object 5 is a token of “the numeral five”, a signifier.
	   
	   Explanatory notes
	   ===================
	   A signifier has the potential to refer to an object.  In this case, the referring signifier is a label.  If that object is a concept, then the referring signifier is a designation.  A label is a representation of an object by a signifier which denotes it.  For instance, the token http://www.bls.gov is a label for the web site maintained for the US Bureau of Labor Statistics. The label is also an identifier, since it is intended to dereference the BLS web site. It is a locator, since the HTTP service is associated with it. Finally, ‘Dan Gillman’ is a name for a co-author of this paper, since the token is linguistic.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.content = None
	    self.whiteSpace = WhiteSpaceRule.Preserve

class Sign(CollectionMember):
	"""
	 Definition
	   ============
	   Something that suggests the presence or existence of a fact, condition, or quality.
	   
	   Examples
	   ==========
	   The terms “Unemployment” and “Arbeitslosigkeit”
	   
	   Explanatory notes
	   ===================
	   It is a perceivable object used to denote a concept or an object.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	def __init__(self)
	    self.representation = None
	    self. = []

class Signified(CollectionMember):
	"""
	 Definition
	   ============
	   Concept or object denoted by the signifier associated to a sign.
	   
	   Examples
	   ==========
	   
	   
	   Explanatory notes
	   ===================
	   Concept or object represented by the sign.
	   
	   Synonyms
	   ==========
	   
	   
	   DDI 3.2 mapping
	   =================
	   
	   
	   RDF mapping
	   =============
	   
	   
	   GSIM mapping
	   ==============.
	"""
	    pass
