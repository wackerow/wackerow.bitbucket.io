.. This code was generated by the Eclipse Acceleo project UCMIS M2T.
   Target language is 'reStructuredText' ('doc') generated on the basis of the model 'GSIM1.2'.

.. _GSIM1.2::BusinessGroup::ParameterInput:

ParameterInput
**************

**Fully qualified class name**: GSIM1.2::BusinessGroup:::index:`ParameterInput`


<b>Definition:</b> Inputs used to specify which configuration should be used for a specific <i>Process Step</i> which has been designed to be configurable.

<b>Explanatory Text: </b><i>Parameter Inputs </i>may be provided where <i>Rules</i> and/or <i>Business Service</i> interfaces associated with a particular <i>Process Step</i> have been designed to be configurable based on inputs passed in to the <i>Process Step.</i>


.. container::
   :name: diagram

   .. dropdown:: Diagram
      :open:

      .. uml:: /BusinessGroup/ParameterInput.pu

.. container::
   :name: inheritance

   .. dropdown:: Inheritance

      .. |_| unicode:: U+2003 .. em space

      .. role:: focus

      .. cssclass:: regular

      | :ref:`BusinessGroup::ProcessInput<GSIM1.2::BusinessGroup::ProcessInput>`

      .. cssclass:: regular

      | |_| ᐊ── :focus:`BusinessGroup::ParameterInput`




.. container::
   :name: attributes

   .. dropdown:: Attributes
    
      .. list-table::
         :widths: 10 10 60 10 10 10
         :header-rows: 1
         :class: datatable-attributes
    
         * - Name
           - Inherited from
           - Description
           - Data Type
           - Multiplicity
           - Default value
         * - DataTypeG
           - \- *own class* \-
           - <b>Description: </b><font color="#333333">The data type of the <i>Parameter Input</i>.</font>
           - String 
           - 1..1
           - 

         * - ParameterRole
           - \- *own class* \-
           - <b>Description: </b>Used to convey the role of this parameter.
             For example:
             Weight
             UpperThreshold
             AgreementLevel
             This will likely become a controlled vocabulary (maybe external to allow more timely maintenance).
           - String 
           - 0..*
           - 

         * - ParameterValue
           - \- *own class* \-
           - <b>Description: </b>The content of the parameter.
           - String 
           - 1..1
           - 


.. container::
   :name: associations

   .. dropdown:: Associations
    
      .. list-table::
         :widths: 5 10 30 5 10 10 5 5 15
         :header-rows: 1
         :class: datatable-associations
       
         * - Direction
           - Association
           - Description
           - Multiplicity of ParameterInput
           - Package of Other Class
           - Other Class
           - Multiplicity of other class
           - Aggregation Kind
           - Inherited from
         * - to
           - ProcessInput correspondsto 
           - 
           - 1..1
           - \- *own package* \-
           - :ref:`GSIM1.2::BusinessGroup::ProcessInputSpecification`
           - 1..1
           - none
           - :ref:`BusinessGroup::ProcessInput<GSIM1.2::BusinessGroup::ProcessInput>`
         * - from
           - ProcessInput has ProcessStepInstance
           - 
           - 1..*
           - \- *own package* \-
           - :ref:`GSIM1.2::BusinessGroup::ProcessStepInstance`
           - 1..1
           - none
           - :ref:`BusinessGroup::ProcessInput<GSIM1.2::BusinessGroup::ProcessInput>`
         * - from
           - ParameterInput usesasparameters ProcessControl
           - 
           - 0..*
           - \- *own package* \-
           - :ref:`GSIM1.2::BusinessGroup::ProcessControl`
           - 0..*
           - none
           - \- *own class* \-

.. container::
   :name: encodings

   .. dropdown:: Syntax representations / encodings
    
      All syntax representations except the Canonical XMI are provided as reference points
      for specific implementations, or for use as defaults if sufficient in the form presented.
    
      .. tab-set::
    
         .. tab-item:: Canonical XMI
            :class-content: encoding
    
            Fragment for the class **ParameterInput** (`entire model as XMI </encoding/xmi/ddi-cdi_canonical-unique-names.xmi>`_)
    
            .. literalinclude:: /xmi/ParameterInput.xmi
               :lines: 2-
               :language: xml
               :linenos:
               :emphasize-lines: 4

         .. tab-item:: XML Schema
            :class-content: encoding

            Fragment for the class **ParameterInput** (`entire XML Schema </encoding/xml-schema/ddi-cdi.xsd>`_)
    
            .. literalinclude:: /BusinessGroup/ParameterInput_xsd.txt 
               :language: xml
               :linenos:
               :emphasize-lines: 1
               :dedent: 2

         .. tab-item:: Ontology (Turtle)
            :class-content: encoding

            Fragment for the class **ParameterInput** (`main ontology </encoding/ontology/ddi-cdi.onto.ttl>`_)
    
            .. literalinclude:: /BusinessGroup/ParameterInput.onto.ttl 
               :language: turtle
               :linenos:
               :emphasize-lines: 3

         .. tab-item:: JSON-LD
            :class-content: encoding

            Fragment for the class **ParameterInput** (`main JSON-LD </encoding/json-ld/ddi-cdi.jsonld>`_)
    
            .. literalinclude:: /BusinessGroup/ParameterInput.jsonld 
               :language: JSON-LD
               :linenos:
               :emphasize-lines: 5

