.. This code was generated by the Eclipse Acceleo project UCMIS M2T.
   Target language is 'reStructuredText' ('doc') generated on the basis of the model 'GSIM1.2'.

.. _GSIM1.2::ExchangeGroup::Protocol:

Protocol
********

**Fully qualified class name**: GSIM1.2::ExchangeGroup:::index:`Protocol`


<b>Definition:</b> The mechanism for exchanging information through an <i>Exchange Channel</i>.

<b>Explanatory Text:</b> A <i>Protocol </i>specifies the mechanism (e.g. SDMX web service, data file exchange, web robot, face to face interview, mailed paper form) of exchanging information through an <i>Exchange Channel</i>.


.. container::
   :name: diagram

   .. dropdown:: Diagram
      :open:

      .. uml:: /ExchangeGroup/Protocol.pu

.. container::
   :name: inheritance

   .. dropdown:: Inheritance

      .. |_| unicode:: U+2003 .. em space

      .. role:: focus

      .. cssclass:: regular

      | :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`

      .. cssclass:: regular

      | |_| ᐊ── :focus:`ExchangeGroup::Protocol`




.. container::
   :name: attributes

   .. dropdown:: Attributes
    
      .. list-table::
         :widths: 10 10 60 10 10 10
         :header-rows: 1
         :class: datatable-attributes
    
         * - Name
           - Inherited from
           - Description
           - Data Type
           - Multiplicity
           - Default value
         * - Description
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
           - <b>Description: </b>The description of the information object.
           - String 
           - 0..1
           - 

         * - Id
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
           - <b>Description: </b>The unique identifier of the information object; assigned by the owner agency.
           - String 
           - 1..1
           - 

         * - LocalID
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
           - <b>Description: </b><font color="#2d2d2d">This is an identifier in a given local context that uniquely references an information object. For example, Local ID could be a variable name in a dataset.</font>
           - String 
           - 0..1
           - 

         * - Name
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
           - <b>Description: </b>A term which designates a concept, in this case an information object. The identifying name will be the preferred designation. There will be many terms to designate the same information object, such as synonyms and terms in other languages.
           - String 
           - 1..1
           - 

         * - Version
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
           - <b>Description: </b>The version designator of the information object assigned by the owner agency.
           - String 
           - 0..1
           - 

         * - VersionDate
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
           - <b>Description: </b>The date on which the version was created.
           - String 
           - 0..1
           - 

         * - VersionRationale
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
           - <b>Description: </b>The reason for making this version of the information object.
           - String 
           - 0..1
           - 


.. container::
   :name: associations

   .. dropdown:: Associations
    
      .. list-table::
         :widths: 5 10 30 5 10 10 5 5 15
         :header-rows: 1
         :class: datatable-associations
       
         * - Direction
           - Association
           - Description
           - Multiplicity of Protocol
           - Package of Other Class
           - Other Class
           - Multiplicity of other class
           - Aggregation Kind
           - Inherited from
         * - to
           - AdministrativeDetails has IdentifiableArtefact
           - 
           - 1..1
           - :ref:`GSIM1.2::Base`
           - :ref:`GSIM1.2::Base::AdministrativeDetails`
           - 0..1
           - composite
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - ChangeDefinition basedupon IdentifiableArtefact
           - 
           - 0..*
           - :ref:`GSIM1.2::BusinessGroup`
           - :ref:`GSIM1.2::BusinessGroup::ChangeDefinition`
           - 0..*
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - ChangeDefinition resultsin IdentifiableArtefact
           - 
           - 0..*
           - :ref:`GSIM1.2::BusinessGroup`
           - :ref:`GSIM1.2::BusinessGroup::ChangeDefinition`
           - 0..*
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - ChangeEvent appliesto IdentifiableArtefact
           - 
           - 1..*
           - :ref:`GSIM1.2::Base`
           - :ref:`GSIM1.2::Base::ChangeEvent`
           - 0..1
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - ChangeEventTuple source IdentifiableArtefact
           - 
           - 1..*
           - :ref:`GSIM1.2::Base`
           - :ref:`GSIM1.2::Base::ChangeEventTuple`
           - 1..1
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - ChangeEventTuple target IdentifiableArtefact
           - 
           - 1..*
           - :ref:`GSIM1.2::Base`
           - :ref:`GSIM1.2::Base::ChangeEventTuple`
           - 1..1
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - from
           - IdentifiableArtefact isadministeredby AgentInRole
           - 
           - 1..*
           - :ref:`GSIM1.2::Base`
           - :ref:`GSIM1.2::Base::AgentInRole`
           - 0..*
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - ProcessSupportInput refersto IdentifiableArtefact
           - 
           - 0..1
           - :ref:`GSIM1.2::BusinessGroup`
           - :ref:`GSIM1.2::BusinessGroup::ProcessSupportInput`
           - 1..1
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - TransformableInput refersto IdentifiableArtefact
           - 
           - 1..1
           - :ref:`GSIM1.2::BusinessGroup`
           - :ref:`GSIM1.2::BusinessGroup::TransformableInput`
           - 1..1
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - to
           - TransformedOutput refersto IdentifiableArtefact
           - 
           - 1..1
           - :ref:`GSIM1.2::BusinessGroup`
           - :ref:`GSIM1.2::BusinessGroup::TransformedOutput`
           - 1..1
           - none
           - :ref:`Base::IdentifiableArtefact<GSIM1.2::Base::IdentifiableArtefact>`
         * - from
           - Protocol has ExchangeChannel
           - 
           - 0..1
           - \- *own package* \-
           - :ref:`GSIM1.2::ExchangeGroup::ExchangeChannel`
           - 0..*
           - none
           - \- *own class* \-

.. container::
   :name: encodings

   .. dropdown:: Syntax representations / encodings
    
      All syntax representations except the Canonical XMI are provided as reference points
      for specific implementations, or for use as defaults if sufficient in the form presented.
    
      .. tab-set::
    
         .. tab-item:: Canonical XMI
            :class-content: encoding
    
            Fragment for the class **Protocol** (`entire model as XMI </encoding/xmi/ddi-cdi_canonical-unique-names.xmi>`_)
    
            .. literalinclude:: /xmi/Protocol.xmi
               :lines: 2-
               :language: xml
               :linenos:
               :emphasize-lines: 4

         .. tab-item:: XML Schema
            :class-content: encoding

            Fragment for the class **Protocol** (`entire XML Schema </encoding/xml-schema/ddi-cdi.xsd>`_)
    
            .. literalinclude:: /ExchangeGroup/Protocol_xsd.txt 
               :language: xml
               :linenos:
               :emphasize-lines: 1
               :dedent: 2

         .. tab-item:: Ontology (Turtle)
            :class-content: encoding

            Fragment for the class **Protocol** (`main ontology </encoding/ontology/ddi-cdi.onto.ttl>`_)
    
            .. literalinclude:: /ExchangeGroup/Protocol.onto.ttl 
               :language: turtle
               :linenos:
               :emphasize-lines: 3

         .. tab-item:: JSON-LD
            :class-content: encoding

            Fragment for the class **Protocol** (`main JSON-LD </encoding/json-ld/ddi-cdi.jsonld>`_)
    
            .. literalinclude:: /ExchangeGroup/Protocol.jsonld 
               :language: JSON-LD
               :linenos:
               :emphasize-lines: 5

