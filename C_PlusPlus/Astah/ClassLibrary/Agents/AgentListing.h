#ifndef CLASSLIBRARY_AGENTS_AGENT_LISTING_H
#define CLASSLIBRARY_AGENTS_AGENT_LISTING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/AgentIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Agents/AgentRelationStructure.h"
#include "ClassLibrary/Agents/Agent.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Agents
{
class AgentListing : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::AgentIndicator contains[];

	Primitive Package::Boolean isOrdered;

	AgentRelationStructure agentRelationStructure[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	Agent agent[];
};

}  // namespace Agents
}  // namespace ClassLibrary
#endif
