#ifndef CLASSLIBRARY_AGENTS_ORGANIZATION_H
#define CLASSLIBRARY_AGENTS_ORGANIZATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/OrganizationName.h"
#include "ClassLibrary/ComplexDataTypes/ContactInformation.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitTypeClassification.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitClassification.h"
#include "ClassLibrary/Representations/StatisticalClassification.h"
#include "ClassLibrary/Agents/Agent.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace Agents
{
class Organization : public Agent
{
public:
	ClassLibrary::ComplexDataTypes::OrganizationName hasOrganizationName[];

	Primitive Package::String ddiId[];

	ClassLibrary::ComplexDataTypes::ContactInformation hasContactInformation;

	ClassLibrary::Representations::StatisticalClassification statisticalClassification[];
	ClassLibrary::GeographicClassification::GeographicUnitTypeClassification geographicUnitTypeClassification[];
	ClassLibrary::GeographicClassification::GeographicUnitClassification geographicUnitClassification[];
};

}  // namespace Agents
}  // namespace ClassLibrary
#endif
