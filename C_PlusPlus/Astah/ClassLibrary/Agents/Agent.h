#ifndef CLASSLIBRARY_AGENTS_AGENT_H
#define CLASSLIBRARY_AGENTS_AGENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/AgentId.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/PrivateImage.h"
#include "ClassLibrary/ComplexDataTypes/AgentRelation.h"
#include "ClassLibrary/ComplexDataTypes/AgentIndicator.h"
#include "ClassLibrary/ComplexDataTypes/AgentAssociation.h"
#include "ClassLibrary/ProcessPattern/Service.h"
#include "ClassLibrary/SamplingMethodology/SampleFrame.h"
#include "ClassLibrary/StudyRelated/Embargo.h"
#include "ClassLibrary/StudyRelated/ExPostEvaluation.h"
#include "ClassLibrary/Workflows/WorkflowService.h"
#include "ClassLibrary/Utility/FundingInformation.h"
#include "ClassLibrary/Representations/ClassificationIndex.h"
#include "ClassLibrary/Representations/ClassificationSeries.h"
#include "ClassLibrary/Representations/AuthorizationSource.h"
#include "ClassLibrary/Representations/CorrespondenceTable.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/AgentListing.h"

namespace ClassLibrary
{
namespace Agents
{
class Agent : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::AgentId hasAgentId[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::PrivateImage image[];

	ClassLibrary::Representations::ClassificationIndex classificationIndex[];
	ClassLibrary::Representations::ClassificationIndex classificationIndex[];
	ClassLibrary::Representations::ClassificationSeries classificationSeries[];
	ClassLibrary::StudyRelated::Embargo embargo;
	ClassLibrary::Utility::FundingInformation fundingInformation[];
	ClassLibrary::SamplingMethodology::SampleFrame sampleFrame;
	ClassLibrary::Representations::AuthorizationSource authorizationSource[];
	ClassLibrary::Representations::CorrespondenceTable correspondenceTable[];
	ClassLibrary::Representations::CorrespondenceTable correspondenceTable[];
	ClassLibrary::Representations::CorrespondenceTable correspondenceTable;
	ClassLibrary::ProcessPattern::Service service[];
	ClassLibrary::StudyRelated::ExPostEvaluation exPostEvaluation[];
	ClassLibrary::StudyRelated::Embargo embargo;
	ClassLibrary::ComplexDataTypes::AgentRelation agentRelation;
	ClassLibrary::ComplexDataTypes::AgentRelation agentRelation;
	ClassLibrary::ComplexDataTypes::AgentIndicator agentIndicator;
	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
	AgentListing agentListing;
	ClassLibrary::Workflows::WorkflowService workflowService[];
	ClassLibrary::ComplexDataTypes::AgentAssociation agentAssociation;
};

}  // namespace Agents
}  // namespace ClassLibrary
#endif
