#ifndef CLASSLIBRARY_AGENTS_MACHINE_H
#define CLASSLIBRARY_AGENTS_MACHINE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/AccessLocation.h"
#include "ClassLibrary/ComplexDataTypes/ContactInformation.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace Agents
{
class Machine : public Agent
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfMachine;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::AccessLocation hasAccessLocation;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry function[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry machineInterface[];

	ClassLibrary::ComplexDataTypes::ContactInformation ownerOperatorContact;

};

}  // namespace Agents
}  // namespace ClassLibrary
#endif
