#ifndef CLASSLIBRARY_AGENTS_INDIVIDUAL_H
#define CLASSLIBRARY_AGENTS_INDIVIDUAL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/IndividualName.h"
#include "ClassLibrary/ComplexDataTypes/ContactInformation.h"
#include "ClassLibrary/Agents/Agent.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace Agents
{
class Individual : public Agent
{
public:
	ClassLibrary::ComplexDataTypes::IndividualName hasIndividualName[];

	Primitive Package::String ddiId[];

	ClassLibrary::ComplexDataTypes::ContactInformation hasContactInformation;

};

}  // namespace Agents
}  // namespace ClassLibrary
#endif
