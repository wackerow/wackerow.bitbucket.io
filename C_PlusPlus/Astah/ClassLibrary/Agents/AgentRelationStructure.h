#ifndef CLASSLIBRARY_AGENTS_AGENT_RELATION_STRUCTURE_H
#define CLASSLIBRARY_AGENTS_AGENT_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/AgentRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Agents/AgentListing.h"

namespace ClassLibrary
{
namespace Agents
{
class AgentRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::DateRange effectiveDates;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry privacy;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::AgentRelation hasMemberRelation[];

	AgentListing agentListing[];
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace Agents
}  // namespace ClassLibrary
#endif
