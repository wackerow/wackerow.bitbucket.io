#ifndef CLASSLIBRARY_BUSINESSWORKFLOW_DATA_PIPELINE_H
#define CLASSLIBRARY_BUSINESSWORKFLOW_DATA_PIPELINE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/BusinessProcessIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace BusinessWorkflow
{
class DataPipeline : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::BusinessProcessIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::CollectionsPattern::SimpleCollection simpleCollection[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace BusinessWorkflow
}  // namespace ClassLibrary
#endif
