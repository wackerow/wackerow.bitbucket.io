#ifndef CLASSLIBRARY_BUSINESSWORKFLOW_BUSINESS_PROCESS_H
#define CLASSLIBRARY_BUSINESSWORKFLOW_BUSINESS_PROCESS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/PairedExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/BusinessProcessCondition.h"
#include "ClassLibrary/ComplexDataTypes/BusinessProcessIndicator.h"
#include "ClassLibrary/Workflows/WorkflowProcess.h"

namespace ClassLibrary
{
namespace BusinessWorkflow
{
class BusinessProcess : public ClassLibrary::Workflows::WorkflowProcess
{
public:
	ClassLibrary::ComplexDataTypes::PairedExternalControlledVocabularyEntry standardModelUsed[];

	ClassLibrary::ComplexDataTypes::BusinessProcessCondition preCondition[];

	ClassLibrary::ComplexDataTypes::BusinessProcessCondition postCondition[];

	ClassLibrary::ComplexDataTypes::BusinessProcessIndicator businessProcessIndicator;
};

}  // namespace BusinessWorkflow
}  // namespace ClassLibrary
#endif
