#ifndef CLASSLIBRARY_BUSINESSWORKFLOW_BUSINESS_ALGORITHM_H
#define CLASSLIBRARY_BUSINESSWORKFLOW_BUSINESS_ALGORITHM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"

namespace ClassLibrary
{
namespace BusinessWorkflow
{
class BusinessAlgorithm : public ClassLibrary::SimpleMethodologyOverview::AlgorithmOverview
{
};

}  // namespace BusinessWorkflow
}  // namespace ClassLibrary
#endif
