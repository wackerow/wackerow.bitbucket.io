#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_SIMPLE_COLLECTION_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_SIMPLE_COLLECTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/CustomMetadata/CustomInstance.h"
#include "ClassLibrary/BusinessWorkflow/DataPipeline.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"
#include "ClassLibrary/LogicalDataDescription/ViewpointRole.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "ClassLibrary/CollectionsPattern/Comparison.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class SimpleCollection : public CollectionMember
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	MemberIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::BusinessWorkflow::DataPipeline dataPipeline;
	ClassLibrary::Workflows::WorkflowStepSequence workflowStepSequence;
	ClassLibrary::LogicalDataDescription::ViewpointRole viewpointRole;
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord;
	ClassLibrary::CustomMetadata::CustomInstance customInstance;
	Comparison comparison[];
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
