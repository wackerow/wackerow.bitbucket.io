#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_COMPARISON_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_COMPARISON_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/ConceptSystemCorrespondence.h"
#include "ClassLibrary/Representations/CorrespondenceTable.h"
#include "ClassLibrary/LogicalDataDescription/RecordRelation.h"
#include "ClassLibrary/CollectionsPattern/ComparisonMap.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class Comparison : public ClassLibrary::Identification::Identifiable
{
public:
	ComparisonMap correspondence[];

	ClassLibrary::Representations::CorrespondenceTable correspondenceTable;
	ClassLibrary::Conceptual::ConceptSystemCorrespondence conceptSystemCorrespondence;
	ClassLibrary::LogicalDataDescription::RecordRelation recordRelation;
	SimpleCollection simpleCollection[];
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
