#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_MEMBER_INDICATOR_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_MEMBER_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CustomValueIndicator.h"
#include "ClassLibrary/ComplexDataTypes/DataPointIndicator.h"
#include "ClassLibrary/ComplexDataTypes/DataStoreIndicator.h"
#include "ClassLibrary/ComplexDataTypes/PhysicalRecordSegmentIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationIndexEntryIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationSeriesIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationItemIndicator.h"
#include "ClassLibrary/ComplexDataTypes/CodeIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ConceptIndicator.h"
#include "ClassLibrary/ComplexDataTypes/StatisticalClassificationIndicator.h"
#include "ClassLibrary/ComplexDataTypes/CustomItemIndicator.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableIndicator.h"
#include "ClassLibrary/ComplexDataTypes/WorkflowStepSequenceIndicator.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitTypeIndicator.h"
#include "ClassLibrary/ComplexDataTypes/LogicalRecordIndicator.h"
#include "ClassLibrary/ComplexDataTypes/AgentIndicator.h"
#include "ClassLibrary/ComplexDataTypes/BusinessProcessIndicator.h"
#include "ClassLibrary/ComplexDataTypes/CategoryIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ValueMappingIndicator.h"
#include "ClassLibrary/ComplexDataTypes/StudyIndicator.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitIndicator.h"
#include "ClassLibrary/ComplexDataTypes/VocabularyEntryIndicator.h"
#include "ClassLibrary/ComplexDataTypes/VariableIndicator.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class MemberIndicator
{
public:
	Primitive Package::Integer index;

	CollectionMember collectionMember[];
	ClassLibrary::ComplexDataTypes::CustomValueIndicator customValueIndicator;
	ClassLibrary::ComplexDataTypes::DataPointIndicator dataPointIndicator;
	ClassLibrary::ComplexDataTypes::DataStoreIndicator dataStoreIndicator;
	ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentIndicator physicalRecordSegmentIndicator;
	ClassLibrary::ComplexDataTypes::ClassificationIndexEntryIndicator classificationIndexEntryIndicator;
	ClassLibrary::ComplexDataTypes::ClassificationSeriesIndicator classificationSeriesIndicator;
	ClassLibrary::ComplexDataTypes::ClassificationItemIndicator classificationItemIndicator;
	ClassLibrary::ComplexDataTypes::CodeIndicator codeIndicator;
	ClassLibrary::ComplexDataTypes::ConceptIndicator conceptIndicator;
	ClassLibrary::ComplexDataTypes::StatisticalClassificationIndicator statisticalClassificationIndicator;
	ClassLibrary::ComplexDataTypes::CustomItemIndicator customItemIndicator;
	ClassLibrary::ComplexDataTypes::InstanceVariableIndicator instanceVariableIndicator;
	ClassLibrary::ComplexDataTypes::WorkflowStepSequenceIndicator workflowStepSequenceIndicator;
	ClassLibrary::ComplexDataTypes::GeographicUnitTypeIndicator geographicUnitTypeIndicator;
	ClassLibrary::ComplexDataTypes::LogicalRecordIndicator logicalRecordIndicator;
	ClassLibrary::ComplexDataTypes::AgentIndicator agentIndicator;
	ClassLibrary::ComplexDataTypes::BusinessProcessIndicator businessProcessIndicator;
	ClassLibrary::ComplexDataTypes::CategoryIndicator categoryIndicator;
	ClassLibrary::ComplexDataTypes::ValueMappingIndicator valueMappingIndicator;
	ClassLibrary::ComplexDataTypes::StudyIndicator studyIndicator;
	ClassLibrary::ComplexDataTypes::GeographicUnitIndicator geographicUnitIndicator;
	ClassLibrary::ComplexDataTypes::VocabularyEntryIndicator vocabularyEntryIndicator;
	ClassLibrary::ComplexDataTypes::VariableIndicator variableIndicator;
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
