#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_MEMBER_RELATION_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_MEMBER_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/CustomValueRelation.h"
#include "ClassLibrary/ComplexDataTypes/DataPointRelation.h"
#include "ClassLibrary/ComplexDataTypes/DataStoreRelation.h"
#include "ClassLibrary/ComplexDataTypes/PhysicalRecordSegmentRelation.h"
#include "ClassLibrary/ComplexDataTypes/CategoryRelation.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationSeriesRelation.h"
#include "ClassLibrary/ComplexDataTypes/LogicalRecordRelation.h"
#include "ClassLibrary/ComplexDataTypes/StatisticalClassificationRelation.h"
#include "ClassLibrary/ComplexDataTypes/ConceptRelation.h"
#include "ClassLibrary/ComplexDataTypes/CustomItemRelation.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableRelation.h"
#include "ClassLibrary/ComplexDataTypes/IndexEntryRelation.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitTypeRelation.h"
#include "ClassLibrary/ComplexDataTypes/WorkflowStepRelation.h"
#include "ClassLibrary/ComplexDataTypes/VocabularyEntryRelation.h"
#include "ClassLibrary/ComplexDataTypes/AgentRelation.h"
#include "ClassLibrary/ComplexDataTypes/ValueMappingRelation.h"
#include "ClassLibrary/ComplexDataTypes/StudyRelation.h"
#include "ClassLibrary/ComplexDataTypes/ViewpointRoleRelation.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitRelation.h"
#include "ClassLibrary/ComplexDataTypes/VariableRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class MemberRelation
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	CollectionMember collectionMember[];
	CollectionMember collectionMember[];
	ClassLibrary::ComplexDataTypes::CustomValueRelation customValueRelation;
	ClassLibrary::ComplexDataTypes::DataPointRelation dataPointRelation;
	ClassLibrary::ComplexDataTypes::DataStoreRelation dataStoreRelation;
	ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentRelation physicalRecordSegmentRelation;
	ClassLibrary::ComplexDataTypes::CategoryRelation categoryRelation;
	ClassLibrary::ComplexDataTypes::ClassificationSeriesRelation classificationSeriesRelation;
	ClassLibrary::ComplexDataTypes::LogicalRecordRelation logicalRecordRelation;
	ClassLibrary::ComplexDataTypes::StatisticalClassificationRelation statisticalClassificationRelation;
	ClassLibrary::ComplexDataTypes::ConceptRelation conceptRelation;
	ClassLibrary::ComplexDataTypes::CustomItemRelation customItemRelation;
	ClassLibrary::ComplexDataTypes::InstanceVariableRelation instanceVariableRelation;
	ClassLibrary::ComplexDataTypes::IndexEntryRelation indexEntryRelation;
	ClassLibrary::ComplexDataTypes::GeographicUnitTypeRelation geographicUnitTypeRelation;
	ClassLibrary::ComplexDataTypes::WorkflowStepRelation workflowStepRelation;
	ClassLibrary::ComplexDataTypes::VocabularyEntryRelation vocabularyEntryRelation;
	ClassLibrary::ComplexDataTypes::AgentRelation agentRelation;
	ClassLibrary::ComplexDataTypes::ValueMappingRelation valueMappingRelation;
	ClassLibrary::ComplexDataTypes::StudyRelation studyRelation;
	ClassLibrary::ComplexDataTypes::ViewpointRoleRelation viewpointRoleRelation;
	ClassLibrary::ComplexDataTypes::GeographicUnitRelation geographicUnitRelation;
	ClassLibrary::ComplexDataTypes::VariableRelation variableRelation;
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
