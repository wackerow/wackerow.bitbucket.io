#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_COLLECTION_MEMBER_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_COLLECTION_MEMBER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleCodebook/VariableStatistics.h"
#include "ClassLibrary/CustomMetadata/CustomItem.h"
#include "ClassLibrary/CustomMetadata/CustomValue.h"
#include "ClassLibrary/CustomMetadata/VocabularyEntry.h"
#include "ClassLibrary/FormatDescription/ValueMapping.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Representations/ClassificationIndexEntry.h"
#include "ClassLibrary/LogicalDataDescription/DataPoint.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "ClassLibrary/CollectionsPattern/ComparisonMap.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class CollectionMember : public ClassLibrary::Identification::Identifiable
{
public:
	MemberRelation memberRelation[];
	MemberRelation memberRelation;
	ClassLibrary::Representations::ClassificationIndexEntry classificationIndexEntry;
	ClassLibrary::StudyRelated::Study study;
	ClassLibrary::SimpleCodebook::VariableStatistics variableStatistics;
	ClassLibrary::CustomMetadata::CustomItem customItem;
	ClassLibrary::CustomMetadata::CustomValue customValue;
	ClassLibrary::CustomMetadata::VocabularyEntry vocabularyEntry;
	MemberIndicator memberIndicator;
	ComparisonMap comparisonMap[];
	ComparisonMap comparisonMap[];
	ClassLibrary::Agents::Agent agent;
	ClassLibrary::FormatDescription::ValueMapping valueMapping;
	ClassLibrary::LogicalDataDescription::DataPoint dataPoint;
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
