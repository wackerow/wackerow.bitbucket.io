#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_RELATION_STRUCTURE_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/CategoryRelationStructure.h"
#include "ClassLibrary/Conceptual/ConceptRelationStructure.h"
#include "ClassLibrary/Conceptual/VariableRelationStructure.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitRelationStructure.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitTypeRelationStructure.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CustomMetadata/CustomItemRelationStructure.h"
#include "ClassLibrary/CustomMetadata/VocabularyRelationStructure.h"
#include "ClassLibrary/FormatDescription/DataPointRelationStructure.h"
#include "ClassLibrary/FormatDescription/PhysicalOrderRelationStructure.h"
#include "ClassLibrary/FormatDescription/PhysicalLayoutRelationStructure.h"
#include "ClassLibrary/StudyRelated/StudyRelationStructure.h"
#include "ClassLibrary/Workflows/WorkflowStepRelationStructure.h"
#include "ClassLibrary/Representations/ClassificationRelationStructure.h"
#include "ClassLibrary/Representations/ClassificationSeriesRelationStructure.h"
#include "ClassLibrary/Representations/IndexEntryRelationStructure.h"
#include "ClassLibrary/Representations/StatisticalClassificationRelationStructure.h"
#include "ClassLibrary/LogicalDataDescription/DataStoreRelationStructure.h"
#include "ClassLibrary/LogicalDataDescription/InstanceVariableRelationStructure.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecordRelationStructure.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Agents/AgentRelationStructure.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class RelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	MemberRelation hasMemberRelation[];

	ClassLibrary::Representations::ClassificationRelationStructure classificationRelationStructure;
	ClassLibrary::Representations::ClassificationSeriesRelationStructure classificationSeriesRelationStructure;
	ClassLibrary::StudyRelated::StudyRelationStructure studyRelationStructure;
	ClassLibrary::Conceptual::CategoryRelationStructure categoryRelationStructure;
	ClassLibrary::Conceptual::ConceptRelationStructure conceptRelationStructure;
	ClassLibrary::CustomMetadata::CustomItemRelationStructure customItemRelationStructure;
	ClassLibrary::CustomMetadata::VocabularyRelationStructure vocabularyRelationStructure;
	ClassLibrary::Workflows::WorkflowStepRelationStructure workflowStepRelationStructure;
	ClassLibrary::Representations::IndexEntryRelationStructure indexEntryRelationStructure;
	ClassLibrary::Representations::StatisticalClassificationRelationStructure statisticalClassificationRelationStructure;
	StructuredCollection structuredCollection[];
	ClassLibrary::Conceptual::VariableRelationStructure variableRelationStructure;
	ClassLibrary::FormatDescription::DataPointRelationStructure dataPointRelationStructure;
	ClassLibrary::FormatDescription::PhysicalOrderRelationStructure physicalOrderRelationStructure;
	ClassLibrary::FormatDescription::PhysicalLayoutRelationStructure physicalLayoutRelationStructure;
	ClassLibrary::LogicalDataDescription::DataStoreRelationStructure dataStoreRelationStructure;
	ClassLibrary::LogicalDataDescription::InstanceVariableRelationStructure instanceVariableRelationStructure;
	ClassLibrary::LogicalDataDescription::LogicalRecordRelationStructure logicalRecordRelationStructure;
	ClassLibrary::Agents::AgentRelationStructure agentRelationStructure;
	ClassLibrary::GeographicClassification::GeographicUnitRelationStructure geographicUnitRelationStructure;
	ClassLibrary::GeographicClassification::GeographicUnitTypeRelationStructure geographicUnitTypeRelationStructure;
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
