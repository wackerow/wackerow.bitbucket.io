#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_COMPARISON_MAP_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_COMPARISON_MAP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CorrespondenceType.h"
#include "ClassLibrary/ComplexDataTypes/Map.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableValueMap.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class ComparisonMap
{
public:
	ClassLibrary::ComplexDataTypes::CorrespondenceType hasCorrespondenceType;

	ClassLibrary::ComplexDataTypes::Map map;
	CollectionMember collectionMember[];
	CollectionMember collectionMember[];
	ClassLibrary::ComplexDataTypes::InstanceVariableValueMap instanceVariableValueMap;
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
