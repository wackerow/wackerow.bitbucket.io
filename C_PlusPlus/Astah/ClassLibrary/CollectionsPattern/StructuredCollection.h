#ifndef CLASSLIBRARY_COLLECTIONSPATTERN_STRUCTURED_COLLECTION_H
#define CLASSLIBRARY_COLLECTIONSPATTERN_STRUCTURED_COLLECTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/VariableCollection.h"
#include "ClassLibrary/Conceptual/ConceptSystem.h"
#include "ClassLibrary/CustomMetadata/CustomStructure.h"
#include "ClassLibrary/CustomMetadata/ControlledVocabulary.h"
#include "ClassLibrary/FormatDescription/PhysicalDataSet.h"
#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/FormatDescription/PhysicalSegmentLayout.h"
#include "ClassLibrary/StudyRelated/StudySeries.h"
#include "ClassLibrary/Workflows/StructuredWorkflowSteps.h"
#include "ClassLibrary/Representations/ClassificationIndex.h"
#include "ClassLibrary/Representations/ClassificationSeries.h"
#include "ClassLibrary/Representations/ClassificationFamily.h"
#include "ClassLibrary/Representations/CodeList.h"
#include "ClassLibrary/LogicalDataDescription/DataStoreLibrary.h"
#include "ClassLibrary/LogicalDataDescription/UnitDataRecord.h"
#include "ClassLibrary/LogicalDataDescription/DataStore.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Agents/AgentListing.h"

namespace ClassLibrary
{
namespace CollectionsPattern
{
class StructuredCollection : public SimpleCollection
{
public:
	ClassLibrary::Representations::ClassificationIndex classificationIndex;
	ClassLibrary::Representations::ClassificationSeries classificationSeries;
	ClassLibrary::StudyRelated::StudySeries studySeries;
	ClassLibrary::Representations::ClassificationFamily classificationFamily;
	ClassLibrary::CustomMetadata::CustomStructure customStructure;
	ClassLibrary::Workflows::StructuredWorkflowSteps structuredWorkflowSteps;
	ClassLibrary::Representations::CodeList codeList;
	RelationStructure relationStructure[];
	ClassLibrary::Conceptual::VariableCollection variableCollection;
	ClassLibrary::FormatDescription::PhysicalDataSet physicalDataSet;
	ClassLibrary::FormatDescription::PhysicalRecordSegment physicalRecordSegment;
	ClassLibrary::Conceptual::ConceptSystem conceptSystem;
	ClassLibrary::LogicalDataDescription::DataStoreLibrary dataStoreLibrary;
	ClassLibrary::LogicalDataDescription::UnitDataRecord unitDataRecord;
	ClassLibrary::CustomMetadata::ControlledVocabulary controlledVocabulary;
	ClassLibrary::Agents::AgentListing agentListing;
	ClassLibrary::LogicalDataDescription::DataStore dataStore;
	ClassLibrary::FormatDescription::PhysicalSegmentLayout physicalSegmentLayout;
};

}  // namespace CollectionsPattern
}  // namespace ClassLibrary
#endif
