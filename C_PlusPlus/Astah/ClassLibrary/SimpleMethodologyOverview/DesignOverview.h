#ifndef CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_DESIGN_OVERVIEW_H
#define CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_DESIGN_OVERVIEW_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/MethodologyOverview.h"
#include "ClassLibrary/MethodologyPattern/Design.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Workflows/WorkflowProcess.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Methodologies/Precondition.h"
#include "ClassLibrary/Methodologies/Goal.h"

namespace ClassLibrary
{
namespace SimpleMethodologyOverview
{
class DesignOverview : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry subectOfDesign;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::Workflows::WorkflowProcess workflowProcess[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassLibrary::Methodologies::Precondition precondition[];
	AlgorithmOverview algorithmOverview[];
	ClassLibrary::StudyRelated::Study study;
	ClassLibrary::Methodologies::Goal goal[];
	ClassLibrary::MethodologyPattern::Design design[];
	MethodologyOverview methodologyOverview[];
};

}  // namespace SimpleMethodologyOverview
}  // namespace ClassLibrary
#endif
