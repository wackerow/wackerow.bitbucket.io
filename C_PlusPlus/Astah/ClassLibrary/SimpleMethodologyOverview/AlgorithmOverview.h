#ifndef CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_ALGORITHM_OVERVIEW_H
#define CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_ALGORITHM_OVERVIEW_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/ProcessOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/MethodologyOverview.h"
#include "ClassLibrary/MethodologyPattern/Algorithm.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Workflows/WorkflowProcess.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace SimpleMethodologyOverview
{
class AlgorithmOverview : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry subjectOfAlgorithm;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::MethodologyPattern::Algorithm algorithm[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassLibrary::Workflows::WorkflowProcess workflowProcess[];
	DesignOverview designOverview[];
	ProcessOverview processOverview[];
	MethodologyOverview methodologyOverview[];
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace SimpleMethodologyOverview
}  // namespace ClassLibrary
#endif
