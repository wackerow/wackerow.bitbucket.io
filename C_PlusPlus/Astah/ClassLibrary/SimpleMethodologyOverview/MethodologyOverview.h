#ifndef CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_METHODOLOGY_OVERVIEW_H
#define CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_METHODOLOGY_OVERVIEW_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/MethodologyPattern/Methodology.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace SimpleMethodologyOverview
{
class MethodologyOverview : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry subjectOfMethodology;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	MethodologyOverview methodologyOverview[];
	MethodologyOverview methodologyOverview[];
	AlgorithmOverview algorithmOverview[];
	ClassLibrary::MethodologyPattern::Methodology methodology[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassLibrary::StudyRelated::Study study[];
	DesignOverview designOverview[];
};

}  // namespace SimpleMethodologyOverview
}  // namespace ClassLibrary
#endif
