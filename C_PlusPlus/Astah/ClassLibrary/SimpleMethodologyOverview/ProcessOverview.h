#ifndef CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_PROCESS_OVERVIEW_H
#define CLASSLIBRARY_SIMPLEMETHODOLOGYOVERVIEW_PROCESS_OVERVIEW_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/ProcessPattern/Process.h"
#include "ClassLibrary/ProcessPattern/ProcessSequence.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace SimpleMethodologyOverview
{
class ProcessOverview : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	AlgorithmOverview algorithmOverview[];
	ClassLibrary::ProcessPattern::Process process[];
	ClassLibrary::ProcessPattern::ProcessSequence processSequence;
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace SimpleMethodologyOverview
}  // namespace ClassLibrary
#endif
