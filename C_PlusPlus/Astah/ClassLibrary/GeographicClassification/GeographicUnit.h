#ifndef CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_H
#define CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/SpatialRelationship.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitIndicator.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitRelation.h"
#include "ClassLibrary/Conceptual/Unit.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/GeographicClassification/GeographicExtent.h"
#include "ClassLibrary/Discovery/SpatialCoverage.h"

namespace ClassLibrary
{
namespace GeographicClassification
{
class GeographicUnit : public ClassLibrary::Conceptual::Unit
{
public:
	ClassLibrary::ComplexDataTypes::DateRange geographicTime;

	ClassLibrary::ComplexDataTypes::SpatialRelationship supercedes[];

	ClassLibrary::ComplexDataTypes::SpatialRelationship precedes[];

	ClassLibrary::ComplexDataTypes::SpatialRelationship spatialRelationship;
	ClassLibrary::Discovery::SpatialCoverage spatialCoverage[];
	ClassLibrary::ComplexDataTypes::GeographicUnitIndicator geographicUnitIndicator;
	ClassLibrary::ComplexDataTypes::GeographicUnitRelation geographicUnitRelation[];
	ClassLibrary::ComplexDataTypes::GeographicUnitRelation geographicUnitRelation;
	ClassLibrary::Conceptual::Population population[];
	GeographicExtent geographicExtent[];
};

}  // namespace GeographicClassification
}  // namespace ClassLibrary
#endif
