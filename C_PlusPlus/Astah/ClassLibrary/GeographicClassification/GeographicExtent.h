#ifndef CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_EXTENT_H
#define CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_EXTENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Polygon.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/AreaCoverage.h"
#include "ClassLibrary/ComplexDataTypes/SpatialPoint.h"
#include "ClassLibrary/ComplexDataTypes/SpatialLine.h"
#include "ClassLibrary/GeographicClassification/GeographicUnit.h"
#include "ClassLibrary/Discovery/BoundingBox.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace GeographicClassification
{
class GeographicExtent : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::Polygon boundingPolygon[];

	ClassLibrary::ComplexDataTypes::Polygon excludingPolygon[];

	ClassLibrary::ComplexDataTypes::DateRange geographicTime;

	ClassLibrary::ComplexDataTypes::AreaCoverage hasAreaCoverage[];

	ClassLibrary::ComplexDataTypes::SpatialPoint hasCentroid;

	ClassLibrary::ComplexDataTypes::SpatialPoint locationPoint;

	ClassLibrary::ComplexDataTypes::SpatialLine isSpatialLine;

	ClassLibrary::Discovery::BoundingBox boundingBox[];
	GeographicUnit geographicUnit[];
};

}  // namespace GeographicClassification
}  // namespace ClassLibrary
#endif
