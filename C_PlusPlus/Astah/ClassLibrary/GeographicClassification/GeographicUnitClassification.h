#ifndef CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_CLASSIFICATION_H
#define CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_CLASSIFICATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/GeographicUnitIndicator.h"
#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Conceptual/ConceptSystem.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitRelationStructure.h"
#include "ClassLibrary/Representations/CodeList.h"
#include "ClassLibrary/Discovery/SpatialCoverage.h"
#include "ClassLibrary/Agents/Organization.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace GeographicClassification
{
class GeographicUnitClassification : public ClassLibrary::Representations::CodeList
{
public:
	ClassLibrary::ComplexDataTypes::GeographicUnitIndicator contains[];

	ClassLibrary::ComplexDataTypes::Date releaseDate;

	ClassLibrary::ComplexDataTypes::DateRange validDates;

	Primitive Package::Boolean isCurrent;

	Primitive Package::Boolean isFloating;

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	GeographicUnitClassification geographicUnitClassification[];
	GeographicUnitClassification geographicUnitClassification[];
	GeographicUnitClassification geographicUnitClassification[];
	GeographicUnitClassification geographicUnitClassification[];
	GeographicUnitRelationStructure geographicUnitRelationStructure[];
	ClassLibrary::Conceptual::ConceptSystem conceptSystem[];
	GeographicUnitClassification geographicUnitClassification[];
	GeographicUnitClassification geographicUnitClassification;
	ClassLibrary::Agents::Organization organization[];
	ClassLibrary::Discovery::SpatialCoverage spatialCoverage[];
};

}  // namespace GeographicClassification
}  // namespace ClassLibrary
#endif
