#ifndef CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_RELATION_STRUCTURE_H
#define CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitRelation.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitClassification.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace GeographicClassification
{
class GeographicUnitRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::GeographicUnitRelation hasMemberRelation[];

	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
	GeographicUnitClassification geographicUnitClassification;
};

}  // namespace GeographicClassification
}  // namespace ClassLibrary
#endif
