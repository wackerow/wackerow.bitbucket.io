#ifndef CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_TYPE_RELATION_STRUCTURE_H
#define CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_TYPE_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitTypeRelation.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitTypeClassification.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace GeographicClassification
{
class GeographicUnitTypeRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::GeographicUnitTypeRelation hasMemberRelation[];

	GeographicUnitTypeClassification geographicUnitTypeClassification;
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace GeographicClassification
}  // namespace ClassLibrary
#endif
