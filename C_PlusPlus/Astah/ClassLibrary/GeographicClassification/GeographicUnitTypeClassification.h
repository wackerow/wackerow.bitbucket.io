#ifndef CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_TYPE_CLASSIFICATION_H
#define CLASSLIBRARY_GEOGRAPHICCLASSIFICATION_GEOGRAPHIC_UNIT_TYPE_CLASSIFICATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/GeographicUnitTypeIndicator.h"
#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Conceptual/ConceptSystem.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitTypeRelationStructure.h"
#include "ClassLibrary/Representations/CodeList.h"
#include "ClassLibrary/Discovery/SpatialCoverage.h"
#include "ClassLibrary/Agents/Organization.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace GeographicClassification
{
class GeographicUnitTypeClassification : public ClassLibrary::Representations::CodeList
{
public:
	ClassLibrary::ComplexDataTypes::GeographicUnitTypeIndicator contains[];

	ClassLibrary::ComplexDataTypes::Date releaseDate;

	ClassLibrary::ComplexDataTypes::DateRange validDates;

	Primitive Package::Boolean isCurrent;

	Primitive Package::Boolean isFloating;

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::Discovery::SpatialCoverage spatialCoverage[];
	ClassLibrary::Conceptual::ConceptSystem conceptSystem[];
	GeographicUnitTypeRelationStructure geographicUnitTypeRelationStructure[];
	GeographicUnitTypeClassification geographicUnitTypeClassification[];
	GeographicUnitTypeClassification geographicUnitTypeClassification[];
	ClassLibrary::Agents::Organization organization[];
	GeographicUnitTypeClassification geographicUnitTypeClassification[];
	GeographicUnitTypeClassification geographicUnitTypeClassification;
	GeographicUnitTypeClassification geographicUnitTypeClassification[];
	GeographicUnitTypeClassification geographicUnitTypeClassification[];
};

}  // namespace GeographicClassification
}  // namespace ClassLibrary
#endif
