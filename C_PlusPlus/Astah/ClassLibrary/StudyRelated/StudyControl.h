#ifndef CLASSLIBRARY_STUDYRELATED_STUDY_CONTROL_H
#define CLASSLIBRARY_STUDYRELATED_STUDY_CONTROL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Workflows/Act.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class StudyControl : public ClassLibrary::Workflows::Act
{
public:
	Study study[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
