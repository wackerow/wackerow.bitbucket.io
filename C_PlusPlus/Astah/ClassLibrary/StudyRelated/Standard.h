#ifndef CLASSLIBRARY_STUDYRELATED_STANDARD_H
#define CLASSLIBRARY_STUDYRELATED_STANDARD_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/StudyRelated/ComplianceStatement.h"
#include "ClassLibrary/StudyRelated/QualityStatement.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class Standard : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ComplianceStatement complianceStatement;
	QualityStatement qualityStatement;
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
