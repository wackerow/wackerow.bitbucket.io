#ifndef CLASSLIBRARY_STUDYRELATED_COMPLIANCE_STATEMENT_H
#define CLASSLIBRARY_STUDYRELATED_COMPLIANCE_STATEMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/StudyRelated/Standard.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class ComplianceStatement : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry externalComplianceCode;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::Conceptual::Concept concept;
	Standard standard[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
