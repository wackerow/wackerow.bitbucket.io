#ifndef CLASSLIBRARY_STUDYRELATED_QUALITY_STATEMENT_H
#define CLASSLIBRARY_STUDYRELATED_QUALITY_STATEMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/StudyRelated/Standard.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class QualityStatement : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	Study study;
	Standard standard[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
