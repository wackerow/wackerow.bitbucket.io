#ifndef CLASSLIBRARY_STUDYRELATED_STUDY_H
#define CLASSLIBRARY_STUDYRELATED_STUDY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/StudyIndicator.h"
#include "ClassLibrary/ComplexDataTypes/StudyRelation.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Conceptual/Universe.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/VariableCollection.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/MethodologyOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/ProcessOverview.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcedure.h"
#include "ClassLibrary/StudyRelated/StudyControl.h"
#include "ClassLibrary/StudyRelated/QualityStatement.h"
#include "ClassLibrary/StudyRelated/StudySeries.h"
#include "ClassLibrary/StudyRelated/ExPostEvaluation.h"
#include "ClassLibrary/StudyRelated/Embargo.h"
#include "ClassLibrary/StudyRelated/Budget.h"
#include "ClassLibrary/BusinessWorkflow/DataPipeline.h"
#include "ClassLibrary/Utility/FundingInformation.h"
#include "ClassLibrary/Representations/AuthorizationSource.h"
#include "ClassLibrary/LogicalDataDescription/DataStore.h"
#include "ClassLibrary/DataCapture/ImplementedInstrument.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Discovery/Access.h"
#include "ClassLibrary/Discovery/Coverage.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class Study : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry kindOfData[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString bibliographicCitation;

	StudyControl studyControl[];
	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::Conceptual::Population population[];
	ClassLibrary::Utility::FundingInformation fundingInformation[];
	ClassLibrary::SimpleMethodologyOverview::DesignOverview designOverview[];
	ClassLibrary::Representations::AuthorizationSource authorizationSource[];
	QualityStatement qualityStatement[];
	ClassLibrary::LogicalDataDescription::DataStore dataStore[];
	StudySeries studySeries[];
	ClassLibrary::Discovery::Access access[];
	ClassLibrary::DataCapture::ImplementedInstrument implementedInstrument[];
	ClassLibrary::SamplingMethodology::SamplingProcedure samplingProcedure[];
	ExPostEvaluation exPostEvaluation;
	ClassLibrary::Conceptual::Universe universe[];
	Embargo embargo[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::SimpleMethodologyOverview::MethodologyOverview methodologyOverview[];
	Budget budget[];
	ClassLibrary::ComplexDataTypes::StudyIndicator studyIndicator;
	ClassLibrary::BusinessWorkflow::DataPipeline dataPipeline;
	ClassLibrary::ComplexDataTypes::StudyRelation studyRelation[];
	ClassLibrary::ComplexDataTypes::StudyRelation studyRelation;
	ClassLibrary::Conceptual::VariableCollection variableCollection[];
	ClassLibrary::SimpleMethodologyOverview::AlgorithmOverview algorithmOverview[];
	ClassLibrary::SimpleMethodologyOverview::ProcessOverview processOverview[];
	ClassLibrary::Discovery::Coverage coverage[];
	ClassLibrary::Conceptual::UnitType unitType[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
