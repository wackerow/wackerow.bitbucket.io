#ifndef CLASSLIBRARY_STUDYRELATED_BUDGET_H
#define CLASSLIBRARY_STUDYRELATED_BUDGET_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class Budget : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	Study study[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
