#ifndef CLASSLIBRARY_STUDYRELATED_EMBARGO_H
#define CLASSLIBRARY_STUDYRELATED_EMBARGO_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class Embargo : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::DateRange embargoDates;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;

	ClassLibrary::Agents::Agent agent[];
	Study study;
	ClassLibrary::Agents::Agent agent[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
