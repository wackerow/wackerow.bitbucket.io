#ifndef CLASSLIBRARY_STUDYRELATED_STUDY_RELATION_STRUCTURE_H
#define CLASSLIBRARY_STUDYRELATED_STUDY_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/StudyRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/StudyRelated/StudySeries.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class StudyRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::StudyRelation hasMemberRelation[];

	StudySeries studySeries[];
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
