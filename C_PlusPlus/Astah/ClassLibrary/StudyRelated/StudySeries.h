#ifndef CLASSLIBRARY_STUDYRELATED_STUDY_SERIES_H
#define CLASSLIBRARY_STUDYRELATED_STUDY_SERIES_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/StudyIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/StudyRelated/StudyRelationStructure.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/LogicalDataDescription/DataStoreLibrary.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class StudySeries : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::StudyIndicator contains[];

	Primitive Package::Boolean isOrdered;

	StudyRelationStructure studyRelationStructure[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	Study study[];
	ClassLibrary::LogicalDataDescription::DataStoreLibrary dataStoreLibrary;
	ClassLibrary::Conceptual::Concept concept[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
