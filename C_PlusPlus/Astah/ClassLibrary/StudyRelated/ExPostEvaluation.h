#ifndef CLASSLIBRARY_STUDYRELATED_EX_POST_EVALUATION_H
#define CLASSLIBRARY_STUDYRELATED_EX_POST_EVALUATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace StudyRelated
{
class ExPostEvaluation : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfEvaluation[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString evaluationProcess[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString outcomes[];

	ClassLibrary::ComplexDataTypes::Date completionDate;

	Study study[];
	ClassLibrary::Agents::Agent agent[];
};

}  // namespace StudyRelated
}  // namespace ClassLibrary
#endif
