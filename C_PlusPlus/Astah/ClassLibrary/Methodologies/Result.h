#ifndef CLASSLIBRARY_METHODOLOGIES_RESULT_H
#define CLASSLIBRARY_METHODOLOGIES_RESULT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Binding.h"
#include "ClassLibrary/Workflows/Parameter.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Methodologies/Precondition.h"
#include "ClassLibrary/Methodologies/AppliedUse.h"
#include "ClassLibrary/Methodologies/Goal.h"

namespace ClassLibrary
{
namespace Methodologies
{
class Result : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::ComplexDataTypes::Binding hasBinding[];

	ClassLibrary::Workflows::Parameter parameter[];
	Precondition precondition[];
	AppliedUse appliedUse[];
	Goal goal[];
	ClassLibrary::Workflows::Parameter parameter[];
};

}  // namespace Methodologies
}  // namespace ClassLibrary
#endif
