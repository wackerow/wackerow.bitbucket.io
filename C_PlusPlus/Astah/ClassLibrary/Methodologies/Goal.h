#ifndef CLASSLIBRARY_METHODOLOGIES_GOAL_H
#define CLASSLIBRARY_METHODOLOGIES_GOAL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/MethodologyPattern/Design.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Methodologies/BusinessFunction.h"
#include "ClassLibrary/Methodologies/Result.h"

namespace ClassLibrary
{
namespace Methodologies
{
class Goal : public BusinessFunction
{
public:
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassLibrary::SimpleMethodologyOverview::DesignOverview designOverview[];
	Result result[];
	ClassLibrary::MethodologyPattern::Design design[];
};

}  // namespace Methodologies
}  // namespace ClassLibrary
#endif
