#ifndef CLASSLIBRARY_METHODOLOGIES_APPLIED_USE_H
#define CLASSLIBRARY_METHODOLOGIES_APPLIED_USE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Methodologies/Guide.h"
#include "ClassLibrary/Methodologies/Result.h"

namespace ClassLibrary
{
namespace Methodologies
{
class AppliedUse : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::Identification::AnnotatedIdentifiable annotatedIdentifiable[];
	Guide guide[];
	ClassLibrary::Conceptual::UnitType unitType[];
	Result result[];
};

}  // namespace Methodologies
}  // namespace ClassLibrary
#endif
