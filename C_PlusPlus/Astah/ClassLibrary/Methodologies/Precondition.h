#ifndef CLASSLIBRARY_METHODOLOGIES_PRECONDITION_H
#define CLASSLIBRARY_METHODOLOGIES_PRECONDITION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/MethodologyPattern/Design.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Methodologies/BusinessFunction.h"
#include "ClassLibrary/Methodologies/Result.h"

namespace ClassLibrary
{
namespace Methodologies
{
class Precondition : public BusinessFunction
{
public:
	ClassLibrary::SimpleMethodologyOverview::DesignOverview designOverview[];
	ClassLibrary::MethodologyPattern::Design design[];
	Result result[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
};

}  // namespace Methodologies
}  // namespace ClassLibrary
#endif
