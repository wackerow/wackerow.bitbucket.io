#ifndef CLASSLIBRARY_METHODOLOGIES_BUSINESS_FUNCTION_H
#define CLASSLIBRARY_METHODOLOGIES_BUSINESS_FUNCTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Methodologies
{
class BusinessFunction : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

};

}  // namespace Methodologies
}  // namespace ClassLibrary
#endif
