#ifndef CLASSLIBRARY_METHODOLOGIES_GUIDE_H
#define CLASSLIBRARY_METHODOLOGIES_GUIDE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Methodologies/AppliedUse.h"

namespace ClassLibrary
{
namespace Methodologies
{
class Guide : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	AppliedUse appliedUse[];
};

}  // namespace Methodologies
}  // namespace ClassLibrary
#endif
