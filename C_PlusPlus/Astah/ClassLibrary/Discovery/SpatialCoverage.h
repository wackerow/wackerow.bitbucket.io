#ifndef CLASSLIBRARY_DISCOVERY_SPATIAL_COVERAGE_H
#define CLASSLIBRARY_DISCOVERY_SPATIAL_COVERAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/GeographicClassification/GeographicUnit.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitTypeClassification.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitClassification.h"
#include "ClassLibrary/EnumerationsRegExp/SpatialObjectType.h"
#include "ClassLibrary/Discovery/BoundingBox.h"
#include "ClassLibrary/Discovery/Coverage.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Discovery
{
class SpatialCoverage : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString description;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry spatialAreaCode[];

	ClassLibrary::EnumerationsRegExp::SpatialObjectType spatialObject;

	BoundingBox boundingBox[];
	ClassLibrary::GeographicClassification::GeographicUnit geographicUnit[];
	ClassLibrary::Conceptual::UnitType unitType[];
	ClassLibrary::Conceptual::UnitType unitType[];
	ClassLibrary::Conceptual::UnitType unitType[];
	Coverage coverage[];
	ClassLibrary::GeographicClassification::GeographicUnitTypeClassification geographicUnitTypeClassification[];
	ClassLibrary::GeographicClassification::GeographicUnitClassification geographicUnitClassification[];
};

}  // namespace Discovery
}  // namespace ClassLibrary
#endif
