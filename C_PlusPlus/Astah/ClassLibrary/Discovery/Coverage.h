#ifndef CLASSLIBRARY_DISCOVERY_COVERAGE_H
#define CLASSLIBRARY_DISCOVERY_COVERAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Utility/DocumentInformation.h"
#include "ClassLibrary/Discovery/TemporalCoverage.h"
#include "ClassLibrary/Discovery/SpatialCoverage.h"
#include "ClassLibrary/Discovery/TopicalCoverage.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Discovery
{
class Coverage : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	TemporalCoverage temporalCoverage;
	SpatialCoverage spatialCoverage;
	ClassLibrary::Utility::DocumentInformation documentInformation;
	TopicalCoverage topicalCoverage;
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace Discovery
}  // namespace ClassLibrary
#endif
