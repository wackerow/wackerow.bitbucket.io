#ifndef CLASSLIBRARY_DISCOVERY_TEMPORAL_COVERAGE_H
#define CLASSLIBRARY_DISCOVERY_TEMPORAL_COVERAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ReferenceDate.h"
#include "ClassLibrary/Discovery/Coverage.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Discovery
{
class TemporalCoverage : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ReferenceDate coverageDate[];

	Coverage coverage[];
};

}  // namespace Discovery
}  // namespace ClassLibrary
#endif
