#ifndef CLASSLIBRARY_DISCOVERY_BOUNDING_BOX_H
#define CLASSLIBRARY_DISCOVERY_BOUNDING_BOX_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/GeographicClassification/GeographicExtent.h"
#include "ClassLibrary/Discovery/SpatialCoverage.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace Discovery
{
class BoundingBox : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	Primitive Package::Real eastLongitude;

	Primitive Package::Real westLongitude;

	Primitive Package::Real northLatitude;

	Primitive Package::Real southLatitude;

	SpatialCoverage spatialCoverage[];
	ClassLibrary::GeographicClassification::GeographicExtent geographicExtent;
};

}  // namespace Discovery
}  // namespace ClassLibrary
#endif
