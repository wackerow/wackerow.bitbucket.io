#ifndef CLASSLIBRARY_DISCOVERY_TOPICAL_COVERAGE_H
#define CLASSLIBRARY_DISCOVERY_TOPICAL_COVERAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Discovery/Coverage.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Discovery
{
class TopicalCoverage : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry subject[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry keyword[];

	Coverage coverage[];
};

}  // namespace Discovery
}  // namespace ClassLibrary
#endif
