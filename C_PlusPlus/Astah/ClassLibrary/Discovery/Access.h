#ifndef CLASSLIBRARY_DISCOVERY_ACCESS_H
#define CLASSLIBRARY_DISCOVERY_ACCESS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Form.h"
#include "ClassLibrary/ComplexDataTypes/AgentAssociation.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/SamplingMethodology/SampleFrame.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Utility/DocumentInformation.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Discovery
{
class Access : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString confidentialityStatement;

	ClassLibrary::ComplexDataTypes::Form accessPermission[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString restrictions;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString citationRequirement;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString depositRequirement;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString accessConditions;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString disclaimer;

	ClassLibrary::ComplexDataTypes::AgentAssociation contactAgent[];

	ClassLibrary::ComplexDataTypes::DateRange validDates;

	ClassLibrary::Utility::DocumentInformation documentInformation[];
	ClassLibrary::StudyRelated::Study study;
	ClassLibrary::SamplingMethodology::SampleFrame sampleFrame;
	ClassLibrary::Utility::DocumentInformation documentInformation[];
};

}  // namespace Discovery
}  // namespace ClassLibrary
#endif
