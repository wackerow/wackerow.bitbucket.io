#ifndef CLASSLIBRARY_SIMPLECODEBOOK_STANDARD_WEIGHT_H
#define CLASSLIBRARY_SIMPLECODEBOOK_STANDARD_WEIGHT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleCodebook/VariableStatistics.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace SimpleCodebook
{
class StandardWeight : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	Primitive Package::Real standardWeightValue;

	VariableStatistics variableStatistics;
};

}  // namespace SimpleCodebook
}  // namespace ClassLibrary
#endif
