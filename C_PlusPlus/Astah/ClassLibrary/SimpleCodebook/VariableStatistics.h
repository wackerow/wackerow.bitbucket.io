#ifndef CLASSLIBRARY_SIMPLECODEBOOK_VARIABLE_STATISTICS_H
#define CLASSLIBRARY_SIMPLECODEBOOK_VARIABLE_STATISTICS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/SummaryStatistic.h"
#include "ClassLibrary/ComplexDataTypes/CategoryStatistic.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/SimpleCodebook/StandardWeight.h"
#include "ClassLibrary/FormatDescription/PhysicalDataSet.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace SimpleCodebook
{
class VariableStatistics : public ClassLibrary::Identification::Identifiable
{
public:
	Primitive Package::Integer totalResponses;

	ClassLibrary::ComplexDataTypes::SummaryStatistic hasSummaryStatistic[];

	ClassLibrary::ComplexDataTypes::CategoryStatistic hasCategoryStatistic[];

	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
	ClassLibrary::FormatDescription::PhysicalDataSet physicalDataSet[];
	StandardWeight standardWeight[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
};

}  // namespace SimpleCodebook
}  // namespace ClassLibrary
#endif
