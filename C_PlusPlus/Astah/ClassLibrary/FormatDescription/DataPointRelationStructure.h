#ifndef CLASSLIBRARY_FORMATDESCRIPTION_DATA_POINT_RELATION_STRUCTURE_H
#define CLASSLIBRARY_FORMATDESCRIPTION_DATA_POINT_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DataPointRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class DataPointRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::DataPointRelation hasMemberRelation[];

	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
	PhysicalRecordSegment physicalRecordSegment[];
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
