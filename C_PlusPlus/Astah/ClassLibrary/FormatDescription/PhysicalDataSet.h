#ifndef CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_DATA_SET_H
#define CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_DATA_SET_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/PhysicalRecordSegmentIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/SimpleCodebook/VariableStatistics.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/FormatDescription/PhysicalOrderRelationStructure.h"
#include "ClassLibrary/LogicalDataDescription/DataStore.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Integer.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class PhysicalDataSet : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	Primitive Package::String physicalFileName;

	Primitive Package::Integer numberOfSegments;

	ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::LogicalDataDescription::DataStore dataStore[];
	ClassLibrary::SimpleCodebook::VariableStatistics variableStatistics[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	PhysicalOrderRelationStructure physicalOrderRelationStructure[];
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
