#ifndef CLASSLIBRARY_FORMATDESCRIPTION_SEGMENT_BY_TEXT_H
#define CLASSLIBRARY_FORMATDESCRIPTION_SEGMENT_BY_TEXT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/FormatDescription/PhysicalSegmentLocation.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class SegmentByText : public PhysicalSegmentLocation
{
public:
	Primitive Package::Integer startLine;

	Primitive Package::Integer endLine;

	Primitive Package::Integer startCharacterPosition;

	Primitive Package::Integer endCharacterPosition;

	Primitive Package::Integer characterLength;

};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
