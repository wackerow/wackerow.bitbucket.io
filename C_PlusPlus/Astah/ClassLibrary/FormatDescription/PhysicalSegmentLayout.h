#ifndef CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_SEGMENT_LAYOUT_H
#define CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_SEGMENT_LAYOUT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/ValueMappingIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/TrimValues.h"
#include "ClassLibrary/EnumerationsRegExp/TableDirectionValues.h"
#include "ClassLibrary/EnumerationsRegExp/TextDirectionValues.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/FormatDescription/PhysicalLayoutRelationStructure.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class PhysicalSegmentLayout : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	Primitive Package::Boolean isDelimited;

	Primitive Package::String delimiter;

	Primitive Package::Boolean isFixedWidth;

	Primitive Package::String escapeCharacter;

	Primitive Package::String lineTerminator[];

	Primitive Package::String quoteCharacter;

	Primitive Package::String commentPrefix;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry encoding;

	Primitive Package::Boolean hasHeader;

	Primitive Package::Integer headerRowCount;

	Primitive Package::Boolean skipBlankRows;

	Primitive Package::Integer skipDataColumns;

	Primitive Package::Boolean skipInitialSpace;

	Primitive Package::Integer skipRows;

	ClassLibrary::EnumerationsRegExp::TrimValues trim;

	Primitive Package::String nullSequence;

	Primitive Package::Boolean headerIsCaseSensitive;

	Primitive Package::Integer arrayBase;

	Primitive Package::Boolean treatConsecutiveDelimitersAsOne;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::EnumerationsRegExp::TableDirectionValues tableDirection;

	ClassLibrary::EnumerationsRegExp::TextDirectionValues textDirection;

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::ValueMappingIndicator contains[];

	Primitive Package::Boolean isOrdered;

	PhysicalRecordSegment physicalRecordSegment;
	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord[];
	PhysicalLayoutRelationStructure physicalLayoutRelationStructure[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
