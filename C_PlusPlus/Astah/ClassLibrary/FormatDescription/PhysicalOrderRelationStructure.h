#ifndef CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_ORDER_RELATION_STRUCTURE_H
#define CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_ORDER_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/PhysicalRecordSegmentRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/FormatDescription/PhysicalDataSet.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class PhysicalOrderRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentRelation hasMemberRelation[];

	PhysicalDataSet physicalDataSet[];
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
