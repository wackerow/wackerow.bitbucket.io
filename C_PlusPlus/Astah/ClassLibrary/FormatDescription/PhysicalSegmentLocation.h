#ifndef CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_SEGMENT_LOCATION_H
#define CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_SEGMENT_LOCATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/FormatDescription/ValueMapping.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class PhysicalSegmentLocation : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ValueMapping valueMapping;
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
