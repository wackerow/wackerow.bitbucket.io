#ifndef CLASSLIBRARY_FORMATDESCRIPTION_UNIT_SEGMENT_LAYOUT_H
#define CLASSLIBRARY_FORMATDESCRIPTION_UNIT_SEGMENT_LAYOUT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/FormatDescription/PhysicalSegmentLayout.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class UnitSegmentLayout : public PhysicalSegmentLayout
{
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
