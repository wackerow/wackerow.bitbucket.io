#ifndef CLASSLIBRARY_FORMATDESCRIPTION_VALUE_MAPPING_H
#define CLASSLIBRARY_FORMATDESCRIPTION_VALUE_MAPPING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/ComplexDataTypes/ValueMappingRelation.h"
#include "ClassLibrary/ComplexDataTypes/ValueMappingIndicator.h"
#include "ClassLibrary/EnumerationsRegExp/OneCharString.h"
#include "ClassLibrary/FormatDescription/PhysicalSegmentLocation.h"
#include "ClassLibrary/LogicalDataDescription/DataPoint.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Integer.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class ValueMapping : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry physicalDataType;

	ClassLibrary::EnumerationsRegExp::OneCharString defaultDecimalSeparator;

	ClassLibrary::EnumerationsRegExp::OneCharString defaultDigitGroupSeparator;

	Primitive Package::String numberPattern;

	ClassLibrary::ComplexDataTypes::ValueString defaultValue;

	Primitive Package::String nullSequence;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry format;

	Primitive Package::Integer length;

	Primitive Package::Integer minimumLength;

	Primitive Package::Integer maximumLength;

	Primitive Package::Integer scale;

	Primitive Package::Integer decimalPositions;

	Primitive Package::Boolean isRequired;

	PhysicalSegmentLocation physicalSegmentLocation[];
	ClassLibrary::ComplexDataTypes::ValueMappingRelation valueMappingRelation;
	ClassLibrary::ComplexDataTypes::ValueMappingIndicator valueMappingIndicator;
	ClassLibrary::LogicalDataDescription::DataPoint dataPoint[];
	ClassLibrary::ComplexDataTypes::ValueMappingRelation valueMappingRelation;
	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
