#ifndef CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_RECORD_SEGMENT_H
#define CLASSLIBRARY_FORMATDESCRIPTION_PHYSICAL_RECORD_SEGMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DataPointIndicator.h"
#include "ClassLibrary/ComplexDataTypes/PhysicalRecordSegmentRelation.h"
#include "ClassLibrary/ComplexDataTypes/PhysicalRecordSegmentIndicator.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/FormatDescription/PhysicalSegmentLayout.h"
#include "ClassLibrary/FormatDescription/DataPointRelationStructure.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace FormatDescription
{
class PhysicalRecordSegment : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	Primitive Package::String physicalFileName;

	ClassLibrary::ComplexDataTypes::DataPointIndicator contains[];

	Primitive Package::Boolean isOrdered;

	PhysicalSegmentLayout physicalSegmentLayout[];
	ClassLibrary::Conceptual::Population population[];
	ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentRelation physicalRecordSegmentRelation[];
	ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentRelation physicalRecordSegmentRelation;
	ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentIndicator physicalRecordSegmentIndicator;
	ClassLibrary::Conceptual::Concept concept[];
	DataPointRelationStructure dataPointRelationStructure[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord[];
};

}  // namespace FormatDescription
}  // namespace ClassLibrary
#endif
