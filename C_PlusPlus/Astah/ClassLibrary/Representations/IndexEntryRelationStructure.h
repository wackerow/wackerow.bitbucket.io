#ifndef CLASSLIBRARY_REPRESENTATIONS_INDEX_ENTRY_RELATION_STRUCTURE_H
#define CLASSLIBRARY_REPRESENTATIONS_INDEX_ENTRY_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/IndexEntryRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/Representations/ClassificationIndex.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Representations
{
class IndexEntryRelationStructure : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::IndexEntryRelation hasMemberRelation[];

	ClassificationIndex classificationIndex[];
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
