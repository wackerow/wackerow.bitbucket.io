#ifndef CLASSLIBRARY_REPRESENTATIONS_CORRESPONDENCE_TABLE_H
#define CLASSLIBRARY_REPRESENTATIONS_CORRESPONDENCE_TABLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/Map.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Representations/LevelStructure.h"
#include "ClassLibrary/Representations/StatisticalClassification.h"
#include "ClassLibrary/CollectionsPattern/Comparison.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace Representations
{
class CorrespondenceTable : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::DateRange effectiveDates;

	ClassLibrary::ComplexDataTypes::Map correspondence[];

	LevelStructure levelStructure[];
	LevelStructure levelStructure[];
	StatisticalClassification statisticalClassification[];
	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassLibrary::CollectionsPattern::Comparison comparison[];
	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::Agents::Agent agent[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
