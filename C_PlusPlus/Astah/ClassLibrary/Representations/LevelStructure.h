#ifndef CLASSLIBRARY_REPRESENTATIONS_LEVEL_STRUCTURE_H
#define CLASSLIBRARY_REPRESENTATIONS_LEVEL_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Level.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/Representations/CorrespondenceTable.h"
#include "ClassLibrary/Representations/CodeList.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Representations
{
class LevelStructure : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::Level containsLevel[];

	ClassLibrary::ComplexDataTypes::DateRange validDateRange;

	CorrespondenceTable correspondenceTable;
	CorrespondenceTable correspondenceTable;
	CodeList codeList;
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
