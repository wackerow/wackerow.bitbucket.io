#ifndef CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_INDEX_H
#define CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_INDEX_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationIndexEntryIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Representations/IndexEntryRelationStructure.h"
#include "ClassLibrary/Representations/StatisticalClassification.h"
#include "ClassLibrary/Representations/ClassificationFamily.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"
#include "ClassLibrary/XMLSchemaDatatypes/LanguageSpecification.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Representations
{
class ClassificationIndex : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::Date releaseDate;

	ClassLibrary::XMLSchemaDatatypes::LanguageSpecification availableLanguage;

	ClassLibrary::ComplexDataTypes::InternationalString corrections[];

	ClassLibrary::ComplexDataTypes::CommandCode codingInstruction[];

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::ClassificationIndexEntryIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::Agents::Agent agent;
	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	IndexEntryRelationStructure indexEntryRelationStructure[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	StatisticalClassification statisticalClassification[];
	ClassificationFamily classificationFamily[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
