#ifndef CLASSLIBRARY_REPRESENTATIONS_VALUE_DOMAIN_H
#define CLASSLIBRARY_REPRESENTATIONS_VALUE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/CustomMetadata/CustomItem.h"
#include "ClassLibrary/Workflows/Parameter.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Representations
{
class ValueDomain : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry recommendedDataType[];

	ClassLibrary::Workflows::Parameter parameter;
	ClassLibrary::CustomMetadata::CustomItem customItem;
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
