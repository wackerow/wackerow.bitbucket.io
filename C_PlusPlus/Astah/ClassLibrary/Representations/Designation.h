#ifndef CLASSLIBRARY_REPRESENTATIONS_DESIGNATION_H
#define CLASSLIBRARY_REPRESENTATIONS_DESIGNATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/SignificationPattern/Signifier.h"
#include "ClassLibrary/SignificationPattern/Sign.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace Representations
{
class Designation : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::SignificationPattern::Signifier representation;

	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::SignificationPattern::Sign sign[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
