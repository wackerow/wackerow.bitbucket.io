#ifndef CLASSLIBRARY_REPRESENTATIONS_SENTINEL_VALUE_DOMAIN_H
#define CLASSLIBRARY_REPRESENTATIONS_SENTINEL_VALUE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/SentinelConceptualDomain.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/Representations/ValueDomain.h"
#include "ClassLibrary/Representations/ValueAndConceptDescription.h"
#include "ClassLibrary/Representations/CodeList.h"

namespace ClassLibrary
{
namespace Representations
{
class SentinelValueDomain : public ValueDomain
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry platformType;

	ClassLibrary::Conceptual::SentinelConceptualDomain sentinelConceptualDomain[];
	ValueAndConceptDescription valueAndConceptDescription[];
	CodeList codeList[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable;
	ClassLibrary::Conceptual::RepresentedVariable representedVariable[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
