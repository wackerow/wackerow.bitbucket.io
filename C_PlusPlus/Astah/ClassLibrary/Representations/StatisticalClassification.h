#ifndef CLASSLIBRARY_REPRESENTATIONS_STATISTICAL_CLASSIFICATION_H
#define CLASSLIBRARY_REPRESENTATIONS_STATISTICAL_CLASSIFICATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationItemIndicator.h"
#include "ClassLibrary/ComplexDataTypes/StatisticalClassificationIndicator.h"
#include "ClassLibrary/ComplexDataTypes/StatisticalClassificationRelation.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Representations/CodeList.h"
#include "ClassLibrary/Representations/CorrespondenceTable.h"
#include "ClassLibrary/Representations/ClassificationIndex.h"
#include "ClassLibrary/Agents/Organization.h"
#include "ClassLibrary/XMLSchemaDatatypes/LanguageSpecification.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Representations
{
class StatisticalClassification : public CodeList
{
public:
	ClassLibrary::ComplexDataTypes::Date releaseDate;

	ClassLibrary::ComplexDataTypes::DateRange validDates;

	Primitive Package::Boolean isCurrent;

	Primitive Package::Boolean isFloating;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString changeFromBase;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purposeOfVariant;

	ClassLibrary::ComplexDataTypes::InternationalString copyright[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString updateChanges[];

	ClassLibrary::XMLSchemaDatatypes::LanguageSpecification availableLanguage;

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::ClassificationItemIndicator contains[];

	StatisticalClassification statisticalClassification[];
	StatisticalClassification statisticalClassification[];
	StatisticalClassification statisticalClassification[];
	StatisticalClassification statisticalClassification;
	CorrespondenceTable correspondenceTable[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassificationIndex classificationIndex[];
	ClassLibrary::ComplexDataTypes::StatisticalClassificationIndicator statisticalClassificationIndicator;
	ClassLibrary::ComplexDataTypes::StatisticalClassificationRelation statisticalClassificationRelation[];
	ClassLibrary::ComplexDataTypes::StatisticalClassificationRelation statisticalClassificationRelation[];
	ClassLibrary::Agents::Organization organization[];
	StatisticalClassification statisticalClassification[];
	StatisticalClassification statisticalClassification[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
