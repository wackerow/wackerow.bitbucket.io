#ifndef CLASSLIBRARY_REPRESENTATIONS_CODE_H
#define CLASSLIBRARY_REPRESENTATIONS_CODE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/ComplexDataTypes/CodeIndicator.h"
#include "ClassLibrary/Conceptual/Category.h"
#include "ClassLibrary/Representations/Designation.h"
#include "ClassLibrary/DataCapture/CodeListResponseDomain.h"

namespace ClassLibrary
{
namespace Representations
{
class Code : public Designation
{
public:
	ClassLibrary::ComplexDataTypes::ValueString representation;

	ClassLibrary::DataCapture::CodeListResponseDomain codeListResponseDomain;
	ClassLibrary::Conceptual::Category category[];
	ClassLibrary::ComplexDataTypes::CodeIndicator codeIndicator;
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
