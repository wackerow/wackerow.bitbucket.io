#ifndef CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_INDEX_ENTRY_H
#define CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_INDEX_ENTRY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationIndexEntryIndicator.h"
#include "ClassLibrary/ComplexDataTypes/IndexEntryRelation.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Representations
{
class ClassificationIndexEntry : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalString entry;

	ClassLibrary::ComplexDataTypes::DateRange validDates;

	ClassLibrary::ComplexDataTypes::CommandCode codingInstruction;

	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
	ClassLibrary::ComplexDataTypes::ClassificationIndexEntryIndicator classificationIndexEntryIndicator;
	ClassLibrary::ComplexDataTypes::IndexEntryRelation indexEntryRelation;
	ClassLibrary::ComplexDataTypes::IndexEntryRelation indexEntryRelation;
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
