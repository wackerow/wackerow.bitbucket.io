#ifndef CLASSLIBRARY_REPRESENTATIONS_SUBSTANTIVE_VALUE_DOMAIN_H
#define CLASSLIBRARY_REPRESENTATIONS_SUBSTANTIVE_VALUE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/Conceptual/SubstantiveConceptualDomain.h"
#include "ClassLibrary/Representations/ValueDomain.h"
#include "ClassLibrary/Representations/ValueAndConceptDescription.h"
#include "ClassLibrary/Representations/CodeList.h"

namespace ClassLibrary
{
namespace Representations
{
class SubstantiveValueDomain : public ValueDomain
{
public:
	ClassLibrary::Conceptual::RepresentedVariable representedVariable;
	ValueAndConceptDescription valueAndConceptDescription[];
	CodeList codeList[];
	ClassLibrary::Conceptual::SubstantiveConceptualDomain substantiveConceptualDomain[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
