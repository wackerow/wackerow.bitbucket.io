#ifndef CLASSLIBRARY_REPRESENTATIONS_CODE_LIST_H
#define CLASSLIBRARY_REPRESENTATIONS_CODE_LIST_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/CodeIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/CategorySet.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/Representations/LevelStructure.h"
#include "ClassLibrary/Representations/ClassificationRelationStructure.h"
#include "ClassLibrary/Representations/SentinelValueDomain.h"
#include "ClassLibrary/Representations/SubstantiveValueDomain.h"
#include "ClassLibrary/DataCapture/CodeListResponseDomain.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Representations
{
class CodeList : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::CodeIndicator contains[];

	Primitive Package::Boolean isOrdered;

	LevelStructure levelStructure[];
	ClassificationRelationStructure classificationRelationStructure;
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	SentinelValueDomain sentinelValueDomain;
	ClassLibrary::Conceptual::CategorySet categorySet[];
	SubstantiveValueDomain substantiveValueDomain;
	ClassLibrary::DataCapture::CodeListResponseDomain codeListResponseDomain;
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
