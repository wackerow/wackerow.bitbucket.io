#ifndef CLASSLIBRARY_REPRESENTATIONS_AUTHORIZATION_SOURCE_H
#define CLASSLIBRARY_REPRESENTATIONS_AUTHORIZATION_SOURCE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Representations/ClassificationItem.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace Representations
{
class AuthorizationSource : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString statementOfAuthorization;

	ClassLibrary::ComplexDataTypes::InternationalString legalMandate;

	ClassLibrary::ComplexDataTypes::Date authorizationDate;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::StudyRelated::Study study[];
	ClassLibrary::Agents::Agent agent[];
	ClassificationItem classificationItem[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
