#ifndef CLASSLIBRARY_REPRESENTATIONS_VALUE_AND_CONCEPT_DESCRIPTION_H
#define CLASSLIBRARY_REPRESENTATIONS_VALUE_AND_CONCEPT_DESCRIPTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/TypedString.h"
#include "ClassLibrary/Conceptual/ConceptualDomain.h"
#include "ClassLibrary/EnumerationsRegExp/CategoryRelationCode.h"
#include "ClassLibrary/Representations/SentinelValueDomain.h"
#include "ClassLibrary/Representations/SubstantiveValueDomain.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace Representations
{
class ValueAndConceptDescription : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString description;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry logicalExpression;

	ClassLibrary::ComplexDataTypes::TypedString regularExpression;

	Primitive Package::String minimumValueInclusive;

	Primitive Package::String maximumValueInclusive;

	Primitive Package::String minimumValueExclusive;

	Primitive Package::String maximumValueExclusive;

	ClassLibrary::EnumerationsRegExp::CategoryRelationCode classificationLevel;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry formatPattern;

	ClassLibrary::Conceptual::ConceptualDomain conceptualDomain;
	SentinelValueDomain sentinelValueDomain;
	SubstantiveValueDomain substantiveValueDomain;
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
