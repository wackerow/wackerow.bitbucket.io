#ifndef CLASSLIBRARY_REPRESENTATIONS_STATISTICAL_CLASSIFICATION_RELATION_STRUCTURE_H
#define CLASSLIBRARY_REPRESENTATIONS_STATISTICAL_CLASSIFICATION_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/StatisticalClassificationRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/Representations/ClassificationSeries.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace Representations
{
class StatisticalClassificationRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::StatisticalClassificationRelation hasMemberRelation[];

	ClassificationSeries classificationSeries[];
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
