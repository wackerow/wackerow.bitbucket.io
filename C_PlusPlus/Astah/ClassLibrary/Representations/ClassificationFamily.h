#ifndef CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_FAMILY_H
#define CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_FAMILY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationSeriesIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/Representations/ClassificationSeriesRelationStructure.h"
#include "ClassLibrary/Representations/ClassificationIndex.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Representations
{
class ClassificationFamily : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::ClassificationSeriesIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	ClassificationSeriesRelationStructure classificationSeriesRelationStructure[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassificationIndex classificationIndex[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
