#ifndef CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_SERIES_H
#define CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_SERIES_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/StatisticalClassificationIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationSeriesIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationSeriesRelation.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/Representations/StatisticalClassificationRelationStructure.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Representations
{
class ClassificationSeries : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry context;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry objectsOrUnitsClassified;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry subject[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry keyword[];

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::StatisticalClassificationIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	StatisticalClassificationRelationStructure statisticalClassificationRelationStructure[];
	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::ComplexDataTypes::ClassificationSeriesIndicator classificationSeriesIndicator;
	ClassLibrary::ComplexDataTypes::ClassificationSeriesRelation classificationSeriesRelation;
	ClassLibrary::ComplexDataTypes::ClassificationSeriesRelation classificationSeriesRelation[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
