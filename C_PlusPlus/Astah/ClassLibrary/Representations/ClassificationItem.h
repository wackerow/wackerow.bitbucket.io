#ifndef CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_ITEM_H
#define CLASSLIBRARY_REPRESENTATIONS_CLASSIFICATION_ITEM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/ClassificationItemIndicator.h"
#include "ClassLibrary/Representations/Code.h"
#include "ClassLibrary/Representations/AuthorizationSource.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Representations
{
class ClassificationItem : public Code
{
public:
	Primitive Package::Boolean isValid;

	Primitive Package::Boolean isGenerated;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString explanatoryNotes[];

	ClassLibrary::ComplexDataTypes::InternationalString futureNotes[];

	ClassLibrary::ComplexDataTypes::InternationalString changeLog;

	ClassLibrary::ComplexDataTypes::InternationalString changeFromPreviousVersion;

	ClassLibrary::ComplexDataTypes::DateRange validDates;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	AuthorizationSource authorizationSource[];
	ClassLibrary::ComplexDataTypes::ClassificationItemIndicator classificationItemIndicator;
	ClassificationItem classificationItem[];
	ClassificationItem classificationItem[];
};

}  // namespace Representations
}  // namespace ClassLibrary
#endif
