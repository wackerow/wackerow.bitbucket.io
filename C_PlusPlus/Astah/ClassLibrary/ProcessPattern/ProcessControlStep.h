#ifndef CLASSLIBRARY_PROCESSPATTERN_PROCESS_CONTROL_STEP_H
#define CLASSLIBRARY_PROCESSPATTERN_PROCESS_CONTROL_STEP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ProcessPattern/ProcessStep.h"
#include "ClassLibrary/ProcessPattern/ProcessSequence.h"

namespace ClassLibrary
{
namespace ProcessPattern
{
class ProcessControlStep : public ProcessStep
{
public:
	ProcessSequence processSequence;
};

}  // namespace ProcessPattern
}  // namespace ClassLibrary
#endif
