#ifndef CLASSLIBRARY_PROCESSPATTERN_PROCESS_SEQUENCE_H
#define CLASSLIBRARY_PROCESSPATTERN_PROCESS_SEQUENCE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleMethodologyOverview/ProcessOverview.h"
#include "ClassLibrary/ProcessPattern/ProcessStepIndicator.h"
#include "ClassLibrary/ProcessPattern/Process.h"
#include "ClassLibrary/ProcessPattern/ProcessControlStep.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"

namespace ClassLibrary
{
namespace ProcessPattern
{
class ProcessSequence : public ClassLibrary::CollectionsPattern::SimpleCollection
{
public:
	ProcessStepIndicator hasMemberIndicator[];

	Process process[];
	ClassLibrary::SimpleMethodologyOverview::ProcessOverview processOverview[];
	ProcessControlStep processControlStep[];
};

}  // namespace ProcessPattern
}  // namespace ClassLibrary
#endif
