#ifndef CLASSLIBRARY_PROCESSPATTERN_PROCESS_STEP_INDICATOR_H
#define CLASSLIBRARY_PROCESSPATTERN_PROCESS_STEP_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/WorkflowStepIndicator.h"
#include "ClassLibrary/ProcessPattern/ProcessStep.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"

namespace ClassLibrary
{
namespace ProcessPattern
{
class ProcessStepIndicator : public ClassLibrary::CollectionsPattern::MemberIndicator
{
public:
	ProcessStep processStep[];
	ClassLibrary::ComplexDataTypes::WorkflowStepIndicator workflowStepIndicator;
};

}  // namespace ProcessPattern
}  // namespace ClassLibrary
#endif
