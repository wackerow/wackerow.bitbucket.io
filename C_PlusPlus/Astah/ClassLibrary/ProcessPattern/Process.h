#ifndef CLASSLIBRARY_PROCESSPATTERN_PROCESS_H
#define CLASSLIBRARY_PROCESSPATTERN_PROCESS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/SimpleMethodologyOverview/ProcessOverview.h"
#include "ClassLibrary/MethodologyPattern/Design.h"
#include "ClassLibrary/MethodologyPattern/Algorithm.h"
#include "ClassLibrary/MethodologyPattern/Methodology.h"
#include "ClassLibrary/ProcessPattern/ProcessSequence.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcess.h"
#include "ClassLibrary/Workflows/WorkflowProcess.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace ProcessPattern
{
class Process : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::Workflows::WorkflowProcess workflowProcess;
	ClassLibrary::MethodologyPattern::Design design[];
	ProcessSequence processSequence;
	ClassLibrary::MethodologyPattern::Algorithm algorithm[];
	ClassLibrary::SimpleMethodologyOverview::ProcessOverview processOverview;
	ClassLibrary::SamplingMethodology::SamplingProcess samplingProcess;
	ClassLibrary::MethodologyPattern::Methodology methodology[];
};

}  // namespace ProcessPattern
}  // namespace ClassLibrary
#endif
