#ifndef CLASSLIBRARY_PROCESSPATTERN_SERVICE_H
#define CLASSLIBRARY_PROCESSPATTERN_SERVICE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ProcessPattern/ProcessStep.h"
#include "ClassLibrary/Workflows/WorkflowService.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace ProcessPattern
{
class Service : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry serviceLocation;

	ProcessStep processStep[];
	ClassLibrary::Workflows::WorkflowService workflowService;
	ClassLibrary::Agents::Agent agent[];
};

}  // namespace ProcessPattern
}  // namespace ClassLibrary
#endif
