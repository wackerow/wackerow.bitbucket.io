#ifndef CLASSLIBRARY_PROCESSPATTERN_PROCESS_STEP_H
#define CLASSLIBRARY_PROCESSPATTERN_PROCESS_STEP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Binding.h"
#include "ClassLibrary/ProcessPattern/ProcessStepRelation.h"
#include "ClassLibrary/ProcessPattern/Service.h"
#include "ClassLibrary/ProcessPattern/ProcessStepIndicator.h"
#include "ClassLibrary/Workflows/Parameter.h"
#include "ClassLibrary/Workflows/WorkflowStep.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"

namespace ClassLibrary
{
namespace ProcessPattern
{
class ProcessStep : public ClassLibrary::CollectionsPattern::CollectionMember
{
public:
	ClassLibrary::ComplexDataTypes::Binding hasInformationFlow[];

	ProcessStepRelation processStepRelation;
	Service service[];
	ProcessStepRelation processStepRelation;
	ClassLibrary::Workflows::Parameter parameter[];
	ProcessStepIndicator processStepIndicator;
	ClassLibrary::Workflows::WorkflowStep workflowStep;
	ClassLibrary::Workflows::Parameter parameter[];
};

}  // namespace ProcessPattern
}  // namespace ClassLibrary
#endif
