#ifndef CLASSLIBRARY_PROCESSPATTERN_PROCESS_STEP_RELATION_H
#define CLASSLIBRARY_PROCESSPATTERN_PROCESS_STEP_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ProcessPattern/ProcessStep.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ProcessPattern
{
class ProcessStepRelation : public ClassLibrary::CollectionsPattern::MemberRelation
{
public:
	ProcessStep processStep[];
	ProcessStep processStep[];
};

}  // namespace ProcessPattern
}  // namespace ClassLibrary
#endif
