#ifndef CLASSLIBRARY_SIGNIFICATIONPATTERN_SIGN_H
#define CLASSLIBRARY_SIGNIFICATIONPATTERN_SIGN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SignificationPattern/Signifier.h"
#include "ClassLibrary/SignificationPattern/Signified.h"
#include "ClassLibrary/Representations/Designation.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"

namespace ClassLibrary
{
namespace SignificationPattern
{
class Sign : public ClassLibrary::CollectionsPattern::CollectionMember
{
public:
	Signifier representation;

	ClassLibrary::Representations::Designation designation;
	Signified signified[];
};

}  // namespace SignificationPattern
}  // namespace ClassLibrary
#endif
