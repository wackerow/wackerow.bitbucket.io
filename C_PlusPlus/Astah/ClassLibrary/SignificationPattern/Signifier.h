#ifndef CLASSLIBRARY_SIGNIFICATIONPATTERN_SIGNIFIER_H
#define CLASSLIBRARY_SIGNIFICATIONPATTERN_SIGNIFIER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/EnumerationsRegExp/WhiteSpaceRule.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace SignificationPattern
{
class Signifier
{
public:
	Primitive Package::String content;

	ClassLibrary::EnumerationsRegExp::WhiteSpaceRule whiteSpace;

	ClassLibrary::ComplexDataTypes::ValueString valueString;
};

}  // namespace SignificationPattern
}  // namespace ClassLibrary
#endif
