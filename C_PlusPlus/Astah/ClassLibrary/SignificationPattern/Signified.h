#ifndef CLASSLIBRARY_SIGNIFICATIONPATTERN_SIGNIFIED_H
#define CLASSLIBRARY_SIGNIFICATIONPATTERN_SIGNIFIED_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/SignificationPattern/Sign.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"

namespace ClassLibrary
{
namespace SignificationPattern
{
class Signified : public ClassLibrary::CollectionsPattern::CollectionMember
{
public:
	ClassLibrary::Conceptual::Concept concept;
	Sign sign;
};

}  // namespace SignificationPattern
}  // namespace ClassLibrary
#endif
