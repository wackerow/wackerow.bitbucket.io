#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_VIEWPOINT_ROLE_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_VIEWPOINT_ROLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class ViewpointRole : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InstanceVariableIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::CollectionsPattern::SimpleCollection simpleCollection[];
	ClassLibrary::Conceptual::Concept concept[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
