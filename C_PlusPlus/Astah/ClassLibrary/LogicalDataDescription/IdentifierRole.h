#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_IDENTIFIER_ROLE_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_IDENTIFIER_ROLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/LogicalDataDescription/ViewpointRole.h"
#include "ClassLibrary/LogicalDataDescription/UnitDataViewpoint.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class IdentifierRole : public ViewpointRole
{
public:
	UnitDataViewpoint unitDataViewpoint;
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
