#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_RECORD_RELATION_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_RECORD_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableValueMap.h"
#include "ClassLibrary/LogicalDataDescription/UnitDataRecord.h"
#include "ClassLibrary/LogicalDataDescription/DataStore.h"
#include "ClassLibrary/CollectionsPattern/Comparison.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class RecordRelation : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::InstanceVariableValueMap correspondence[];

	UnitDataRecord unitDataRecord[];
	ClassLibrary::CollectionsPattern::Comparison comparison[];
	DataStore dataStore[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
