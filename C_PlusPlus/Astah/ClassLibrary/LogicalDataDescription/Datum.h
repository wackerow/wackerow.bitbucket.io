#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_DATUM_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_DATUM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/Representations/Designation.h"
#include "ClassLibrary/LogicalDataDescription/DataPoint.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class Datum : public ClassLibrary::Representations::Designation
{
public:
	ClassLibrary::ComplexDataTypes::ValueString representation;

	DataPoint dataPoint[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
