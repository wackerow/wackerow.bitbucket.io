#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_STORE_RELATION_STRUCTURE_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_STORE_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DataStoreRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/LogicalDataDescription/DataStoreLibrary.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class DataStoreRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::DataStoreRelation hasMemberRelation[];

	DataStoreLibrary dataStoreLibrary[];
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
