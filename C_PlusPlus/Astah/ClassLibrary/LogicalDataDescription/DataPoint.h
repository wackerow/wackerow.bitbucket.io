#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_POINT_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_POINT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DataPointRelation.h"
#include "ClassLibrary/ComplexDataTypes/DataPointIndicator.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/FormatDescription/ValueMapping.h"
#include "ClassLibrary/LogicalDataDescription/Datum.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class DataPoint : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	Datum datum;
	ClassLibrary::ComplexDataTypes::DataPointRelation dataPointRelation[];
	ClassLibrary::ComplexDataTypes::DataPointIndicator dataPointIndicator;
	ClassLibrary::ComplexDataTypes::DataPointRelation dataPointRelation[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::FormatDescription::ValueMapping valueMapping;
	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
