#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_STORE_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_STORE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/LogicalRecordIndicator.h"
#include "ClassLibrary/ComplexDataTypes/DataStoreRelation.h"
#include "ClassLibrary/ComplexDataTypes/DataStoreIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/FormatDescription/PhysicalDataSet.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/LogicalDataDescription/RecordRelation.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecordRelationStructure.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Integer.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class DataStore : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	Primitive Package::String characterSet;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry dataStoreType;

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	Primitive Package::Integer recordCount;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString aboutMissing;

	ClassLibrary::ComplexDataTypes::LogicalRecordIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::FormatDescription::PhysicalDataSet physicalDataSet[];
	ClassLibrary::StudyRelated::Study study;
	ClassLibrary::ComplexDataTypes::DataStoreRelation dataStoreRelation[];
	ClassLibrary::ComplexDataTypes::DataStoreIndicator dataStoreIndicator;
	ClassLibrary::ComplexDataTypes::DataStoreRelation dataStoreRelation[];
	ClassLibrary::Conceptual::Concept concept[];
	RecordRelation recordRelation;
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	LogicalRecordRelationStructure logicalRecordRelationStructure[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
