#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_UNIT_DATA_RECORD_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_UNIT_DATA_RECORD_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "ClassLibrary/LogicalDataDescription/RecordRelation.h"
#include "ClassLibrary/LogicalDataDescription/UnitDataViewpoint.h"
#include "ClassLibrary/LogicalDataDescription/InstanceVariableRelationStructure.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class UnitDataRecord : public LogicalRecord
{
public:
	RecordRelation recordRelation[];
	UnitDataViewpoint unitDataViewpoint[];
	InstanceVariableRelationStructure instanceVariableRelationStructure[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	ClassLibrary::Conceptual::Concept concept[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
