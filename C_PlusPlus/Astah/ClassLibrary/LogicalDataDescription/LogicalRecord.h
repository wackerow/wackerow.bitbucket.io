#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_LOGICAL_RECORD_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_LOGICAL_RECORD_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableIndicator.h"
#include "ClassLibrary/ComplexDataTypes/BusinessProcessCondition.h"
#include "ClassLibrary/ComplexDataTypes/LogicalRecordRelation.h"
#include "ClassLibrary/ComplexDataTypes/LogicalRecordIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/FormatDescription/PhysicalSegmentLayout.h"
#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class LogicalRecord : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InstanceVariableIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::ComplexDataTypes::BusinessProcessCondition businessProcessCondition;
	ClassLibrary::FormatDescription::PhysicalSegmentLayout physicalSegmentLayout;
	ClassLibrary::ComplexDataTypes::LogicalRecordRelation logicalRecordRelation[];
	ClassLibrary::FormatDescription::PhysicalRecordSegment physicalRecordSegment;
	ClassLibrary::CollectionsPattern::SimpleCollection simpleCollection[];
	ClassLibrary::ComplexDataTypes::LogicalRecordIndicator logicalRecordIndicator;
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::ComplexDataTypes::LogicalRecordRelation logicalRecordRelation;
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
