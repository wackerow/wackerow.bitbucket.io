#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_STORE_LIBRARY_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_DATA_STORE_LIBRARY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/DataStoreIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/StudyRelated/StudySeries.h"
#include "ClassLibrary/LogicalDataDescription/DataStoreRelationStructure.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class DataStoreLibrary : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::DataStoreIndicator contains[];

	Primitive Package::Boolean isOrdered;

	DataStoreRelationStructure dataStoreRelationStructure[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::StudyRelated::StudySeries studySeries[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
