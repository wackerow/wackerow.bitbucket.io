#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_INSTANCE_VARIABLE_RELATION_STRUCTURE_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_INSTANCE_VARIABLE_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/LogicalDataDescription/UnitDataRecord.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class InstanceVariableRelationStructure : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString criteria;

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::InstanceVariableRelation hasMemberRelation[];

	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
	UnitDataRecord unitDataRecord[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
