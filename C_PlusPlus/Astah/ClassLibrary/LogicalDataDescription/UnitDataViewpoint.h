#ifndef CLASSLIBRARY_LOGICALDATADESCRIPTION_UNIT_DATA_VIEWPOINT_H
#define CLASSLIBRARY_LOGICALDATADESCRIPTION_UNIT_DATA_VIEWPOINT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/LogicalDataDescription/AttributeRole.h"
#include "ClassLibrary/LogicalDataDescription/IdentifierRole.h"
#include "ClassLibrary/LogicalDataDescription/MeasureRole.h"
#include "ClassLibrary/LogicalDataDescription/UnitDataRecord.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace LogicalDataDescription
{
class UnitDataViewpoint : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	AttributeRole attributeRole[];
	IdentifierRole identifierRole[];
	MeasureRole measureRole[];
	UnitDataRecord unitDataRecord[];
};

}  // namespace LogicalDataDescription
}  // namespace ClassLibrary
#endif
