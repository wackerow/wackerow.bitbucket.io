#ifndef CLASSLIBRARY_COMPLEXDATATYPES_EXTERNAL_CONTROLLED_VOCABULARY_ENTRY_H
#define CLASSLIBRARY_COMPLEXDATATYPES_EXTERNAL_CONTROLLED_VOCABULARY_ENTRY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"
#include "ClassLibrary/XMLSchemaDatatypes/language.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ExternalControlledVocabularyEntry
{
public:
	Primitive Package::String controlledVocabularyID;

	Primitive Package::String controlledVocabularyName;

	Primitive Package::String controlledVocabularyAgencyName;

	Primitive Package::String controlledVocabularyVersionID;

	Primitive Package::String otherValue;

	ClassLibrary::XMLSchemaDatatypes::anyUri uri;

	Primitive Package::String content;

	ClassLibrary::XMLSchemaDatatypes::language language;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
