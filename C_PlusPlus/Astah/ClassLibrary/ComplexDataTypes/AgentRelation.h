#ifndef CLASSLIBRARY_COMPLEXDATATYPES_AGENT_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_AGENT_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AgentRelation
{
public:
	DateRange effectiveDates;

	ExternalControlledVocabularyEntry semantic;

	RelationSpecification hasRelationSpecification;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::Agents::Agent agent[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
