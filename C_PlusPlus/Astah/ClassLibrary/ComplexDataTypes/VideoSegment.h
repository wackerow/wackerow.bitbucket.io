#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VIDEO_SEGMENT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VIDEO_SEGMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class VideoSegment
{
public:
	ExternalControlledVocabularyEntry typeOfVideoClip;

	Primitive Package::String videoClipBegin;

	Primitive Package::String videoClipEnd;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
