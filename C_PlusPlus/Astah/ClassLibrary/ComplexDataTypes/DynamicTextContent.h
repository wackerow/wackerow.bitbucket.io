#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DYNAMIC_TEXT_CONTENT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DYNAMIC_TEXT_CONTENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DynamicTextContent
{
public:
	InternationalStructuredString purpose;

	Primitive Package::Integer orderPosition;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
