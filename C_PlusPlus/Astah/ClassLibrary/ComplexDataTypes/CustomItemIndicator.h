#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_ITEM_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_ITEM_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/CustomMetadata/CustomItem.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CustomItemIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::CustomMetadata::CustomItem customItem[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
