#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DATA_STORE_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DATA_STORE_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/LogicalDataDescription/DataStore.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DataStoreIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::LogicalDataDescription::DataStore dataStore[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
