#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INSTANCE_VARIABLE_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INSTANCE_VARIABLE_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class InstanceVariableRelation
{
public:
	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
