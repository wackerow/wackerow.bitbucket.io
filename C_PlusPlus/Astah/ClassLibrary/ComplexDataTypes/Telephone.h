#ifndef CLASSLIBRARY_COMPLEXDATATYPES_TELEPHONE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_TELEPHONE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Telephone
{
public:
	Primitive Package::String telephoneNumber;

	ExternalControlledVocabularyEntry typeOfTelephone;

	DateRange effectiveDates;

	ExternalControlledVocabularyEntry privacy;

	Primitive Package::Boolean isPreferred;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
