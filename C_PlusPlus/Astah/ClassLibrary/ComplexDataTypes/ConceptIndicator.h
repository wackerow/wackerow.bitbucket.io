#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CONCEPT_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CONCEPT_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ConceptIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
