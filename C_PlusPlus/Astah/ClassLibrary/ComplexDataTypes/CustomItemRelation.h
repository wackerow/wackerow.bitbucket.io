#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_ITEM_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_ITEM_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CustomMetadata/CustomItem.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CustomItemRelation
{
public:
	RelationSpecification hasRelationSepcification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::CustomMetadata::CustomItem customItem[];
	ClassLibrary::CustomMetadata::CustomItem customItem[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
