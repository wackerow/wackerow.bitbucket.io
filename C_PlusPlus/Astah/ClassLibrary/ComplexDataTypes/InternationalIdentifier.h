#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INTERNATIONAL_IDENTIFIER_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INTERNATIONAL_IDENTIFIER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class InternationalIdentifier
{
public:
	Primitive Package::String identifierContent;

	ExternalControlledVocabularyEntry managingAgency;

	Primitive Package::Boolean isURI;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
