#ifndef CLASSLIBRARY_COMPLEXDATATYPES_RESOURCE_IDENTIFIER_H
#define CLASSLIBRARY_COMPLEXDATATYPES_RESOURCE_IDENTIFIER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalIdentifier.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ResourceIdentifier : public InternationalIdentifier
{
public:
	ExternalControlledVocabularyEntry typeOfRelatedResource[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
