#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VOCABULARY_ENTRY_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VOCABULARY_ENTRY_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CustomMetadata/VocabularyEntry.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class VocabularyEntryRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::CustomMetadata::VocabularyEntry vocabularyEntry[];
	ClassLibrary::CustomMetadata::VocabularyEntry vocabularyEntry[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
