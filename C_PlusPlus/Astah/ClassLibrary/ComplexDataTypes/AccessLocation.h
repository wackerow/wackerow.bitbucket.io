#ifndef CLASSLIBRARY_COMPLEXDATATYPES_ACCESS_LOCATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_ACCESS_LOCATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AccessLocation
{
public:
	ClassLibrary::XMLSchemaDatatypes::anyUri uri[];

	ExternalControlledVocabularyEntry mimeType;

	InternationalString physicalLocation[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
