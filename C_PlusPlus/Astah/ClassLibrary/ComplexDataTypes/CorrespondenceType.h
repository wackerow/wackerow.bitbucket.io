#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CORRESPONDENCE_TYPE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CORRESPONDENCE_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/MappingRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CorrespondenceType
{
public:
	InternationalStructuredString commonality;

	InternationalStructuredString difference;

	ExternalControlledVocabularyEntry commonalityTypeCode[];

	ClassLibrary::EnumerationsRegExp::MappingRelation hasMappingRelation;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
