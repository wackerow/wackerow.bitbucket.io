#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VIEWPOINT_ROLE_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VIEWPOINT_ROLE_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ViewpointRoleRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
