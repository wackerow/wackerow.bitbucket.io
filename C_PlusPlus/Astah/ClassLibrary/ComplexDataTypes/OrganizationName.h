#ifndef CLASSLIBRARY_COMPLEXDATATYPES_ORGANIZATION_NAME_H
#define CLASSLIBRARY_COMPLEXDATATYPES_ORGANIZATION_NAME_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class OrganizationName : public ObjectName
{
public:
	InternationalString abbreviation;

	ExternalControlledVocabularyEntry typeOfOrganizationName;

	DateRange effectiveDates;

	Primitive Package::Boolean isFormal;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
