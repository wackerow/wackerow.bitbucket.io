#ifndef CLASSLIBRARY_COMPLEXDATATYPES_RELATION_SPECIFICATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_RELATION_SPECIFICATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace ComplexDataTypes
{
typedef enum 
{
	Unordered,
	List,
	ParentChild,
	WholePart,
	AcyclicPrecedence,
	Equivalence,
	GeneralSpecfic

} RelationSpecification;

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
