#ifndef CLASSLIBRARY_COMPLEXDATATYPES_OBJECT_NAME_H
#define CLASSLIBRARY_COMPLEXDATATYPES_OBJECT_NAME_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ObjectName
{
public:
	Primitive Package::String content;

	ExternalControlledVocabularyEntry context;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
