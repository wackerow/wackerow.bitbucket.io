#ifndef CLASSLIBRARY_COMPLEXDATATYPES_TARGET_SAMPLE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_TARGET_SAMPLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/Conceptual/Universe.h"
#include "Primitive Package/Boolean.h"
#include "Primitive Package/Integer.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class TargetSample
{
public:
	Primitive Package::Boolean isPrimary;

	Primitive Package::Integer targetSize;

	Primitive Package::Real targetPercent;

	ClassLibrary::Conceptual::UnitType unitType;
	ClassLibrary::Conceptual::Universe universe;
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
