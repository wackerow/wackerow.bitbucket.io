#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CATEGORY_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CATEGORY_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/Category.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CategoryIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::Conceptual::Category category[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
