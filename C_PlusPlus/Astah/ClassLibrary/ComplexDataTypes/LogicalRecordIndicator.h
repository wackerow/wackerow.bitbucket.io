#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LOGICAL_RECORD_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LOGICAL_RECORD_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LogicalRecordIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
