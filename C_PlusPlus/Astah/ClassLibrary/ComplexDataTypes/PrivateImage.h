#ifndef CLASSLIBRARY_COMPLEXDATATYPES_PRIVATE_IMAGE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_PRIVATE_IMAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Image.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class PrivateImage : public Image
{
public:
	DateRange effectiveDates;

	ExternalControlledVocabularyEntry privacy;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
