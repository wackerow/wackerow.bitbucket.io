#ifndef CLASSLIBRARY_COMPLEXDATATYPES_ADDRESS_H
#define CLASSLIBRARY_COMPLEXDATATYPES_ADDRESS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/SpatialPoint.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Address
{
public:
	ExternalControlledVocabularyEntry typeOfAddress;

	Primitive Package::String line[];

	Primitive Package::String cityPlaceLocal;

	Primitive Package::String stateProvince;

	Primitive Package::String postalCode;

	ExternalControlledVocabularyEntry countryCode;

	ExternalControlledVocabularyEntry timeZone;

	DateRange effectiveDates;

	ExternalControlledVocabularyEntry privacy;

	Primitive Package::Boolean isPreferred;

	SpatialPoint geographicPoint;

	ExternalControlledVocabularyEntry regionalCoverage;

	ExternalControlledVocabularyEntry typeOfLocation;

	ObjectName locationName;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
