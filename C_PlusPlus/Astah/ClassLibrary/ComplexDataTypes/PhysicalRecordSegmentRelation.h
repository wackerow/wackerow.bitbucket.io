#ifndef CLASSLIBRARY_COMPLEXDATATYPES_PHYSICAL_RECORD_SEGMENT_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_PHYSICAL_RECORD_SEGMENT_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class PhysicalRecordSegmentRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::FormatDescription::PhysicalRecordSegment physicalRecordSegment[];
	ClassLibrary::FormatDescription::PhysicalRecordSegment physicalRecordSegment[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
