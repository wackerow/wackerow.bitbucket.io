#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INSTANCE_VARIABLE_VALUE_MAP_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INSTANCE_VARIABLE_VALUE_MAP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/ComplexDataTypes/CorrespondenceType.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/EnumerationsRegExp/ValueRelationshipType.h"
#include "ClassLibrary/CollectionsPattern/ComparisonMap.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class InstanceVariableValueMap
{
public:
	ClassLibrary::EnumerationsRegExp::ValueRelationshipType valueRelationship;

	ValueString setValue;

	CorrespondenceType hasCorrespondenceType;

	ClassLibrary::CollectionsPattern::ComparisonMap comparisonMap[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
