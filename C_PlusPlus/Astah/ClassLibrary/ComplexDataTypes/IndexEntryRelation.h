#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INDEX_ENTRY_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INDEX_ENTRY_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/Representations/ClassificationIndexEntry.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class IndexEntryRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Representations::ClassificationIndexEntry classificationIndexEntry[];
	ClassLibrary::Representations::ClassificationIndexEntry classificationIndexEntry[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
