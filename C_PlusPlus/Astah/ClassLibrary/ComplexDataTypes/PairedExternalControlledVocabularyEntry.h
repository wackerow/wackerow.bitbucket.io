#ifndef CLASSLIBRARY_COMPLEXDATATYPES_PAIRED_EXTERNAL_CONTROLLED_VOCABULARY_ENTRY_H
#define CLASSLIBRARY_COMPLEXDATATYPES_PAIRED_EXTERNAL_CONTROLLED_VOCABULARY_ENTRY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class PairedExternalControlledVocabularyEntry
{
public:
	ExternalControlledVocabularyEntry term;

	ExternalControlledVocabularyEntry extent;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
