#ifndef CLASSLIBRARY_COMPLEXDATATYPES_TYPED_STRING_H
#define CLASSLIBRARY_COMPLEXDATATYPES_TYPED_STRING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class TypedString
{
public:
	ExternalControlledVocabularyEntry typeOfContent;

	Primitive Package::String content;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
