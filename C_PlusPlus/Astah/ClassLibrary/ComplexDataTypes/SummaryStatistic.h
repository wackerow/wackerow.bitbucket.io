#ifndef CLASSLIBRARY_COMPLEXDATATYPES_SUMMARY_STATISTIC_H
#define CLASSLIBRARY_COMPLEXDATATYPES_SUMMARY_STATISTIC_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/Statistic.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class SummaryStatistic
{
public:
	ExternalControlledVocabularyEntry typeOfSummaryStatistic;

	Statistic hasStatistic[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
