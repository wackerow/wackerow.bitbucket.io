#ifndef CLASSLIBRARY_COMPLEXDATATYPES_BUSINESS_PROCESS_CONDITION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_BUSINESS_PROCESS_CONDITION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class BusinessProcessCondition
{
public:
	Primitive Package::String sql;

	CommandCode rejectionCriteria;

	InternationalStructuredString dataDescription[];

	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
