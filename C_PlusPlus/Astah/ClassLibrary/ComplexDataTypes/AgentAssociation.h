#ifndef CLASSLIBRARY_COMPLEXDATATYPES_AGENT_ASSOCIATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_AGENT_ASSOCIATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/BibliographicName.h"
#include "ClassLibrary/ComplexDataTypes/PairedExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AgentAssociation
{
public:
	BibliographicName agentName;

	PairedExternalControlledVocabularyEntry role[];

	ClassLibrary::Agents::Agent agent[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
