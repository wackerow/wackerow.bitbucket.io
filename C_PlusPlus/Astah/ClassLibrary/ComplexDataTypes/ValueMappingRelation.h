#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VALUE_MAPPING_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VALUE_MAPPING_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/FormatDescription/ValueMapping.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ValueMappingRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::FormatDescription::ValueMapping valueMapping[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::FormatDescription::ValueMapping valueMapping[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
