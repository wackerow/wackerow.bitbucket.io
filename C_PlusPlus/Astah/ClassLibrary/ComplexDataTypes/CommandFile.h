#ifndef CLASSLIBRARY_COMPLEXDATATYPES_COMMAND_FILE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_COMMAND_FILE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CommandFile
{
public:
	ExternalControlledVocabularyEntry programLanguage;

	InternationalString location;

	ClassLibrary::XMLSchemaDatatypes::anyUri uri;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
