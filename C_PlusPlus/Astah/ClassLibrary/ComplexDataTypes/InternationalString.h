#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INTERNATIONAL_STRING_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INTERNATIONAL_STRING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LanguageSpecificStringType.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class InternationalString
{
public:
	LanguageSpecificStringType languageSpecificString[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
