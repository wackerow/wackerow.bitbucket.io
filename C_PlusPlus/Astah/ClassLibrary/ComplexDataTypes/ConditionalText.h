#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CONDITIONAL_TEXT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CONDITIONAL_TEXT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DynamicTextContent.h"
#include "ClassLibrary/ComplexDataTypes/CommandCode.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ConditionalText : public DynamicTextContent
{
public:
	CommandCode expression;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
