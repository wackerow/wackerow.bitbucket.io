#ifndef CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/GeographicClassification/GeographicUnit.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class GeographicUnitIndicator
{
public:
	Primitive Package::Integer index;

	Primitive Package::Integer isInLevel;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::GeographicClassification::GeographicUnit geographicUnit[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
