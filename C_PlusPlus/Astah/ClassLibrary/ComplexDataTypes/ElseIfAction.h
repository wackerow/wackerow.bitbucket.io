#ifndef CLASSLIBRARY_COMPLEXDATATYPES_ELSE_IF_ACTION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_ELSE_IF_ACTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ElseIfAction
{
public:
	CommandCode condition;

	ClassLibrary::Workflows::WorkflowStepSequence workflowStepSequence[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
