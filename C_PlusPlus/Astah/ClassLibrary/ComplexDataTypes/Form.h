#ifndef CLASSLIBRARY_COMPLEXDATATYPES_FORM_H
#define CLASSLIBRARY_COMPLEXDATATYPES_FORM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Form
{
public:
	Primitive Package::String formNumber;

	ClassLibrary::XMLSchemaDatatypes::anyUri uri;

	InternationalString statement;

	Primitive Package::Boolean isRequired;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
