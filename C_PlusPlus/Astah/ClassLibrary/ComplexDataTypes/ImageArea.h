#ifndef CLASSLIBRARY_COMPLEXDATATYPES_IMAGE_AREA_H
#define CLASSLIBRARY_COMPLEXDATATYPES_IMAGE_AREA_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/EnumerationsRegExp/ShapeCoded.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ImageArea
{
public:
	Primitive Package::String coordinates;

	ClassLibrary::EnumerationsRegExp::ShapeCoded shape;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
