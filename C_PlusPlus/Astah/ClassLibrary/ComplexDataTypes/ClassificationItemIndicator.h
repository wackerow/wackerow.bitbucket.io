#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_ITEM_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_ITEM_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Representations/ClassificationItem.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ClassificationItemIndicator
{
public:
	Primitive Package::Integer index;

	Primitive Package::Integer hasLevel;

	ClassLibrary::Representations::ClassificationItem classificationItem[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
