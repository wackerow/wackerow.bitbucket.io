#ifndef CLASSLIBRARY_COMPLEXDATATYPES_SPATIAL_POINT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_SPATIAL_POINT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/SpatialCoordinate.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class SpatialPoint
{
public:
	SpatialCoordinate xCoordinate;

	SpatialCoordinate yCoordinate;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
