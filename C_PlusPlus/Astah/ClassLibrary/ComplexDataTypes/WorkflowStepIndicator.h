#ifndef CLASSLIBRARY_COMPLEXDATATYPES_WORKFLOW_STEP_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_WORKFLOW_STEP_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ProcessPattern/ProcessStepIndicator.h"
#include "ClassLibrary/Workflows/WorkflowStep.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class WorkflowStepIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::ProcessPattern::ProcessStepIndicator processStepIndicator[];
	ClassLibrary::Workflows::WorkflowStep workflowStep[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
