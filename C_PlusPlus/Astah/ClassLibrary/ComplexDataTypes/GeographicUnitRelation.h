#ifndef CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/GeographicClassification/GeographicUnit.h"
#include "ClassLibrary/EnumerationsRegExp/SpatialRelationSpecification.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class GeographicUnitRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ClassLibrary::EnumerationsRegExp::SpatialRelationSpecification hasSpatialRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::GeographicClassification::GeographicUnit geographicUnit[];
	ClassLibrary::GeographicClassification::GeographicUnit geographicUnit[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
