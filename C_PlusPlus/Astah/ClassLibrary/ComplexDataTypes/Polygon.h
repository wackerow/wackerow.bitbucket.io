#ifndef CLASSLIBRARY_COMPLEXDATATYPES_POLYGON_H
#define CLASSLIBRARY_COMPLEXDATATYPES_POLYGON_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/SpatialPoint.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Polygon
{
public:
	ClassLibrary::XMLSchemaDatatypes::anyUri uri;

	Primitive Package::String polygonLinkCode;

	ExternalControlledVocabularyEntry shapeFileFormat;

	SpatialPoint point[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
