#ifndef CLASSLIBRARY_COMPLEXDATATYPES_RANGE_VALUE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_RANGE_VALUE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class RangeValue : public ValueString
{
public:
	Primitive Package::Boolean included;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
