#ifndef CLASSLIBRARY_COMPLEXDATATYPES_AGENT_ID_H
#define CLASSLIBRARY_COMPLEXDATATYPES_AGENT_ID_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AgentId
{
public:
	Primitive Package::String agentIdValue;

	Primitive Package::String agentIdType;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
