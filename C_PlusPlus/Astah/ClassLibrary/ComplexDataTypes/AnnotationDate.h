#ifndef CLASSLIBRARY_COMPLEXDATATYPES_ANNOTATION_DATE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_ANNOTATION_DATE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AnnotationDate : public Date
{
public:
	ExternalControlledVocabularyEntry typeOfDate[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
