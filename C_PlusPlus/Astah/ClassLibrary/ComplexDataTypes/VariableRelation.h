#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VARIABLE_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VARIABLE_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class VariableRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Conceptual::ConceptualVariable conceptualVariable[];
	ClassLibrary::Conceptual::ConceptualVariable conceptualVariable[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
