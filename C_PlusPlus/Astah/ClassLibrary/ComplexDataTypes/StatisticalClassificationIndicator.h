#ifndef CLASSLIBRARY_COMPLEXDATATYPES_STATISTICAL_CLASSIFICATION_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_STATISTICAL_CLASSIFICATION_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Representations/StatisticalClassification.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class StatisticalClassificationIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::Representations::StatisticalClassification statisticalClassification[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
