#ifndef CLASSLIBRARY_COMPLEXDATATYPES_FIXED_TEXT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_FIXED_TEXT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LanguageSpecificStructuredStringType.h"
#include "ClassLibrary/EnumerationsRegExp/WhiteSpaceRule.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class FixedText : public LanguageSpecificStructuredStringType
{
public:
	ClassLibrary::EnumerationsRegExp::WhiteSpaceRule whiteSpace;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
