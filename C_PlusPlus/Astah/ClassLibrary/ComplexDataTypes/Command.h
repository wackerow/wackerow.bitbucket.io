#ifndef CLASSLIBRARY_COMPLEXDATATYPES_COMMAND_H
#define CLASSLIBRARY_COMPLEXDATATYPES_COMMAND_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Command
{
public:
	ExternalControlledVocabularyEntry programLanguage;

	Primitive Package::String commandContent;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
