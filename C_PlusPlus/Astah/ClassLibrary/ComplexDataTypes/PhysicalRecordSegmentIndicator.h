#ifndef CLASSLIBRARY_COMPLEXDATATYPES_PHYSICAL_RECORD_SEGMENT_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_PHYSICAL_RECORD_SEGMENT_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class PhysicalRecordSegmentIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::FormatDescription::PhysicalRecordSegment physicalRecordSegment[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
