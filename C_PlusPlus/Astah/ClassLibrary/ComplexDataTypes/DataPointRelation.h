#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DATA_POINT_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DATA_POINT_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/LogicalDataDescription/DataPoint.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DataPointRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::LogicalDataDescription::DataPoint dataPoint[];
	ClassLibrary::LogicalDataDescription::DataPoint dataPoint[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
