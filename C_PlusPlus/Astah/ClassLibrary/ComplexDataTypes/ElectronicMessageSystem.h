#ifndef CLASSLIBRARY_COMPLEXDATATYPES_ELECTRONIC_MESSAGE_SYSTEM_H
#define CLASSLIBRARY_COMPLEXDATATYPES_ELECTRONIC_MESSAGE_SYSTEM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ElectronicMessageSystem
{
public:
	Primitive Package::String contactAddress;

	ExternalControlledVocabularyEntry typeOfService;

	DateRange effectiveDates;

	ExternalControlledVocabularyEntry privacy;

	Primitive Package::Boolean isPreferred;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
