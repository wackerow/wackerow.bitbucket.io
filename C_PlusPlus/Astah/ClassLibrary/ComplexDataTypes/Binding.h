#ifndef CLASSLIBRARY_COMPLEXDATATYPES_BINDING_H
#define CLASSLIBRARY_COMPLEXDATATYPES_BINDING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Workflows/Parameter.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Binding
{
public:
	ClassLibrary::Workflows::Parameter parameter[];
	ClassLibrary::Workflows::Parameter parameter[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
