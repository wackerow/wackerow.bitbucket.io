#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VARIABLE_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VARIABLE_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class VariableIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::Conceptual::ConceptualVariable conceptualVariable[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
