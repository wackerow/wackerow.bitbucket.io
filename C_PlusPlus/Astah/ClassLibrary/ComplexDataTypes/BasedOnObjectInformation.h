#ifndef CLASSLIBRARY_COMPLEXDATATYPES_BASED_ON_OBJECT_INFORMATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_BASED_ON_OBJECT_INFORMATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class BasedOnObjectInformation
{
public:
	InternationalString basedOnRationaleDescription;

	ExternalControlledVocabularyEntry basedOnRationaleCode;

	ClassLibrary::Identification::Identifiable identifiable[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
