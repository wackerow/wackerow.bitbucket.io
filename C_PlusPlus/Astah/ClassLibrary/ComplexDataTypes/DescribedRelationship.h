#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DESCRIBED_RELATIONSHIP_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DESCRIBED_RELATIONSHIP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DescribedRelationship
{
public:
	InternationalStructuredString rationale;

	ClassLibrary::Identification::Identifiable identifiable[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
