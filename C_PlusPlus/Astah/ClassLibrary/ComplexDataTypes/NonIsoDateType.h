#ifndef CLASSLIBRARY_COMPLEXDATATYPES_NON_ISO_DATE_TYPE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_NON_ISO_DATE_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class NonIsoDateType
{
public:
	Primitive Package::String dateContent;

	ExternalControlledVocabularyEntry nonIsoDateFormat;

	ExternalControlledVocabularyEntry calendar;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
