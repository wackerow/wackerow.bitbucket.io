#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LITERAL_TEXT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LITERAL_TEXT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DynamicTextContent.h"
#include "ClassLibrary/ComplexDataTypes/FixedText.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LiteralText : public DynamicTextContent
{
public:
	FixedText text;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
