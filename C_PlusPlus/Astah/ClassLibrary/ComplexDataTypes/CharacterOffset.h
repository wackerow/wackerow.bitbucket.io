#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CHARACTER_OFFSET_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CHARACTER_OFFSET_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CharacterOffset
{
public:
	Primitive Package::Integer startCharOffset;

	Primitive Package::Integer endCharOffset;

	Primitive Package::Integer characterLength;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
