#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VALUE_MAPPING_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VALUE_MAPPING_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/FormatDescription/ValueMapping.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ValueMappingIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::FormatDescription::ValueMapping valueMapping[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
