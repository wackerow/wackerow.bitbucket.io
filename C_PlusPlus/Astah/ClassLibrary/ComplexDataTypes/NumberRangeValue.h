#ifndef CLASSLIBRARY_COMPLEXDATATYPES_NUMBER_RANGE_VALUE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_NUMBER_RANGE_VALUE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Primitive Package/Boolean.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class NumberRangeValue
{
public:
	Primitive Package::Boolean isInclusive;

	Primitive Package::Real decimalValue;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
