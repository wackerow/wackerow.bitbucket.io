#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CATEGORY_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CATEGORY_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/Category.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CategoryRelation
{
public:
	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::Conceptual::Category category[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Conceptual::Category category[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
