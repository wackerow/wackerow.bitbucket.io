#ifndef CLASSLIBRARY_COMPLEXDATATYPES_AUDIO_SEGMENT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_AUDIO_SEGMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AudioSegment
{
public:
	ExternalControlledVocabularyEntry typeOfAudioClip;

	Primitive Package::String audioClipBegin;

	Primitive Package::String audioClipEnd;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
