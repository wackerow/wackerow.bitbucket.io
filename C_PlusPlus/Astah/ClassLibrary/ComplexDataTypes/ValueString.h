#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VALUE_STRING_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VALUE_STRING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SignificationPattern/Signifier.h"
#include "ClassLibrary/EnumerationsRegExp/WhiteSpaceRule.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ValueString
{
public:
	Primitive Package::String content;

	ClassLibrary::EnumerationsRegExp::WhiteSpaceRule whiteSpace;

	ClassLibrary::SignificationPattern::Signifier signifier[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
