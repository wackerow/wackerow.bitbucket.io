#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INSTANCE_VARIABLE_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INSTANCE_VARIABLE_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class InstanceVariableIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
