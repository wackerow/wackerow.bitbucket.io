#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LEVEL_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LEVEL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Level
{
public:
	Primitive Package::Integer levelNumber;

	LabelForDisplay displayLabel[];

	ClassLibrary::Conceptual::Concept concept[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
