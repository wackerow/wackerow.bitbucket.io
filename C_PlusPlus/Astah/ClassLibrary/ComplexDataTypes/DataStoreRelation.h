#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DATA_STORE_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DATA_STORE_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/LogicalDataDescription/DataStore.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DataStoreRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::LogicalDataDescription::DataStore dataStore[];
	ClassLibrary::LogicalDataDescription::DataStore dataStore[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
