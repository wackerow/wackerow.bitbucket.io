#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CONCEPT_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CONCEPT_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ConceptRelation
{
public:
	RelationSpecification hasRelationSepcification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::Conceptual::Concept concept[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
