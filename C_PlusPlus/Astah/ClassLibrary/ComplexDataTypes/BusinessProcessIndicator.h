#ifndef CLASSLIBRARY_COMPLEXDATATYPES_BUSINESS_PROCESS_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_BUSINESS_PROCESS_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/BusinessWorkflow/BusinessProcess.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class BusinessProcessIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::BusinessWorkflow::BusinessProcess businessProcess[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
