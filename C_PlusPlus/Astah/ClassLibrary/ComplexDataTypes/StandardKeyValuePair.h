#ifndef CLASSLIBRARY_COMPLEXDATATYPES_STANDARD_KEY_VALUE_PAIR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_STANDARD_KEY_VALUE_PAIR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class StandardKeyValuePair
{
public:
	ExternalControlledVocabularyEntry attributeKey;

	ExternalControlledVocabularyEntry attributeValue;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
