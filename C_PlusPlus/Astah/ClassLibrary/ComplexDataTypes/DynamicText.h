#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DYNAMIC_TEXT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DYNAMIC_TEXT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DynamicTextContent.h"
#include "ClassLibrary/XMLSchemaDatatypes/LanguageSpecification.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DynamicText
{
public:
	DynamicTextContent textContent[];

	Primitive Package::Boolean isStructureRequired;

	ClassLibrary::XMLSchemaDatatypes::LanguageSpecification audienceLanguage;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
