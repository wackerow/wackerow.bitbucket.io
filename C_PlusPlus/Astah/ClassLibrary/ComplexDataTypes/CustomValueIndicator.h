#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_VALUE_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_VALUE_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/CustomMetadata/CustomValue.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CustomValueIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CustomMetadata::CustomValue customValue[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
