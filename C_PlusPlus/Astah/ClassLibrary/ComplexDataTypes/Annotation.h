#ifndef CLASSLIBRARY_COMPLEXDATATYPES_ANNOTATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_ANNOTATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/AgentAssociation.h"
#include "ClassLibrary/ComplexDataTypes/AnnotationDate.h"
#include "ClassLibrary/ComplexDataTypes/InternationalIdentifier.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/ResourceIdentifier.h"
#include "ClassLibrary/EnumerationsRegExp/IsoDateType.h"
#include "ClassLibrary/XMLSchemaDatatypes/LanguageSpecification.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Annotation
{
public:
	InternationalString title;

	InternationalString subTitle[];

	InternationalString alternativeTitle[];

	AgentAssociation creator[];

	AgentAssociation publisher[];

	AgentAssociation contributor[];

	AnnotationDate date[];

	ClassLibrary::XMLSchemaDatatypes::LanguageSpecification languageOfObject;

	InternationalIdentifier identifier[];

	InternationalString copyright[];

	ExternalControlledVocabularyEntry typeOfResource[];

	InternationalString informationSource[];

	Primitive Package::String versionIdentification;

	AgentAssociation versioningAgent[];

	InternationalString summary;

	ResourceIdentifier relatedResource[];

	InternationalString provenance[];

	InternationalString rights[];

	ClassLibrary::EnumerationsRegExp::IsoDateType recordCreationDate;

	ClassLibrary::EnumerationsRegExp::IsoDateType recordLastRevisionDate;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
