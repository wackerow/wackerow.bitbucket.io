#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_INDEX_ENTRY_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_INDEX_ENTRY_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Representations/ClassificationIndexEntry.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ClassificationIndexEntryIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::Representations::ClassificationIndexEntry classificationIndexEntry[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
