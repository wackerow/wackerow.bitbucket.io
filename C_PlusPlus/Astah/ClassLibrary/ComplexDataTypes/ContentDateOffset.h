#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CONTENT_DATE_OFFSET_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CONTENT_DATE_OFFSET_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/Real.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ContentDateOffset : public ExternalControlledVocabularyEntry
{
public:
	Primitive Package::Real numberOfUnits;

	Primitive Package::Boolean isNegativeOffset;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
