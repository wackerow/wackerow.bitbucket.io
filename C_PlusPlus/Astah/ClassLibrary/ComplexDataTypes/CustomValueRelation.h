#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_VALUE_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CUSTOM_VALUE_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CustomMetadata/CustomValue.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CustomValueRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::CustomMetadata::CustomValue customValue[];
	ClassLibrary::CustomMetadata::CustomValue customValue[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
