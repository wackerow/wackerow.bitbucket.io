#ifndef CLASSLIBRARY_COMPLEXDATATYPES_SPATIAL_COORDINATE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_SPATIAL_COORDINATE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/EnumerationsRegExp/PointFormat.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class SpatialCoordinate
{
public:
	Primitive Package::String coordinateValue;

	ClassLibrary::EnumerationsRegExp::PointFormat coordinateType;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
