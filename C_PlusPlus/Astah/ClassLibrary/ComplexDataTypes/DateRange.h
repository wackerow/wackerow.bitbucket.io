#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DATE_RANGE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DATE_RANGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Date.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DateRange
{
public:
	Date startDate;

	Date endDate;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
