#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_SERIES_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_SERIES_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Representations/ClassificationSeries.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ClassificationSeriesIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::Representations::ClassificationSeries classificationSeries[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
