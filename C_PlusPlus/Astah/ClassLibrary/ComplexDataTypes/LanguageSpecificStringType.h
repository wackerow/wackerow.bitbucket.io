#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LANGUAGE_SPECIFIC_STRING_TYPE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LANGUAGE_SPECIFIC_STRING_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/XMLSchemaDatatypes/language.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"
#include "Primitive Package/UnlimitedNatural.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LanguageSpecificStringType
{
public:
	Primitive Package::String content;

	ClassLibrary::XMLSchemaDatatypes::language language;

	Primitive Package::Boolean isTranslated;

	Primitive Package::Boolean isTranslatable;

	ClassLibrary::XMLSchemaDatatypes::language translationSourceLanguage[];

	Primitive Package::UnlimitedNatural translationDate;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
