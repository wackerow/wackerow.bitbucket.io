#ifndef CLASSLIBRARY_COMPLEXDATATYPES_SEGMENT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_SEGMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/AudioSegment.h"
#include "ClassLibrary/ComplexDataTypes/VideoSegment.h"
#include "ClassLibrary/ComplexDataTypes/TextualSegment.h"
#include "ClassLibrary/ComplexDataTypes/ImageArea.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Segment
{
public:
	AudioSegment usesAudioSegment[];

	VideoSegment usesVideoSegment[];

	Primitive Package::String xml[];

	TextualSegment useseTextualSegment[];

	ImageArea usesImageArea[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
