#ifndef CLASSLIBRARY_COMPLEXDATATYPES_VOCABULARY_ENTRY_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_VOCABULARY_ENTRY_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/CustomMetadata/VocabularyEntry.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class VocabularyEntryIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::CustomMetadata::VocabularyEntry vocabularyEntry[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
