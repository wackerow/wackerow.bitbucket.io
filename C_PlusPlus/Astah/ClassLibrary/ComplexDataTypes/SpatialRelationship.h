#ifndef CLASSLIBRARY_COMPLEXDATATYPES_SPATIAL_RELATIONSHIP_H
#define CLASSLIBRARY_COMPLEXDATATYPES_SPATIAL_RELATIONSHIP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/GeographicClassification/GeographicUnit.h"
#include "ClassLibrary/EnumerationsRegExp/SpatialObjectPairs.h"
#include "ClassLibrary/EnumerationsRegExp/SpatialRelationSpecification.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class SpatialRelationship
{
public:
	ClassLibrary::EnumerationsRegExp::SpatialObjectPairs hasSpatialObjectPair;

	ClassLibrary::EnumerationsRegExp::SpatialRelationSpecification hasSpatialRelationSpecification;

	Date eventDate;

	ClassLibrary::GeographicClassification::GeographicUnit geographicUnit[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
