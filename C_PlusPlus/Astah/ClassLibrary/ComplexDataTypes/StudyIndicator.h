#ifndef CLASSLIBRARY_COMPLEXDATATYPES_STUDY_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_STUDY_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class StudyIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
