#ifndef CLASSLIBRARY_COMPLEXDATATYPES_WORKFLOW_STEP_SEQUENCE_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_WORKFLOW_STEP_SEQUENCE_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Workflows/WorkflowStepSequence.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class WorkflowStepSequenceIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::Workflows::WorkflowStepSequence workflowStepSequence[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
