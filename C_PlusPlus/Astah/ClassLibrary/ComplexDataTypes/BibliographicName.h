#ifndef CLASSLIBRARY_COMPLEXDATATYPES_BIBLIOGRAPHIC_NAME_H
#define CLASSLIBRARY_COMPLEXDATATYPES_BIBLIOGRAPHIC_NAME_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class BibliographicName : public InternationalString
{
public:
	Primitive Package::String affiliation;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
