#ifndef CLASSLIBRARY_COMPLEXDATATYPES_STATISTICAL_CLASSIFICATION_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_STATISTICAL_CLASSIFICATION_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/Representations/StatisticalClassification.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class StatisticalClassificationRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Representations::StatisticalClassification statisticalClassification[];
	ClassLibrary::Representations::StatisticalClassification statisticalClassification[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
