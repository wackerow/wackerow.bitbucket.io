#ifndef CLASSLIBRARY_COMPLEXDATATYPES_REFERENCE_DATE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_REFERENCE_DATE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/AnnotationDate.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ReferenceDate : public AnnotationDate
{
public:
	ExternalControlledVocabularyEntry subject[];

	ExternalControlledVocabularyEntry keyword[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
