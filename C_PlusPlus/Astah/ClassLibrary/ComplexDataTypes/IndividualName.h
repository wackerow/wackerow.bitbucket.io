#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INDIVIDUAL_NAME_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INDIVIDUAL_NAME_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalString.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/SexSpecificationType.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class IndividualName
{
public:
	Primitive Package::String prefix;

	Primitive Package::String firstGiven;

	Primitive Package::String middle[];

	Primitive Package::String lastFamily;

	Primitive Package::String suffix;

	InternationalString fullName;

	DateRange effectiveDates;

	InternationalString abbreviation;

	ExternalControlledVocabularyEntry typeOfIndividualName;

	ClassLibrary::EnumerationsRegExp::SexSpecificationType sex;

	Primitive Package::Boolean isPreferred;

	ExternalControlledVocabularyEntry context;

	Primitive Package::Boolean isFormal;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
