#ifndef CLASSLIBRARY_COMPLEXDATATYPES_INTERNATIONAL_STRUCTURED_STRING_H
#define CLASSLIBRARY_COMPLEXDATATYPES_INTERNATIONAL_STRUCTURED_STRING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LanguageSpecificStructuredStringType.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class InternationalStructuredString
{
public:
	LanguageSpecificStructuredStringType languageSpecificStructuredString[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
