#ifndef CLASSLIBRARY_COMPLEXDATATYPES_NUMBER_RANGE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_NUMBER_RANGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/NumberRangeValue.h"
#include "ClassLibrary/ComplexDataTypes/DoubleNumberRangeValue.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class NumberRange
{
public:
	LabelForDisplay label[];

	NumberRangeValue highCode;

	DoubleNumberRangeValue highCodeDouble;

	NumberRangeValue lowCode;

	DoubleNumberRangeValue lowCodeDouble;

	Primitive Package::String regExp;

	Primitive Package::Boolean isTopCoded;

	Primitive Package::Boolean isBottomCoded;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
