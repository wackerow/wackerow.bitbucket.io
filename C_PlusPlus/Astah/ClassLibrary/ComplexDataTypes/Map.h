#ifndef CLASSLIBRARY_COMPLEXDATATYPES_MAP_H
#define CLASSLIBRARY_COMPLEXDATATYPES_MAP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/ComplexDataTypes/CorrespondenceType.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/CollectionsPattern/ComparisonMap.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Map
{
public:
	DateRange validDates;

	CorrespondenceType hasCorrespondenceType;

	LabelForDisplay displayLabel;

	InternationalStructuredString usage;

	ClassLibrary::CollectionsPattern::ComparisonMap comparisonMap[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::Conceptual::Concept concept[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
