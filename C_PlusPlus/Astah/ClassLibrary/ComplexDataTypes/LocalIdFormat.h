#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LOCAL_ID_FORMAT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LOCAL_ID_FORMAT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LocalIdFormat
{
public:
	Primitive Package::String localIdValue;

	Primitive Package::String localIdType;

	Primitive Package::String localIdVersion;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
