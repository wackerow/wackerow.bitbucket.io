#ifndef CLASSLIBRARY_COMPLEXDATATYPES_EMAIL_H
#define CLASSLIBRARY_COMPLEXDATATYPES_EMAIL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Email
{
public:
	Primitive Package::String internetEmail;

	ExternalControlledVocabularyEntry typeOfEmail;

	DateRange effectiveDates;

	ExternalControlledVocabularyEntry privacy;

	Primitive Package::Boolean isPreferred;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
