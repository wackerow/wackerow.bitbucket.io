#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_SERIES_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CLASSIFICATION_SERIES_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/Representations/ClassificationSeries.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ClassificationSeriesRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::Representations::ClassificationSeries classificationSeries[];
	ClassLibrary::Representations::ClassificationSeries classificationSeries[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
