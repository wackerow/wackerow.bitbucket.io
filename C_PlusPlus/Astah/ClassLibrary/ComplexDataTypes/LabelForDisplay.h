#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LABEL_FOR_DISPLAY_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LABEL_FOR_DISPLAY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LabelForDisplay : public InternationalStructuredString
{
public:
	ExternalControlledVocabularyEntry locationVariant;

	DateRange validDates;

	Primitive Package::Integer maxLength;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
