#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LOGICAL_RECORD_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LOGICAL_RECORD_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LogicalRecordRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
