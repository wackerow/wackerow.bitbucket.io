#ifndef CLASSLIBRARY_COMPLEXDATATYPES_AREA_COVERAGE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_AREA_COVERAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AreaCoverage
{
public:
	ExternalControlledVocabularyEntry typeOfArea;

	ExternalControlledVocabularyEntry measurementUnit;

	Primitive Package::Real areaMeasure;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
