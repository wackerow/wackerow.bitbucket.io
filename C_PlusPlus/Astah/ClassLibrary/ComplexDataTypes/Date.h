#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DATE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DATE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/NonIsoDateType.h"
#include "ClassLibrary/EnumerationsRegExp/IsoDateType.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Date
{
public:
	ClassLibrary::EnumerationsRegExp::IsoDateType isoDate;

	NonIsoDateType nonIsoDate[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
