#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CODE_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CODE_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CategoryStatistic.h"
#include "ClassLibrary/Representations/Code.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CodeIndicator
{
public:
	Primitive Package::Integer index;

	Primitive Package::Integer isInLevel;

	CategoryStatistic categoryStatistic;
	ClassLibrary::Representations::Code code[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
