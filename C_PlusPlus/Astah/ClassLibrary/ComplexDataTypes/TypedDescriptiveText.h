#ifndef CLASSLIBRARY_COMPLEXDATATYPES_TYPED_DESCRIPTIVE_TEXT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_TYPED_DESCRIPTIVE_TEXT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class TypedDescriptiveText
{
public:
	ExternalControlledVocabularyEntry typeOfContent;

	InternationalStructuredString descriptiveText;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
