#ifndef CLASSLIBRARY_COMPLEXDATATYPES_IMAGE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_IMAGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"
#include "ClassLibrary/XMLSchemaDatatypes/LanguageSpecification.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Image
{
public:
	ClassLibrary::XMLSchemaDatatypes::anyUri uri;

	ExternalControlledVocabularyEntry typeOfImage;

	Primitive Package::Integer dpi;

	ClassLibrary::XMLSchemaDatatypes::LanguageSpecification languageOfImage;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
