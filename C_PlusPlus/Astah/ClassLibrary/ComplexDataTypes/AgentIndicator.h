#ifndef CLASSLIBRARY_COMPLEXDATATYPES_AGENT_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_AGENT_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "ClassLibrary/Agents/Agent.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class AgentIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
