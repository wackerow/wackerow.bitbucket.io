#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LINE_PARAMETER_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LINE_PARAMETER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LineParameter
{
public:
	Primitive Package::Integer startLine;

	Primitive Package::Integer startOffset;

	Primitive Package::Integer endLine;

	Primitive Package::Integer endOffset;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
