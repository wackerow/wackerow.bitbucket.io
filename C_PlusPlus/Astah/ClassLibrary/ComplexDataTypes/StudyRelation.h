#ifndef CLASSLIBRARY_COMPLEXDATATYPES_STUDY_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_STUDY_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class StudyRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::StudyRelated::Study study[];
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
