#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CATEGORY_STATISTIC_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CATEGORY_STATISTIC_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/Statistic.h"
#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/ComplexDataTypes/CodeIndicator.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CategoryStatistic
{
public:
	ExternalControlledVocabularyEntry typeOfCategoryStatistic;

	Statistic hasStatistic[];

	ValueString categoryValue;

	ValueString filterValue;

	CodeIndicator codeIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
