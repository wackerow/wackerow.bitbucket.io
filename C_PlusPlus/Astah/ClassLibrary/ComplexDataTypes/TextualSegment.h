#ifndef CLASSLIBRARY_COMPLEXDATATYPES_TEXTUAL_SEGMENT_H
#define CLASSLIBRARY_COMPLEXDATATYPES_TEXTUAL_SEGMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LineParameter.h"
#include "ClassLibrary/ComplexDataTypes/CharacterOffset.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class TextualSegment
{
public:
	LineParameter lineParamenter;

	CharacterOffset characterParameter;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
