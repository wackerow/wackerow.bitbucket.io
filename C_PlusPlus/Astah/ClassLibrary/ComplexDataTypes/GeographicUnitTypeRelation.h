#ifndef CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_TYPE_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_TYPE_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/EnumerationsRegExp/SpatialRelationSpecification.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class GeographicUnitTypeRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ClassLibrary::EnumerationsRegExp::SpatialRelationSpecification hasSpatialRelationSpecification;

	Primitive Package::Boolean isExhaustiveCoverage;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::Conceptual::UnitType unitType[];
	ClassLibrary::Conceptual::UnitType unitType[];
	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
