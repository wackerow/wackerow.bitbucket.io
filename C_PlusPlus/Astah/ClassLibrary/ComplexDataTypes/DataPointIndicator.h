#ifndef CLASSLIBRARY_COMPLEXDATATYPES_DATA_POINT_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_DATA_POINT_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/LogicalDataDescription/DataPoint.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class DataPointIndicator
{
public:
	Primitive Package::Integer index;

	ClassLibrary::LogicalDataDescription::DataPoint dataPoint[];
	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
