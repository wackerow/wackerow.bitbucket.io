#ifndef CLASSLIBRARY_COMPLEXDATATYPES_COMMAND_CODE_H
#define CLASSLIBRARY_COMPLEXDATATYPES_COMMAND_CODE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/CommandFile.h"
#include "ClassLibrary/ComplexDataTypes/Command.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class CommandCode
{
public:
	InternationalStructuredString description;

	CommandFile usesCommandFile[];

	Command usesCommand[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
