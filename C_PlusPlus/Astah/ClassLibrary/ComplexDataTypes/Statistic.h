#ifndef CLASSLIBRARY_COMPLEXDATATYPES_STATISTIC_H
#define CLASSLIBRARY_COMPLEXDATATYPES_STATISTIC_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/EnumerationsRegExp/ComputationBaseList.h"
#include "Primitive Package/Boolean.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class Statistic
{
public:
	Primitive Package::Boolean isWeighted;

	ClassLibrary::EnumerationsRegExp::ComputationBaseList computationBase;

	Primitive Package::Real decimalValue;

	Primitive Package::Real doubleValue;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
