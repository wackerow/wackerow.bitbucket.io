#ifndef CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_TYPE_INDICATOR_H
#define CLASSLIBRARY_COMPLEXDATATYPES_GEOGRAPHIC_UNIT_TYPE_INDICATOR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/CollectionsPattern/MemberIndicator.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class GeographicUnitTypeIndicator
{
public:
	Primitive Package::Integer index;

	Primitive Package::Integer isInLevel;

	ClassLibrary::CollectionsPattern::MemberIndicator memberIndicator[];
	ClassLibrary::Conceptual::UnitType unitType[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
