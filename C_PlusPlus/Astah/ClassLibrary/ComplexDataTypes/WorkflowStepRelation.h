#ifndef CLASSLIBRARY_COMPLEXDATATYPES_WORKFLOW_STEP_RELATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_WORKFLOW_STEP_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/EnumerationsRegExp/TemporalRelationSpecification.h"
#include "ClassLibrary/Workflows/WorkflowStep.h"
#include "ClassLibrary/CollectionsPattern/MemberRelation.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class WorkflowStepRelation
{
public:
	RelationSpecification hasRelationSpecification;

	ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::EnumerationsRegExp::TemporalRelationSpecification hasTemporalRelationSpecification;

	ClassLibrary::CollectionsPattern::MemberRelation memberRelation[];
	ClassLibrary::Workflows::WorkflowStep workflowStep[];
	ClassLibrary::Workflows::WorkflowStep workflowStep[];
};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
