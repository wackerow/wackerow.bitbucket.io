#ifndef CLASSLIBRARY_COMPLEXDATATYPES_WEB_LINK_H
#define CLASSLIBRARY_COMPLEXDATATYPES_WEB_LINK_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class WebLink
{
public:
	Primitive Package::Boolean isPreferred;

	ClassLibrary::XMLSchemaDatatypes::anyUri uri;

	ExternalControlledVocabularyEntry typeOfWebsite;

	DateRange effectiveDates;

	ExternalControlledVocabularyEntry privacy;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
