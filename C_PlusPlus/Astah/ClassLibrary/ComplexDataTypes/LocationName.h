#ifndef CLASSLIBRARY_COMPLEXDATATYPES_LOCATION_NAME_H
#define CLASSLIBRARY_COMPLEXDATATYPES_LOCATION_NAME_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class LocationName : public ObjectName
{
public:
	DateRange effectiveDates;

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
