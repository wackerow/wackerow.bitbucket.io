#ifndef CLASSLIBRARY_COMPLEXDATATYPES_CONTACT_INFORMATION_H
#define CLASSLIBRARY_COMPLEXDATATYPES_CONTACT_INFORMATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/WebLink.h"
#include "ClassLibrary/ComplexDataTypes/Email.h"
#include "ClassLibrary/ComplexDataTypes/ElectronicMessageSystem.h"
#include "ClassLibrary/ComplexDataTypes/Address.h"
#include "ClassLibrary/ComplexDataTypes/Telephone.h"

namespace ClassLibrary
{
namespace ComplexDataTypes
{
class ContactInformation
{
public:
	WebLink website[];

	Email hasEmail[];

	ElectronicMessageSystem electronicMessaging[];

	Address hasAddress[];

	Telephone hasTelephone[];

};

}  // namespace ComplexDataTypes
}  // namespace ClassLibrary
#endif
