#ifndef CLASSLIBRARY_WORKFLOWS_IF_THEN_ELSE_H
#define CLASSLIBRARY_WORKFLOWS_IF_THEN_ELSE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ElseIfAction.h"
#include "ClassLibrary/Workflows/ConditionalControlConstruct.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"

namespace ClassLibrary
{
namespace Workflows
{
class IfThenElse : public ConditionalControlConstruct
{
public:
	ClassLibrary::ComplexDataTypes::ElseIfAction elseIf[];

	WorkflowStepSequence workflowStepSequence;
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
