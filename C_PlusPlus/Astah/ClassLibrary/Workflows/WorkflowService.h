#ifndef CLASSLIBRARY_WORKFLOWS_WORKFLOW_SERVICE_H
#define CLASSLIBRARY_WORKFLOWS_WORKFLOW_SERVICE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/ProcessPattern/Service.h"
#include "ClassLibrary/Workflows/WorkflowStep.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace Workflows
{
class WorkflowService : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry serviceInterface[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry serviceLocation;

	ClassLibrary::ComplexDataTypes::Date estimatedDuration;

	WorkflowStep workflowStep;
	ClassLibrary::ProcessPattern::Service service[];
	ClassLibrary::Agents::Agent agent[];
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
