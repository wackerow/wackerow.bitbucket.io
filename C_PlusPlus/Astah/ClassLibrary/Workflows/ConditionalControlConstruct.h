#ifndef CLASSLIBRARY_WORKFLOWS_CONDITIONAL_CONTROL_CONSTRUCT_H
#define CLASSLIBRARY_WORKFLOWS_CONDITIONAL_CONTROL_CONSTRUCT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/Workflows/WorkflowStep.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"

namespace ClassLibrary
{
namespace Workflows
{
class ConditionalControlConstruct : public WorkflowStep
{
public:
	ClassLibrary::ComplexDataTypes::CommandCode condition;

	WorkflowStepSequence workflowStepSequence;
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
