#ifndef CLASSLIBRARY_WORKFLOWS_WORKFLOW_STEP_H
#define CLASSLIBRARY_WORKFLOWS_WORKFLOW_STEP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Binding.h"
#include "ClassLibrary/ComplexDataTypes/WorkflowStepIndicator.h"
#include "ClassLibrary/ComplexDataTypes/WorkflowStepRelation.h"
#include "ClassLibrary/ProcessPattern/ProcessStep.h"
#include "ClassLibrary/Workflows/Parameter.h"
#include "ClassLibrary/Workflows/WorkflowService.h"
#include "ClassLibrary/Workflows/Split.h"
#include "ClassLibrary/Workflows/SplitJoin.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Workflows
{
class WorkflowStep : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::ComplexDataTypes::Binding hasInformationFlow[];

	Parameter parameter[];
	Parameter parameter[];
	WorkflowService workflowService[];
	Split split[];
	SplitJoin splitJoin[];
	ClassLibrary::ProcessPattern::ProcessStep processStep[];
	ClassLibrary::ComplexDataTypes::WorkflowStepIndicator workflowStepIndicator;
	ClassLibrary::ComplexDataTypes::WorkflowStepRelation workflowStepRelation[];
	ClassLibrary::ComplexDataTypes::WorkflowStepRelation workflowStepRelation;
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
