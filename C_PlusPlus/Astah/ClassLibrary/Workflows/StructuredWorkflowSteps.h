#ifndef CLASSLIBRARY_WORKFLOWS_STRUCTURED_WORKFLOW_STEPS_H
#define CLASSLIBRARY_WORKFLOWS_STRUCTURED_WORKFLOW_STEPS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Workflows/WorkflowStepSequence.h"
#include "ClassLibrary/Workflows/WorkflowStepRelationStructure.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"

namespace ClassLibrary
{
namespace Workflows
{
class StructuredWorkflowSteps : public WorkflowStepSequence
{
public:
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	WorkflowStepRelationStructure workflowStepRelationStructure[];
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
