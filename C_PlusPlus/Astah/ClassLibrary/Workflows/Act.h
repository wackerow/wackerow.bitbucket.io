#ifndef CLASSLIBRARY_WORKFLOWS_ACT_H
#define CLASSLIBRARY_WORKFLOWS_ACT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/Workflows/WorkflowStep.h"

namespace ClassLibrary
{
namespace Workflows
{
class Act : public WorkflowStep
{
public:
	ClassLibrary::Conceptual::InstanceVariable instanceVariable;
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
