#ifndef CLASSLIBRARY_WORKFLOWS_PARAMETER_H
#define CLASSLIBRARY_WORKFLOWS_PARAMETER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Binding.h"
#include "ClassLibrary/ProcessPattern/ProcessStep.h"
#include "ClassLibrary/SamplingMethodology/SampleFrame.h"
#include "ClassLibrary/Workflows/WorkflowStep.h"
#include "ClassLibrary/Representations/ValueDomain.h"
#include "ClassLibrary/DataCapture/ResponseDomain.h"
#include "ClassLibrary/Methodologies/Result.h"
#include "Primitive Package/UnlimitedNatural.h"
#include "Primitive Package/Boolean.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace Workflows
{
class Parameter
{
public:
	Primitive Package::UnlimitedNatural alias;

	ClassLibrary::ComplexDataTypes::ValueString defaultValue;

	Primitive Package::Boolean isArray;

	Primitive Package::UnlimitedNatural limitArrayIndex;

	Primitive Package::String agency;

	Primitive Package::String id;

	Primitive Package::String version;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::Methodologies::Result result[];
	WorkflowStep workflowStep;
	ClassLibrary::DataCapture::ResponseDomain responseDomain;
	WorkflowStep workflowStep;
	ClassLibrary::Representations::ValueDomain valueDomain[];
	ClassLibrary::ComplexDataTypes::Binding binding;
	ClassLibrary::ProcessPattern::ProcessStep processStep;
	ClassLibrary::ProcessPattern::ProcessStep processStep;
	ClassLibrary::SamplingMethodology::SampleFrame sampleFrame;
	ClassLibrary::ComplexDataTypes::Binding binding;
	ClassLibrary::SamplingMethodology::SampleFrame sampleFrame;
	ClassLibrary::Methodologies::Result result[];
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
