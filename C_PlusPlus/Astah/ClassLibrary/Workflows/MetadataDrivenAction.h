#ifndef CLASSLIBRARY_WORKFLOWS_METADATA_DRIVEN_ACTION_H
#define CLASSLIBRARY_WORKFLOWS_METADATA_DRIVEN_ACTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Workflows/Act.h"

namespace ClassLibrary
{
namespace Workflows
{
class MetadataDrivenAction : public Act
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString activityDescription;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfMetadataDrivenAction;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString quasiVTL;

};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
