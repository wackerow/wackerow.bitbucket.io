#ifndef CLASSLIBRARY_WORKFLOWS_SPLIT_H
#define CLASSLIBRARY_WORKFLOWS_SPLIT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Workflows/WorkflowStep.h"

namespace ClassLibrary
{
namespace Workflows
{
class Split : public WorkflowStep
{
public:
	WorkflowStep workflowStep[];
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
