#ifndef CLASSLIBRARY_WORKFLOWS_LOOP_H
#define CLASSLIBRARY_WORKFLOWS_LOOP_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/Workflows/ConditionalControlConstruct.h"

namespace ClassLibrary
{
namespace Workflows
{
class Loop : public ConditionalControlConstruct
{
public:
	ClassLibrary::ComplexDataTypes::CommandCode initialValue;

	ClassLibrary::ComplexDataTypes::CommandCode stepValue;

};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
