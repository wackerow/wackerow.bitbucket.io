#ifndef CLASSLIBRARY_WORKFLOWS_REPEAT_WHILE_H
#define CLASSLIBRARY_WORKFLOWS_REPEAT_WHILE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Workflows/ConditionalControlConstruct.h"

namespace ClassLibrary
{
namespace Workflows
{
class RepeatWhile : public ConditionalControlConstruct
{
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
