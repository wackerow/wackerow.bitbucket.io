#ifndef CLASSLIBRARY_WORKFLOWS_WORKFLOW_STEP_RELATION_STRUCTURE_H
#define CLASSLIBRARY_WORKFLOWS_WORKFLOW_STEP_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/WorkflowStepRelation.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/EnumerationsRegExp/TemporalRelationSpecification.h"
#include "ClassLibrary/Workflows/StructuredWorkflowSteps.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Workflows
{
class WorkflowStepRelationStructure : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::WorkflowStepRelation hasMemberRelation[];

	ClassLibrary::EnumerationsRegExp::TemporalRelationSpecification hasTemporalRelationSpecification;

	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
	StructuredWorkflowSteps structuredWorkflowSteps[];
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
