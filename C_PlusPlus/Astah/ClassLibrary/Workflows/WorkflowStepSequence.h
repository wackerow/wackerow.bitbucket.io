#ifndef CLASSLIBRARY_WORKFLOWS_WORKFLOW_STEP_SEQUENCE_H
#define CLASSLIBRARY_WORKFLOWS_WORKFLOW_STEP_SEQUENCE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/WorkflowStepIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/WorkflowStepSequenceIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ElseIfAction.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcess.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/Workflows/IfThenElse.h"
#include "ClassLibrary/Workflows/ConditionalControlConstruct.h"
#include "ClassLibrary/Workflows/WorkflowProcess.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Workflows
{
class WorkflowStepSequence : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfWorkflowStepSequence[];

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::WorkflowStepIndicator contains[];

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	Primitive Package::Boolean isOrdered;

	IfThenElse ifThenElse[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::SimpleCollection simpleCollection[];
	ClassLibrary::ComplexDataTypes::WorkflowStepSequenceIndicator workflowStepSequenceIndicator;
	ClassLibrary::SamplingMethodology::SamplingProcess samplingProcess[];
	ConditionalControlConstruct conditionalControlConstruct[];
	ClassLibrary::ComplexDataTypes::ElseIfAction elseIfAction;
	WorkflowProcess workflowProcess[];
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
