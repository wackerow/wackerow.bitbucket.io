#ifndef CLASSLIBRARY_WORKFLOWS_WORKFLOW_MASTER_SEQUENCE_H
#define CLASSLIBRARY_WORKFLOWS_WORKFLOW_MASTER_SEQUENCE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/WorkflowStepSequenceIndicator.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"

namespace ClassLibrary
{
namespace Workflows
{
class WorkflowMasterSequence : public WorkflowStepSequence
{
public:
	ClassLibrary::ComplexDataTypes::WorkflowStepSequenceIndicator contains[];

};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
