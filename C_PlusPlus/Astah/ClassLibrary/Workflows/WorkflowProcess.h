#ifndef CLASSLIBRARY_WORKFLOWS_WORKFLOW_PROCESS_H
#define CLASSLIBRARY_WORKFLOWS_WORKFLOW_PROCESS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/ProcessPattern/Process.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Workflows
{
class WorkflowProcess : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::SimpleMethodologyOverview::DesignOverview designOverview[];
	ClassLibrary::SimpleMethodologyOverview::AlgorithmOverview algorithmOverview[];
	ClassLibrary::ProcessPattern::Process process[];
	WorkflowStepSequence workflowStepSequence;
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
