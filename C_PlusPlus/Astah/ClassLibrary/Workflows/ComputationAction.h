#ifndef CLASSLIBRARY_WORKFLOWS_COMPUTATION_ACTION_H
#define CLASSLIBRARY_WORKFLOWS_COMPUTATION_ACTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Workflows/Act.h"

namespace ClassLibrary
{
namespace Workflows
{
class ComputationAction : public Act
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString activityDescription;

	ClassLibrary::ComplexDataTypes::CommandCode usesCommandCode;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfComputation;

};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
