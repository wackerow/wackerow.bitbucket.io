#ifndef CLASSLIBRARY_WORKFLOWS_REPEAT_UNTIL_H
#define CLASSLIBRARY_WORKFLOWS_REPEAT_UNTIL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Workflows/ConditionalControlConstruct.h"

namespace ClassLibrary
{
namespace Workflows
{
class RepeatUntil : public ConditionalControlConstruct
{
};

}  // namespace Workflows
}  // namespace ClassLibrary
#endif
