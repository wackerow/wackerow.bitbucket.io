#ifndef CLASSLIBRARY_DATACAPTURE_NUMERIC_RESPONSE_DOMAIN_H
#define CLASSLIBRARY_DATACAPTURE_NUMERIC_RESPONSE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/NumberRange.h"
#include "ClassLibrary/DataCapture/ResponseDomain.h"

namespace ClassLibrary
{
namespace DataCapture
{
class NumericResponseDomain : public ResponseDomain
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry numericTypeCode;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry unit;

	ClassLibrary::ComplexDataTypes::NumberRange usesNumberRange[];

};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
