#ifndef CLASSLIBRARY_DATACAPTURE_TEXT_RESPONSE_DOMAIN_H
#define CLASSLIBRARY_DATACAPTURE_TEXT_RESPONSE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/TypedString.h"
#include "ClassLibrary/DataCapture/ResponseDomain.h"
#include "Primitive Package/Integer.h"

namespace ClassLibrary
{
namespace DataCapture
{
class TextResponseDomain : public ResponseDomain
{
public:
	Primitive Package::Integer maximumLength;

	Primitive Package::Integer minimumLength;

	ClassLibrary::ComplexDataTypes::TypedString regularExpression;

};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
