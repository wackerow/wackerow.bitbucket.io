#ifndef CLASSLIBRARY_DATACAPTURE_INSTRUMENT_COMPONENT_H
#define CLASSLIBRARY_DATACAPTURE_INSTRUMENT_COMPONENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Workflows/Act.h"
#include "ClassLibrary/DataCapture/ExternalAid.h"
#include "ClassLibrary/DataCapture/Instruction.h"

namespace ClassLibrary
{
namespace DataCapture
{
class InstrumentComponent : public ClassLibrary::Workflows::Act
{
public:
	ExternalAid externalAid[];
	Instruction instruction[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
