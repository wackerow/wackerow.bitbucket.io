#ifndef CLASSLIBRARY_DATACAPTURE_REPRESENTED_QUESTION_H
#define CLASSLIBRARY_DATACAPTURE_REPRESENTED_QUESTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DynamicText.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/DataCapture/Capture.h"
#include "ClassLibrary/DataCapture/InstanceQuestion.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace DataCapture
{
class RepresentedQuestion : public Capture
{
public:
	ClassLibrary::ComplexDataTypes::DynamicText questionText[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString questionIntent;

	Primitive Package::Real estimatedResponseTimeInSeconds;

	InstanceQuestion instanceQuestion;
	ClassLibrary::Conceptual::RepresentedVariable representedVariable[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
