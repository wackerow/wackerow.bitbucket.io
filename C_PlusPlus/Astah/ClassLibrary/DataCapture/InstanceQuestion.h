#ifndef CLASSLIBRARY_DATACAPTURE_INSTANCE_QUESTION_H
#define CLASSLIBRARY_DATACAPTURE_INSTANCE_QUESTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/DataCapture/InstrumentComponent.h"
#include "ClassLibrary/DataCapture/RepresentedQuestion.h"

namespace ClassLibrary
{
namespace DataCapture
{
class InstanceQuestion : public InstrumentComponent
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	RepresentedQuestion representedQuestion[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
