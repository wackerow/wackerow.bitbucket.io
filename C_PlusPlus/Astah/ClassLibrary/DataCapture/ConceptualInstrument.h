#ifndef CLASSLIBRARY_DATACAPTURE_CONCEPTUAL_INSTRUMENT_H
#define CLASSLIBRARY_DATACAPTURE_CONCEPTUAL_INSTRUMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/Workflows/WorkflowProcess.h"
#include "ClassLibrary/DataCapture/ImplementedInstrument.h"
#include "ClassLibrary/DataCapture/Capture.h"

namespace ClassLibrary
{
namespace DataCapture
{
class ConceptualInstrument : public ClassLibrary::Workflows::WorkflowProcess
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString description;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ImplementedInstrument implementedInstrument[];
	Capture capture[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
