#ifndef CLASSLIBRARY_DATACAPTURE_REPRESENTED_MEASUREMENT_H
#define CLASSLIBRARY_DATACAPTURE_REPRESENTED_MEASUREMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/DataCapture/Capture.h"
#include "ClassLibrary/DataCapture/InstanceMeasurement.h"

namespace ClassLibrary
{
namespace DataCapture
{
class RepresentedMeasurement : public Capture
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry measurementType;

	InstanceMeasurement instanceMeasurement;
	ClassLibrary::Conceptual::RepresentedVariable representedVariable[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
