#ifndef CLASSLIBRARY_DATACAPTURE_INSTANCE_MEASUREMENT_H
#define CLASSLIBRARY_DATACAPTURE_INSTANCE_MEASUREMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/DataCapture/InstrumentComponent.h"
#include "ClassLibrary/DataCapture/RepresentedMeasurement.h"

namespace ClassLibrary
{
namespace DataCapture
{
class InstanceMeasurement : public InstrumentComponent
{
public:
	RepresentedMeasurement representedMeasurement[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
