#ifndef CLASSLIBRARY_DATACAPTURE_SCALE_RESPONSE_DOMAIN_H
#define CLASSLIBRARY_DATACAPTURE_SCALE_RESPONSE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/DataCapture/ResponseDomain.h"

namespace ClassLibrary
{
namespace DataCapture
{
class ScaleResponseDomain : public ResponseDomain
{
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
