#ifndef CLASSLIBRARY_DATACAPTURE_INSTRUMENT_CODE_H
#define CLASSLIBRARY_DATACAPTURE_INSTRUMENT_CODE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/DataCapture/InstrumentComponent.h"

namespace ClassLibrary
{
namespace DataCapture
{
class InstrumentCode : public InstrumentComponent
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry purposeOfCode;

	ClassLibrary::ComplexDataTypes::CommandCode usesCommandCode;

};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
