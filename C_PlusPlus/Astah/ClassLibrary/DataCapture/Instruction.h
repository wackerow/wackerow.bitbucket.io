#ifndef CLASSLIBRARY_DATACAPTURE_INSTRUCTION_H
#define CLASSLIBRARY_DATACAPTURE_INSTRUCTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DynamicText.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/DataCapture/Capture.h"
#include "ClassLibrary/DataCapture/InstrumentComponent.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace DataCapture
{
class Instruction : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::DynamicText instructionText[];

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	Capture capture[];
	InstrumentComponent instrumentComponent[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
