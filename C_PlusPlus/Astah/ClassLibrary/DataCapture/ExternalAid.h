#ifndef CLASSLIBRARY_DATACAPTURE_EXTERNAL_AID_H
#define CLASSLIBRARY_DATACAPTURE_EXTERNAL_AID_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/DataCapture/InstrumentComponent.h"
#include "ClassLibrary/DataCapture/Capture.h"

namespace ClassLibrary
{
namespace DataCapture
{
class ExternalAid : public ClassLibrary::Utility::ExternalMaterial
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry stimulusType;

	InstrumentComponent instrumentComponent[];
	Capture capture[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
