#ifndef CLASSLIBRARY_DATACAPTURE_CAPTURE_H
#define CLASSLIBRARY_DATACAPTURE_CAPTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/DataCapture/ResponseDomain.h"
#include "ClassLibrary/DataCapture/Instruction.h"
#include "ClassLibrary/DataCapture/ExternalAid.h"
#include "ClassLibrary/DataCapture/ConceptualInstrument.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace DataCapture
{
class Capture : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry captureSource;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry analysisUnit[];

	ResponseDomain responseDomain[];
	Instruction instruction[];
	ExternalAid externalAid[];
	ClassLibrary::Conceptual::Concept concept[];
	ConceptualInstrument conceptualInstrument;
	ClassLibrary::Conceptual::InstanceVariable instanceVariable;
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
