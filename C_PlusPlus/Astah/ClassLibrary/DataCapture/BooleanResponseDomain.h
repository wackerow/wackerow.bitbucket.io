#ifndef CLASSLIBRARY_DATACAPTURE_BOOLEAN_RESPONSE_DOMAIN_H
#define CLASSLIBRARY_DATACAPTURE_BOOLEAN_RESPONSE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/Category.h"
#include "ClassLibrary/DataCapture/ResponseDomain.h"

namespace ClassLibrary
{
namespace DataCapture
{
class BooleanResponseDomain : public ResponseDomain
{
public:
	ClassLibrary::Conceptual::Category category[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
