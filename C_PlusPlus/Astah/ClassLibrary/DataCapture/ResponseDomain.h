#ifndef CLASSLIBRARY_DATACAPTURE_RESPONSE_DOMAIN_H
#define CLASSLIBRARY_DATACAPTURE_RESPONSE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/Workflows/Parameter.h"
#include "ClassLibrary/DataCapture/Capture.h"
#include "ClassLibrary/DataCapture/CodeListResponseDomain.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace DataCapture
{
class ResponseDomain : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel;

	ClassLibrary::Workflows::Parameter parameter[];
	Capture capture[];
	ClassLibrary::Conceptual::RepresentedVariable representedVariable[];
	CodeListResponseDomain codeListResponseDomain;
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
