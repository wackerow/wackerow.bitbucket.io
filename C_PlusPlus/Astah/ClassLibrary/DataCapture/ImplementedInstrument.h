#ifndef CLASSLIBRARY_DATACAPTURE_IMPLEMENTED_INSTRUMENT_H
#define CLASSLIBRARY_DATACAPTURE_IMPLEMENTED_INSTRUMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/DataCapture/ConceptualInstrument.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"

namespace ClassLibrary
{
namespace DataCapture
{
class ImplementedInstrument : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfInstrument;

	ClassLibrary::XMLSchemaDatatypes::anyUri uri[];

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage[];

	ConceptualInstrument conceptualInstrument;
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
