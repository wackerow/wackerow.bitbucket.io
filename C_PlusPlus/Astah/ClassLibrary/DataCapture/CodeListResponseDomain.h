#ifndef CLASSLIBRARY_DATACAPTURE_CODE_LIST_RESPONSE_DOMAIN_H
#define CLASSLIBRARY_DATACAPTURE_CODE_LIST_RESPONSE_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Representations/Code.h"
#include "ClassLibrary/Representations/CodeList.h"
#include "ClassLibrary/DataCapture/ResponseDomain.h"

namespace ClassLibrary
{
namespace DataCapture
{
class CodeListResponseDomain : public ResponseDomain
{
public:
	ClassLibrary::Representations::Code code[];
	ClassLibrary::Representations::CodeList codeList[];
	ResponseDomain responseDomain[];
};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
