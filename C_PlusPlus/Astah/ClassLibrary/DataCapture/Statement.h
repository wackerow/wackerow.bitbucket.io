#ifndef CLASSLIBRARY_DATACAPTURE_STATEMENT_H
#define CLASSLIBRARY_DATACAPTURE_STATEMENT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/DynamicText.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/DataCapture/InstrumentComponent.h"

namespace ClassLibrary
{
namespace DataCapture
{
class Statement : public InstrumentComponent
{
public:
	ClassLibrary::ComplexDataTypes::DynamicText statementText[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry purposeOfStatement;

};

}  // namespace DataCapture
}  // namespace ClassLibrary
#endif
