#ifndef CLASSLIBRARY_METHODOLOGYPATTERN_DESIGN_H
#define CLASSLIBRARY_METHODOLOGYPATTERN_DESIGN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/MethodologyPattern/Algorithm.h"
#include "ClassLibrary/MethodologyPattern/Methodology.h"
#include "ClassLibrary/ProcessPattern/Process.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Methodologies/Precondition.h"
#include "ClassLibrary/Methodologies/Goal.h"

namespace ClassLibrary
{
namespace MethodologyPattern
{
class Design : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::Methodologies::Precondition precondition[];
	Algorithm algorithm[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassLibrary::ProcessPattern::Process process[];
	Methodology methodology[];
	ClassLibrary::SimpleMethodologyOverview::DesignOverview designOverview;
	ClassLibrary::Methodologies::Goal goal[];
};

}  // namespace MethodologyPattern
}  // namespace ClassLibrary
#endif
