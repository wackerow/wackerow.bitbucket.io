#ifndef CLASSLIBRARY_METHODOLOGYPATTERN_ALGORITHM_H
#define CLASSLIBRARY_METHODOLOGYPATTERN_ALGORITHM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/MethodologyPattern/Design.h"
#include "ClassLibrary/MethodologyPattern/Methodology.h"
#include "ClassLibrary/ProcessPattern/Process.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace MethodologyPattern
{
class Algorithm : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	ClassLibrary::SimpleMethodologyOverview::AlgorithmOverview algorithmOverview;
	Design design[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	ClassLibrary::ProcessPattern::Process process[];
	Methodology methodology[];
};

}  // namespace MethodologyPattern
}  // namespace ClassLibrary
#endif
