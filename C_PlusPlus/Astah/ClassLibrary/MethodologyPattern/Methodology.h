#ifndef CLASSLIBRARY_METHODOLOGYPATTERN_METHODOLOGY_H
#define CLASSLIBRARY_METHODOLOGYPATTERN_METHODOLOGY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/SimpleMethodologyOverview/MethodologyOverview.h"
#include "ClassLibrary/MethodologyPattern/Design.h"
#include "ClassLibrary/MethodologyPattern/Algorithm.h"
#include "ClassLibrary/ProcessPattern/Process.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace MethodologyPattern
{
class Methodology : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	Design design[];
	ClassLibrary::SimpleMethodologyOverview::MethodologyOverview methodologyOverview;
	Methodology methodology[];
	Methodology methodology[];
	Algorithm algorithm[];
	ClassLibrary::ProcessPattern::Process process[];
};

}  // namespace MethodologyPattern
}  // namespace ClassLibrary
#endif
