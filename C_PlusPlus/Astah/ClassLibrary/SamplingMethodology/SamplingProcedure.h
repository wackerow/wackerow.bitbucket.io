#ifndef CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_PROCEDURE_H
#define CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_PROCEDURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleMethodologyOverview/MethodologyOverview.h"
#include "ClassLibrary/SamplingMethodology/SamplingDesign.h"
#include "ClassLibrary/SamplingMethodology/SamplingAlgorithm.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcess.h"
#include "ClassLibrary/StudyRelated/Study.h"

namespace ClassLibrary
{
namespace SamplingMethodology
{
class SamplingProcedure : public ClassLibrary::SimpleMethodologyOverview::MethodologyOverview
{
public:
	SamplingDesign samplingDesign[];
	ClassLibrary::StudyRelated::Study study[];
	SamplingAlgorithm samplingAlgorithm[];
	SamplingProcedure samplingProcedure[];
	SamplingProcedure samplingProcedure[];
	SamplingProcess samplingProcess[];
};

}  // namespace SamplingMethodology
}  // namespace ClassLibrary
#endif
