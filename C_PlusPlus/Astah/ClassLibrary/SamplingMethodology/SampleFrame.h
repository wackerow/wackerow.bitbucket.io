#ifndef CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLE_FRAME_H
#define CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLE_FRAME_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Workflows/Parameter.h"
#include "ClassLibrary/Discovery/Access.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/Agent.h"

namespace ClassLibrary
{
namespace SamplingMethodology
{
class SampleFrame : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString limitations;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString updateProcedures;

	ClassLibrary::ComplexDataTypes::DateRange referencePeriod;

	ClassLibrary::ComplexDataTypes::DateRange validPeriod;

	SampleFrame sampleFrame[];
	SampleFrame sampleFrame[];
	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::Conceptual::UnitType unitType;
	ClassLibrary::Conceptual::Population population;
	ClassLibrary::Workflows::Parameter parameter[];
	ClassLibrary::Conceptual::UnitType unitType[];
	ClassLibrary::Discovery::Access access[];
	ClassLibrary::Workflows::Parameter parameter[];
};

}  // namespace SamplingMethodology
}  // namespace ClassLibrary
#endif
