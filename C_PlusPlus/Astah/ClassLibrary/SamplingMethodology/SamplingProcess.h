#ifndef CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_PROCESS_H
#define CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_PROCESS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ProcessPattern/Process.h"
#include "ClassLibrary/SamplingMethodology/SamplingDesign.h"
#include "ClassLibrary/SamplingMethodology/SamplePopulationResult.h"
#include "ClassLibrary/SamplingMethodology/SamplingAlgorithm.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcedure.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace SamplingMethodology
{
class SamplingProcess : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;

	SamplingDesign samplingDesign[];
	ClassLibrary::ProcessPattern::Process process[];
	ClassLibrary::Workflows::WorkflowStepSequence workflowStepSequence;
	SamplePopulationResult samplePopulationResult[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
	SamplingAlgorithm samplingAlgorithm[];
	SamplingProcedure samplingProcedure[];
};

}  // namespace SamplingMethodology
}  // namespace ClassLibrary
#endif
