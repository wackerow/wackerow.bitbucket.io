#ifndef CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLE_POPULATION_RESULT_H
#define CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLE_POPULATION_RESULT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Date.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcess.h"
#include "ClassLibrary/Methodologies/Result.h"
#include "Primitive Package/Integer.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace SamplingMethodology
{
class SamplePopulationResult : public ClassLibrary::Methodologies::Result
{
public:
	Primitive Package::Integer sizeOfSample;

	Primitive Package::String strataName;

	ClassLibrary::ComplexDataTypes::Date sampleDate;

	ClassLibrary::Conceptual::Population population[];
	ClassLibrary::Conceptual::UnitType unitType[];
	SamplingProcess samplingProcess[];
};

}  // namespace SamplingMethodology
}  // namespace ClassLibrary
#endif
