#ifndef CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_GOAL_H
#define CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_GOAL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/TargetSample.h"
#include "ClassLibrary/SamplingMethodology/SamplingDesign.h"
#include "ClassLibrary/Methodologies/Goal.h"
#include "Primitive Package/Integer.h"
#include "Primitive Package/Real.h"

namespace ClassLibrary
{
namespace SamplingMethodology
{
class SamplingGoal : public ClassLibrary::Methodologies::Goal
{
public:
	Primitive Package::Integer overallTargetSampleSize;

	Primitive Package::Real overallTargetSamplePercent;

	ClassLibrary::ComplexDataTypes::TargetSample targetSampleSize[];

	SamplingDesign samplingDesign[];
};

}  // namespace SamplingMethodology
}  // namespace ClassLibrary
#endif
