#ifndef CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_ALGORITHM_H
#define CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_ALGORITHM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CommandCode.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcedure.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcess.h"
#include "ClassLibrary/SamplingMethodology/SamplingDesign.h"

namespace ClassLibrary
{
namespace SamplingMethodology
{
class SamplingAlgorithm : public ClassLibrary::SimpleMethodologyOverview::AlgorithmOverview
{
public:
	ClassLibrary::ComplexDataTypes::CommandCode codifiedExpressionOfAlgorithm;

	SamplingProcedure samplingProcedure[];
	SamplingProcess samplingProcess[];
	SamplingDesign samplingDesign[];
};

}  // namespace SamplingMethodology
}  // namespace ClassLibrary
#endif
