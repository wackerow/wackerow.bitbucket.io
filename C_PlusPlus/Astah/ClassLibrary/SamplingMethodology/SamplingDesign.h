#ifndef CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_DESIGN_H
#define CLASSLIBRARY_SAMPLINGMETHODOLOGY_SAMPLING_DESIGN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcedure.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcess.h"
#include "ClassLibrary/SamplingMethodology/SamplingGoal.h"
#include "ClassLibrary/SamplingMethodology/SamplingAlgorithm.h"

namespace ClassLibrary
{
namespace SamplingMethodology
{
class SamplingDesign : public ClassLibrary::SimpleMethodologyOverview::DesignOverview
{
public:
	SamplingProcedure samplingProcedure[];
	SamplingProcess samplingProcess[];
	SamplingGoal samplingGoal[];
	SamplingAlgorithm samplingAlgorithm[];
};

}  // namespace SamplingMethodology
}  // namespace ClassLibrary
#endif
