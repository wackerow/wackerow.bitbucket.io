#ifndef CLASSLIBRARY_IDENTIFICATION_ANNOTATED_IDENTIFIABLE_H
#define CLASSLIBRARY_IDENTIFICATION_ANNOTATED_IDENTIFIABLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/Annotation.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Methodologies/AppliedUse.h"

namespace ClassLibrary
{
namespace Identification
{
class AnnotatedIdentifiable : public Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::Annotation hasAnnotation;

	ClassLibrary::Methodologies::AppliedUse appliedUse[];
	ClassLibrary::Utility::ExternalMaterial externalMaterial[];
};

}  // namespace Identification
}  // namespace ClassLibrary
#endif
