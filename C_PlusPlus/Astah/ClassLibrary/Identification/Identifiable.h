#ifndef CLASSLIBRARY_IDENTIFICATION_IDENTIFIABLE_H
#define CLASSLIBRARY_IDENTIFICATION_IDENTIFIABLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LocalIdFormat.h"
#include "ClassLibrary/ComplexDataTypes/BasedOnObjectInformation.h"
#include "ClassLibrary/ComplexDataTypes/DescribedRelationship.h"
#include "ClassLibrary/EnumerationsRegExp/IsoDateType.h"
#include "Primitive Package/String.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Identification
{
class Identifiable
{
public:
	Primitive Package::String agency;

	Primitive Package::String id;

	Primitive Package::String version;

	Primitive Package::String versionResponsibility;

	Primitive Package::String versionRationale;

	ClassLibrary::EnumerationsRegExp::IsoDateType versionDate;

	Primitive Package::Boolean isUniversallyUnique;

	Primitive Package::Boolean isPersistent;

	ClassLibrary::ComplexDataTypes::LocalIdFormat localId[];

	ClassLibrary::ComplexDataTypes::BasedOnObjectInformation basedOnObject;

	ClassLibrary::ComplexDataTypes::BasedOnObjectInformation basedOnObjectInformation[];
	ClassLibrary::ComplexDataTypes::DescribedRelationship describedRelationship;
};

}  // namespace Identification
}  // namespace ClassLibrary
#endif
