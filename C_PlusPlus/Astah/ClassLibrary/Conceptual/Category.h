#ifndef CLASSLIBRARY_CONCEPTUAL_CATEGORY_H
#define CLASSLIBRARY_CONCEPTUAL_CATEGORY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/CategoryRelation.h"
#include "ClassLibrary/ComplexDataTypes/CategoryIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Representations/Code.h"
#include "ClassLibrary/DataCapture/BooleanResponseDomain.h"

namespace ClassLibrary
{
namespace Conceptual
{
class Category : public Concept
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;

	ClassLibrary::Representations::Code code;
	ClassLibrary::ComplexDataTypes::CategoryRelation categoryRelation[];
	ClassLibrary::ComplexDataTypes::CategoryRelation categoryRelation;
	ClassLibrary::DataCapture::BooleanResponseDomain booleanResponseDomain;
	ClassLibrary::ComplexDataTypes::CategoryIndicator categoryIndicator;
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
