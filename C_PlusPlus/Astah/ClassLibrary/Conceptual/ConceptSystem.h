#ifndef CLASSLIBRARY_CONCEPTUAL_CONCEPT_SYSTEM_H
#define CLASSLIBRARY_CONCEPTUAL_CONCEPT_SYSTEM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ConceptIndicator.h"
#include "ClassLibrary/Conceptual/ConceptualDomain.h"
#include "ClassLibrary/Conceptual/ConceptSystemCorrespondence.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/ConceptRelationStructure.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitTypeClassification.h"
#include "ClassLibrary/GeographicClassification/GeographicUnitClassification.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Conceptual
{
class ConceptSystem : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::ConceptIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ConceptualDomain conceptualDomain;
	ConceptSystemCorrespondence conceptSystemCorrespondence[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	Concept concept[];
	ConceptRelationStructure conceptRelationStructure[];
	ClassLibrary::GeographicClassification::GeographicUnitTypeClassification geographicUnitTypeClassification[];
	ClassLibrary::GeographicClassification::GeographicUnitClassification geographicUnitClassification[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
