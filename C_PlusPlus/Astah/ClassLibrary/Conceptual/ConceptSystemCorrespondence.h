#ifndef CLASSLIBRARY_CONCEPTUAL_CONCEPT_SYSTEM_CORRESPONDENCE_H
#define CLASSLIBRARY_CONCEPTUAL_CONCEPT_SYSTEM_CORRESPONDENCE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Map.h"
#include "ClassLibrary/Conceptual/ConceptSystem.h"
#include "ClassLibrary/CollectionsPattern/Comparison.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Conceptual
{
class ConceptSystemCorrespondence : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::Map correspondence[];

	ConceptSystem conceptSystem[];
	ClassLibrary::CollectionsPattern::Comparison comparison[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
