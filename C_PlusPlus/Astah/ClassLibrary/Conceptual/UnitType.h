#ifndef CLASSLIBRARY_CONCEPTUAL_UNIT_TYPE_H
#define CLASSLIBRARY_CONCEPTUAL_UNIT_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/TargetSample.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitTypeRelation.h"
#include "ClassLibrary/ComplexDataTypes/GeographicUnitTypeIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/Unit.h"
#include "ClassLibrary/Conceptual/Universe.h"
#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/SamplingMethodology/SampleFrame.h"
#include "ClassLibrary/SamplingMethodology/SamplePopulationResult.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Discovery/SpatialCoverage.h"
#include "ClassLibrary/Methodologies/AppliedUse.h"

namespace ClassLibrary
{
namespace Conceptual
{
class UnitType : public Concept
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;

	ClassLibrary::ComplexDataTypes::TargetSample targetSample[];
	ClassLibrary::Discovery::SpatialCoverage spatialCoverage[];
	ClassLibrary::Discovery::SpatialCoverage spatialCoverage[];
	ClassLibrary::Methodologies::AppliedUse appliedUse[];
	ClassLibrary::Discovery::SpatialCoverage spatialCoverage[];
	Unit unit[];
	ClassLibrary::SamplingMethodology::SampleFrame sampleFrame[];
	Universe universe;
	ClassLibrary::SamplingMethodology::SamplePopulationResult samplePopulationResult;
	ClassLibrary::SamplingMethodology::SampleFrame sampleFrame[];
	ClassLibrary::ComplexDataTypes::GeographicUnitTypeRelation geographicUnitTypeRelation[];
	ClassLibrary::ComplexDataTypes::GeographicUnitTypeRelation geographicUnitTypeRelation;
	Concept concept;
	ClassLibrary::ComplexDataTypes::GeographicUnitTypeIndicator geographicUnitTypeIndicator;
	ConceptualVariable conceptualVariable;
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
