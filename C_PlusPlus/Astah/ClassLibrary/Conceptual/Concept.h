#ifndef CLASSLIBRARY_CONCEPTUAL_CONCEPT_H
#define CLASSLIBRARY_CONCEPTUAL_CONCEPT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Map.h"
#include "ClassLibrary/ComplexDataTypes/ConceptIndicator.h"
#include "ClassLibrary/ComplexDataTypes/ConceptRelation.h"
#include "ClassLibrary/ComplexDataTypes/Level.h"
#include "ClassLibrary/Conceptual/Universe.h"
#include "ClassLibrary/Conceptual/VariableCollection.h"
#include "ClassLibrary/Conceptual/ConceptSystem.h"
#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/SignificationPattern/Signified.h"
#include "ClassLibrary/CustomMetadata/CustomStructure.h"
#include "ClassLibrary/CustomMetadata/CustomItem.h"
#include "ClassLibrary/CustomMetadata/CustomInstance.h"
#include "ClassLibrary/CustomMetadata/ControlledVocabulary.h"
#include "ClassLibrary/FormatDescription/PhysicalDataSet.h"
#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/FormatDescription/PhysicalSegmentLayout.h"
#include "ClassLibrary/StudyRelated/ComplianceStatement.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/StudyRelated/StudySeries.h"
#include "ClassLibrary/BusinessWorkflow/DataPipeline.h"
#include "ClassLibrary/Workflows/WorkflowStepSequence.h"
#include "ClassLibrary/Representations/ClassificationIndex.h"
#include "ClassLibrary/Representations/ClassificationSeries.h"
#include "ClassLibrary/Representations/ClassificationFamily.h"
#include "ClassLibrary/Representations/CodeList.h"
#include "ClassLibrary/Representations/Designation.h"
#include "ClassLibrary/LogicalDataDescription/ViewpointRole.h"
#include "ClassLibrary/LogicalDataDescription/DataStore.h"
#include "ClassLibrary/LogicalDataDescription/DataStoreLibrary.h"
#include "ClassLibrary/LogicalDataDescription/LogicalRecord.h"
#include "ClassLibrary/LogicalDataDescription/UnitDataRecord.h"
#include "ClassLibrary/DataCapture/Capture.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Agents/AgentListing.h"

namespace ClassLibrary
{
namespace Conceptual
{
class Concept : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString definition;

	ClassLibrary::Representations::ClassificationIndex classificationIndex[];
	ClassLibrary::Representations::ClassificationSeries classificationSeries[];
	ClassLibrary::BusinessWorkflow::DataPipeline dataPipeline[];
	ClassLibrary::Workflows::WorkflowStepSequence workflowStepSequence[];
	ClassLibrary::Representations::ClassificationFamily classificationFamily[];
	ClassLibrary::SignificationPattern::Signified signified[];
	ClassLibrary::CustomMetadata::CustomStructure customStructure[];
	ClassLibrary::StudyRelated::ComplianceStatement complianceStatement[];
	ClassLibrary::Representations::CodeList codeList[];
	ClassLibrary::CustomMetadata::CustomItem customItem;
	ClassLibrary::Representations::Designation designation;
	ClassLibrary::LogicalDataDescription::ViewpointRole viewpointRole[];
	Universe universe[];
	VariableCollection variableCollection[];
	ClassLibrary::ComplexDataTypes::Map map[];
	ClassLibrary::ComplexDataTypes::Map map[];
	ClassLibrary::FormatDescription::PhysicalDataSet physicalDataSet[];
	ClassLibrary::FormatDescription::PhysicalRecordSegment physicalRecordSegment[];
	ClassLibrary::ComplexDataTypes::ConceptIndicator conceptIndicator;
	ClassLibrary::DataCapture::Capture capture[];
	ClassLibrary::ComplexDataTypes::ConceptRelation conceptRelation;
	ClassLibrary::ComplexDataTypes::ConceptRelation conceptRelation;
	ClassLibrary::StudyRelated::Study study[];
	ClassLibrary::CollectionsPattern::SimpleCollection simpleCollection[];
	ConceptSystem conceptSystem[];
	ClassLibrary::LogicalDataDescription::DataStore dataStore[];
	ClassLibrary::LogicalDataDescription::DataStoreLibrary dataStoreLibrary[];
	ConceptualVariable conceptualVariable[];
	Population population[];
	ClassLibrary::LogicalDataDescription::LogicalRecord logicalRecord[];
	ClassLibrary::Agents::AgentListing agentListing[];
	UnitType unitType[];
	ClassLibrary::LogicalDataDescription::UnitDataRecord unitDataRecord[];
	ClassLibrary::CustomMetadata::CustomInstance customInstance[];
	ClassLibrary::CustomMetadata::ControlledVocabulary controlledVocabulary[];
	ClassLibrary::StudyRelated::StudySeries studySeries[];
	ClassLibrary::FormatDescription::PhysicalSegmentLayout physicalSegmentLayout[];
	ClassLibrary::ComplexDataTypes::Level level;
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
