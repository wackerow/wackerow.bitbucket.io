#ifndef CLASSLIBRARY_CONCEPTUAL_INSTANCE_VARIABLE_H
#define CLASSLIBRARY_CONCEPTUAL_INSTANCE_VARIABLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableIndicator.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableRelation.h"
#include "ClassLibrary/ComplexDataTypes/InstanceVariableValueMap.h"
#include "ClassLibrary/ComplexDataTypes/ViewpointRoleRelation.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/SimpleCodebook/VariableStatistics.h"
#include "ClassLibrary/CustomMetadata/CustomValue.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Workflows/Act.h"
#include "ClassLibrary/Representations/SentinelValueDomain.h"
#include "ClassLibrary/LogicalDataDescription/Datum.h"
#include "ClassLibrary/LogicalDataDescription/DataPoint.h"
#include "ClassLibrary/DataCapture/Capture.h"

namespace ClassLibrary
{
namespace Conceptual
{
class InstanceVariable : public RepresentedVariable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString variableRole;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry physicalDataType;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry platformType;

	ClassLibrary::StudyRelated::Study study[];
	ClassLibrary::CustomMetadata::CustomValue customValue;
	ClassLibrary::LogicalDataDescription::Datum datum;
	ClassLibrary::SimpleCodebook::VariableStatistics variableStatistics;
	ClassLibrary::SimpleCodebook::VariableStatistics variableStatistics;
	ClassLibrary::LogicalDataDescription::DataPoint dataPoint;
	ClassLibrary::ComplexDataTypes::InstanceVariableIndicator instanceVariableIndicator;
	ClassLibrary::ComplexDataTypes::InstanceVariableRelation instanceVariableRelation[];
	ClassLibrary::ComplexDataTypes::InstanceVariableRelation instanceVariableRelation;
	Population population[];
	ClassLibrary::Representations::SentinelValueDomain sentinelValueDomain[];
	ConceptualVariable conceptualVariable[];
	ClassLibrary::ComplexDataTypes::InstanceVariableValueMap instanceVariableValueMap[];
	ClassLibrary::ComplexDataTypes::InstanceVariableValueMap instanceVariableValueMap[];
	ClassLibrary::ComplexDataTypes::ViewpointRoleRelation viewpointRoleRelation[];
	ClassLibrary::ComplexDataTypes::ViewpointRoleRelation viewpointRoleRelation;
	ClassLibrary::Workflows::Act act[];
	ClassLibrary::SimpleCodebook::VariableStatistics variableStatistics;
	RepresentedVariable representedVariable[];
	ClassLibrary::DataCapture::Capture capture[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
