#ifndef CLASSLIBRARY_CONCEPTUAL_REPRESENTED_VARIABLE_H
#define CLASSLIBRARY_CONCEPTUAL_REPRESENTED_VARIABLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/Conceptual/Universe.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/CustomMetadata/CustomItem.h"
#include "ClassLibrary/Representations/SubstantiveValueDomain.h"
#include "ClassLibrary/Representations/SentinelValueDomain.h"
#include "ClassLibrary/DataCapture/ResponseDomain.h"
#include "ClassLibrary/DataCapture/RepresentedQuestion.h"
#include "ClassLibrary/DataCapture/RepresentedMeasurement.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace Conceptual
{
class RepresentedVariable : public ConceptualVariable
{
public:
	Primitive Package::String unitOfMeasurement;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry hasIntendedDataType;

	ClassLibrary::Representations::SubstantiveValueDomain substantiveValueDomain[];
	ClassLibrary::CustomMetadata::CustomItem customItem;
	ClassLibrary::DataCapture::ResponseDomain responseDomain;
	Universe universe[];
	ClassLibrary::DataCapture::RepresentedQuestion representedQuestion[];
	ClassLibrary::DataCapture::RepresentedMeasurement representedMeasurement;
	ConceptualVariable conceptualVariable[];
	ClassLibrary::Representations::SentinelValueDomain sentinelValueDomain[];
	InstanceVariable instanceVariable;
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
