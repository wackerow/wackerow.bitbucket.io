#ifndef CLASSLIBRARY_CONCEPTUAL_CONCEPTUAL_DOMAIN_H
#define CLASSLIBRARY_CONCEPTUAL_CONCEPTUAL_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/Conceptual/ConceptSystem.h"
#include "ClassLibrary/Representations/ValueAndConceptDescription.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Conceptual
{
class ConceptualDomain : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::Representations::ValueAndConceptDescription valueAndConceptDescription[];
	ConceptSystem conceptSystem[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
