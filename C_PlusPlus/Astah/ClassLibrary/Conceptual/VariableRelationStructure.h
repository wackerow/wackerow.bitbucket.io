#ifndef CLASSLIBRARY_CONCEPTUAL_VARIABLE_RELATION_STRUCTURE_H
#define CLASSLIBRARY_CONCEPTUAL_VARIABLE_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/VariableRelation.h"
#include "ClassLibrary/Conceptual/VariableCollection.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace Conceptual
{
class VariableRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::VariableRelation hasMemberRelation[];

	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
	VariableCollection variableCollection[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
