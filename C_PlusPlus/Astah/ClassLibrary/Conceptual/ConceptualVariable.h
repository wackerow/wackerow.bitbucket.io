#ifndef CLASSLIBRARY_CONCEPTUAL_CONCEPTUAL_VARIABLE_H
#define CLASSLIBRARY_CONCEPTUAL_CONCEPTUAL_VARIABLE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/VariableRelation.h"
#include "ClassLibrary/ComplexDataTypes/VariableIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/SentinelConceptualDomain.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/Conceptual/SubstantiveConceptualDomain.h"

namespace ClassLibrary
{
namespace Conceptual
{
class ConceptualVariable : public Concept
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;

	SentinelConceptualDomain sentinelConceptualDomain[];
	InstanceVariable instanceVariable;
	Concept concept[];
	RepresentedVariable representedVariable;
	ClassLibrary::ComplexDataTypes::VariableRelation variableRelation[];
	ClassLibrary::ComplexDataTypes::VariableRelation variableRelation[];
	UnitType unitType[];
	ClassLibrary::ComplexDataTypes::VariableIndicator variableIndicator;
	SubstantiveConceptualDomain substantiveConceptualDomain[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
