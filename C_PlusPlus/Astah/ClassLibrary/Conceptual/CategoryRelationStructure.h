#ifndef CLASSLIBRARY_CONCEPTUAL_CATEGORY_RELATION_STRUCTURE_H
#define CLASSLIBRARY_CONCEPTUAL_CATEGORY_RELATION_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/RelationSpecification.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/CategoryRelation.h"
#include "ClassLibrary/Conceptual/CategorySet.h"
#include "ClassLibrary/EnumerationsRegExp/TotalityType.h"
#include "ClassLibrary/CollectionsPattern/RelationStructure.h"
#include "ClassLibrary/Identification/Identifiable.h"

namespace ClassLibrary
{
namespace Conceptual
{
class CategoryRelationStructure : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;

	ClassLibrary::EnumerationsRegExp::TotalityType totality;

	ClassLibrary::ComplexDataTypes::CategoryRelation hasMemberRelation[];

	CategorySet categorySet[];
	ClassLibrary::CollectionsPattern::RelationStructure relationStructure[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
