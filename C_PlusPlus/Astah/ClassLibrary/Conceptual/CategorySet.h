#ifndef CLASSLIBRARY_CONCEPTUAL_CATEGORY_SET_H
#define CLASSLIBRARY_CONCEPTUAL_CATEGORY_SET_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CategoryIndicator.h"
#include "ClassLibrary/Conceptual/ConceptSystem.h"
#include "ClassLibrary/Conceptual/CategoryRelationStructure.h"
#include "ClassLibrary/Representations/CodeList.h"

namespace ClassLibrary
{
namespace Conceptual
{
class CategorySet : public ConceptSystem
{
public:
	ClassLibrary::ComplexDataTypes::CategoryIndicator contains[];

	CategoryRelationStructure categoryRelationStructure[];
	ClassLibrary::Representations::CodeList codeList;
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
