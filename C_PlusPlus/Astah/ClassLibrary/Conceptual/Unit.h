#ifndef CLASSLIBRARY_CONCEPTUAL_UNIT_H
#define CLASSLIBRARY_CONCEPTUAL_UNIT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace Conceptual
{
class Unit : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	Population population[];
	UnitType unitType[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
