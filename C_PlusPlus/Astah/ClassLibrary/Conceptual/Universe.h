#ifndef CLASSLIBRARY_CONCEPTUAL_UNIVERSE_H
#define CLASSLIBRARY_CONCEPTUAL_UNIVERSE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/TargetSample.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/Population.h"
#include "ClassLibrary/Conceptual/UnitType.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Conceptual
{
class Universe : public Concept
{
public:
	Primitive Package::Boolean isInclusive;

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;

	Population population;
	Concept concept[];
	ClassLibrary::ComplexDataTypes::TargetSample targetSample[];
	UnitType unitType[];
	ClassLibrary::StudyRelated::Study study[];
	RepresentedVariable representedVariable;
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
