#ifndef CLASSLIBRARY_CONCEPTUAL_SUBSTANTIVE_CONCEPTUAL_DOMAIN_H
#define CLASSLIBRARY_CONCEPTUAL_SUBSTANTIVE_CONCEPTUAL_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/ConceptualDomain.h"
#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/Representations/SubstantiveValueDomain.h"

namespace ClassLibrary
{
namespace Conceptual
{
class SubstantiveConceptualDomain : public ConceptualDomain
{
public:
	ClassLibrary::Representations::SubstantiveValueDomain substantiveValueDomain;
	ConceptualVariable conceptualVariable;
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
