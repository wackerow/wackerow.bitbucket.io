#ifndef CLASSLIBRARY_CONCEPTUAL_VARIABLE_COLLECTION_H
#define CLASSLIBRARY_CONCEPTUAL_VARIABLE_COLLECTION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/VariableIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/VariableRelationStructure.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace Conceptual
{
class VariableCollection : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry groupingSemantic;

	ClassLibrary::ComplexDataTypes::VariableIndicator contains[];

	Primitive Package::Boolean isOrdered;

	Concept concept[];
	VariableRelationStructure variableRelationStructure[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	ClassLibrary::StudyRelated::Study study[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
