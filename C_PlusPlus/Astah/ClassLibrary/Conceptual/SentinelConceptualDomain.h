#ifndef CLASSLIBRARY_CONCEPTUAL_SENTINEL_CONCEPTUAL_DOMAIN_H
#define CLASSLIBRARY_CONCEPTUAL_SENTINEL_CONCEPTUAL_DOMAIN_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/Conceptual/ConceptualDomain.h"
#include "ClassLibrary/Conceptual/ConceptualVariable.h"
#include "ClassLibrary/Representations/SentinelValueDomain.h"

namespace ClassLibrary
{
namespace Conceptual
{
class SentinelConceptualDomain : public ConceptualDomain
{
public:
	ClassLibrary::Representations::SentinelValueDomain sentinelValueDomain;
	ConceptualVariable conceptualVariable;
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
