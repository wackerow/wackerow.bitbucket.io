#ifndef CLASSLIBRARY_CONCEPTUAL_POPULATION_H
#define CLASSLIBRARY_CONCEPTUAL_POPULATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/LabelForDisplay.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/DateRange.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/Conceptual/Universe.h"
#include "ClassLibrary/Conceptual/Unit.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/GeographicClassification/GeographicUnit.h"
#include "ClassLibrary/SamplingMethodology/SamplePopulationResult.h"
#include "ClassLibrary/SamplingMethodology/SampleFrame.h"
#include "ClassLibrary/FormatDescription/PhysicalRecordSegment.h"
#include "ClassLibrary/StudyRelated/Study.h"

namespace ClassLibrary
{
namespace Conceptual
{
class Population : public Concept
{
public:
	ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;

	ClassLibrary::ComplexDataTypes::DateRange timePeriodOfPopulation[];

	ClassLibrary::StudyRelated::Study study[];
	ClassLibrary::FormatDescription::PhysicalRecordSegment physicalRecordSegment;
	Universe universe[];
	Unit unit[];
	ClassLibrary::SamplingMethodology::SamplePopulationResult samplePopulationResult;
	ClassLibrary::SamplingMethodology::SampleFrame sampleFrame[];
	InstanceVariable instanceVariable;
	Concept concept[];
	ClassLibrary::GeographicClassification::GeographicUnit geographicUnit[];
};

}  // namespace Conceptual
}  // namespace ClassLibrary
#endif
