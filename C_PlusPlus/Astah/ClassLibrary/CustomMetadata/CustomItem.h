#ifndef CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_ITEM_H
#define CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_ITEM_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/CustomItemIndicator.h"
#include "ClassLibrary/ComplexDataTypes/CustomItemRelation.h"
#include "ClassLibrary/Conceptual/RepresentedVariable.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/CustomMetadata/CustomValue.h"
#include "ClassLibrary/Representations/ValueDomain.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Integer.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace CustomMetadata
{
class CustomItem : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	Primitive Package::Integer maxOccurs;

	Primitive Package::Integer minOccurs;

	Primitive Package::String key;

	CustomValue customValue;
	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
	ClassLibrary::Conceptual::RepresentedVariable representedVariable[];
	ClassLibrary::Representations::ValueDomain valueDomain[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::ComplexDataTypes::CustomItemIndicator customItemIndicator;
	ClassLibrary::ComplexDataTypes::CustomItemRelation customItemRelation;
	ClassLibrary::ComplexDataTypes::CustomItemRelation customItemRelation[];
};

}  // namespace CustomMetadata
}  // namespace ClassLibrary
#endif
