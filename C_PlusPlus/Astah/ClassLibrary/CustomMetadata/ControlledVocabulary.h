#ifndef CLASSLIBRARY_CUSTOMMETADATA_CONTROLLED_VOCABULARY_H
#define CLASSLIBRARY_CUSTOMMETADATA_CONTROLLED_VOCABULARY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/VocabularyEntryIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/CustomMetadata/VocabularyRelationStructure.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace CustomMetadata
{
class ControlledVocabulary : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::VocabularyEntryIndicator contains[];

	Primitive Package::Boolean isOrdered;

	VocabularyRelationStructure vocabularyRelationStructure[];
	ClassLibrary::Conceptual::Concept concept[];
	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
};

}  // namespace CustomMetadata
}  // namespace ClassLibrary
#endif
