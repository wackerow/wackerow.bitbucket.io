#ifndef CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_STRUCTURE_H
#define CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_STRUCTURE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/CustomItemIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/CustomMetadata/CustomItemRelationStructure.h"
#include "ClassLibrary/CustomMetadata/CustomInstance.h"
#include "ClassLibrary/CollectionsPattern/StructuredCollection.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace CustomMetadata
{
class CustomStructure : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::ComplexDataTypes::CustomItemIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::CollectionsPattern::StructuredCollection structuredCollection[];
	ClassLibrary::Conceptual::Concept concept[];
	CustomItemRelationStructure customItemRelationStructure[];
	CustomInstance customInstance;
};

}  // namespace CustomMetadata
}  // namespace ClassLibrary
#endif
