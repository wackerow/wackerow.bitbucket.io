#ifndef CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_VALUE_H
#define CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_VALUE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ValueString.h"
#include "ClassLibrary/ComplexDataTypes/CustomValueIndicator.h"
#include "ClassLibrary/ComplexDataTypes/CustomValueRelation.h"
#include "ClassLibrary/Conceptual/InstanceVariable.h"
#include "ClassLibrary/CustomMetadata/CustomItem.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace CustomMetadata
{
class CustomValue : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::ValueString value;

	Primitive Package::String key;

	ClassLibrary::Conceptual::InstanceVariable instanceVariable[];
	CustomItem customItem[];
	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
	ClassLibrary::ComplexDataTypes::CustomValueIndicator customValueIndicator;
	ClassLibrary::ComplexDataTypes::CustomValueRelation customValueRelation;
	ClassLibrary::ComplexDataTypes::CustomValueRelation customValueRelation[];
};

}  // namespace CustomMetadata
}  // namespace ClassLibrary
#endif
