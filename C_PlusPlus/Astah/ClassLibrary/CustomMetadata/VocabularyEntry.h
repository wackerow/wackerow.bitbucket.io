#ifndef CLASSLIBRARY_CUSTOMMETADATA_VOCABULARY_ENTRY_H
#define CLASSLIBRARY_CUSTOMMETADATA_VOCABULARY_ENTRY_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/VocabularyEntryRelation.h"
#include "ClassLibrary/ComplexDataTypes/VocabularyEntryIndicator.h"
#include "ClassLibrary/CollectionsPattern/CollectionMember.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"

namespace ClassLibrary
{
namespace CustomMetadata
{
class VocabularyEntry : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::InternationalStructuredString entryTerm;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString definition;

	ClassLibrary::CollectionsPattern::CollectionMember collectionMember[];
	ClassLibrary::ComplexDataTypes::VocabularyEntryRelation vocabularyEntryRelation;
	ClassLibrary::ComplexDataTypes::VocabularyEntryRelation vocabularyEntryRelation;
	ClassLibrary::ComplexDataTypes::VocabularyEntryIndicator vocabularyEntryIndicator;
};

}  // namespace CustomMetadata
}  // namespace ClassLibrary
#endif
