#ifndef CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_INSTANCE_H
#define CLASSLIBRARY_CUSTOMMETADATA_CUSTOM_INSTANCE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ObjectName.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/CustomValueIndicator.h"
#include "ClassLibrary/Conceptual/Concept.h"
#include "ClassLibrary/EnumerationsRegExp/CollectionType.h"
#include "ClassLibrary/CustomMetadata/CustomStructure.h"
#include "ClassLibrary/CollectionsPattern/SimpleCollection.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "Primitive Package/Boolean.h"

namespace ClassLibrary
{
namespace CustomMetadata
{
class CustomInstance : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::ObjectName name[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::EnumerationsRegExp::CollectionType type;

	ClassLibrary::ComplexDataTypes::CustomValueIndicator contains[];

	Primitive Package::Boolean isOrdered;

	ClassLibrary::Conceptual::Concept concept[];
	CustomStructure customStructure[];
	ClassLibrary::CollectionsPattern::SimpleCollection simpleCollection[];
};

}  // namespace CustomMetadata
}  // namespace ClassLibrary
#endif
