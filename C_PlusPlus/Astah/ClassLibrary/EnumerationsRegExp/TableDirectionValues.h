#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_TABLE_DIRECTION_VALUES_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_TABLE_DIRECTION_VALUES_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Ltr,
	Rtl,
	Auto

} TableDirectionValues;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
