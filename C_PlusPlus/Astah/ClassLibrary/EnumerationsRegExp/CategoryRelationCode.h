#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_CATEGORY_RELATION_CODE_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_CATEGORY_RELATION_CODE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Nominal,
	Ordinal,
	Interval,
	Ratio,
	Continuous

} CategoryRelationCode;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
