#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_POINT_FORMAT_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_POINT_FORMAT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	DecimalDegree,
	DegreesMinutesSeconds,
	DecimalMinutes,
	Meters,
	Feet

} PointFormat;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
