#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_WHITE_SPACE_RULE_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_WHITE_SPACE_RULE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Preserve,
	Replace,
	collapse

} WhiteSpaceRule;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
