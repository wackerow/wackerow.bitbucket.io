#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_TOTALITY_TYPE_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_TOTALITY_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Total,
	Partial,
	Unknown

} TotalityType;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
