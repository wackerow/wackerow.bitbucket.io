#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_TRIM_VALUES_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_TRIM_VALUES_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Start,
	End,
	Both,
	Neither

} TrimValues;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
