#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_MAPPING_RELATION_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_MAPPING_RELATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	ExactMatch,
	CloseMatch,
	Disjoint

} MappingRelation;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
