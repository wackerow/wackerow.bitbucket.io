#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_SEX_SPECIFICATION_TYPE_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_SEX_SPECIFICATION_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Masculine,
	Feminine,
	GenderNeutral

} SexSpecificationType;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
