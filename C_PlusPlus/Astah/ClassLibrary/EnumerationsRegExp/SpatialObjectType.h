#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_SPATIAL_OBJECT_TYPE_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_SPATIAL_OBJECT_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Point,
	Polygon,
	Line,
	LinearRing,
	Raster

} SpatialObjectType;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
