#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_SHAPE_CODED_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_SHAPE_CODED_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Rectangle,
	Circle,
	Polygon,
	LinearRing

} ShapeCoded;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
