#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_COLLECTION_TYPE_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_COLLECTION_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Bag,
	Set

} CollectionType;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
