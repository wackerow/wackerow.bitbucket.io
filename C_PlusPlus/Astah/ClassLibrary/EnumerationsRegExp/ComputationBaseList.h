#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_COMPUTATION_BASE_LIST_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_COMPUTATION_BASE_LIST_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Total,
	ValidOnly,
	MissingOnly

} ComputationBaseList;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
