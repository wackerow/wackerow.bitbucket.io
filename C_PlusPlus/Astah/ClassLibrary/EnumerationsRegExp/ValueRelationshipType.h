#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_VALUE_RELATIONSHIP_TYPE_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_VALUE_RELATIONSHIP_TYPE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Equal,
	NotEqual,
	GreaterThan,
	GreaterThanOrEqualTo,
	LessThan,
	LessThanOrEqualTo

} ValueRelationshipType;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
