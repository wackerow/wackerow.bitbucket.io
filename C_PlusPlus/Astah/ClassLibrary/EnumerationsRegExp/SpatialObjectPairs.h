#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_SPATIAL_OBJECT_PAIRS_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_SPATIAL_OBJECT_PAIRS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	PointToPoint,
	PointToLine,
	PointToArea,
	LineToLine,
	LineToArea,
	AreaToArea

} SpatialObjectPairs;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
