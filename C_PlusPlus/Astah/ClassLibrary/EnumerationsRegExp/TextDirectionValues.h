#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_TEXT_DIRECTION_VALUES_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_TEXT_DIRECTION_VALUES_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Ltr,
	Rtl,
	Auto,
	Inherit

} TextDirectionValues;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
