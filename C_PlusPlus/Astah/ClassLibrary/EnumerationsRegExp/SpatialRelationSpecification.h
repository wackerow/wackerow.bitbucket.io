#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_SPATIAL_RELATION_SPECIFICATION_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_SPATIAL_RELATION_SPECIFICATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	Equals,
	Disjoint,
	Intersects,
	Contains,
	Touches

} SpatialRelationSpecification;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
