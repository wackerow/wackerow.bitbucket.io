#ifndef CLASSLIBRARY_ENUMERATIONSREGEXP_TEMPORAL_RELATION_SPECIFICATION_H
#define CLASSLIBRARY_ENUMERATIONSREGEXP_TEMPORAL_RELATION_SPECIFICATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

namespace ClassLibrary
{
namespace EnumerationsRegExp
{
typedef enum 
{
	TemporalMeets,
	TemporalContains,
	TemporalFinishes,
	TemporalPrecedes,
	TemporalStarts,
	TemporalOverlaps,
	TemporalEquals

} TemporalRelationSpecification;

}  // namespace EnumerationsRegExp
}  // namespace ClassLibrary
#endif
