#ifndef CLASSLIBRARY_UTILITY_D_D_I4_VERSION_H
#define CLASSLIBRARY_UTILITY_D_D_I4_VERSION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace Utility
{
class DDI4Version
{
public:
	Primitive Package::String value;


public:
	DDI4Version();
};

}  // namespace Utility
}  // namespace ClassLibrary
#endif
