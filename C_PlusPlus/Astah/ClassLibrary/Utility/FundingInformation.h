#ifndef CLASSLIBRARY_UTILITY_FUNDING_INFORMATION_H
#define CLASSLIBRARY_UTILITY_FUNDING_INFORMATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/StudyRelated/Study.h"
#include "ClassLibrary/Utility/DocumentInformation.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Agents/Agent.h"
#include "Primitive Package/String.h"

namespace ClassLibrary
{
namespace Utility
{
class FundingInformation : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry funderRole;

	Primitive Package::String grantNumber[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;

	ClassLibrary::Agents::Agent agent[];
	ClassLibrary::StudyRelated::Study study[];
	DocumentInformation documentInformation[];
};

}  // namespace Utility
}  // namespace ClassLibrary
#endif
