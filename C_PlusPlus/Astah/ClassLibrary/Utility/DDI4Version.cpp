#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "DDI4Version.h"

namespace ClassLibrary
{
namespace Utility
{

DDI4Version::DDI4Version()
:value(DR0.2)
{
}


}  // namespace Utility
}  // namespace ClassLibrary
