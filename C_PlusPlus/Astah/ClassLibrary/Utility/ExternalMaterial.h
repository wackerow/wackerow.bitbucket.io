#ifndef CLASSLIBRARY_UTILITY_EXTERNAL_MATERIAL_H
#define CLASSLIBRARY_UTILITY_EXTERNAL_MATERIAL_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/ExternalControlledVocabularyEntry.h"
#include "ClassLibrary/ComplexDataTypes/InternationalStructuredString.h"
#include "ClassLibrary/ComplexDataTypes/Segment.h"
#include "ClassLibrary/ComplexDataTypes/Annotation.h"
#include "ClassLibrary/SimpleMethodologyOverview/DesignOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/AlgorithmOverview.h"
#include "ClassLibrary/SimpleMethodologyOverview/MethodologyOverview.h"
#include "ClassLibrary/MethodologyPattern/Algorithm.h"
#include "ClassLibrary/MethodologyPattern/Design.h"
#include "ClassLibrary/SamplingMethodology/SamplingProcess.h"
#include "ClassLibrary/StudyRelated/Standard.h"
#include "ClassLibrary/StudyRelated/Budget.h"
#include "ClassLibrary/Utility/DocumentInformation.h"
#include "ClassLibrary/Representations/ClassificationIndex.h"
#include "ClassLibrary/Representations/StatisticalClassification.h"
#include "ClassLibrary/Representations/CorrespondenceTable.h"
#include "ClassLibrary/DataCapture/Instruction.h"
#include "ClassLibrary/Identification/Identifiable.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "ClassLibrary/Methodologies/Guide.h"
#include "ClassLibrary/Methodologies/Goal.h"
#include "ClassLibrary/Methodologies/Precondition.h"
#include "ClassLibrary/XMLSchemaDatatypes/anyUri.h"

namespace ClassLibrary
{
namespace Utility
{
class ExternalMaterial : public ClassLibrary::Identification::Identifiable
{
public:
	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfMaterial;

	ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;

	ClassLibrary::XMLSchemaDatatypes::anyUri uri[];

	ClassLibrary::ComplexDataTypes::InternationalStructuredString relationshipDescription[];

	ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry mimeType;

	ClassLibrary::ComplexDataTypes::Segment usesSegment[];

	ClassLibrary::ComplexDataTypes::Annotation citationOfExternalMaterial;

	ClassLibrary::SimpleMethodologyOverview::DesignOverview designOverview[];
	ClassLibrary::StudyRelated::Standard standard[];
	ClassLibrary::SimpleMethodologyOverview::AlgorithmOverview algorithmOverview[];
	ClassLibrary::Methodologies::Guide guide[];
	ClassLibrary::DataCapture::Instruction instruction[];
	ClassLibrary::Methodologies::Goal goal[];
	ClassLibrary::MethodologyPattern::Algorithm algorithm[];
	ClassLibrary::MethodologyPattern::Design design[];
	ClassLibrary::Representations::ClassificationIndex classificationIndex[];
	ClassLibrary::StudyRelated::Budget budget[];
	ClassLibrary::Representations::StatisticalClassification statisticalClassification[];
	ClassLibrary::Representations::CorrespondenceTable correspondenceTable[];
	DocumentInformation documentInformation[];
	ClassLibrary::SimpleMethodologyOverview::MethodologyOverview methodologyOverview[];
	ClassLibrary::SamplingMethodology::SamplingProcess samplingProcess[];
	ClassLibrary::Identification::AnnotatedIdentifiable annotatedIdentifiable[];
	ClassLibrary::Methodologies::Precondition precondition[];
};

}  // namespace Utility
}  // namespace ClassLibrary
#endif
