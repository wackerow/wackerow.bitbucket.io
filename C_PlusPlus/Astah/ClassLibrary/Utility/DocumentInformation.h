#ifndef CLASSLIBRARY_UTILITY_DOCUMENT_INFORMATION_H
#define CLASSLIBRARY_UTILITY_DOCUMENT_INFORMATION_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ClassLibrary/ComplexDataTypes/TypedDescriptiveText.h"
#include "ClassLibrary/Utility/DDI4Version.h"
#include "ClassLibrary/Utility/FundingInformation.h"
#include "ClassLibrary/Utility/ExternalMaterial.h"
#include "ClassLibrary/Discovery/Access.h"
#include "ClassLibrary/Discovery/Coverage.h"
#include "ClassLibrary/Identification/AnnotatedIdentifiable.h"
#include "Primitive Package/Boolean.h"
#include "Primitive Package/UnlimitedNatural.h"

namespace ClassLibrary
{
namespace Utility
{
class DocumentInformation : public ClassLibrary::Identification::AnnotatedIdentifiable
{
public:
	ClassLibrary::ComplexDataTypes::TypedDescriptiveText contentCoverage[];

	Primitive Package::Boolean isPublished;

	Primitive Package::UnlimitedNatural hasPrimaryContent;

	DDI4Version ofType;

	ClassLibrary::Discovery::Access access[];
	ClassLibrary::Discovery::Coverage coverage[];
	FundingInformation fundingInformation[];
	ExternalMaterial externalMaterial[];
	ClassLibrary::Discovery::Access access[];
};

}  // namespace Utility
}  // namespace ClassLibrary
#endif
