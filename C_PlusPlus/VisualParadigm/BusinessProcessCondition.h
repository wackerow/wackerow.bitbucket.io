#ifndef BUSINESSPROCESSCONDITION_H
#define BUSINESSPROCESSCONDITION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * A BusinessProcess precondition or post condition which describes the condition which must be met to begin (pre) or exit (post) a process. It may use a specified LogicalRecord. The Logical Record has SQL that describes it, rejectionCriteria against which its adequacy may be tested and an optional annotation that describes its provenance.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class BusinessProcessCondition {

		private:
			/**
			 * SQL SELECT statement that describes the dataset
			 */
			string sql;
			/**
			 * Criteria for failing an input dataset
			 */
			ClassLibrary::ComplexDataTypes::CommandCode rejectionCriteria;
			/**
			 * Provides an alternative to or supplements the SQL data description.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalStructuredString> dataDescription;
		};
	}
}

#endif
