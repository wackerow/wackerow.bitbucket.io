#ifndef SEGMENTBYTEXT_H
#define SEGMENTBYTEXT_H

namespace ClassLibrary {
	namespace FormatDescription {
		/**
 * Definition
 * ============
 * Defines the location of a segment of text through character, and optionally line, counts. An adequate description will always include a startCharacterPosition and then may include an endCharacterPosition or a characterLength.
 * If StartLine is specified, the character counts begin within that line.
 * An endCharacterPosition of 0 indicates that whole lines are specified
 * 
 * 
 * Examples
 * ==========
 * The segment beginning at line 3, character 4 and ending at line 27 character 13.
 * Alternatively the segment beginning at character 257 and ending at character 1350 of the whole body of text.
 * StartLine of 10, endLine of 12, startCharacterPosition of 1, endCharacterPosition of 0 means all of lines 10,11, and 12. 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SegmentByText : ClassLibrary::FormatDescription::PhysicalSegmentLocation {

		private:
			/**
			 * The line number, where 1 is the first line, on which to begin counting characters. If missing this defaults to 1 (the first line).
			 */
			int startLine;
			/**
			 * The last line on which to count characters. If missing this defaults to startLine.
			 */
			int endLine;
			/**
			 * The character position of the first character of the segment, with the count beginning at character 1 of startLine.
			 */
			int startCharacterPosition;
			/**
			 * The character position of the last character of the segment.  If endLine is specified, the count begins at character 1 of endLine. If startLine and endLine are not specified, the count begins at character 1 of the first line of the resource and the count includes any line termination characters. The resulting segment may include text from multiple lines of the resource.
			 */
			int endCharacterPosition;
			/**
			 * The number of characters in the segment. The segment may include text from multiple lines of the resource. If it does, the length includes any line termination characters.
			 */
			int characterLength;
		};
	}
}

#endif
