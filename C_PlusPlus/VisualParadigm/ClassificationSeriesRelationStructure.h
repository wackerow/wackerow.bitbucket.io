#ifndef CLASSIFICATIONSERIESRELATIONSTRUCTURE_H
#define CLASSIFICATIONSERIESRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * Describes the complex relation structure of a classification family.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ClassificationSeriesRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Member relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ClassificationSeriesRelation> hasMemberRelation;
		};
	}
}

#endif
