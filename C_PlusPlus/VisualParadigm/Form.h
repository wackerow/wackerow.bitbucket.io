#ifndef FORM_H
#define FORM_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * A link to a form used by the metadata containing the form number, a statement regarding the contents of the form, a statement as to the mandatory nature of the form and a privacy level designation.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:FormType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Form {

		private:
			/**
			 * The number or other means of identifying the form.
			 */
			string formNumber;
			/**
			 * The URN or URL of the form.
			 */
			ClassLibrary::XMLSchemaDatatypes::anyUri uri;
			/**
			 * A statement regarding the use, coverage, and purpose of the form.
			 */
			ClassLibrary::ComplexDataTypes::InternationalString statement;
			/**
			 * Set to "true" if the form is required. Set to "false" if the form is optional.
			 */
			boolean isRequired;
		};
	}
}

#endif
