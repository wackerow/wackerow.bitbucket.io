#ifndef IMAGEAREA_H
#define IMAGEAREA_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and Coordinates provides the information required to define it.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ImageAreaType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ImageArea {

		private:
			/**
			 * A comma-delimited list of x,y coordinates, listed as a set of adjacent points for rectangles and polygons, and as a center-point and a radius for circles (x,y,r).
			 */
			string coordinates;
			/**
			 * A fixed set of valid responses includes Rectangle, Circle, and Polygon.
			 */
			ClassLibrary::EnumerationsRegExp::ShapeCoded shape;
		};
	}
}

#endif
