#ifndef SENTINELCONCEPTUALDOMAIN_H
#define SENTINELCONCEPTUALDOMAIN_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * Description or list of possible sentinel concepts , e.g. missing values.
 * 
 * Examples
 * ==========
 * Refused, 
 * Don't know, 
 * Lost in processing
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * missing categories
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SentinelConceptualDomain : ClassLibrary::Conceptual::ConceptualDomain {
		};
	}
}

#endif
