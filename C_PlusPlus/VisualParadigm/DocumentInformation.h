#ifndef DOCUMENTINFORMATION_H
#define DOCUMENTINFORMATION_H

namespace ClassLibrary {
	namespace Utility {
		/**
 * Definition
 * ============
 * Provides a consistent path to identifying information regarding the DDI Document and is automatically available for all Functional Views (/DocumentInformation/Annotation etc.). It covers annotation information, coverage (general, plus specific spatial, temporal, and topical coverage), access information from the producer (persistent access control), access information from the local distributor (Local Access Control), information on related series (i.e. a study done as a series of data capture events over time, qualitative material that is part of a larger set, one of a series of funded data capture activities, etc.), funding information, and other document level information used to identify and discover information about a DDI document regardless of its Functional View type. Use the Annotation at this level to provide the full annotation of the DDI Document. Note that related materials can be entered within the Annotation fields.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Annotation associated with a specific class is the annotation of the metadata class itself, such as the Study, InstanceVariable, Concept, etc. The annotation associated with Document Information is to refer to the Document itself. Note that not all DDI instances are intended to be persistent documents. For those that are DocumentInformation provides a consistent set of discovery level information on the content of the whole rather than specific parts of the instance. 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class DocumentInformation : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Allows for the addition of a Purpose, Table of Contents, List of Variables, or other forms of information more specific than a general abstract. Provides both the content (using description) and a type identification using an External Controlled Vocabulary Entry.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::TypedDescriptiveText> contentCoverage;
			/**
			 * Default value is False. Indicates that the metadata instance will not be changed without versioning, and is a stable target for referencing.
			 */
			boolean isPublished;
			/**
			 * A whitespace-delimited list of the DDI URN identifiers of the objects contained in an XML instance or RDF graph which could be considered the primary objects or entry points. In a Codebook View, for example, the top-level Codebook object, fo example. In some views, such as the Agents View, there my be more than one primary object (Individuals, Organizations, and Machines in this case).
			 */
			UnlimitedNatural hasPrimaryContent;
			/**
			 * Automatically brings in the version number for of DDI 4
			 */
			ClassLibrary::Utility::DDI4Version ofType;
		};
	}
}

#endif
