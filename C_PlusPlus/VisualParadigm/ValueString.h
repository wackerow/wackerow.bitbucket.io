#ifndef VALUESTRING_H
#define VALUESTRING_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * The Value expressed as an xs:string with the ability to preserve whitespace if critical to the understanding of the content. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ValueType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ValueString {

		private:
			/**
			 * The actual content of this value as a string
			 */
			string content;
			/**
			 * The usual setting "collapse" states that leading and trailing white space will be removed and multiple adjacent white spaces will be treated as a single white space. When setting to "replace" all occurrences of #x9 (tab), #xA (line feed) and #xD (carriage return) are replaced with #x20 (space) but leading and trailing spaces will be retained. If the existence of any of these white spaces is critical to the understanding of the content, change the value of this attribute to "preserve".
			 */
			ClassLibrary::EnumerationsRegExp::WhiteSpaceRule whiteSpace;
		};
	}
}

#endif
