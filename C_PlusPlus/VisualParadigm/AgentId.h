#ifndef AGENTID_H
#define AGENTID_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Persistent identifier for a researcher using a system like ORCID
 * 
 * Examples
 * ==========
 * ORCID
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AgentId {

		private:
			/**
			 * The identifier for the agent.
			 */
			string agentIdValue;
			/**
			 * The identifier system in use.
			 */
			string agentIdType;
		};
	}
}

#endif
