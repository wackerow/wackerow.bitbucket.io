#ifndef POPULATION_H
#define POPULATION_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * Set of specific units (people, entities, objects, events) with specification of time and geography.
 * 
 * Examples
 * ==========
 * 1. Canadian adult persons residing in Canada on 13 November 1956
 * 2. US computer companies at the end of 2012  
 * 3. Universities in Denmark 1 January 2011.
 * 
 * Explanatory notes
 * ===================
 * Population is the most specific in the conceptually narrowing hierarchy of UnitType, Universe and Population. Several Populations having differing time and or geography may specialize the same Universe.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Population
 */
class Population : ClassLibrary::Conceptual::Concept {

		private:
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * A short natural language account of the characteristics of the object.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;
			/**
			 * The time period associated with the Population
			 */
			std::vector<ClassLibrary::ComplexDataTypes::DateRange> timePeriodOfPopulation;
		};
	}
}

#endif
