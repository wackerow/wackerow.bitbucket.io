#ifndef SIGNIFIED_H
#define SIGNIFIED_H

namespace ClassLibrary {
	namespace SignificationPattern {
		/**
 * Definition
 * ============
 * Concept or object denoted by the signifier associated to a sign.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Concept or object represented by the sign.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Signified : ClassLibrary::CollectionsPattern::CollectionMember {
		};
	}
}

#endif
