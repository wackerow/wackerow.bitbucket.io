#ifndef COMPUTATIONACTION_H
#define COMPUTATIONACTION_H

namespace ClassLibrary {
	namespace Workflows {
		/**
 * Definition
 * ============
 * Provides an extensible framework for specific computation or transformation objects.
 * 
 * Examples
 * ==========
 * In data processing, a ComputationAction might be a code statement in a code sequence.
 * 
 * In data capture, a ComputationAction might takes the form of a question, a measurement or instrument code.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:ComputationItemType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ComputationAction : ClassLibrary::Workflows::Act {

		private:
			/**
			 * Describes the transformation activity
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString activityDescription;
			/**
			 * The command code performed as a part of the transformation or computation
			 */
			ClassLibrary::ComplexDataTypes::CommandCode usesCommandCode;
			/**
			 * Allows for the specification of a computation or transformation type supporting the use of an external controlled vocabulary
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfComputation;
		};
	}
}

#endif
