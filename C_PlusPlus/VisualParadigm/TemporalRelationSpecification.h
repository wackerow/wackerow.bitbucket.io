#ifndef TEMPORALRELATIONSPECIFICATION_H
#define TEMPORALRELATIONSPECIFICATION_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum TemporalRelationSpecification {
			TemporalMeets, 
			TemporalContains, 
			TemporalFinishes, 
			TemporalPrecedes, 
			TemporalStarts, 
			TemporalOverlaps, 
			TemporalEquals
		};
	}
}

#endif
