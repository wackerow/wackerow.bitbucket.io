#ifndef TELEPHONE_H
#define TELEPHONE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Details of a telephone number including the number, type of number, a privacy setting and an indication of whether this is the preferred contact number.
 * 
 * Examples
 * ==========
 * +12 345 67890123
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:TelephoneType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Telephone {

		private:
			/**
			 * The telephone number including country code if appropriate.
			 */
			string telephoneNumber;
			/**
			 * Indicates type of telephone number provided (home, fax, office, cell, etc.). Supports the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfTelephone;
			/**
			 * Time period during which the telephone number is valid.
			 */
			ClassLibrary::ComplexDataTypes::DateRange effectiveDates;
			/**
			 * Specify the level privacy for the telephone number as public, restricted, or private. Supports the use of an external controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry privacy;
			/**
			 * Set to "true" if this is the preferred telephone number for contact.
			 */
			boolean isPreferred;
		};
	}
}

#endif
