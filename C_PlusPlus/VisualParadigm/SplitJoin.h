#ifndef SPLITJOIN_H
#define SPLITJOIN_H

namespace ClassLibrary {
	namespace Workflows {
		/**
 * Definition
 * ============
 * SplitJoin consists of process steps that are executed concurrently (execution with barrier synchronization). That is, SplitJoin completes when all of its components processes have completed. 
 * 
 * Examples
 * ==========
 * A GANTT chart where a number of processes running parallel to each other are all prerequisites of a subsequent step.
 * 
 * Explanatory notes
 * ===================
 * Supports parallel processing that requires completion of all included process steps to exit.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SplitJoin : ClassLibrary::Workflows::WorkflowStep {
		};
	}
}

#endif
