#ifndef ORGANIZATION_H
#define ORGANIZATION_H

namespace ClassLibrary {
	namespace Agents {
		/**
 * Definition
 * ============
 * A framework of authority designated to act toward some purpose.
 * 
 * Examples
 * ==========
 * U.S. Census Bureau, University of Michigan/ISR, Norwegian Social Data Archive 
 * 
 * Explanatory notes
 * ===================
 * related to org:Organization which is described as "Represents a collection of people organized together into a community or other..."
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Organization : ClassLibrary::Agents::Agent {

		private:
			/**
			 * Names by which the organization is known.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::OrganizationName> hasOrganizationName;
			/**
			 * The agency identifier of the organization as registered at the DDI Alliance register.
			 */
			std::vector<string> ddiId;
			/**
			 * Contact information for the organization including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.
			 */
			ClassLibrary::ComplexDataTypes::ContactInformation hasContactInformation;
		};
	}
}

#endif
