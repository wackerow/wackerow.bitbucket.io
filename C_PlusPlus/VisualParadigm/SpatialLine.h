#ifndef SPATIALLINE_H
#define SPATIALLINE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Defines a geographic line (minimum of 2 points)
 * 
 * Examples
 * ==========
 * Street, River
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SpatialLine {

		private:
			/**
			 * A URI for the spatial file containing the line
			 */
			ClassLibrary::XMLSchemaDatatypes::anyUri uri;
			/**
			 * The lineLinkCode is the identifier of the specific line within the file.
			 */
			string lineLinkCode;
			/**
			 * The format of the shape file existing at the location indicated by the sibling ExternalURI element.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry shapeFileFormat;
			/**
			 * A geographic point defined by a latitude and longitude. A minimum of 2 points is required in order to define the line.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::SpatialPoint> point;
		};
	}
}

#endif
