#ifndef SIGN_H
#define SIGN_H

namespace ClassLibrary {
	namespace SignificationPattern {
		/**
 * Definition
 * ============
 * Something that suggests the presence or existence of a fact, condition, or quality.
 * 
 * Examples
 * ==========
 * The terms “Unemployment” and “Arbeitslosigkeit”
 * 
 * Explanatory notes
 * ===================
 * It is a perceivable object used to denote a concept or an object.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Sign : ClassLibrary::CollectionsPattern::CollectionMember {

		private:
			/**
			 * A perceivable object used to denote a signified.
			 */
			ClassLibrary::SignificationPattern::Signifier representation;
		};
	}
}

#endif
