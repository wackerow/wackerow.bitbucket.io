#ifndef CUSTOMITEM_H
#define CUSTOMITEM_H

namespace ClassLibrary {
	namespace CustomMetadata {
		/**
 * Definition
 * ============
 * A custom item description. This allows the definition of an item which is a member of a CustomStructure.
 * It defines the minimum and maximum number of occurrences and representation of a CustomValue.
 * 
 * Examples
 * ==========
 * DegreeOfBurden for a question as required by the U.S. OMB.
 * 
 * BloodPressureCuffSize - the size of a blood pressure cuff specified for a particular protocol.
 * 
 * Explanatory notes
 * ===================
 * This definition will be referenced by a CustomValue when recording a key,value instance
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CustomItem : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * The maximum number of occurrences of an associated CustomValue
			 */
			int maxOccurs;
			/**
			 * The minimum number of occurrences of an associated CustomValue
			 */
			int minOccurs;
			/**
			 * The key to be used by a key,value pair
			 */
			string key;
		};
	}
}

#endif
