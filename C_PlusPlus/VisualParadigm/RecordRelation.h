#ifndef RECORDRELATION_H
#define RECORDRELATION_H

namespace ClassLibrary {
	namespace LogicalDataDescription {
		/**
 * Definition
 * ============
 * The RecordRelation object is used to indicate relationships among record types within and between LogicalRecords. For InstanceVariables existing in a LogicalRecord with multiple record layouts, pairs of InstanceVariables may function as paired keys to permit the expression of hierarchical links between records of different types. These links between keys in different record types could also be used to link records in a relational structure.
 * 
 * Examples
 * ==========
 * One LogicalRecord containing a PersonIdentifier and a PersonName and another LogicalRecord containing a MeasurementID, a PersonID, a SystolicPressure, and a DiastolicPressure could be linked by a RecordRelation. The RecordRelation could employ an InstanceVariableValueMap to describe the linkage between  PersonIdentifier and PersonID.
 * 
 * Explanatory notes
 * ===================
 * A household-level LogicalRecord might contain an InstanceVariable called HouseholdID and a person-level LogicalRecord might contain an InstanceVariable called HID. Describing a link between HouseholdID and HID would allow the linking of a person-level LogicalRecord to their corresponding household-level LogicalRecord.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class RecordRelation : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A display label for the CollectionCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
			/**
			 * Correspondences between Instance Variables of different Logical Record Layout.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InstanceVariableValueMap> correspondence;
		};
	}
}

#endif
