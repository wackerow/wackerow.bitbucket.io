#ifndef TYPEDDESCRIPTIVETEXT_H
#define TYPEDDESCRIPTIVETEXT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * This Complex Data Type bundles a descriptiveText with an External Controlled Vocabulary Entry allowing structured content and a means of typing that content. For example specifying that the description provides a Table of Contents for a document.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class TypedDescriptiveText {

		private:
			/**
			 * Uses a controlled vocabulary entry to classify the description provided.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfContent;
			/**
			 * A short natural language account of the characteristics of the object.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;
		};
	}
}

#endif
