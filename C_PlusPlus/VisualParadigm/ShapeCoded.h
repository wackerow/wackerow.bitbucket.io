#ifndef SHAPECODED_H
#define SHAPECODED_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum ShapeCoded {
			Rectangle, 
			Circle, 
			Polygon, 
			LinearRing
		};
	}
}

#endif
