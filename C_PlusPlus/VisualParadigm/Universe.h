#ifndef UNIVERSE_H
#define UNIVERSE_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * A defined class of people, entities, events, or objects, with no specification of time and geography, contextualizing a Unit Type
 * 
 * Examples
 * ==========
 * 1. Canadian adults (not limited to those residing in Canada)
 * 2. Computer companies 
 * 3. Universities
 * 
 * Explanatory notes
 * ===================
 * Universe sits in a hierarchy between UnitType and Population, with UnitType being most general and Population most specific. A Universe is a set of entities defined by a more narrow specification than that of an underlying UnitType. A Population further narrows the specification to a specific time and geography.
 * 
 * If the Universe consist of people a number of factors may be used in defining membership in the Universe, such as age, sex, race, residence, income, veteran status, criminal convictions, etc. The universe may consist of elements other than persons, such as housing units, court cases, deaths, countries, etc. A universe may be described as "inclusive" or "exclusive". 
 * 
 * Not to be confused with Population, which includes the specification of time and geography.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * c:UniverseType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Population
 */
class Universe : ClassLibrary::Conceptual::Concept {

		private:
			/**
			 * The default value is "true". The description statement of a universe is generally stated in inclusive terms such as "All persons with university degree". Occasionally a universe is defined by what it excludes, i.e., "All persons except those with university degree". In this case the value would be changed to "false".
			 */
			boolean isInclusive;
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * A short natural language account of the characteristics of the object.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;
		};
	}
}

#endif
