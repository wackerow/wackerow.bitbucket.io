#ifndef POINTFORMAT_H
#define POINTFORMAT_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum PointFormat {
			DecimalDegree, 
			DegreesMinutesSeconds, 
			DecimalMinutes, 
			Meters, 
			Feet
		};
	}
}

#endif
