#ifndef AUDIOSEGMENT_H
#define AUDIOSEGMENT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes the type and length of the audio segment.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:AudioType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AudioSegment {

		private:
			/**
			 * The type of audio clip provided. Supports the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfAudioClip;
			/**
			 * The point to begin the audio clip. If no point is provided the assumption is that the start point is the beginning of the clip provided.
			 */
			string audioClipBegin;
			/**
			 * The point to end the audio clip. If no point is provided the assumption is that the end point is the end of the clip provided.
			 */
			string audioClipEnd;
		};
	}
}

#endif
