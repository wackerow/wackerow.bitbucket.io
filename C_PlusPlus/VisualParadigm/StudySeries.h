#ifndef STUDYSERIES_H
#define STUDYSERIES_H

namespace ClassLibrary {
	namespace StudyRelated {
		/**
 * Definition
 * ============
 * A collection of studies which can be structured as a simple sequence, or with a more complex structure.
 * 
 * Examples
 * ==========
 * An annual series of surveys.
 * 
 * Explanatory notes
 * ===================
 * A set of studies may be defined in many ways. A study may be repeated over time. One or more studies may attempt to replicate an earlier study. The StudySeries allows for the description of the relationships among a set of studies.
 * 
 * A simple ordered or unordered sequence of studies can be described via the "contains StudyIndicator" property. More complex relationships among studies may also be described using the optional "isStructuredBy StudyRelationsStructure".
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class StudySeries : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * The name of the series as a whole
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * A general descriptive overview of the series.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::StudyIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
