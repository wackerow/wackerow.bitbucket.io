#ifndef PRECONDITION_H
#define PRECONDITION_H

namespace ClassLibrary {
	namespace Methodologies {
		/**
 * Definition
 * ============
 * A precondition is a state. The state includes one or more goals that were previously achieved. This state is the necessary condition before a process can begin.
 * 
 * Examples
 * ==========
 * Using goals and preconditions processes can be chained to form work flows. Note that these work flows do not include data flows. Instead data flows consisting ouf outputs, inputs and bindings are a separate swim lane. 
 * 
 * Explanatory notes
 * ===================
 * A precondition related to a design defines the state that must exist in order for a design being applied. For example in applying a Sampling Design there may be a precondition for the existance of a sampling frame meeting certain specifications.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Precondition : ClassLibrary::Methodologies::BusinessFunction {
		};
	}
}

#endif
