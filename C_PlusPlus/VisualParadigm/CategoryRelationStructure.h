#ifndef CATEGORYRELATIONSTRUCTURE_H
#define CATEGORYRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * Relation structure of categories within a collection. Allows for the specification of complex relationships among categories.
 * 
 * Examples
 * ==========
 * The category of student might be described as having sub-types of primary school student and high school student.
 * 
 * Explanatory notes
 * ===================
 * The CategoryRelationStructure employs a set of CategoryRelations to describe the relationship among concepts. Each CategoryRelation is a one to many description of connections between categories. Together they might commonly describe relationships as complex as a hierarchy. 
 * This is a kind of a ConceptRelationStructure restricted to categories (which are concepts).
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CategoryRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list. Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Restricted to CategoryRelation. CategoryRelations making up the relation structure.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::CategoryRelation> hasMemberRelation;
		};
	}
}

#endif
