#ifndef VIDEOSEGMENT_H
#define VIDEOSEGMENT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes the type and length of the video segment.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:VideoType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class VideoSegment {

		private:
			/**
			 * The type of video clip provided. Supports the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfVideoClip;
			/**
			 * The point to begin the video clip. If no point is provided the assumption is that the start point is the beginning of the clip provided.
			 */
			string videoClipBegin;
			/**
			 * The point to end the video clip. If no point is provided the assumption is that the end point is the end of the clip provided.
			 */
			string videoClipEnd;
		};
	}
}

#endif
