#ifndef VARIABLESTATISTICS_H
#define VARIABLESTATISTICS_H

namespace ClassLibrary {
	namespace SimpleCodebook {
		/**
 * Definition
 * ============
 * Contains summary and category level statistics for the referenced variable.
 * 
 * Examples
 * ==========
 * A mean of values for a variable "Height". Counts for each level of variable "Gender" (male and female)
 * 
 * Explanatory notes
 * ===================
 *  Includes information on the total number of responses, the weights in calculating the statistics, variable level summary statistics, and category statistics. The category statistics may be provided as unfiltered values or filtered through a single variable. For example the category statistics for Sex filtered by the variable Country for a multi-national data file. Note that if no weighting factor is identified, all of the statistics provided are unweighted.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * pi:VariableStatisticsType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class VariableStatistics : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * The total number of responses to this variable. This element is especially useful if the number of responses does not match added case counts. It may also be used to sum the frequencies for variable categories.
			 */
			int totalResponses;
			/**
			 * Statistic that provides a summary value, filtered or unfiltered, weighted or unweighted, expressed as an xs:decimal or xs:double
			 */
			std::vector<ClassLibrary::ComplexDataTypes::SummaryStatistic> hasSummaryStatistic;
			/**
			 * Statistics that provides a category specific values, filtered or unfiltered, weighted or unweighted, expressed as an xs:decimal or xs:double
			 */
			std::vector<ClassLibrary::ComplexDataTypes::CategoryStatistic> hasCategoryStatistic;
		};
	}
}

#endif
