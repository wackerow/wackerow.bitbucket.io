#ifndef AGENTLISTING_H
#define AGENTLISTING_H

namespace ClassLibrary {
	namespace Agents {
		/**
 * Definition
 * ============
 * A listing of Agents of any type. The AgentList may be organized to describe relationships between members using AgentRelationStructure.
 * 
 * Examples
 * ==========
 * Organizations contributing to a project. Individuals within an agency. All organizations, indivduals, and machines identified within the collections of an archive.
 * 
 * Explanatory notes
 * ===================
 * Relationships between agents are fluid and reflect effective dates of the relationship. An agent may have multiple relationships which may be sequencial or concurrent. Relationships may or may not be hierarchical in nature. All Agents are serialized individually and brought into relationships as appropriate.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AgentListing : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of the Agent Listing. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
