#ifndef VOCABULARYENTRY_H
#define VOCABULARYENTRY_H

namespace ClassLibrary {
	namespace CustomMetadata {
		/**
 * Definition
 * ============
 * One entry term and its definition in an ordered list comprising a controlled vocabulary.
 * 
 * Examples
 * ==========
 * aunt - The female sibling of a parent
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class VocabularyEntry : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * the term being defined
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString entryTerm;
			/**
			 * An explanation (definition) of the value
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString definition;
		};
	}
}

#endif
