#ifndef VARIABLERELATIONSTRUCTURE_H
#define VARIABLERELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * RelationStructure for use with any mixture of Variables in the Variable Cascade (Conceptual, Represented, Instance).
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * A VariableCollection can be described as a simple list, or as having a more complex structure. All of the "administrative" variables for a study might be described as just a simple list without a VariableRelationStructure. If, however, the administrative variables needed to be described in subgroups, a VariableRelationStructure could be employed.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class VariableRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Member relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::VariableRelation> hasMemberRelation;
		};
	}
}

#endif
