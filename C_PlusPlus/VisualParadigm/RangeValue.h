#ifndef RANGEVALUE_H
#define RANGEVALUE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes a bounding value of a string.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:RangeValueType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class RangeValue : ClassLibrary::ComplexDataTypes::ValueString {

		private:
			/**
			 * Set to "true" if the value is included in the range.
			 */
			boolean included;
		};
	}
}

#endif
