#ifndef COMMAND_H
#define COMMAND_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Provides the following information on the command. The content of the command and the programming language used.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CommandType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Command {

		private:
			/**
			 * Designates the programming language used for the command. Supports the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry programLanguage;
			/**
			 * Content of the command itself expressed in the language designated in Programming Language.
			 */
			string commandContent;
		};
	}
}

#endif
