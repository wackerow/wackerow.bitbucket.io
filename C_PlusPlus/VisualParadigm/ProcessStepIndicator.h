#ifndef PROCESSSTEPINDICATOR_H
#define PROCESSSTEPINDICATOR_H

namespace ClassLibrary {
	namespace ProcessPattern {
		/**
 * Definition
 * ============
 * Member Indicator limited to ProcessStep
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ProcessStepIndicator : ClassLibrary::CollectionsPattern::MemberIndicator {
		};
	}
}

#endif
