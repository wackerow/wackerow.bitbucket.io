#ifndef ACCESSLOCATION_H
#define ACCESSLOCATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * A set of access information for a Machine including URI, mime type, and physical location
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AccessLocation {

		private:
			/**
			 * A URI for access, normally expressed as a URL
			 */
			std::vector<ClassLibrary::XMLSchemaDatatypes::anyUri> uri;
			/**
			 * The mime type. Supports the use of an controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry mimeType;
			/**
			 * The physical location of the machine
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> physicalLocation;
		};
	}
}

#endif
