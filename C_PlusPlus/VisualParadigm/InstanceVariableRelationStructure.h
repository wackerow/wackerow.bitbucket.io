#ifndef INSTANCEVARIABLERELATIONSTRUCTURE_H
#define INSTANCEVARIABLERELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace LogicalDataDescription {
		/**
 * Definition
 * ============
 * A realization of RelationStructure that is used to describe the order of InstanceVariables in a record for those cases where the variables have a more complicated logical order than a simple sequence.
 * 
 * 
 * Examples
 * ==========
 * The HL7 FHIR (Fast Healthcare Interoperability Resources) EHR is a non-homogeneous, hierarchical, recursive aggregate datatype.
 * 
 * The openEHR archetypes that are one component in an EHR are non-homogeneous, hierarchical, non-recursive aggregate datatypes.
 * 
 * Explanatory notes
 * ===================
 * For a simple sequence the order can be defined by the index values of the LogicalRecord's InstanceVariableIndicators that a UnitDataRecord inherits. Alternatively, the InstanceVariableRelationStructure can be used by a UnitDataRecord to describe a StructuredCollection as needed. 
 * 
 * In terms of ISO-11404 an InstanceVariableRelationStructure is able to create "aggregate datatypes". These aggregate datatypes may be homogeneous if all the component datatypes are the same datatype or non-homogeneous. Aggregate datatypes may be hierarchical or not and they may be recursive or not. Under IS0-11404 the InstanceVariableRelationStructure qualifies as a "datatype generator".
 * 
 * Synonyms
 * ==========
 * An information model, a struct in C, a nested object in JSON
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class InstanceVariableRelationStructure : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString criteria;
			/**
			 * A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
			/**
			 * Controlled vocabulary for the order relation semantics. It should contain, at least, the following: Self_Or_Descendant_Of, Part_Of, Less_Than_Or_Equal_To, Subtype_Of, Subclass_Of.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Controlled Vocabulary to specify whether the relation is total, partial or unknown.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Member relations of InstanceVariables that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InstanceVariableRelation> hasMemberRelation;
		};
	}
}

#endif
