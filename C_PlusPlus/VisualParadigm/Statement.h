#ifndef STATEMENT_H
#define STATEMENT_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * A Statement is a type of Instrument Component containing  human readable text or referred material. 
 * 
 * Examples
 * ==========
 * Introductory text; Explanations; Labels, Headers, Help screens, etc.  "Thank you for agreeing to take this survey. We will start with a brief set of demographic questions." "The following set of questions are related to your household income. Please consider all members of the household and all sources of income when answering these questions."
 * 
 * Explanatory notes
 * ===================
 * It is not directly related to another specific Instrument Component such as an InstanceQueston or InstanceMeasurement. It may be placed anywhere in a WorkflowStepSequence. 
 * 
 * Synonyms
 * ==========
 * DDI:StatementItem
 * 
 * DDI 3.2 mapping
 * =================
 * d:StatementItemType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Statement : ClassLibrary::DataCapture::InstrumentComponent {

		private:
			/**
			 * Structured human-readable text that allows for dynamic content. For example, the insertion of a name or gender specific pronoun. Repeat ONLY for capturing the same content in multiple languages.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::DynamicText> statementText;
			/**
			 * Describes the use of the statement within the instrument. For example, a section divider, welcome statement, or other text.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry purposeOfStatement;
		};
	}
}

#endif
