#ifndef AGENTRELATIONSTRUCTURE_H
#define AGENTRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Agents {
		/**
 * Definition
 * ============
 * Defines the relationships between Agents in a collection. Clarifies valid period and the purpose the relationship serve
 * 
 * Examples
 * ==========
 * An individual employed by an Organization. A unit or project (organization) within another Organization.
 * 
 * Explanatory notes
 * ===================
 * Describes relations between agents not roles within a project or in relationship to a product. Roles are defined by the parent class and relationship name that uses an Agent as a target.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AgentRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * The effective start and end date of the relationship
			 */
			ClassLibrary::ComplexDataTypes::DateRange effectiveDates;
			/**
			 * Define the level of privacy regarding this relationship. Supports the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry privacy;
			/**
			 * Explanation of the intent of creating the relation. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list. Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relation structure
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Agent relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentRelation> hasMemberRelation;
		};
	}
}

#endif
