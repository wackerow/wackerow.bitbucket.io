#ifndef UNITTYPE_H
#define UNITTYPE_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * A Unit Type is a class of objects of interest.
 * 
 * Examples
 * ==========
 * Person, Establishment, Household, State, Country, Dog, Automobile
 * 
 * Explanatory notes
 * ===================
 * UnitType is the most general in the hierarchy of UnitType, Universe, and Population. It is a description of the basic characteristic for a general set of Units.  A Universe is a set of entities defined by a more narrow specification than that of an underlying UnitType. A Population further narrows the specification to a specific time and geography.
 * A Unit Type is used to describe a class or group of Units based on a single characteristic with no specification of time and geography.  For example, the Unit Type of “Person” groups together a set of Units based on the characteristic that they are ‘Persons’.
 * It concerns not only Unit Types used in dissemination, but anywhere in the statistical process. E.g. using administrative data might involve the use of a fiscal unit. [GSIM 1.1]
 * 
 * Synonyms
 * ==========
 * Object Class [ISO11179]
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Unit Type
 */
class UnitType : ClassLibrary::Conceptual::Concept {

		private:
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * A short natural language account of the characteristics of the object.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;
		};
	}
}

#endif
