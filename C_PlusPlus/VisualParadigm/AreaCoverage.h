#ifndef AREACOVERAGE_H
#define AREACOVERAGE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Use to specify the area of land, water, total or other area coverage in terms of square miles/kilometers or other measure as part of a Geographic Extent.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:AreaCoverageType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AreaCoverage {

		private:
			/**
			 * Specify the type of area covered i.e. Total, Land, Water, etc. Supports the use of an external controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfArea;
			/**
			 * Records the measurement unit, for example, Square Kilometer, Square Mile. Supports the use of an external controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry measurementUnit;
			/**
			 * The area measure expressed as a decimal for the measurement unit designated.
			 */
			Real areaMeasure;
		};
	}
}

#endif
