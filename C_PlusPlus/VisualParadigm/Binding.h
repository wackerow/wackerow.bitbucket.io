#ifndef BINDING_H
#define BINDING_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Binds two parameters together to direct flow of data through a process
 * 
 * Examples
 * ==========
 * From the output of an InstanceQuestion to the input of a ComputationAction
 * 
 * Explanatory notes
 * ===================
 * Binding is used to define the flow of data into, out of, and within a process. It is separate from the flow of a process. When used in the workflow of a data capture process most data may go from a capture to an instance variable, but when needed as a check sum in a loop, a recoding process, or conditional content for DynamicText, Binding provides the means for explicitly directing the movement of data from one point to another in the process.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Binding {
		};
	}
}

#endif
