#ifndef CUSTOMVALUEINDICATOR_H
#define CUSTOMVALUEINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Member Indicator for use with member type CustomValue
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CustomValueIndicator {

		private:
			/**
			 * Index value of member in an ordered array
			 */
			int index;
		};
	}
}

#endif
