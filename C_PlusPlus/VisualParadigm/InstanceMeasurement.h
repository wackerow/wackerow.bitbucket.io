#ifndef INSTANCEMEASUREMENT_H
#define INSTANCEMEASUREMENT_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * An instance measurement instantiates a represented measurement, so that it can be used as an Act in the Process Steps that define a data capture process.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class InstanceMeasurement : ClassLibrary::DataCapture::InstrumentComponent {
		};
	}
}

#endif
