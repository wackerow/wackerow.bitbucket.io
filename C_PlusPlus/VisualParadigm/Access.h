#ifndef ACCESS_H
#define ACCESS_H

namespace ClassLibrary {
	namespace Discovery {
		/**
 * Definition
 * ============
 * Describes access to the annotated object. This item includes a confidentiality statement, descriptions of the access permissions required, restrictions to access, citation requirements, depositor requirements, conditions for access, a disclaimer, any time limits for access restrictions, and contact information regarding access.
 * 
 * Examples
 * ==========
 * A proprietary InstanceQuestion might have specific access restrictions.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:AccessType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Access : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * A statement regarding the confidentiality of the related data or metadata.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString confidentialityStatement;
			/**
			 * A link to a form used to provide access to the data or metadata including a statement of the purpose of the form.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Form> accessPermission;
			/**
			 * A statement regarding restrictions to access. May be expressed in multiple languages and supports the use of structured content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString restrictions;
			/**
			 * A statement regarding the citation requirement. May be expressed in multiple languages and supports the use of structured content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString citationRequirement;
			/**
			 * A statement regarding depositor requirements. May be expressed in multiple languages and supports the use of structured content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString depositRequirement;
			/**
			 * A statement regarding conditions for access. May be expressed in multiple languages and supports the use of structured content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString accessConditions;
			/**
			 * A disclaimer regarding the liability of the data producers or providers. May be expressed in multiple languages and supports the use of structured content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString disclaimer;
			/**
			 * The agent to contact regarding access including the role of the agent.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentAssociation> contactAgent;
			/**
			 * The date range or start date of the access description.
			 */
			ClassLibrary::ComplexDataTypes::DateRange validDates;
		};
	}
}

#endif
