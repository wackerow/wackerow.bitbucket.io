#ifndef CATEGORYRELATION_H
#define CATEGORYRELATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Source target relationship between categories in a classification relation structure
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CategoryRelation {

		private:
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
		};
	}
}

#endif
