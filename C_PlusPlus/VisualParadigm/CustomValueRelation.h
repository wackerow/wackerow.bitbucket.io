#ifndef CUSTOMVALUERELATION_H
#define CUSTOMVALUERELATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Defines the relation between CustomValues
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CustomValueRelation {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
		};
	}
}

#endif
