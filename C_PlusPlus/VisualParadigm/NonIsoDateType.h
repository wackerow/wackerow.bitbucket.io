#ifndef NONISODATETYPE_H
#define NONISODATETYPE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Used to preserve an historical date, formatted in a non-ISO fashion.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:HistoricalDateType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class NonIsoDateType {

		private:
			/**
			 * This is the date expressed in a non-ISO compliant structure. Primarily used to retain legacy content or to express non-Gregorian calender dates.
			 */
			string dateContent;
			/**
			 * Indicate the structure of the date provided in NonISODate. For example if the NonISODate contained 4/1/2000 the Historical Date Format would be mm/dd/yyyy. The use of a controlled vocabulary is strongly recommended to support interoperability.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry nonIsoDateFormat;
			/**
			 * Specifies the type of calendar used (e.g., Gregorian, Julian, Jewish).
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry calendar;
		};
	}
}

#endif
