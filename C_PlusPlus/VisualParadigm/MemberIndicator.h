#ifndef MEMBERINDICATOR_H
#define MEMBERINDICATOR_H

namespace ClassLibrary {
	namespace CollectionsPattern {
		/**
 * Definition
 * ============
 * Provides ability to declare an optional sequence or index order to a Member. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class MemberIndicator {

		private:
			/**
			 * Index number expressed as an integer. The position of the member in an ordered array. Optional for unordered Collections.
			 */
			int index;
		};
	}
}

#endif
