#ifndef WORKFLOWPROCESS_H
#define WORKFLOWPROCESS_H

namespace ClassLibrary {
	namespace Workflows {
		/**
 * Definition
 * ============
 * A Workflow Process is a realization of Process which identifies the Workflow Step Sequence which contains the WorkflowSteps and their order. It may identify the algorithm that it implements.
 * 
 * Examples
 * ==========
 * Overall description of steps taken when ingesting a dataset into an archive; A Sampling Process; A step or sub-step of the Generic Longitudinal Business Process Model (GLBPM).
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class WorkflowProcess : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Explanation of the ways in which the pattern is employed. Supports  the use of multiple languages and  structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
		};
	}
}

#endif
