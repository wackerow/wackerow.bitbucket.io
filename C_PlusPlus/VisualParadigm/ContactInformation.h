#ifndef CONTACTINFORMATION_H
#define CONTACTINFORMATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Contact information for the individual or organization including location specification, address, web site, phone numbers, and other means of communication access. Address, location, telephone, and other means of communication can be repeated to express multiple means of a single type or change over time. Each major piece of contact information contains the element EffectiveDates in order to date stamp the period for which the information is valid.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:ContactInformationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ContactInformation {

		private:
			/**
			 * The URL of the Agent's website
			 */
			std::vector<ClassLibrary::ComplexDataTypes::WebLink> website;
			/**
			 * Email contact information
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Email> hasEmail;
			/**
			 * Electronic messaging other than email
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ElectronicMessageSystem> electronicMessaging;
			/**
			 * The address for contact.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Address> hasAddress;
			/**
			 * Telephone for contact
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Telephone> hasTelephone;
		};
	}
}

#endif
