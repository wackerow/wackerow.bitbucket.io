#ifndef WORKFLOWSERVICE_H
#define WORKFLOWSERVICE_H

namespace ClassLibrary {
	namespace Workflows {
		/**
 * Definition
 * ============
 * A means of performing a Workflow Step as part of a concrete implementation of a Business Function  (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve).
 * 
 * Examples
 * ==========
 * A specification for an air flow monitor used to capture a measure for an ImplementedMeasure (a subtype of Act).
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class WorkflowService : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Specifies how to communicate with the service.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry> serviceInterface;
			/**
			 * Specifies where the service can be accessed.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry serviceLocation;
			/**
			 * The estimated time period associated with the operation of the Service. This may be expressed as a time, date-time, or duration.
			 */
			ClassLibrary::ComplexDataTypes::Date estimatedDuration;
		};
	}
}

#endif
