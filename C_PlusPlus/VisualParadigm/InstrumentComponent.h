#ifndef INSTRUMENTCOMPONENT_H
#define INSTRUMENTCOMPONENT_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * InstrumentComponent is an abstract object which extends an Act (a type of Process Step). The purpose of InstrumentComponent is to provide a common parent for Capture (e.g., Question, Measure), Statement, and Instructions.
 * 
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * InstrumentComponent acts as a substitution head (abstract) for semantically meaningful for objects that extend it and exist in an instrument.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class InstrumentComponent : ClassLibrary::Workflows::Act {
		};
	}
}

#endif
