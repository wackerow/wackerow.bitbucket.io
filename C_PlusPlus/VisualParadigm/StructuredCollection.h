#ifndef STRUCTUREDCOLLECTION_H
#define STRUCTUREDCOLLECTION_H

namespace ClassLibrary {
	namespace CollectionsPattern {
		/**
 * Definition
 * ============
 * Structured Collection container extends a Simple Collection allowing it to optionally have an RelationshpStructure to model hierarchies and nesting or more complex network structures. Like Simple Collection it is also a subtype of Member to allow for nested collections. A Collection can be described directly as having unordered members or an indexed array of members through a set of MemberIndicators. 
 * 
 * Examples
 * ==========
 * Node Sets, Schemes, Groups, Concept Systems are all types of Collections.
 * 
 * Explanatory notes
 * ===================
 * Members have to belong to some Collection, except in the case of nested Collections where the top level Collection is a Member that doesn't belong to any Collection.
 * If a Collection is ordered as a simple list that ordering can be indicated by an index property of each MemberIndicator. If the Collection is unordered, the index is not necessary.
 * Collection is not extended, it is realized (see Collection Pattern documentation).
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Node Set
 */
class StructuredCollection : ClassLibrary::CollectionsPattern::SimpleCollection {
		};
	}
}

#endif
