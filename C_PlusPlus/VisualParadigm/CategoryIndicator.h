#ifndef CATEGORYINDICATOR_H
#define CATEGORYINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Member Indicator for use when the member type is restricted to Category.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CategoryIndicator {

		private:
			/**
			 * Index value of member in an ordered array
			 */
			int index;
		};
	}
}

#endif
