#ifndef COMPARISON_H
#define COMPARISON_H

namespace ClassLibrary {
	namespace CollectionsPattern {
		/**
 * Definition
 * ============
 * The minimal pattern for a comparison including a mapping between members.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Comparison : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * A map defining the match relationship and the members in the relationship
			 */
			std::vector<ClassLibrary::CollectionsPattern::ComparisonMap> correspondence;
		};
	}
}

#endif
