#ifndef TABLEDIRECTIONVALUES_H
#define TABLEDIRECTIONVALUES_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum TableDirectionValues {
			Ltr, 
			Rtl, 
			Auto
		};
	}
}

#endif
