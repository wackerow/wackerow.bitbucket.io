#ifndef CHARACTEROFFSET_H
#define CHARACTEROFFSET_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Specification of the character offset for the beginning and end of the segment, or beginning and length.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CharacterParameterType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CharacterOffset {

		private:
			/**
			 * Number of characters from beginning of the document, indicating the inclusive start of the text range.
			 */
			int startCharOffset;
			/**
			 * Number of characters from the beginning of the document, indicating the inclusive end of the text segment.
			 */
			int endCharOffset;
			/**
			 * can be used to describe a text segment as start and length
			 */
			int characterLength;
		};
	}
}

#endif
