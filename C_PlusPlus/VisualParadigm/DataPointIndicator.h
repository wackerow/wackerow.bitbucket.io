#ifndef DATAPOINTINDICATOR_H
#define DATAPOINTINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Member Indicator for use with member type DataPoint
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class DataPointIndicator {

		private:
			/**
			 * Index value of member in an ordered array
			 */
			int index;
		};
	}
}

#endif
