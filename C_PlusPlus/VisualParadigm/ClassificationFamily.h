#ifndef CLASSIFICATIONFAMILY_H
#define CLASSIFICATIONFAMILY_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * A Classification Family is a collection of Classification Series related from a particular point of view. The Classification Family is related by being based on a common Concept (e.g. economic activity).[GSIM1.1]
 * 
 * Examples
 * ==========
 * A family of industrial classifications each a separate series (i.e. U.S. Standard Industrial Classification (SIC) and North American Industrial Classification System (NAICS)
 * 
 * Explanatory notes
 * ===================
 * Different classification databases may use different types of Classification Families and have different names for the families, as no standard has been agreed upon. [GSIM1.1]
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Classification Family
 */
class ClassificationFamily : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Allows for the identification of the member Classification Series and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ClassificationSeriesIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
