#ifndef QUALITYSTATEMENT_H
#define QUALITYSTATEMENT_H

namespace ClassLibrary {
	namespace StudyRelated {
		/**
 * Definition
 * ============
 * A statement of quality which may be related to an external standard or contain a simple overview, rationale, and usage statement. When relating to an external standard information on compliance may be added providing a reference to a ComplianceConcept, an ExternalComplianceCode, as well as a description.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:QualityStatementType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class QualityStatement : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
			/**
			 * Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
		};
	}
}

#endif
