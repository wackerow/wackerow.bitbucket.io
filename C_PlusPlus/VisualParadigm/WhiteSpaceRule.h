#ifndef WHITESPACERULE_H
#define WHITESPACERULE_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum WhiteSpaceRule {
			Preserve, 
			Replace, 
			collapse
		};
	}
}

#endif
