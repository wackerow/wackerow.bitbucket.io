#ifndef TARGETSAMPLE_H
#define TARGETSAMPLE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Specifies details of target sample size, percentage, type, and universe
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class TargetSample {

		private:
			/**
			 * Default value is True. The information is for the primary sample unit or universe. Change to False if the information relates to a secondary or sub- sample universe.
			 */
			boolean isPrimary;
			/**
			 * The target size of the sample expressed as an integer.
			 */
			int targetSize;
			/**
			 * The target size of the sample expressed as an integer (70% expressed as .7)
			 */
			Real targetPercent;
		};
	}
}

#endif
