#ifndef CONCEPTRELATIONSTRUCTURE_H
#define CONCEPTRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * Relation structure of concepts within a collection. Allows for the specification of complex relationships among concepts.
 * 
 * Examples
 * ==========
 * A concept of vacation might be described as having sub-types of beach vacation and mountain vacation.
 * 
 * Explanatory notes
 * ===================
 * The ConceptRelationStructure employs a set of ConceptRelations to describe the relationship among concepts. Each ConceptRelation is a one to many description of connections between concepts. Together they can describe relationships as complex as a hierarchy or even a complete cyclical network as in a concept map.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ConceptRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Restricted to ConceptRelation. ConceptRelations making up the relation structure.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ConceptRelation> hasMemberRelation;
		};
	}
}

#endif
