#ifndef BUSINESSPROCESS_H
#define BUSINESSPROCESS_H

namespace ClassLibrary {
	namespace BusinessWorkflow {
		/**
 * Definition
 * ============
 * BusinessProcesses could be Generic Longitudinal Business Process Model (GLBPM) and/or Generic Statistical Business Process Model (GSBPM) steps and/or sub-steps. BusinessProcesses participate in a DataPipeline by virtue of their preconditions and postconditions. 
 * 
 * A BusinessProcess differs from a WorkflowStep by virtue of its granularity. Higher level operations are the subject of BusinessProcesses. A BusinessProcess has a WorkflowStepSequence through which these higher levels operations may optionally be decomposed into WorkflowSteps.
 * 
 * BusinessProcess preconditions and postconditions differ from input and output parameters because they don't take variables and values. Instead the subject of preconditions and postconditions are typically whole datasets.
 * 
 * Given this level of granularity it is possible to view a BusinessProcess through the Open Archival Information System (OAIS) Reference Model. So a BusinessProcess may optionally be described as a component of a Submission Information Package (SIP), an Archival Information Package (AIP) or a Dissemination Information Package (DIP).
 * 
 * Examples
 * ==========
 * In a single BusinessProcess we might receive data from a data source or merge data from different data sources or create new variables and recodes or format data for data dissemination
 * 
 * Explanatory notes
 * ===================
 * One or more PairedExternalControlledVocabularyEntry objects may be used to identify and characterize controlled vocabularies like the GLBPM, GSBPM and OAIS. The PairedExternalControlledVocabularyEntry both identifies both the model (term) and the step/sub-step of the model (extent). If a BusinessProcess combines two steps from the same model (GLBPM, GSBPM, OAIS, etc.) or from different models, multiple PairedExternalControlledVocabularyEntry objects can be specified.
 * 
 * Synonyms
 * ==========
 * Task
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class BusinessProcess : ClassLibrary::Workflows::WorkflowProcess {

		private:
			/**
			 * If a standard process model such as The Generic Statistical Business Process Model (GSBPN), the Generic Longitudinal Business Process Model (GLBPM), Open Archive Information System model (OAIS), etc. has been related to this business process, the model and step or sub-step is noted here using the Paired External Controlled Vocabulary Entry. Enter the name of the model in "term" and the step, sub-step, or specific portion of the model in "extent".
			 */
			std::vector<ClassLibrary::ComplexDataTypes::PairedExternalControlledVocabularyEntry> standardModelUsed;
			/**
			 * A condition for beginning the Business Process which may include the specification of the data set required for beginning the Business Process.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::BusinessProcessCondition> preCondition;
			/**
			 * A condition for completing and exiting the Business Process which may include the specification of the data set resulting from the Business Process.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::BusinessProcessCondition> postCondition;
		};
	}
}

#endif
