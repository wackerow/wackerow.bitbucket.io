#ifndef PROCESSSEQUENCE_H
#define PROCESSSEQUENCE_H

namespace ClassLibrary {
	namespace ProcessPattern {
		/**
 * Definition
 * ============
 * A collection of Process Steps organized as a sequence.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ProcessSequence : ClassLibrary::CollectionsPattern::SimpleCollection {

		private:
			/**
			 * Restricts target to ProcessStepIndicator
			 */
			std::vector<ClassLibrary::ProcessPattern::ProcessStepIndicator> hasMemberIndicator;
		};
	}
}

#endif
