#ifndef CONCEPTUALDOMAIN_H
#define CONCEPTUALDOMAIN_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * Set of Concepts, both sentinel and substantive, that can be described by either enumeration or by an expression.
 * 
 * Examples
 * ==========
 * Substantive: Housing Unit Tenure - Owned, Rented, Vacant; Sentinel: Non-response - Refused, Don't Know, Not Applicable 
 * 
 * Explanatory notes
 * ===================
 * Intent of a Conceptual Domain is defining a set of concepts used to measure a broader concept. For effective use they should be discrete (non-overlapping) and provide exhaustive coverage of the broader concept.  
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ConceptualDomain : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
		};
	}
}

#endif
