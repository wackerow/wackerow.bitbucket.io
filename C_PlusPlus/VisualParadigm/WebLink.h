#ifndef WEBLINK_H
#define WEBLINK_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * A web site (normally a URL) with information on type of site, privacy flag, and effective dates.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:URLType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class WebLink {

		private:
			/**
			 * Set to "true" if this is the preferred URL.
			 */
			boolean isPreferred;
			/**
			 * Normally a URL
			 */
			ClassLibrary::XMLSchemaDatatypes::anyUri uri;
			/**
			 * The type of URL for example personal, project, organization, division, etc.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfWebsite;
			/**
			 * The period for which this URL is valid.
			 */
			ClassLibrary::ComplexDataTypes::DateRange effectiveDates;
			/**
			 * Indicates the privacy level of this URL
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry privacy;
		};
	}
}

#endif
