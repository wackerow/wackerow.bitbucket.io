#ifndef SAMPLINGPROCESS_H
#define SAMPLINGPROCESS_H

namespace ClassLibrary {
	namespace SamplingMethodology {
		/**
 * Definition
 * ============
 * Provides the details of the process used to apply the sampling design and obtain the sample. Sampling Process contains a Workflow Step which provides the constituents of a Workflow. It can be a composition (a set of Control Constructs) or atomic (an Act) and may be performed by a service. This allows the use of existing Workflow models to express various sampling process actions. Sampling Frames and Sample Population can be used as designated Inputs or Outputs using Parameters.
 * 
 * Examples
 * ==========
 * A Split which takes a sampling stage and divides the sample frame into different subsets. A different sampling technique is applied to each subset. Once a split occurs each subset can have stages underneath, and the number of states under each split subset may differ.
 * A Stage is the application of a single sampling algorithm applied to a sampling frame. For instance, the US Current Population Survey samples geographic areas first before identifying household to contact within each of those areas.
 * A Stratification of a stage into multiple subsets. Each stratified group will be sampled using the same sampling approach. For example stratifying a state by ZIP Code areas in each of 5 mean income quintiles and then doing a random sample of the households in a set of Zip Codes. Allows for oversampling of identified subpopulations.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SamplingProcess : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
