#ifndef NUMBERRANGE_H
#define NUMBERRANGE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Structures a numeric range. Low and High values are designated. The structure identifies Low values that should be treated as bottom coded (Stated value and bellow, High values that should be treated as top coded (stated value and higher), and provides a regular expression to further define the valid content of the range.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:NumberRangeType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class NumberRange {

		private:
			/**
			 * A display label for the number range. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> label;
			/**
			 * High or top value on the range expressed as an xs:decimal within an isInclusive Boolean flag.
			 */
			ClassLibrary::ComplexDataTypes::NumberRangeValue highCode;
			/**
			 * High or top value on the range expressed as an xs:double within an isInclusive Boolean flag.
			 */
			ClassLibrary::ComplexDataTypes::DoubleNumberRangeValue highCodeDouble;
			/**
			 * High or top value on the range expressed as an xs:decimal within an isInclusive Boolean flag.
			 */
			ClassLibrary::ComplexDataTypes::NumberRangeValue lowCode;
			/**
			 * Low or bottom value on the range expressed as an xs:double within an isInclusive Boolean flag.
			 */
			ClassLibrary::ComplexDataTypes::DoubleNumberRangeValue lowCodeDouble;
			/**
			 * Regular expression defining the allowed syntax of the number.
			 */
			string regExp;
			/**
			 * Default value is False. The High or top code represents the value expressed and anything higher.
			 */
			boolean isTopCoded;
			/**
			 * Default value is False. The Low or bottom code represents the value expressed and anything lower.
			 */
			boolean isBottomCoded;
		};
	}
}

#endif
