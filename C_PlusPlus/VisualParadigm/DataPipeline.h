#ifndef DATAPIPELINE_H
#define DATAPIPELINE_H

namespace ClassLibrary {
	namespace BusinessWorkflow {
		/**
 * Definition
 * ============
 * A DataPipeline is a single traversal of the Generic Longitudinal Business Model (GLBPM) and/or the Generic Statistical Business process Model (GSBPM) in the course of a study where a study is either one off or a wave in a StudySeries.
 * 
 * Examples
 * ==========
 * In a study where the study is a wave in a StudySeries, we do a single traversal of the Generic Longitudinal Business Process Model. From one wave to the next the traversal may be different. Each traversal is described in a DataPipeline.
 * 
 * Extract Transform and Load (ETL) platforms support data pipelines to move data between systems. Using an ETL platform, data engineers create data pipelines to orchestrate the movement, transformation, validation, and loading of data, from source to final destination. The DataPipeline describes this "orchestration".
 * 
 * A prospective DataPipeline gives guidance to data engineers. It is a design pattern. A retrospective DataPipeline documents an ETL data pipeline after the fact.
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * Traversal, Data Lifecycle
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class DataPipeline : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Specifies BusinessProcess that participates in the DataPipeline
			 */
			std::vector<ClassLibrary::ComplexDataTypes::BusinessProcessIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
