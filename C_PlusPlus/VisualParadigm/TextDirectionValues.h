#ifndef TEXTDIRECTIONVALUES_H
#define TEXTDIRECTIONVALUES_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum TextDirectionValues {
			Ltr, 
			Rtl, 
			Auto, 
			Inherit
		};
	}
}

#endif
