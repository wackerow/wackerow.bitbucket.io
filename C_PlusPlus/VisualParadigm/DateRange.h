#ifndef DATERANGE_H
#define DATERANGE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Expresses a date/time range using a start date and end date (both with the structure of Date and supporting the use of ISO and non-ISO date structures). Use in all locations where a range of dates is required, i.e. validFor, embargoPeriod, collectionPeriod, etc.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:DateType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class DateRange {

		private:
			/**
			 * The date (time) designating the beginning of the period or range.
			 */
			ClassLibrary::ComplexDataTypes::Date startDate;
			/**
			 * The date (time) designating the end of the period or range.
			 */
			ClassLibrary::ComplexDataTypes::Date endDate;
		};
	}
}

#endif
