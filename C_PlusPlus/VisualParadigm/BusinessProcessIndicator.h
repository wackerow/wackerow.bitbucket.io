#ifndef BUSINESSPROCESSINDICATOR_H
#define BUSINESSPROCESSINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Allows for the identification of the BusinessProcess specifically as a member and optionally provides an index for the member within an ordered array. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class BusinessProcessIndicator {

		private:
			/**
			 * Provides an index for the member within an ordered array
			 */
			int index;
		};
	}
}

#endif
