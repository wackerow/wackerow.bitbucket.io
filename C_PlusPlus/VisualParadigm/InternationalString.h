#ifndef INTERNATIONALSTRING_H
#define INTERNATIONALSTRING_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Packaging structure for multiple language versions of the same string content. Where an element of this type is repeatable, the expectation is that each repetition contains different content, each of which can be expressed in multiple languages. The language designation goes on the individual String.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:InternationalStringType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class InternationalString {

		private:
			/**
			 * A non-formatted string of text with an attribute that designates the language of the text. Repeat this object to express the same content in another language.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LanguageSpecificStringType> languageSpecificString;
		};
	}
}

#endif
