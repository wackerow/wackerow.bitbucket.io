#ifndef INSTRUMENTCODE_H
#define INSTRUMENTCODE_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * An InstrumentComponent that specifies the performance of a specific computation within the context of an instrument flow.
 * 
 * Examples
 * ==========
 * quality control, edit check, checksums, compute filler text, compute values for use in administering the instrument
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:ComputationItemType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class InstrumentCode : ClassLibrary::DataCapture::InstrumentComponent {

		private:
			/**
			 * The purpose of the code (e.g., quality control, edit check, checksums, compute filler text, compute values for use in administering the instrument)
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry purposeOfCode;
			/**
			 * Describes the code used to execute the command using the options of inline textual description, inline code, and/or an external file.
			 */
			ClassLibrary::ComplexDataTypes::CommandCode usesCommandCode;
		};
	}
}

#endif
