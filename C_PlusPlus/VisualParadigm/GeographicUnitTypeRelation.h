#ifndef GEOGRAPHICUNITTYPERELATION_H
#define GEOGRAPHICUNITTYPERELATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes structured relationship between Unit Types used to define Geographic Unit Types. Extended to include the ability to define a Spatial Relationship using a controlled vocabulary.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows". 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class GeographicUnitTypeRelation {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides information on the spatial relationship between the parent and child.
			 */
			ClassLibrary::EnumerationsRegExp::SpatialRelationSpecification hasSpatialRelationSpecification;
			/**
			 * An aggregation of the units of the child Unit Type will result in exhaustive coverage of the parent Unit Type
			 */
			boolean isExhaustiveCoverage;
			/**
			 * Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of its totality using an enumeration list.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
		};
	}
}

#endif
