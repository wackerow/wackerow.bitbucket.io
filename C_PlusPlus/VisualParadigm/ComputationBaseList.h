#ifndef COMPUTATIONBASELIST_H
#define COMPUTATIONBASELIST_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum ComputationBaseList {
			Total, 
			ValidOnly, 
			MissingOnly
		};
	}
}

#endif
