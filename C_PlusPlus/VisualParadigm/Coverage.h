#ifndef COVERAGE_H
#define COVERAGE_H

namespace ClassLibrary {
	namespace Discovery {
		/**
 * Definition
 * ============
 * Coverage information for an annotated object. Includes coverage information for temporal, topical, and spatial coverage. 
 * 
 * Examples
 * ==========
 * A survey might have ask people about motels they stayed in (topical coverage) in the year 2015 (temporal coverage), while they were travelling in Kansas (spatial coverage). This is different than the temporal, and spatial attributes of the population studied � (international travelers to the US surveyed in 2017).
 * 
 * Explanatory notes
 * ===================
 * Coverage is a container for the more specific temporal, spatial, and topical coverages to which it refers.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CoverageType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Coverage : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A generic description including temporal, topical, and spatial coverage that is the equivalent of dc:coverage (the refinement base of dcterms:spatial and dcterms:temporal. Use specific coverage content for detailed information. Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
