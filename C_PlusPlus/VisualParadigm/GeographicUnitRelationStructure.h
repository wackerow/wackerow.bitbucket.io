#ifndef GEOGRAPHICUNITRELATIONSTRUCTURE_H
#define GEOGRAPHICUNITRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace GeographicClassification {
		/**
 * Definition
 * ============
 * Defines the relationships between Geographic Unit Types in a collection. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class GeographicUnitRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship context for the relationship using an External Controlled Vocabulary. Examples might include "parent-child", or "immediate supervisee"
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Controlled Vocabulary to specify whether the relation is total, partial or unknown.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * MemberRelation used to define the relationship of members within the collection
			 */
			std::vector<ClassLibrary::ComplexDataTypes::GeographicUnitRelation> hasMemberRelation;
		};
	}
}

#endif
