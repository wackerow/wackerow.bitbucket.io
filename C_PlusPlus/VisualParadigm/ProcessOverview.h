#ifndef PROCESSOVERVIEW_H
#define PROCESSOVERVIEW_H

namespace ClassLibrary {
	namespace SimpleMethodologyOverview {
		/**
 * Definition
 * ============
 * Process is an implementation of an algorithm. It is the series of steps taken as a whole. It is decomposable into ProcessSteps, but this decomposition is not necessary. 
 * 
 * Examples
 * ==========
 * In descriptive document regarding ingest of a study into a repository: "Validation processes were run comparing all data against the provided documentation to check for out-of-range and invalid content."
 * 
 * Explanatory notes
 * ===================
 * In a descriptive document, Process Overview provides a means of presenting the reader with a clear view of an overall process without going into the details of the process steps. Process Overview should be used when a description of the process in general is all that is desired.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ProcessOverview : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text. Edit
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
