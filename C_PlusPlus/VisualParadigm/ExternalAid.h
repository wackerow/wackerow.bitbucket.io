#ifndef EXTERNALAID_H
#define EXTERNALAID_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * Any external material used in an instrument that aids or facilitates data capture, or that is presented to a respondent and about which measurements are made. 
 * 
 * Examples
 * ==========
 * Image, link, external aid, stimulus, physical object.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:OtherMaterialType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ExternalAid : ClassLibrary::Utility::ExternalMaterial {

		private:
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry stimulusType;
		};
	}
}

#endif
