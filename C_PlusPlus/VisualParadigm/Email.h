#ifndef EMAIL_H
#define EMAIL_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * An e-mail address which conforms to the internet format (RFC 822) including its type and time period for which it is valid.
 * 
 * Examples
 * ==========
 * info@ddialliance.org; ex.ample@somewhere.org
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * e-mail address; internet email address
 * 
 * DDI 3.2 mapping
 * =================
 * r:EmailType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Email {

		private:
			/**
			 * The email address expressed as a string (should follow the Internet format specification - RFC 5322) e.g. user@server.ext, more complex and flexible examples are also supported by the format.
			 */
			string internetEmail;
			/**
			 * Code indicating the type of e-mail address. Supports the use of an external controlled vocabulary. (e.g. home, office)
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfEmail;
			/**
			 * Time period for which the e-mail address is valid.
			 */
			ClassLibrary::ComplexDataTypes::DateRange effectiveDates;
			/**
			 * Indicates the level of privacy
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry privacy;
			/**
			 * Set to true if this is the preferred email
			 */
			boolean isPreferred;
		};
	}
}

#endif
