#ifndef EXPOSTEVALUATION_H
#define EXPOSTEVALUATION_H

namespace ClassLibrary {
	namespace StudyRelated {
		/**
 * Definition
 * ============
 * Evaluation for the purpose of reviewing the study, data collection, data processing, or management processes. Results may feed into a revision process for future data collection or management. Identifies the type of evaluation undertaken, who did the evaluation, the evaluation process, outcomes and completion date.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ExPostEvaluationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ExPostEvaluation : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Brief identification of the type of evaluation. Supports the use of an external controlled vocabulary.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry> typeOfEvaluation;
			/**
			 * Describes the evaluation process. Supports multi-lingual content. Allows the optional use of structured content.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalStructuredString> evaluationProcess;
			/**
			 * Describes the outcomes of the evaluation process. Supports multi-lingual content. Allows the optional use of structured content.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalStructuredString> outcomes;
			/**
			 * Identifies the date the evaluation was completed.
			 */
			ClassLibrary::ComplexDataTypes::Date completionDate;
		};
	}
}

#endif
