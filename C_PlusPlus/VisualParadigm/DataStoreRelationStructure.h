#ifndef DATASTORERELATIONSTRUCTURE_H
#define DATASTORERELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace LogicalDataDescription {
		/**
 * Definition
 * ============
 * A structure for describing a complex relation of DataStores within a DataStoreLibrary
 * 
 * Examples
 * ==========
 * If the succession of DataStores are created by a StudySeries that is a time series, from one DataStore to the next part/whole relations can obtain as supplements are added and subtracted.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class DataStoreRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Member relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::DataStoreRelation> hasMemberRelation;
		};
	}
}

#endif
