#ifndef LOGICALRECORDRELATIONSTRUCTURE_H
#define LOGICALRECORDRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace LogicalDataDescription {
		/**
 * Definition
 * ============
 * Allows for the complex structuring of relationships between LogicalRecords in a DataStore
 * 
 * Examples
 * ==========
 * A DataStore with a Household, Family, and Person LogicalRecord type. Allows for describing parent/child, whole/part, or other relationships as appropriate
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class LogicalRecordRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Logical Record relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LogicalRecordRelation> hasMemberRelation;
		};
	}
}

#endif
