#ifndef METADATADRIVENACTION_H
#define METADATADRIVENACTION_H

namespace ClassLibrary {
	namespace Workflows {
		/**
 * Definition
 * ============
 * Whereas ComputationActions are used in statistical packages like SPSS, Stata, SAS and R to perform data management and data transformations, MetadataDrivenActions are used by ETL (Extract Transform Load) platforms along with ComputationActions.
 * 
 * In ETLs the user is presented with a menu of MetadataDrivenActions that are captured here in an external controlled vocabulary.
 * 
 * In ETLs users enter into a dialog with the platform through which they customize the MetadataDrivenAction. The user writes no code. The dialog is saved.
 * 
 * In the course of this dialog the user might specify a data source, specify which variables to keep or drop, rename variables, specify join specifics, create a value map between two variables and so forth.
 * 
 * MetadataDrivenActions represent this dialog in a Standard Data Transformation Language (SDTL) called Variable Transformation Language (VTL). To be more precise MetaDataDrivenAction uses VTL with certain "extensions". Together they form quasi-VTL. quasi-VTL in turn conforms to a known schema. Using this schema users can render the quasi-VTL as XHTML and record it in the MetadataDrivenAction quasiVTL property. quasiVTL supports structured text.
 * 
 * Examples
 * ==========
 * Some data management systems present users with a menu of actions they can use in a sequence to perform a specific task. The system engages the user in a dialog driven by an action template. The dialog completes the template which the system then uses to perform the action. The user doesn't write any code.
 * 
 * Explanatory notes
 * ===================
 * We would like NOT to create specific subtypes of MetadataDrivenAction -- one for each transformation. In fact there are a dozen or more such actions that will be captured in the MetadataDrivenAction types external controlled vocabulary. As an alternative, users can optionally write structured text based on a known schema. In this approach we are able to "evolve" the Standard Data Transformation Language (SDTL) as needed without changing our model. Instead, as our language "evolves", we one have to update its schema.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class MetadataDrivenAction : ClassLibrary::Workflows::Act {

		private:
			/**
			 * Describes the transformation activity
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString activityDescription;
			/**
			 * Allows for the specification of a transformation type supporting the use of an external controlled vocabulary
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfMetadataDrivenAction;
			/**
			 * A definition of the action in XHTML that conforms to the quasi-VTL schema. Each action has its own definition that a user optionally completes.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString quasiVTL;
		};
	}
}

#endif
