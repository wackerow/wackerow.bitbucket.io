#ifndef ORGANIZATIONNAME_H
#define ORGANIZATIONNAME_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Names by which the organization is known. Use the attribute isFormal="true" to designate the legal or formal name of the Organization. The preferred name should be noted with the isPreferred attribute. Names may be typed with TypeOfOrganizationName to indicate their appropriate usage.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:OrganizationNameType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class OrganizationName : ClassLibrary::ComplexDataTypes::ObjectName {

		private:
			/**
			 * An abbreviation or acronym for the name. This may be expressed in multiple languages. It is assumed that if only a single language is provided that it may be used in any of the other languages within which the name itself is expressed.
			 */
			ClassLibrary::ComplexDataTypes::InternationalString abbreviation;
			/**
			 * The type of organization name provided. the use of a controlled vocabulary is strongly recommended. At minimum this should include, e.g. PreviousFormalName, Nickname (or CommonName), Other.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfOrganizationName;
			/**
			 * The time period for which this name is accurate and in use.
			 */
			ClassLibrary::ComplexDataTypes::DateRange effectiveDates;
			/**
			 * The legal or formal name of the organization should have the isFormal attribute set to true. To avoid confusion only one organization name should have the isFormal attribute set to true. Use the TypeOfOrganizationName to further differentiate the type and applied usage when multiple names are provided.
			 */
			boolean isFormal;
		};
	}
}

#endif
