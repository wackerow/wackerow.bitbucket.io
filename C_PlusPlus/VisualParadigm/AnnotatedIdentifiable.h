#ifndef ANNOTATEDIDENTIFIABLE_H
#define ANNOTATEDIDENTIFIABLE_H

namespace ClassLibrary {
	namespace Identification {
		/**
 * Definition
 * ============
 * Used to identify objects for purposes of internal and/or external referencing. Elements of this type are versioned. Provides identification and administrative metadata about the object. Adds optional annotation. Use this as the extension base for First Order Classes that contain intellectual content that needs to be discoverable in its own right.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AnnotatedIdentifiable : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides annotation information on the object to support citation and crediting of the creator(s) of the object.
			 */
			ClassLibrary::ComplexDataTypes::Annotation hasAnnotation;
		};
	}
}

#endif
