#ifndef SPATIALCOORDINATE_H
#define SPATIALCOORDINATE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Lists the value and format type for the coordinate value. Note that this is a single value (X coordinate or Y coordinate) rather than a coordinate pair.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:SpatialCoordinateType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SpatialCoordinate {

		private:
			/**
			 * The value of the coordinate expressed as a string.
			 */
			string coordinateValue;
			/**
			 * Identifies the type of point coordinate system using a controlled vocabulary. Point formats include decimal degree, degrees minutes seconds, decimal minutes, meters, and feet.
			 */
			ClassLibrary::EnumerationsRegExp::PointFormat coordinateType;
		};
	}
}

#endif
