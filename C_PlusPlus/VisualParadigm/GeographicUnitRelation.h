#ifndef GEOGRAPHICUNITRELATION_H
#define GEOGRAPHICUNITRELATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes structured relationship between Geographic Units.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows". 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class GeographicUnitRelation {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides information on the spatial relationship between the parent and child.
			 */
			ClassLibrary::EnumerationsRegExp::SpatialRelationSpecification hasSpatialRelationSpecification;
			/**
			 * Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of its totality using an enumeration list.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
		};
	}
}

#endif
