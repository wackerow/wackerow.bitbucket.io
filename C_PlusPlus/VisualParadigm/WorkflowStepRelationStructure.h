#ifndef WORKFLOWSTEPRELATIONSTRUCTURE_H
#define WORKFLOWSTEPRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Workflows {
		/**
 * Definition
 * ============
 * In the context of data management a WorkflowStepRelationStructure describes a network of data transformations and data products. Like all RelationStructures, a WorkflowStepRelationStructure is a Directed Acyclic Graph (DAG). A forward looking WorkflowStepRelationStructure describes the succession of data transformations and data products. A backward looking WorkflowStepRelationStructure describes the antecedents of one or a combination of data transformations and/or data products.
 * 
 * Examples
 * ==========
 * WorkflowStepRelationStructures are useful in the context of data transformations and data products. Imagine a network of data transformations. One WorkflowStepRelationStructure could describe this network in a forward direction, Another WorkflowStepRelationStructure could describe this network in a backward direction.
 * 
 * Looking backward, we can use this network to identify the antecedents of a particular data product. Looking forward, we can use this network to determine the effect(s) of a single or a combination of data transformations.
 * 
 * If we had rendered these networks using XML, we might use xpath queries to look upstream or downstream in a network. Looking upstream we might document provenance or, again data lineage.
 * 
 * If we render these networks in RDF, then we can write comparable SPARQL queries.
 * 
 * Explanatory notes
 * ===================
 * The WorkflowStepRelationStructure was introduced specifically to support the data management use case. So far in the context of data capture and questionnaires there has not been a need for the WorkflowStepRelationStructure.
 * 
 * Synonyms
 * ==========
 * Dataflow, Data Provenance, Data Lineage
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class WorkflowStepRelationStructure : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list. Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * WorkflowStep relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::WorkflowStepRelation> hasMemberRelation;
			/**
			 * Supports the use of Allan's Intervals for describing temporal relations. Allan's Intervals are specific types of analytic precedence relations and should be used to refine a RelationSpecification of analyticPrecedence.
			 */
			ClassLibrary::EnumerationsRegExp::TemporalRelationSpecification hasTemporalRelationSpecification;
		};
	}
}

#endif
