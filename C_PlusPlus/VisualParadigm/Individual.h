#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

namespace ClassLibrary {
	namespace Agents {
		/**
 * Definition
 * ============
 * A person who may have a relationship to another Agent and who may be specified as being associated with an act.
 * 
 * Examples
 * ==========
 * Analyst performing edits on data, interviewer conducting an interview, a creator.
 * 
 * Explanatory notes
 * ===================
 * A set of information describing an individual and means of unique identification and/or contact. Information may be repeated and provided effective date ranges to retain a history of the individual within a metadata record. Actions and relationships are specified by the use of the Individual as the target of a relationship (Creator) or within a collection of Agents in an AgentListing (employee of an Organization).
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Individual : ClassLibrary::Agents::Agent {

		private:
			/**
			 * The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::IndividualName> hasIndividualName;
			/**
			 * The agency identifier of the individual according to the DDI Alliance agent registry.
			 */
			std::vector<string> ddiId;
			/**
			 * Contact information for the individual including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.
			 */
			ClassLibrary::ComplexDataTypes::ContactInformation hasContactInformation;
		};
	}
}

#endif
