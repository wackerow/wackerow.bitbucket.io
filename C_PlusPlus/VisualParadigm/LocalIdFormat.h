#ifndef LOCALIDFORMAT_H
#define LOCALIDFORMAT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id.
 * 
 * Examples
 * ==========
 * The name of a variable within a dataset is a localId. Software systems might use their own identifier systems for performance reasons (e.g. incrementing integers).
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class LocalIdFormat {

		private:
			/**
			 * Value of the local ID.
			 */
			string localIdValue;
			/**
			 * Type of identifier, specifying the context of the identifier.
			 */
			string localIdType;
			/**
			 * Version of the Local ID.
			 */
			string localIdVersion;
		};
	}
}

#endif
