#ifndef WORKFLOWSTEPSEQUENCEINDICATOR_H
#define WORKFLOWSTEPSEQUENCEINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Identify WorkflowStepSequence's that are organized into a master sequence for executing a WorkflowProcess.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class WorkflowStepSequenceIndicator {

		private:
			/**
			 * Provides an index for the member within an ordered array
			 */
			int index;
		};
	}
}

#endif
