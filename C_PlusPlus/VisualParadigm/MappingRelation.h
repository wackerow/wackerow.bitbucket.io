#ifndef MAPPINGRELATION_H
#define MAPPINGRELATION_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum MappingRelation {
			ExactMatch, 
			CloseMatch, 
			Disjoint
		};
	}
}

#endif
