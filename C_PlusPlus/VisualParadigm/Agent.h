#ifndef AGENT_H
#define AGENT_H

namespace ClassLibrary {
	namespace Agents {
		/**
 * Definition
 * ============
 * An actor that performs a role in relation to a process or product.
 * 
 * Examples
 * ==========
 * Analyst performing edits on data, interviewer conducting an interview, a relational database management system managing data, organization publishing data on a regular basis, creator or contributor of a publication.
 * 
 * Explanatory notes
 * ===================
 * foaf:Agent is: An agent (eg. person, group, software or physical artifact)
 * prov:Agent is An agent is something that bears some form of responsibility for an activity taking place, for the existence of an entity, or for another agent's activity.
 * 
 * Synonyms
 * ==========
 * Agent
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Agent : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * An identifier within a specified system for specifying an agent
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentId> hasAgentId;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::PrivateImage> image;
		};
	}
}

#endif
