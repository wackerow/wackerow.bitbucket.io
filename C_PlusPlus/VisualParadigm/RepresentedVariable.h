#ifndef REPRESENTEDVARIABLE_H
#define REPRESENTEDVARIABLE_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * A combination of a characteristic of a universe to be measured and how that measure will be represented.
 * 
 * 
 * Examples
 * ==========
 * The pair (Number of Employees, Integer), where "Number of Employees" is the characteristic of the population (Variable) and "Integer" is how that measure will be represented (Value Domain).
 * 
 * Explanatory notes
 * ===================
 * Extends from ConceptualVariable and can contain all descriptive fields without creating a ConceptualVariable. By referencing a ConceptualVariable it can indicate a common relationship with RepresentedVariables expressing the same characteristic of a universe measured in another way, such as Age of Persons in hours rather than years. RepresentedVariable constrains the coverage of the UnitType to a specific Universe. In the above case the Universe with the measurement of Age in hours may be constrained to Persons under 5 days (120 hours old). RepresentedVariable can define sentinel values for multiple storage systems which have the same conceptual domain but specialized value domains.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * l:RepresentedVariableType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class RepresentedVariable : ClassLibrary::Conceptual::ConceptualVariable {

		private:
			/**
			 * The unit in which the data values are measured (kg, pound, euro).
			 */
			string unitOfMeasurement;
			/**
			 * The data type intended to be used by this variable. Supports the optional use of an external controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry hasIntendedDataType;
		};
	}
}

#endif
