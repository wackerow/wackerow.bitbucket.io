#ifndef SPATIALPOINT_H
#define SPATIALPOINT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * A geographic point consisting of an X and Y coordinate. Each coordinate value is expressed separately providing its value and format.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:PointType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SpatialPoint {

		private:
			/**
			 * An X coordinate (latitudinal equivalent) value and format expressed using the Spatial Coordinate structure.
			 */
			ClassLibrary::ComplexDataTypes::SpatialCoordinate xCoordinate;
			/**
			 * A Y coordinate (longitudinal equivalent) value and format expressed using the Spatial Coordinate structure.
			 */
			ClassLibrary::ComplexDataTypes::SpatialCoordinate yCoordinate;
		};
	}
}

#endif
