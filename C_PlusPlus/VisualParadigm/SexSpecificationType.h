#ifndef SEXSPECIFICATIONTYPE_H
#define SEXSPECIFICATIONTYPE_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum SexSpecificationType {
			Masculine, 
			Feminine, 
			GenderNeutral
		};
	}
}

#endif
