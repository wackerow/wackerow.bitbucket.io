#ifndef SCALERESPONSEDOMAIN_H
#define SCALERESPONSEDOMAIN_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * A response domain capturing a scaled response, such as a Likert scale. 
 * Note: This item still must be modeled and is incomplete at this time.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ScaleResponseDomain : ClassLibrary::DataCapture::ResponseDomain {
		};
	}
}

#endif
