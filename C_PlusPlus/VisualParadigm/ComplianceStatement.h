#ifndef COMPLIANCESTATEMENT_H
#define COMPLIANCESTATEMENT_H

namespace ClassLibrary {
	namespace StudyRelated {
		/**
 * Definition
 * ============
 * Allows for a quality statement based on frameworks to be described using itemized properties. A reference to a concept, a coded value, or both can be used to specify the property from the standard framework identified in StandardUsed. Usage can provide further details or a general description of compliance with a standard.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ComplianceType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ComplianceStatement : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Specification of a code which relates to an area of coverage of the standard. Supports the use of an external controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry externalComplianceCode;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
		};
	}
}

#endif
