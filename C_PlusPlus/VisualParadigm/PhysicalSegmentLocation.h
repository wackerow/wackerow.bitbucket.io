#ifndef PHYSICALSEGMENTLOCATION_H
#define PHYSICALSEGMENTLOCATION_H

namespace ClassLibrary {
	namespace FormatDescription {
		/**
 * Definition
 * ============
 * Defines the location of a segment in a DataStore (e.g. a text file). This is abstract since there are many different ways to describe the location of a segment - character counts, start and end times, etc.
 * 
 * Examples
 * ==========
 * A segment of text in a plain text file beginning at character 3 and ending at character 123.
 * 
 * Explanatory notes
 * ===================
 * While this has no properties or relationships of its own, it is useful as a target of relationships where its extensions may serve.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class PhysicalSegmentLocation : ClassLibrary::Identification::AnnotatedIdentifiable {
		};
	}
}

#endif
