#ifndef INDEXENTRYRELATIONSTRUCTURE_H
#define INDEXENTRYRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * Structures relationship of Classification Index Entries in a Classification Index.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class IndexEntryRelationStructure : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Index Entry Relations  that comprise the relationship structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::IndexEntryRelation> hasMemberRelation;
		};
	}
}

#endif
