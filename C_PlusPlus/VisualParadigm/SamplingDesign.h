#ifndef SAMPLINGDESIGN_H
#define SAMPLINGDESIGN_H

namespace ClassLibrary {
	namespace SamplingMethodology {
		/**
 * Definition
 * ============
 * Sampling plan defines the how the sample is to be obtained through the description of methodology or a model based approach, defining the intended target population, stratification or split procedures and recommended sample frames. The content of a sampling plan is intended to be reusable. The application of the sample plan is captured in Sampling Process.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SamplingDesign : ClassLibrary::SimpleMethodologyOverview::DesignOverview {
		};
	}
}

#endif
