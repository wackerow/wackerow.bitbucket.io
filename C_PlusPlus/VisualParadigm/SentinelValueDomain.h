#ifndef SENTINELVALUEDOMAIN_H
#define SENTINELVALUEDOMAIN_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * The Value Domain for a sentinel conceptual domain. Sentinel values are defined in ISO 11404 as
 * "element of a value space that is not completely consistent with a datatype's properties and characterizing operations...". A common example would be codes for missing values.
 * 
 * Examples
 * ==========
 * Missing categories expressed as Codes <-9, refused>; <-8, Don't Know> for a numeric variable with values greater than zero.  
 * 
 * Explanatory notes
 * ===================
 * Sentinel values are used for processing, not to describe subject matter. Typical examples include missing values or invalid entry codes. Sentinel Value Domains are typically of the enumerated type, but they can be the described type, too.
 * 
 * This is not to say that sentinel values carry no information. Data on gender might be enumerated by "0, male" and "1, female". These are the substantive values (see Substantive Value Domain). However, there may be the need to include missing values along with that data, such as "m, missing" and "r, refused". These are sentinel values.
 * 
 * ISO/IEC 11404 - General purpose datatypes, defines sentinel values in terms of how that standard defines datatypes. But, the fact that the sentinels don't fit in terms of the calculations and statistics one would perform on the "clean" data is a distinguishing characteristic. In the example above, one would not include missing or refused data in calculating a ratio of females to the total population.
 * 
 * Sentinel values may be described rather than enumerated. For instance, there might be a range of values, each representing an out of range value, but there could be too many to enumerate. It is easier to describe the range.
 * 
 * In some software missing values are represented as values not in the datatype of the valid values. R has values of NA, NaN, Inf, and -Inf. SAS and Stata have values displayed as ".", ".A" through ".Z", and "._"
 * 
 * Other software might use values like 999 for missing that would otherwise be the same datatype as valid values but outside the parameters of the domain.
 * 
 * In the gender example above:
 * For SPSS the sentinel values might be represented as:
 * 998 = "refused"
 * 999 = "not recorded"
 * 
 * For SAS or Stata the sentinel values might be represented as:
 * .R = "refused"
 * .N = "not recorded"
 * 
 * Sentinel values can also be used for other purposes beyond missing. For a numeric variable "A" might represent a value somewhere in a defined range to prevent disclosure of information about an individual. This might be considered a "semi-missing value".
 * In SAS or Stata for example:
 * .A = "greater than or equal to 100 and less than 1000 "
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Enumerated Value Domain
 */
class SentinelValueDomain : ClassLibrary::Representations::ValueDomain {

		private:
			/**
			 * The type of platform under which sentinel codes will be used. Statistical software platforms use different sets of codes to indicate missing values. The external controlled vocabulary should list platform tpes and a description of the allowed missing value types. A sample list would be: � BlankString - A Blank string indicates missing. Comparison is based on lexical order.  � EmptyString - An empty string indicates missing. Use in comparisons returns missing.   � Rstyle - Codes drawn from  NA and the IEEE 754 values of  NaN  -Inf   +Inf.   Comparisons return NA.   � SASNumeric - codes drawn from . ._ .A .B .C .D .E .F .G .H .I .J .K .L .M .N .O .P .Q .R .S .T .U .V .W .X .Y .Z    Sentinel code treated as less than any substantive value   � SPSSstyle - System missing (a dot) a set of individual values drawn from the same datatype as the SubstantiveValueDomain, and a range of values  drawn from the same datatype as the SubstantiveValueDomain.  Comparisons return system missing.  Some functions substitute with valid values (e.g. SUM replaces missing values with 0)   � StataNumeric - codes drawn from . ._ .A .B .C .D .E .F .G .H .I .J .K .L .M .N .O .P .Q .R .S .T .U .V .W .X .Y .Z  Sentinel code treated as greater than any substantive value  � Unrestricted - No restrictions on codes for sentinel values. Use in comparisons is indeterminate.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry platformType;
		};
	}
}

#endif
