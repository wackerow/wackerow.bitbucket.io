#ifndef NUMBERRANGEVALUE_H
#define NUMBERRANGEVALUE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes a bounding value for a number range expressed as an xs:demical.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:NumberRangeValueType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class NumberRangeValue {

		private:
			/**
			 * Indicates that the value is included in the range. Set to false if the range includes numbers up to but no including the designated value.
			 */
			boolean isInclusive;
			/**
			 * Bounding value expressed as an xs:double
			 */
			Real decimalValue;
		};
	}
}

#endif
