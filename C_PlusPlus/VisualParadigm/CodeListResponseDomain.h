#ifndef CODELISTRESPONSEDOMAIN_H
#define CODELISTRESPONSEDOMAIN_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * A response domain capturing a coded response (where both codes and their related category value are displayed) for a question. This response domain allows the single selection of one coded response.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:CodeDomainType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CodeListResponseDomain : ClassLibrary::DataCapture::ResponseDomain {
		};
	}
}

#endif
