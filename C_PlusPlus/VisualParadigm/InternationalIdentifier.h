#ifndef INTERNATIONALIDENTIFIER_H
#define INTERNATIONALIDENTIFIER_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * An identifier whose scope of uniqueness is broader than the local archive. Common forms of an international identifier are ISBN, ISSN, DOI or similar designator. Provides both the value of the identifier and the agency who manages it.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * For use in Annotation or other citation format. 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:InternationalIdentifierType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class InternationalIdentifier {

		private:
			/**
			 * An identifier as it should be listed for identification purposes.
			 */
			string identifierContent;
			/**
			 * The identification of the Agency which assigns and manages the identifier, i.e., ISBN, ISSN, DOI, etc.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry managingAgency;
			/**
			 * Set to "true" if Identifier is a URI
			 */
			boolean isURI;
		};
	}
}

#endif
