#ifndef IDENTIFIERROLE_H
#define IDENTIFIERROLE_H

namespace ClassLibrary {
	namespace LogicalDataDescription {
		/**
 * Definition
 * ============
 * An IdentifierRole identifies one or more InstanceVariables as being identifiers within a ViewPoint. An IdentifierRole is a SimpleCollection of InstanceVariables acting in the IdentifierRole.
 * 
 * 
 * Examples
 * ==========
 * A data record with four variables: "PersonId", "Systolic", "Diastolic", "Seated" might have a Viewpoint with 
 * PersonId defined as having the IdentifierRole
 * Systolic defined as having the MeasureRole
 * Diastolic defined as having the MeasureRole
 * Seated defined as having the AttributeRole
 * 
 * PersonId, Systolic, Diastolic, Seated
 * 123,122,20,yes
 * 145,130,90,no
 * 
 * 
 * Explanatory notes
 * ===================
 * See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class IdentifierRole : ClassLibrary::LogicalDataDescription::ViewpointRole {
		};
	}
}

#endif
