#ifndef SUMMARYSTATISTIC_H
#define SUMMARYSTATISTIC_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes summary statistics for a variable.
 * 
 * Examples
 * ==========
 * Mean, standard deviation
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * pi:SummaryStatisticType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SummaryStatistic {

		private:
			/**
			 * Type of summary statistic, such as count, mean, mode, median, etc. Supports the use of an external controlled vocabulary. DDI strongly recommends the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfSummaryStatistic;
			/**
			 * The value of the statistics and whether it is weighted and/or includes missing values. May expressed as xs:decimal and/or xs:double
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Statistic> hasStatistic;
		};
	}
}

#endif
