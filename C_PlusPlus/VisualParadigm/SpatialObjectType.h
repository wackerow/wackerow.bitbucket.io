#ifndef SPATIALOBJECTTYPE_H
#define SPATIALOBJECTTYPE_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum SpatialObjectType {
			Point, 
			Polygon, 
			Line, 
			LinearRing, 
			Raster
		};
	}
}

#endif
