#ifndef CUSTOMINSTANCE_H
#define CUSTOMINSTANCE_H

namespace ClassLibrary {
	namespace CustomMetadata {
		/**
 * Definition
 * ============
 * A set of CustomValues to be attached to some object.
 * 
 * Examples
 * ==========
 * A set of OMB required information about a question.
 * 
 * The set of openEHR protocol information to be attached to a blood pressure Capture
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CustomInstance : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * see Collection
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * see Collection
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * see Collection
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::CustomValueIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
