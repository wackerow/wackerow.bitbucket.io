#ifndef LOCATIONNAME_H
#define LOCATIONNAME_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Name of the location using the DDI Name structure and the ability to add an effective date.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:LocationNameType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class LocationName : ClassLibrary::ComplexDataTypes::ObjectName {

		private:
			/**
			 * The time period for which this name is accurate and in use.
			 */
			ClassLibrary::ComplexDataTypes::DateRange effectiveDates;
		};
	}
}

#endif
