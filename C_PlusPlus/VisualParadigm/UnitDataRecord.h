#ifndef UNITDATARECORD_H
#define UNITDATARECORD_H

namespace ClassLibrary {
	namespace LogicalDataDescription {
		/**
 * Definition
 * ============
 * Gives a UnitDataRecord structure to a Logical Record. UnitDataRecord structures a LogicalRecord as either a SimpleCollection or a StructuredCollection (InstanceVariableRelationStructure) of instance variables.
 * 
 * The UnitDataRecord also organizes instance variables in a UnitDataViewpoint. The UnitDataViewpoint assigns roles to Instance Variables. In any one UnitDataViewpoint UnitDataRecord instance variables play one of three roles: identifier, measure or attribute. The same UnitDataRecord may have many UnitDataViewpoints.
 * 
 * The UnitDataViewPoint can work in conjunction with the InstanceVariableRelationStructure. Together they can break out instance variables into complex (hierarchical) identifier, measure and attribute groups. 
 * 
 * Examples
 * ==========
 * The UnitDataRecord can be used to specify the structure of a table in a relational database. It can be used to specify the structure of both the fact and dimension tables in a data warehouse. It can be used to specify the structure of a big data table. Likewise it can be used to specify the structures of "column-based" and "row-based" tables.
 * 
 * The UnitDataRecord may also form an information model if it is a StructuredCollection. Blood Pressure as described by an openEHR archetype is an information model. So is a collection of FHIR (HL7) resources that form an Electronic Health Record (EHR). 
 * 
 * Explanatory notes
 * ===================
 * The <a href="http://www.openehr.org/ckm/">openEHR archetype</a> is a use case that motivates the use of the InstanceVariableRelationStructure together with a UnitDataViewPoint. In the Blood Pressure archetype as well as most other archetypes attributes are not a SimpleCollection. Instead they are part of a StructuredCollection that breaks out attributes into "Protocol", "State" and "Events". In other archetypes the measure may be structured. For example body composition data includes a "base model" (fat mass, fat percentage, fat free mass), an "atomic level" (chemical elements, hydrogen, carbon, oxygen), a "molecular level" (minerals, protein, fat, water) and so forth.
 * 
 * It is, however, possible to break out attributes into not one but many SimpleCollections in the event attributes and/or data do not form StructuredCollections. That is because the UnitDataViewpoint supports not just one but many attribute roles within the same viewpoint.
 * 
 * Synonyms
 * ==========
 * Table Definition if the UnitDataRecord is a SimpleCollection, Information Model if the UnitDataRecord is a StructuredCollection
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class UnitDataRecord : ClassLibrary::LogicalDataDescription::LogicalRecord {
		};
	}
}

#endif
