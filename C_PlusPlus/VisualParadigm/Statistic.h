#ifndef STATISTIC_H
#define STATISTIC_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * The value of the statistic expressed as an xs:decimal and/or xs:double. Indicates whether it is weighted value and the computation base.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * pi:StatisticType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Statistic {

		private:
			/**
			 * Set to "true" if the statistic is weighted using the weight designated in VariableStatistics.
			 */
			boolean isWeighted;
			/**
			 * Defines the cases included in determining the statistic. The options are total=all cases, valid and missing (invalid); validOnly=Only valid values, missing (invalid) are not included in the calculation; missingOnly=Only missing (invalid) cases included in the calculation.
			 */
			ClassLibrary::EnumerationsRegExp::ComputationBaseList computationBase;
			/**
			 * The value of the statistic expressed as a decimal
			 */
			Real decimalValue;
			/**
			 * The value of the statistic expressed as a double.
			 */
			Real doubleValue;
		};
	}
}

#endif
