#ifndef METHODOLOGY_H
#define METHODOLOGY_H

namespace ClassLibrary {
	namespace MethodologyPattern {
		/**
 * Definition
 * ============
 * Methodology brings together the design, algorithm, and process as a logically structured approach to an activity such as sampling, weighting, harmonization, imputation, collection management, etc. A methodology in normally informed by earlier research and clarifies how earlier research methods were incorporated into the current work.
 * 
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Methodology : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * A linguistic signifier. Human  understandable name (word, phrase, or  mnemonic) that reflects the ISO/IEC  11179-5 naming principles. If more than  one name is provided provide a context to  differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the ways in which some  decision or object is employed. Supports  the use of multiple languages and  structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
			/**
			 * Explanation of the reasons some decision  was made or some object exists. Supports  the use of multiple languages and  structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;
			/**
			 * Short natural language account of the  information obtained from the  combination of properties and  relationships associated with an object.  Supports the use of multiple languages  and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
