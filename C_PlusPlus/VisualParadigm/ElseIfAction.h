#ifndef ELSEIFACTION_H
#define ELSEIFACTION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * An additional condition and resulting step to take if the condition in the parent IfThenElse is false. It cannot exist without a parent IfThenElse.
 * 
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ElseIfAction {

		private:
			/**
			 * The condition to verify if the parent IfThenElse condition is false.
			 */
			ClassLibrary::ComplexDataTypes::CommandCode condition;
		};
	}
}

#endif
