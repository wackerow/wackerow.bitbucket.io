#ifndef RANKINGRESPONSEDOMAIN_H
#define RANKINGRESPONSEDOMAIN_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * A response domain capturing a ranking response which supports a "ranking" or "Ordering" of provided categories. 
 * Note: This item still must be modeled and is incomplete at this time.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class RankingResponseDomain : ClassLibrary::DataCapture::ResponseDomain {
		};
	}
}

#endif
