#ifndef VIEWPOINTROLE_H
#define VIEWPOINTROLE_H

namespace ClassLibrary {
	namespace LogicalDataDescription {
		/**
 * Definition
 * ============
 * A ViewpointRole designates the function an InstanceVariable performs in the context of the Viewpoint. (IdentifierRole, AttributeRole, or MeasureRole of interest).
 * 
 * Each of three roles within a Viewpoint may be a collection. This happens when a role is mapped to multiple instance variables. In this event a role forms a SimpleCollection. There are SimpleCollections of instance variables in each role.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ViewpointRole : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InstanceVariableIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
