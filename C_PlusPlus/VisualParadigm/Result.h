#ifndef RESULT_H
#define RESULT_H

namespace ClassLibrary {
	namespace Methodologies {
		/**
 * Definition
 * ============
 * Describes the results of a process for the purpose of linking these results to guidance for future usage in specified situations. Result is abstract and serves as a substitution base for the specified result of a specific instantiation of a methodology process.
 * 
 * Examples
 * ==========
 * The use of a weight resulting from a sampling process by an analyst within the context of a specific set of variables. The class containing the weight would use the extension base Result, adding any additional required properties and relationships to describe it.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Result : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
			/**
			 * Can bind data attached to parameters into, out of, and between processes.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Binding> hasBinding;
		};
	}
}

#endif
