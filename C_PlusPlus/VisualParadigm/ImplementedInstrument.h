#ifndef IMPLEMENTEDINSTRUMENT_H
#define IMPLEMENTEDINSTRUMENT_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * A specific data capture tool.  ImplementedInstruments are mode and/or unit specific.
 * 
 * Examples
 * ==========
 * Two versions of a conceptual instrument, computer assisted personal interviewing (CAPI) and mail, would be separate ImplementedInstruments. These might both reference the same ConceptualInstrument. 
 * A piece of equipment used to measure blood pressure.
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * aka PhysicalInstrument
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ImplementedInstrument : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * Specification of the type of instrument according to the classification system of the documentor.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfInstrument;
			/**
			 * A reference to an external representation of the the data collection instrument, such as an image of a questionnaire or programming script.
			 */
			std::vector<ClassLibrary::XMLSchemaDatatypes::anyUri> uri;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalStructuredString> usage;
		};
	}
}

#endif
