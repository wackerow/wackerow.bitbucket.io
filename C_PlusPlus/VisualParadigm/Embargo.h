#ifndef EMBARGO_H
#define EMBARGO_H

namespace ClassLibrary {
	namespace StudyRelated {
		/**
 * Definition
 * ============
 * Provides information about data that are not currently available because of policies established by the principal investigators and/or data producers. Embargo provides a name and label of the embargo, the dates covered by the embargo, the rationale or reason for the embargo, a reference to the agency establishing the embargo, and a reference to the agency enforcing the embargo.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:EmbargoType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Embargo : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * Provides the date range of the embargo
			 */
			ClassLibrary::ComplexDataTypes::DateRange embargoDates;
			/**
			 * Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;
		};
	}
}

#endif
