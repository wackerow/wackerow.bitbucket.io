#ifndef CUSTOMITEMRELATION_H
#define CUSTOMITEMRELATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Defines a relationship between CustomItems .
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CustomItemRelation {

		private:
			/**
			 * The type of relationship between the two CustomItems. The verb in the sentence describing the relationship between the source (subject) and the target (object).
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSepcification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
		};
	}
}

#endif
