#ifndef AGENTASSOCIATION_H
#define AGENTASSOCIATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * A basic structure for declaring the name of an Agent inline, reference to an Agent, and role specification. This object is used primarily within Annotation.
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:ContributorType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AgentAssociation {

		private:
			/**
			 * Full name of the contributor. Language equivalents should be expressed within the International String structure.
			 */
			ClassLibrary::ComplexDataTypes::BibliographicName agentName;
			/**
			 * The role of the of the Agent within the context of the parent property name with information on the extent to which the role applies. Allows for use of external controlled vocabularies. Reference should be made to the vocabulary within the structure of the role. Recommended role for contributors can be found in the CASRAI Contributor Roles Vocabulary (CRediT) http://dictionary.casrai.org/Contributor_Roles, or the OPENRIF contribution ontology https://github.com/openrif/contribution-ontology
			 */
			std::vector<ClassLibrary::ComplexDataTypes::PairedExternalControlledVocabularyEntry> role;
		};
	}
}

#endif
