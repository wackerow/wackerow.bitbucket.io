#ifndef NUMERICRESPONSEDOMAIN_H
#define NUMERICRESPONSEDOMAIN_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * A response domain capturing a numeric response (the intent is to analyze the response as a number) for a question.
 * 
 * Examples
 * ==========
 * Systolic blood pressure level, as an integer from 0 to 200
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:NumericDomainType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class NumericResponseDomain : ClassLibrary::DataCapture::ResponseDomain {

		private:
			/**
			 * Identification of the numeric type such as integer, decimal, etc. supports the use of an external controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry numericTypeCode;
			/**
			 * An optional unit of measurement.  Examples include units from the International System of Units (SI) such as meter, seconds, mole, kilograms. Other examples include miles, tons, pints.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry unit;
			/**
			 * Defines the valid number range or number values for the representation.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::NumberRange> usesNumberRange;
		};
	}
}

#endif
