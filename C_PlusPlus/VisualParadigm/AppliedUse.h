#ifndef APPLIEDUSE_H
#define APPLIEDUSE_H

namespace ClassLibrary {
	namespace Methodologies {
		/**
 * Definition
 * ============
 * Links the guidance instructions to specific unit types.
 * 
 * Examples
 * ==========
 * Links a guide for the use of a sample (result) obtained in the first stage of complex sample with the unit type. A sample of Counties from which households will be selected in the next sample stage.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AppliedUse : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
