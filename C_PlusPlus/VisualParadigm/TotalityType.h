#ifndef TOTALITYTYPE_H
#define TOTALITYTYPE_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum TotalityType {
			Total, 
			Partial, 
			Unknown
		};
	}
}

#endif
