#ifndef ALGORITHMOVERVIEW_H
#define ALGORITHMOVERVIEW_H

namespace ClassLibrary {
	namespace SimpleMethodologyOverview {
		/**
 * Definition
 * ============
 * High level, descriptive, human informative, algorithm statement used to describe the overall methodology. 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * This would most commonly be used in a Codebook along with a MethodologyOverview and a DesignOverview.The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AlgorithmOverview : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A term identifying the subject of the algorithm being described. Supports the use of a controlled vocabulary
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry subjectOfAlgorithm;
			/**
			 * Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
