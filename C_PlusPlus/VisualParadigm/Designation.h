#ifndef DESIGNATION_H
#define DESIGNATION_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * A sign denoting a concept.
 * 
 * Examples
 * ==========
 * A linking of the term “Unemployment” to an particular underlying concept.
 * 
 * Explanatory notes
 * ===================
 * The representation of a concept by a sign (e.g., string, pictogram, bitmap) which denotes it. An example is the code M designating the marital status Married, which is a concept. In this context, M means Married.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Designation
 */
class Designation : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * A perceivable object used to denote a signified, i.e. a concept in this case.
			 */
			ClassLibrary::SignificationPattern::Signifier representation;
		};
	}
}

#endif
