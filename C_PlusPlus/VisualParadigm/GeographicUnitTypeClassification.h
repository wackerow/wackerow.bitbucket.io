#ifndef GEOGRAPHICUNITTYPECLASSIFICATION_H
#define GEOGRAPHICUNITTYPECLASSIFICATION_H

namespace ClassLibrary {
	namespace GeographicClassification {
		/**
 * Definition
 * ============
 * A structured collection of Unit Types defining a geographic structure. As a subtype of CodeList it may be used directly to describe a value domain.
 * 
 * Examples
 * ==========
 * Country--State--County
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class GeographicUnitTypeClassification : ClassLibrary::Representations::CodeList {

		private:
			/**
			 * Define the code and Unit Type that is a member of the Geographic Unit Type Classification. They may be unordered or ordered and assigned to a specific level.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::GeographicUnitTypeIndicator> contains;
			/**
			 * Date the Classification was released
			 */
			ClassLibrary::ComplexDataTypes::Date releaseDate;
			/**
			 * The date the statistical classification enters production use and the date on which the Classification was superseded by a successor version or otherwise ceased to be valid. (Source: GSIM Statistical Classification)
			 */
			ClassLibrary::ComplexDataTypes::DateRange validDates;
			/**
			 * Indicates if the Statistical Classification is currently valid.
			 */
			boolean isCurrent;
			/**
			 * Indicates if the Classification is a floating classification. In a floating statistical classification, a validity period should be defined for all Classification Items which will allow the display of the item structure and content at different points of time. (Source: GSIM StatisticalClassification/Floating)
			 */
			boolean isFloating;
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
		};
	}
}

#endif
