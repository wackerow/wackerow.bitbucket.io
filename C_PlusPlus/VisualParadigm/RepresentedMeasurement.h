#ifndef REPRESENTEDMEASUREMENT_H
#define REPRESENTEDMEASUREMENT_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * The description of a reusable non-question measurement that can be used as a template that describes the components of a measurement. A type code can be used to describe the type of measure that was used.
 * 
 * Examples
 * ==========
 * A measure providing a geographic point; A blood pressure protocol; A protocol for measuring an ocean current
 * 
 * Explanatory notes
 * ===================
 * Non-question measurements often involve the use of specific machines or protocols to ensure consistency and comparability of the measure. The RepresentedMeasurement allows the reusable format and protocol to be housed in a repository for use by multiple studies or repeated studies.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class RepresentedMeasurement : ClassLibrary::DataCapture::Capture {

		private:
			/**
			 * The type of measurement performed
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry measurementType;
		};
	}
}

#endif
