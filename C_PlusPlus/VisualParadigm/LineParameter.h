#ifndef LINEPARAMETER_H
#define LINEPARAMETER_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Specification of the line and offset for the beginning and end of the segment.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:LineParameterType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class LineParameter {

		private:
			/**
			 * Number of lines from beginning of the document.
			 */
			int startLine;
			/**
			 * Number of characters from start of the line specified in StartLine.
			 */
			int startOffset;
			/**
			 * Number of lines from beginning of the document.
			 */
			int endLine;
			/**
			 * Number of characters from the start of the line specified in EndLine.
			 */
			int endOffset;
		};
	}
}

#endif
