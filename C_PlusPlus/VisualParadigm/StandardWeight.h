#ifndef STANDARDWEIGHT_H
#define STANDARDWEIGHT_H

namespace ClassLibrary {
	namespace SimpleCodebook {
		/**
 * Definition
 * ============
 * Provides an identified value for a standard weight expressed as an xs:float. This object may be referenced by a variable or statistic and used as a weight for analysis.
 * 
 * Examples
 * ==========
 * A simple sample survey without an individual weight variable will have a StandardWeight to be used on all cases: 1% sample has a sample weight of 100
 * 
 * Explanatory notes
 * ===================
 * A simple random sample survey without an individual weight variable will have a StandardWeight to be used on all cases. This is common when the sample is a simple random sample of a population with no oversampling to provide larger case counts for small populations.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:StandardWeightType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class StandardWeight : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Provides the standard weight used for weighted analysis of data expressed as an xs:float. This element is referenced by the variable and/or statistics calculated using the standard weight.
			 */
			Real standardWeightValue;
		};
	}
}

#endif
