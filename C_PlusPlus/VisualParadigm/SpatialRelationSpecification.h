#ifndef SPATIALRELATIONSPECIFICATION_H
#define SPATIALRELATIONSPECIFICATION_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum SpatialRelationSpecification {
			Equals, 
			Disjoint, 
			Intersects, 
			Contains, 
			Touches
		};
	}
}

#endif
