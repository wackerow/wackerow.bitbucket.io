#ifndef CONDITIONALTEXT_H
#define CONDITIONALTEXT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Text which has a changeable value depending on a stated condition, response to earlier questions, or as input from a set of metrics (pre-supplied data).
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:ConditionalTextType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ConditionalText : ClassLibrary::ComplexDataTypes::DynamicTextContent {

		private:
			/**
			 * The condition on which the associated text varies expressed by a command code. For example, a command that inserts an age by calculating the difference between today�s date and a previously defined date of birth.
			 */
			ClassLibrary::ComplexDataTypes::CommandCode expression;
		};
	}
}

#endif
