#ifndef CORRESPONDENCETYPE_H
#define CORRESPONDENCETYPE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes the commonalities and differences between two members using a textual description of both commonalities and differences plus an optional coding of the type of commonality.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * cm:CorrespondenceType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CorrespondenceType {

		private:
			/**
			 * A description of the common features of the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString commonality;
			/**
			 * A description of the differences between the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString difference;
			/**
			 * Commonality expressed as a term or code. Supports the use of an external controlled vocabulary. If repeated, clarify each external controlled vocabulary used.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry> commonalityTypeCode;
			/**
			 * Allows specification of exact match, close match, or disjoint. These relationships can be further defined by describing commonalities or differences or providing additional controlled vocabulary description of relationship.
			 */
			ClassLibrary::EnumerationsRegExp::MappingRelation hasMappingRelation;
		};
	}
}

#endif
