#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

namespace ClassLibrary {
	namespace Discovery {
		/**
 * Definition
 * ============
 * A type of Spatial coverage describing a rectangular area within which the actual range of location fits. A BoundingBox is described by 4 numbers - the maxima of the north, south, east, and west coordinates found in the area.
 * 
 * Examples
 * ==========
 * Burkino Faso: (N) 15.082773; (S) 9.395691; (E) 2.397927; (W) -5.520837
 * 
 * Explanatory notes
 * ===================
 * A BoundingBox is often described by two x,y coordinates where the x coordinates are used for the North and South Latitudes and y coordinates for the West and East Longitudes
 * 
 * Synonyms
 * ==========
 * r:BoundingBox
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class BoundingBox : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * The easternmost coordinate expressed as a decimal between the values of -180 and 180 degrees
			 */
			Real eastLongitude;
			/**
			 * The westernmost coordinate expressed as a decimal between the values of -180 and 180 degrees
			 */
			Real westLongitude;
			/**
			 * The northernmost coordinate expressed as a decimal between the values of -90 and 90 degrees.
			 */
			Real northLatitude;
			/**
			 * The southermost latitude expressed as a decimal between the values of -90 and 90 degrees
			 */
			Real southLatitude;
		};
	}
}

#endif
