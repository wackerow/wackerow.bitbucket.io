#ifndef CODELIST_H
#define CODELIST_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * A list of Codes and associated Categories. May be flat or hierarchical. A hierarchical structure may have an indexed order for intended presentation even though the content within levels of the hierarchy are conceptually unordered. For hierarchical structures ClassificationRelationStructure is used to provide additional information on the structure and organization of the categories. Note that a CategorySet can be structured by a ClassificationRelationStructure without the need for associating any Codes with the Categories. This allows for the creation of a CategorySet, for example for a response domain, without an associated CodeList.
 * 
 * Examples
 * ==========
 * The codes "M" and "F" could point to "Male" and "Female" categories respectively.
 * 
 * A CodeList for an occupational classification system like ISCO-08 could use a ClassificationRelationStructure to describe a hierarchy (Chief Executives and Administrative and Commercial Managers as subtypes of Managers)
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * l:CodeListType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Code List
 */
class CodeList : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::CodeIndicator> contains;
			/**
			 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			boolean isOrdered;
		};
	}
}

#endif
