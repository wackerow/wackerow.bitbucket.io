#ifndef INDIVIDUALNAME_H
#define INDIVIDUALNAME_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix. The preferred compilation of the name parts may also be provided. The legal or formal name of the individual should have the isFormal attribute set to true. The preferred name should be noted with the isPreferred attribute. The attribute sex provides information to assist in the appropriate use of pronouns.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * a:IndividualNameType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class IndividualName {

		private:
			/**
			 * Title that precedes the name of the individual, such as Ms., or Dr.
			 */
			string prefix;
			/**
			 * First (given) name of the individual
			 */
			string firstGiven;
			/**
			 * Middle name or initial of the individual
			 */
			std::vector<string> middle;
			/**
			 * Last (family) name /surname of the individual
			 */
			string lastFamily;
			/**
			 * Title that follows the name of the individual, such as Esq.
			 */
			string suffix;
			/**
			 * This provides a means of providing a full name as a single object for display or print such as identification badges etc. For example a person with the name of William Grace for official use may prefer a display name of Bill Grace on a name tag or other informal publication.
			 */
			ClassLibrary::ComplexDataTypes::InternationalString fullName;
			/**
			 * Clarifies when the name information is accurate.
			 */
			ClassLibrary::ComplexDataTypes::DateRange effectiveDates;
			/**
			 * An abbreviation or acronym for the name. This may be expressed in multiple languages. It is assumed that if only a single language is provided that it may be used in any of the other languages within which the name itself is expressed.
			 */
			ClassLibrary::ComplexDataTypes::InternationalString abbreviation;
			/**
			 * The type of individual name provided. the use of a controlled vocabulary is strongly recommended. At minimum his should include, e.g. PreviousFormalName, Nickname (or CommonName), Other.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfIndividualName;
			/**
			 * Sex allows for the specification of male, female or neutral. The purpose of providing this information is to assist others in the appropriate use of pronouns when addressing the individual. Note that many countries/languages may offer a neutral pronoun form.
			 */
			ClassLibrary::EnumerationsRegExp::SexSpecificationType sex;
			/**
			 * If more than one name for the object is provided, use the isPreferred attribute to indicate which is the preferred name content. All other names should be set to isPreferred="false".
			 */
			boolean isPreferred;
			/**
			 * A name may be specific to a particular context, i.e. common usage, business, social, etc.. Identify the context related to the specified name. Supports the use of an external controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry context;
			/**
			 * The legal or formal name of the individual should have the isFormal attribute set to true. To avoid confusion only one individual name should have the isFormal attribute set to true. Use the TypeOfIndividualName to further differentiate the type and applied usage when multiple names are provided.
			 */
			boolean isFormal;
		};
	}
}

#endif
