#ifndef TYPEDSTRING_H
#define TYPEDSTRING_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * TypedString combines a typeOfContent with a content defined as a simple string. May be used wherever a simple string needs to support a type definition to clarify its content
 * 
 * Examples
 * ==========
 * content is a regular expression and typeOfContent is used to define the syntax used.
 * 
 * Explanatory notes
 * ===================
 * This is a generic type + string where property name and documentation should be used to define any specification for the content. If international structured string content is required use TypedStructuredString 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class TypedString {

		private:
			/**
			 * Optional use of a controlled vocabulary to specifically type the associated content.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfContent;
			/**
			 * Content of the property expressed as a simple string.
			 */
			string content;
		};
	}
}

#endif
