#ifndef CONCEPT_H
#define CONCEPT_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * Unit of thought differentiated by characteristics [GSIM 1.1]
 * 
 * Examples
 * ==========
 * Poverty, Income, Household relationship, Family, Gender, Business Establishment, Satisfaction, etc.
 * 
 * Explanatory notes
 * ===================
 * Many DDI4 classes are subtypes of the Concept class including Category, Universe, UnitType, ConceptualVariable. This class realizes the pattern class Signified and as such a Concept can be denoted by a Sign.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * c:ConceptType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Concept
 */
class Concept : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Natural language statement conveying the meaning of a concept, differentiating it from other concepts. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString definition;
		};
	}
}

#endif
