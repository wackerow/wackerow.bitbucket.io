#ifndef DESIGNOVERVIEW_H
#define DESIGNOVERVIEW_H

namespace ClassLibrary {
	namespace SimpleMethodologyOverview {
		/**
 * Definition
 * ============
 * High level, descriptive, human informative, design statement The design may be used to specify how a process will be performed in general. This would most commonly be used in a Codebook along with an AlgorithmOverview and a MethodologyOverview. The design informs a specific or implemented process as to its general parameters. Supports specification of any realization of Goal. 
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * Allows for the use of any realized Process. The methodology, design, and algorithm of a specific realized process should be used if available. The use of a generic Process such as a WorkflowProcess containing an Act would be appropriate here. Restriction would be done by inclusion of the appropriate realized process class(es) in a Functional View.
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class DesignOverview : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A term describing the subject of the design. Supports the use of an external controlled vocabulary
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry subectOfDesign;
			/**
			 * Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
