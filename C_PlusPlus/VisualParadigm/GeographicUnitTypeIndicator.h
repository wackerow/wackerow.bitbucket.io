#ifndef GEOGRAPHICUNITTYPEINDICATOR_H
#define GEOGRAPHICUNITTYPEINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Provides ability to declare an optional sequence or index order to a Unit Type describing a Geographic Unit Type. Extended to support membership in a specific level.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class GeographicUnitTypeIndicator {

		private:
			/**
			 * Index number expressed as an integer. The position of the member in an ordered array. Optional for unordered Collections.
			 */
			int index;
			/**
			 * Indicates the level within which the CodeItem resides
			 */
			int isInLevel;
		};
	}
}

#endif
