#ifndef SPATIALRELATIONSHIP_H
#define SPATIALRELATIONSHIP_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Used to specify a relationship between one spatial object and another. Includes definition of the spatial object types being related, the spatial relation specification, and the event date.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SpatialRelationship {

		private:
			/**
			 * Provides the types of the two spatial objects.
			 */
			ClassLibrary::EnumerationsRegExp::SpatialObjectPairs hasSpatialObjectPair;
			/**
			 * Defines the relationship of the two spatial objects
			 */
			ClassLibrary::EnumerationsRegExp::SpatialRelationSpecification hasSpatialRelationSpecification;
			/**
			 * The date of the relationship change.
			 */
			ClassLibrary::ComplexDataTypes::Date eventDate;
		};
	}
}

#endif
