#ifndef PROCESSSTEPRELATION_H
#define PROCESSSTEPRELATION_H

namespace ClassLibrary {
	namespace ProcessPattern {
		/**
 * Definition
 * ============
 * Specifies MemberRelation to ProcessSteps for Process in Process Pattern
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ProcessStepRelation : ClassLibrary::CollectionsPattern::MemberRelation {
		};
	}
}

#endif
