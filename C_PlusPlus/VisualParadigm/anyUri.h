#ifndef ANYURI_H
#define ANYURI_H

namespace ClassLibrary {
	namespace XMLSchemaDatatypes {
		/**
 * anyUri is like the primitive datatype "anyUri" of the XML Schema datatypes.
 * 
 * Definition from XML Schema Part 2: Datatypes Second Edition:
 * "anyURI" represents a Uniform Resource Identifier Reference (URI). An "anyURI" value can be absolute or relative, and may have an optional fragment identifier (i.e., it may be a URI Reference). This type should be used to specify the intention that the value fulfills the role of a URI as defined by [RFC 2396], as amended by [RFC 2732].
 * 
 * For details see at https://www.w3.org/TR/xmlschema-2/#anyUri
 */
class anyUri {
		};
	}
}

#endif
