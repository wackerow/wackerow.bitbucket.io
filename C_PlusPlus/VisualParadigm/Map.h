#ifndef MAP_H
#define MAP_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Describes the correspondence between concepts in a correspondence table related to one or more Statistical Classifications.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * A Map is the pairing of similar Concepts. Each Concept in the Map belongs to a different Collection. The collection of maps for all the Concepts in corresponding Collections is a Correspondence Table.
 * 
 * A simple example might map the following 2 martial status category sets:
 * MS1 -
 * single
 * married
 * widowed
 * divorced
 * 
 * MS2 -
 * single
 * married
 * 
 * So, a correspondence table between these 2 category sets might look like this:
 * MS1                                               MS2
 * single                                             single
 * widowed                                          "
 * divorced                                          "
 * married                                            married
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Map
 */
class Map {

		private:
			/**
			 * Date from which the Map became valid. The date must be defined if the Map belongs to a floating CorrespondenceTable. Date at which the Map became invalid. The date must be defined if the Map belongs to a floating Correspondence Table and is no longer valid.
			 */
			ClassLibrary::ComplexDataTypes::DateRange validDates;
			/**
			 * Type of correspondence in terms of commonalities and differences between two members.
			 */
			ClassLibrary::ComplexDataTypes::CorrespondenceType hasCorrespondenceType;
			/**
			 * A display label for the OrderedMemberCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.
			 */
			ClassLibrary::ComplexDataTypes::LabelForDisplay displayLabel;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
		};
	}
}

#endif
