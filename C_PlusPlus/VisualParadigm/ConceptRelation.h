#ifndef CONCEPTRELATION_H
#define CONCEPTRELATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Specifies the relationship between concepts in a collection of concepts. Use controlled vocabulary provided in hasRelationSpecification to identify the type of relationship (e.g. ParentChild, WholePart, etc.)
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ConceptRelation {

		private:
			/**
			 * RelationSpecification Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSepcification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
		};
	}
}

#endif
