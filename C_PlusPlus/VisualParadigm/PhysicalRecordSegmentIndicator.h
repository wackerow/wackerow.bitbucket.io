#ifndef PHYSICALRECORDSEGMENTINDICATOR_H
#define PHYSICALRECORDSEGMENTINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Member Indicator for use with member type PhysicalRecordSegment
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class PhysicalRecordSegmentIndicator {

		private:
			/**
			 * Index value of member in an ordered array
			 */
			int index;
		};
	}
}

#endif
