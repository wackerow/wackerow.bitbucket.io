#ifndef LANGUAGESPECIFICSTRINGTYPE_H
#define LANGUAGESPECIFICSTRINGTYPE_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Allows for non-formatted language specific strings that may be translations from other languages, or that may be translatable into other languages. Only one string per language/location type is allowed. LanguageSpecificString contains the following attributes, xmlang to designate the language, isTranslated with a default value of false to designate if an object is a translation of another language, isTranslatable with a default value of true to designate if the content can be translated, translationSourceLanguage to indicate the source languages used in creating this translation, and translationDate.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:StringType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class LanguageSpecificStringType {

		private:
			/**
			 * Value of this string
			 */
			string content;
			/**
			 * Indicates the language of content. Note that language allows for a simple 2 or 3 character language code or a language code extended by a country code , for example en-au for English as used in Australia.
			 */
			ClassLibrary::XMLSchemaDatatypes::language language;
			/**
			 * Indicates whether content is a translation (true) or an original (false).
			 */
			boolean isTranslated;
			/**
			 * Indicates whether content is translatable (true) or not (false). An example of something that is not translatable would be a MNEMONIC of an object or a number.
			 */
			boolean isTranslatable;
			/**
			 * List the language code of the source. Repeat of multiple language sources are used.
			 */
			std::vector<ClassLibrary::XMLSchemaDatatypes::language> translationSourceLanguage;
			/**
			 * The date the content was translated. Provision of translation date allows user to verify if translation was available during data collection or other time linked activity.
			 */
			UnlimitedNatural translationDate;
		};
	}
}

#endif
