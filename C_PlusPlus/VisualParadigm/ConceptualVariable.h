#ifndef CONCEPTUALVARIABLE_H
#define CONCEPTUALVARIABLE_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * The use of a Concept as a characteristic of a UnitType intended to be measured
 * 
 * Examples
 * ==========
 * 1. Sex of person
 * 2. Number of employees 
 * 3. Value of endowment
 * 
 * Explanatory notes
 * ===================
 * Note that DDI varies from GSIM in the use of a UnitType rather than a Universe. "The use of a Concept as a characteristic of a Universe intended to be measured" [GSIM 1.1]
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * c:ConceptualVariableType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Variable
 */
class ConceptualVariable : ClassLibrary::Conceptual::Concept {

		private:
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * A short natural language account of the characteristics of the object.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;
		};
	}
}

#endif
