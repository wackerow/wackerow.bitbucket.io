#ifndef SEGMENT_H
#define SEGMENT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * A structure used to express explicit segments or regions within different types of external materials (Textual, Audio, Video, XML, and Image). Provides the appropriate start, stop, or region definitions for each type.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:SegmentType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Segment {

		private:
			/**
			 * Describes the type and length of the audio segment.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AudioSegment> usesAudioSegment;
			/**
			 * Describes the type and length of the video segment.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::VideoSegment> usesVideoSegment;
			/**
			 * An X-Pointer expression identifying a node in the XML document.
			 */
			std::vector<string> xml;
			/**
			 * Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment
			 */
			std::vector<ClassLibrary::ComplexDataTypes::TextualSegment> useseTextualSegment;
			/**
			 * Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and Coordinates provides the information required to define it.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ImageArea> usesImageArea;
		};
	}
}

#endif
