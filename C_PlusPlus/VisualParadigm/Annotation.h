#ifndef ANNOTATION_H
#define ANNOTATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Provides annotation information on the object to support citation and crediting of the creator(s) of the object.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:CitationType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Annotation {

		private:
			/**
			 * Full authoritative title. List any additional titles for this item as AlternativeTitle.
			 */
			ClassLibrary::ComplexDataTypes::InternationalString title;
			/**
			 * Secondary or explanatory title.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> subTitle;
			/**
			 * An alternative title by which a data collection is commonly referred, or an abbreviation  for the title.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> alternativeTitle;
			/**
			 * Person, corporate body, or agency responsible for the substantive and intellectual content of the described object.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentAssociation> creator;
			/**
			 * Person or organization responsible for making the resource available in its present form.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentAssociation> publisher;
			/**
			 * The name of a contributing author or creator, who worked in support of the primary creator given above.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentAssociation> contributor;
			/**
			 * A date associated with the annotated object (not the coverage period). Use typeOfDate to specify the type of date such as Version, Publication, Submitted, Copyrighted, Accepted, etc.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AnnotationDate> date;
			/**
			 * Language of the intellectual content of the described object. Multiple languages are supported by the structure itself as defined in the transformation to specific bindings. Use language codes supported by xs:language which include the 2 and 3 character and extended structures defined by RFC4646 or its successors. Listing of language codes: http://www-01.sil.org/iso639-3/codes.asp Rules for construction of the code: http://www.rfc-editor.org/rfc/bcp/bcp47.txt
			 */
			ClassLibrary::XMLSchemaDatatypes::LanguageSpecification languageOfObject;
			/**
			 * An identifier or locator. Contains identifier and Managing agency (ISBN, ISSN, DOI, local archive). Indicates if it is a URI.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalIdentifier> identifier;
			/**
			 * The copyright statement.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> copyright;
			/**
			 * Provide the type of the resource. This supports the use of a controlled vocabulary. It should be appropriate to the level of the annotation.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry> typeOfResource;
			/**
			 * The name or identifier of source information for the annotated object.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> informationSource;
			/**
			 * Means of identifying the current version of the annotated object.
			 */
			string versionIdentification;
			/**
			 * The agent responsible for the version. May have an associated role.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::AgentAssociation> versioningAgent;
			/**
			 * A summary description (abstract) of the annotated object.
			 */
			ClassLibrary::ComplexDataTypes::InternationalString summary;
			/**
			 * Provide the identifier, managing agency, and type of resource related to this object. Use to specify related resources similar to Dublin Core isPartOf and hasPart to indicate collection/series membership for objects where there is an identifiable record. If not an identified object use the relationship to ExternalMaterial using a type that indicates a series description.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ResourceIdentifier> relatedResource;
			/**
			 * A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and interpretation.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> provenance;
			/**
			 * Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> rights;
			/**
			 * Date the record was created
			 */
			ClassLibrary::EnumerationsRegExp::IsoDateType recordCreationDate;
			/**
			 * Date the record was last revised
			 */
			ClassLibrary::EnumerationsRegExp::IsoDateType recordLastRevisionDate;
		};
	}
}

#endif
