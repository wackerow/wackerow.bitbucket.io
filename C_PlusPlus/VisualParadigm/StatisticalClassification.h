#ifndef STATISTICALCLASSIFICATION_H
#define STATISTICALCLASSIFICATION_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * A Statistical Classification is a set of Categories which may be assigned to one or more variables registered in statistical surveys or administrative files, and used in the production and dissemination of statistics. The Categories at each Level of the classification structure must be mutually exclusive and jointly exhaustive of all objects/units in the population of interest. (Source: GSIM StatisticalClassification)
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * The Categories are defined with reference to one or more characteristics of a particular universe of units of observation. A Statistical Classification may have a flat, linear structure or may be hierarchically structured, such that all Categories at lower Levels are sub-Categories of Categories at the next Level up.Categories in Statistical Classifications are represented in the information model as Classification Items. (Source: GSIM StatisticalClassification
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Statistical Classification
 */
class StatisticalClassification : ClassLibrary::Representations::CodeList {

		private:
			/**
			 * Date the Statistical Classification was released
			 */
			ClassLibrary::ComplexDataTypes::Date releaseDate;
			/**
			 * The date the statistical classification enters production use and the date on which the Statistical Classification was superseded by a successor version or otherwise ceased to be valid. (Source: GSIM Statistical Classification)
			 */
			ClassLibrary::ComplexDataTypes::DateRange validDates;
			/**
			 * Indicates if the Statistical Classification is currently valid.
			 */
			boolean isCurrent;
			/**
			 * Indicates if the Statistical Classification is a floating classification. In a floating statistical classification, a validity period should be defined for all Classification Items which will allow the display of the item structure and content at different points of time. (Source: GSIM StatisticalClassification/Floating
			 */
			boolean isFloating;
			/**
			 * Describes the relationship between the variant and its base Statistical Classification, including regroupings, aggregations added and extensions. (Source: GSIM StatisticalClassification/Changes from base Statistical Classification)
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString changeFromBase;
			/**
			 * If the Statistical Classification is a variant, notes the specific purpose for which it was developed. (Source: GSIM StatisticalClassification/Purpose of variant)
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purposeOfVariant;
			/**
			 * Copyright of the statistical classification.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalString> copyright;
			/**
			 * Summary description of changes which have occurred since the most recent classification version or classification update came into force.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalStructuredString> updateChanges;
			/**
			 * A list of languages in which the Statistical Classification is available. Supports the indication of multiple languages within a single property. Supports use of codes defined by the RFC 1766.
			 */
			ClassLibrary::XMLSchemaDatatypes::LanguageSpecification availableLanguage;
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString rationale;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
			/**
			 * Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ClassificationItemIndicator> contains;
		};
	}
}

#endif
