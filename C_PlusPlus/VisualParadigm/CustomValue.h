#ifndef CUSTOMVALUE_H
#define CUSTOMVALUE_H

namespace ClassLibrary {
	namespace CustomMetadata {
		/**
 * Definition
 * ============
 * An instance of a  key, value pair for a  particular  key.
 * 
 * Examples
 * ==========
 * OMBBurden,"High"
 * 
 * openEHRBPposition, "upper arm"
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CustomValue : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * the value associated with a particular key expressed as a string. The ultimate value representation is defined by the corresponding CustomValue.
			 */
			ClassLibrary::ComplexDataTypes::ValueString value;
			/**
			 * The unique String identifying the value
			 */
			string key;
		};
	}
}

#endif
