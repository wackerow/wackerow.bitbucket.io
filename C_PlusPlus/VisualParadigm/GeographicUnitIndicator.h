#ifndef GEOGRAPHICUNITINDICATOR_H
#define GEOGRAPHICUNITINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Provides ability to declare an optional sequence or index order to a Geographic Unit
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class GeographicUnitIndicator {

		private:
			/**
			 * Index number expressed as an integer. The position of the member in an ordered array. Optional for unordered Collections.
			 */
			int index;
			/**
			 * Indicates the level within which the CodeItem resides
			 */
			int isInLevel;
		};
	}
}

#endif
