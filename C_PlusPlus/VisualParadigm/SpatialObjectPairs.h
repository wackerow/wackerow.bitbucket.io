#ifndef SPATIALOBJECTPAIRS_H
#define SPATIALOBJECTPAIRS_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum SpatialObjectPairs {
			PointToPoint, 
			PointToLine, 
			PointToArea, 
			LineToLine, 
			LineToArea, 
			AreaToArea
		};
	}
}

#endif
