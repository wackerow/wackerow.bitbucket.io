#ifndef STANDARD_H
#define STANDARD_H

namespace ClassLibrary {
	namespace StudyRelated {
		/**
 * Definition
 * ============
 * Identifies the external standard used and describes the level of compliance with the standard in terms specific aspects of the standard's content.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:StandardType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Standard : ClassLibrary::Identification::AnnotatedIdentifiable {
		};
	}
}

#endif
