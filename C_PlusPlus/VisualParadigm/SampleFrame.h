#ifndef SAMPLEFRAME_H
#define SAMPLEFRAME_H

namespace ClassLibrary {
	namespace SamplingMethodology {
		/**
 * Definition
 * ============
 * A set of information used to identify a sample population within a larger population. The sample frame will contain identifying information for individual units within the frame as well as characteristics which will support analysis and the division of the frame population into sub-groups.
 * 
 * Examples
 * ==========
 * A listing of all business enterprises by their primary office address with information on their industry classification, work staff size, and production costs. A telephone directory. An address list of housing units.
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SampleFrame : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * Explanation of the ways in which the sample frame is to be employed. Supports use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
			/**
			 * An explanation of the intent of the sample frame. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * An explanation of any limitations of the sample frame. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString limitations;
			/**
			 * A description of how the sample frame is updated and maintained. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString updateProcedures;
			/**
			 * The reference period for the sample frame.
			 */
			ClassLibrary::ComplexDataTypes::DateRange referencePeriod;
			/**
			 * The period for which the current version is valid
			 */
			ClassLibrary::ComplexDataTypes::DateRange validPeriod;
		};
	}
}

#endif
