#ifndef SAMPLINGALGORITHM_H
#define SAMPLINGALGORITHM_H

namespace ClassLibrary {
	namespace SamplingMethodology {
		/**
 * Definition
 * ============
 * Generic description of a sampling algorithm expressed by a Sampling Design and implemented by a Sampling Process. May include common algebraic formula or statistical package instructions (use of specific selection models).
 * 
 * Examples
 * ==========
 * Random Sample; Stratified Random Sample
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class SamplingAlgorithm : ClassLibrary::SimpleMethodologyOverview::AlgorithmOverview {

		private:
			/**
			 * The algorithm expressed as algebraic or computer code.
			 */
			ClassLibrary::ComplexDataTypes::CommandCode codifiedExpressionOfAlgorithm;
		};
	}
}

#endif
