#ifndef PHYSICALSEGMENTLAYOUT_H
#define PHYSICALSEGMENTLAYOUT_H

namespace ClassLibrary {
	namespace FormatDescription {
		/**
 * Definition
 * ============
 * The PhysicalSegmentLayout is an abstract class used as an extension point in the description of the different layout styles of data structure description.
 * 
 * Examples
 * ==========
 * Examples include UnitSegmentLayouts, event data layouts, and cube layouts (e.g. summary data).
 * 
 * Explanatory notes
 * ===================
 * A PhysicalLayout is a physical description (EventLayout, UnitSegmentLayout, or CubeLayout) of the associated Logical Record Layout consisting of a Collection of Value Mappings describing the physical representation of each related Instance Variable.
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class PhysicalSegmentLayout : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Indicates that the data are in a delimited format. If not set to true, is FixedWitdh must be set to true.
			 */
			boolean isDelimited;
			/**
			 * The Delimiting character in the data. Must be used if isDelimited is true. "The separator between cells, set by the delimiter property of a dialect description. The default is ,." see https://www.w3.org/TR/tabular-data-model/#encoding  From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  delimiter: �An atomic property that sets the delimiter flag to the single provided value, which MUST be a string. The default is ",".� From https://www.w3.org/TR/tabular-data-model/ 8 �The separator between cells, set by the delimiter property of a dialect description. The default is ,.� From http://specs.frictionlessdata.io/csv-dialect/ delimiter:  �specifies a one-character string to use as the field separator. Default = ,�
			 */
			string delimiter;
			/**
			 * Set to true if the file is fixed-width. If true, isDelimited must be set to false.
			 */
			boolean isFixedWidth;
			/**
			 * "The string that is used to escape the quote character within escaped cells, or null"  see https://www.w3.org/TR/tabular-data-model/#encoding From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect   � doupleQuote: A boolean atomic property that, if true, sets the escape character flag to ". If false, to \. The default is true.� From http://specs.frictionlessdata.io/csv-dialect/ doubleQuote: �controls the handling of quotes inside fields. If true, two consecutive quotes should be interpreted as one. Default = true�
			 */
			string escapeCharacter;
			/**
			 * "The strings that can be used at the end of a row, set by the lineTerminators property of a dialect description. The default is [CRLF, LF]."  see https://www.w3.org/TR/tabular-data-model/#encoding From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  lineTerminators: �An atomic property that sets the line terminators flag to either an array containing the single provided string value, or the provided array. The default is ["\r\n", "\n"].� From http://specs.frictionlessdata.io/csv-dialect/ lineTerminator: �specifies the character sequence which should terminate rows. Default = \r\n�
			 */
			std::vector<string> lineTerminator;
			/**
			 * The string that is used around escaped cells, or null, set by the quoteChar property of a dialect description. The default is ". see https://www.w3.org/TR/tabular-data-model/#encoding From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  quoteChar: �An atomic property that sets the quote character flag to the single provided value, which MUST be a string or null. If the value is null, the escape character flag is also set to null. The default is '"'.� From http://specs.frictionlessdata.io/csv-dialect/ quoteChar: �specifies a one-character string to use as the quoting character. Default = "�
			 */
			string quoteCharacter;
			/**
			 * A string used to indicate that an input line is a comment, a string which precedes a comment in the data file.  From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  commentPrefix: �An atomic property that sets the comment prefix flag to the single provided value, which MUST be a string. The default is "#".�
			 */
			string commentPrefix;
			/**
			 * The character encoding of the represented data. From https://www.w3.org/TR/tabular-metadata/ 5.9 Dialect: encoding: �An atomic property that sets the encoding flag to the single provided string value, which MUST be a defined in [encoding]. The default is "utf-8".�     From https://www.w3.org/TR/tabular-metadata/ 7.2 Encoding:  �CSV files should be encoded using UTF-8, and should be in Unicode Normal Form C as defined in [UAX15]. If a CSV file is not encoded using UTF-8, the encoding should be specified through the charset parameter in the Content-Type header:�
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry encoding;
			/**
			 * True if the file contains a header containing column names. From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  header: �A boolean atomic property that, if true, sets the header row count flag to 1, and if false to 0, unless headerRowCount is provided, in which case the value provided for the header property is ignored. The default is true.� From http://specs.frictionlessdata.io/csv-dialect/ header: �indicates whether the file includes a header row. If true the first row in the file is a header row, not data. Default = true�
			 */
			boolean hasHeader;
			/**
			 * The number of lines in the header From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  headerRowCount: �A numeric atomic property that sets the header row count flag to the single provided value, which MUST be a non-negative integer. The default is 1.�
			 */
			int headerRowCount;
			/**
			 * If True, blank rows are ignored. From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  skipBlankRows: �A boolean atomic property that sets the skip blank rows flag to the single provided boolean value. The default is false.�
			 */
			boolean skipBlankRows;
			/**
			 * The number of columns to skip at the beginning of the row. From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  skipColumns: �A numeric atomic property that sets the skip columns flag to the single provided numeric value, which MUST be a non-negative integer. The default is 0.� See also https://www.w3.org/TR/tabular-metadata/ 8 skip columns: �The number of columns to skip at the beginning of each row, set by the skipColumns property of a dialect description. The default is 0. A value other than 0 will mean that the source numbers of columns will be different from their numbers.�
			 */
			int skipDataColumns;
			/**
			 * If true skip spaces at the beginning of a line or following a delimiter. From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  skipInitialSpace: �A boolean atomic property that, if true, sets the trim flag to "start" and if false, to false. If the trim property is provided, the skipInitialSpace property is ignored. The default is false.� From http://specs.frictionlessdata.io/csv-dialect/ skipInitialSpace: �specifies how to interpret whitespace which immediately follows a delimiter; if false, it means that whitespace immediately after a delimiter should be treated as part of the following field. Default = true�
			 */
			boolean skipInitialSpace;
			/**
			 * Number of input rows to skip preceding the header or data From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  skipRows: �A numeric atomic property that sets the skip rows flag to the single provided numeric value, which MUST be a non-negative integer. The default is 0.� From https://www.w3.org/TR/tabular-metadata/ 8  skip rows: �The number of rows to skip at the beginning of the file, before a header row or tabular data, set by the skipRows property of a dialect description. The default is 0. A value greater than 0 will mean that the source numbers of rows will be different from their numbers.�
			 */
			int skipRows;
			/**
			 * Specifies which spaces to remove from a data value (start, end, both, neither) From https://www.w3.org/TR/tabular-metadata/  5.9 Dialect  trim: �An atomic property that, if the boolean true, sets the trim flag to true and if the boolean false to false. If the value provided is a string, sets the trim flag to the provided value, which MUST be one of "true", "false", "start", or "end". The default is true.�
			 */
			ClassLibrary::EnumerationsRegExp::TrimValues trim;
			/**
			 * A string indicating a null value. From https://www.w3.org/TR/tabular-metadata/ 4.3 �null � the string or strings which cause the value of cells having string value matching any of these values to be null.� From Inherited 5.7 null: �An atomic property giving the string or strings used for null values within the data. If the string value of the cell is equal to any one of these values, the cell value is null. See Parsing Cells in [tabular-data-model] for more details. If not specified, the default for the null property is the empty string "". The value of this property becomes the null annotation for the described column.� From http://specs.frictionlessdata.io/csv-dialect/ nullSequence: �specifies the null sequence (for example \N). Not set by default�
			 */
			string nullSequence;
			/**
			 * If True, the case of the labels in the header is significant. From http://specs.frictionlessdata.io/csv-dialect/ caseSensitiveHeader: �indicates that case in the header is meaningful. For example, columns CAT and Cat should not be equated. Default = false�
			 */
			boolean headerIsCaseSensitive;
			/**
			 * Number cells, rows, columns starting from this value. This is a DDI notion allowing numbering of columns, rows, etc. from either 0 or 1. W3C appears to just use 1. From https://www.w3.org/TR/tabular-data-model/ 4.3 �number � the position of the column amongst the columns for the associated table, starting from 1.�
			 */
			int arrayBase;
			/**
			 * If True, consecutive delimiters are treated the same way as a single delimiter, if false consiecutive delimiters indicate a missing value. From PHDD consecutiveDelimitersAsOne: �Indicates how consecutive delimiters should be handed by the software. Equivalent element in DDI 3.2: p:Delimiter/@treatConsecutiveDelimiterAsOne DDI 3.2 Documentation: http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/schemas/reusable_xsd/complexTypes/DelimiterType.html#a6�
			 */
			boolean treatConsecutiveDelimitersAsOne;
			/**
			 * Short natural language account of the physical layout obtained from the combination of associated  properties and relationships. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
			/**
			 * Indicates the direction in which columns are arranged in each row. From https://www.w3.org/TR/tabular-metadata/  5.3.2 tableDirection: �An atomic property that MUST have a single string value that is one of "rtl", "ltr", or "auto". Indicates whether the tables in the group should be displayed with the first column on the right, on the left, or based on the first character in the table that has a specific direction. The value of this property becomes the value of the table direction annotation for all the tables in the table group. See Bidirectional Tables in [tabular-data-model] for details. The default value for this property is "auto".�
			 */
			ClassLibrary::EnumerationsRegExp::TableDirectionValues tableDirection;
			/**
			 * Indicates the reading order of text within cells. From https://www.w3.org/TR/tabular-metadata/ Inherited 5.7 textDirection: �An atomic property that MUST have a single string value that is one of "ltr", "rtl", "auto" or "inherit" (the default). Indicates whether the text within cells should be displayed as left-to-right text (ltr), as right-to-left text (rtl), according to the content of the cell (auto) or in the direction inherited from the table direction annotation of the table. The value of this property determines the text direction annotation for the column, and the text direction annotation for the cells within that column: if the value is inherit then the value of the text direction annotation is the value of the table direction annotation on the table, otherwise it is the value of this property. See Bidirectional Tables in [tabular-data-model] for details.�
			 */
			ClassLibrary::EnumerationsRegExp::TextDirectionValues textDirection;
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * The PhysicalLayout contains a description of the physical representation of each InstanceVariable in a ValueMapping. This relationship fills the role of "Contains" from the Collection pattern. Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ValueMappingIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
