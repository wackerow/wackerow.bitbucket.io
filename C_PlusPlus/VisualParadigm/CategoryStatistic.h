#ifndef CATEGORYSTATISTIC_H
#define CATEGORYSTATISTIC_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Statistics related to a specific category of an InstanceVariable within a data set.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * pi:CategoryStatisticType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class CategoryStatistic {

		private:
			/**
			 * Type of category statistic. Supports the use of an external controlled vocabulary. DDI strongly recommends the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfCategoryStatistic;
			/**
			 * The value of the identified type of statistic for the category. May be repeated to provide unweighted or weighted values and different computation bases.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Statistic> hasStatistic;
			/**
			 * Value of the category to which the statistics apply
			 */
			ClassLibrary::ComplexDataTypes::ValueString categoryValue;
			/**
			 * The value of the filter variable to which the category statistic is restricted
			 */
			ClassLibrary::ComplexDataTypes::ValueString filterValue;
		};
	}
}

#endif
