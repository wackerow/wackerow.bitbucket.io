#ifndef FIXEDTEXT_H
#define FIXEDTEXT_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * The static portion of the text expressed as a StructuredString with the ability to preserve whitespace if critical to the understanding of the content.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:TextType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class FixedText : ClassLibrary::ComplexDataTypes::LanguageSpecificStructuredStringType {

		private:
			/**
			 * The default setting states that leading and trailing white space will be removed and multiple adjacent white spaces will be treated as a single white space. If the existance of any of these white spaces is critical to the understanding of the content, change the value of this attribute to "preserve".
			 */
			ClassLibrary::EnumerationsRegExp::WhiteSpaceRule whiteSpace;
		};
	}
}

#endif
