#ifndef VALUERELATIONSHIPTYPE_H
#define VALUERELATIONSHIPTYPE_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum ValueRelationshipType {
			Equal, 
			NotEqual, 
			GreaterThan, 
			GreaterThanOrEqualTo, 
			LessThan, 
			LessThanOrEqualTo
		};
	}
}

#endif
