#ifndef TRIMVALUES_H
#define TRIMVALUES_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum TrimValues {
			Start, 
			End, 
			Both, 
			Neither
		};
	}
}

#endif
