#ifndef RELATIONSTRUCTURE_H
#define RELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace CollectionsPattern {
		/**
 * Definition
 * ============
 * The set of MemberRelations used to structure a Collection
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * The optional RelationStructure is used to describe relations among Members of the Collection that are more complex than a simple list. There can be multiple types of relation structures for a Collection and these in turn may each have a different semantic. These differences can be described by the properties of the Relation Structure, or in the case of hybrid structures described by the individual MemberRelations.
 * Each MemberRelations functions like an adjacency list (https://en.wikipedia.org/wiki/Adjacency_list) in graph theory, describing the relationships of one Member to multiple other members.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class RelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Controlled Vocabulary to specify whether the relation is total, partial or unknown.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship context for the relationship using an External Controlled Vocabulary. Examples might include "parent-child", or "immediate supervisee"
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * MemberRelation used to define the relationship of members within the collection
			 */
			std::vector<ClassLibrary::CollectionsPattern::MemberRelation> hasMemberRelation;
		};
	}
}

#endif
