#ifndef RELATIONSPECIFICATION_H
#define RELATIONSPECIFICATION_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		enum RelationSpecification {
			Unordered, 
			List, 
			ParentChild, 
			WholePart, 
			AcyclicPrecedence, 
			Equivalence, 
			GeneralSpecfic
		};
	}
}

#endif
