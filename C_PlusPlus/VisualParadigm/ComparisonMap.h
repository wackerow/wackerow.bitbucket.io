#ifndef COMPARISONMAP_H
#define COMPARISONMAP_H

namespace ClassLibrary {
	namespace CollectionsPattern {
		/**
 * Definition
 * ============
 * Provides a basic pattern for a comparison map which identifies source and target members and details about their match. Source and target were retained as in describing differences and commonalities the direction of the comparison may be important.
 * 
 * Examples
 * ==========
 * Comparing the Classification Items between Statistical Classifications; describing the comparative relationship between InstanceVariables in two LogicalRecords for use in linking records
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ComparisonMap {

		private:
			/**
			 * Describes the relationship between the source and target members using both controlled vocabularies and descriptive text.
			 */
			ClassLibrary::ComplexDataTypes::CorrespondenceType hasCorrespondenceType;
		};
	}
}

#endif
