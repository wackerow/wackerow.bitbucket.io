#ifndef REPRESENTEDQUESTION_H
#define REPRESENTEDQUESTION_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * The description of a reusable question, that can be used as a template that describes the components of a question
 * 
 * Examples
 * ==========
 * A question in a question bank such as the content and format for a question on Race for all survey/censuses fielded by U.S. agencies as prescribed by the U.S. Office of Management and Budget (OMB)
 * 
 * Explanatory notes
 * ===================
 * A question that is repeated across waves of a panel study can be reused and also allow reference to a RepresentedVariable. A question that has been tested for consistency of response and is used by multiple studies.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * d:QuestionItemType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class RepresentedQuestion : ClassLibrary::DataCapture::Capture {

		private:
			/**
			 * The text of the question which may be literal or dynamic (altered to personalize the question text) in terms of content.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::DynamicText> questionText;
			/**
			 * The purpose or intent of the question.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString questionIntent;
			/**
			 * An estimation of the number of seconds required to respond to the question. Used for estimating overall questionnaire completion time.
			 */
			Real estimatedResponseTimeInSeconds;
		};
	}
}

#endif
