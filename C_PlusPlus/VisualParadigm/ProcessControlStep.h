#ifndef PROCESSCONTROLSTEP_H
#define PROCESSCONTROLSTEP_H

namespace ClassLibrary {
	namespace ProcessPattern {
		/**
 * Definition
 * ============
 * A Process Step that controls the execution flow of the Process by determining the next Process Step in its scope.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ProcessControlStep : ClassLibrary::ProcessPattern::ProcessStep {
		};
	}
}

#endif
