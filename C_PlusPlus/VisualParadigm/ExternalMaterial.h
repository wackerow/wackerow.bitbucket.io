#ifndef EXTERNALMATERIAL_H
#define EXTERNALMATERIAL_H

namespace ClassLibrary {
	namespace Utility {
		/**
 * Definition
 * ============
 * ExternalMaterial describes the location, structure, and relationship to the DDI metadata instance for any material held external to that instance. This includes citations to such material, an external reference to a URL (or other URI), and a statement about the relationship between the cited ExternalMaterial the contents of the DDI instance. It should be used as follows:as a target object from a relationship which clarifies its role within a class; or as the target of a relatedResource within an annotation.
 * 
 * 
 * Examples
 * ==========
 * ExternalMaterial is used to identify material and optionally specific sections of the material that have a relationship to a class. There is a generic relatedMaterial on AnnotatedIdentifiable. This should be used to attach any additional material that the user identifies as important to the class. The properties typeOfMaterial, descriptiveText, and relationshipDescription should be used to clarify the purpose and coverage of the related material.
 * 
 * Explanatory notes
 * ===================
 * Within the DDI model, ExternalMaterial is used as an extension base for specific external materials found such as an External Aid. It is used as a base for specifically related material (e.g. ExternalAid) by creating a relationship whose name clarifies the purpose of the related material.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * r:OtherMaterialType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ExternalMaterial : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Designation of the type of material being described. Supports the use of a controlled vocabulary.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfMaterial;
			/**
			 * A description of the referenced material. This field can map to a Dublin Core abstract. Note that Dublin Core does not support structure within the abstract element. Supports multiple languages and optional structured content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString descriptiveText;
			/**
			 * URI for the location of the material
			 */
			std::vector<ClassLibrary::XMLSchemaDatatypes::anyUri> uri;
			/**
			 * Describes the reason the external material is being related to the DDI metadata instance.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::InternationalStructuredString> relationshipDescription;
			/**
			 * Provides a standard Internet MIME type for use by processing applications.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry mimeType;
			/**
			 * Can describe a segment within a larger object such as a text or video segment.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::Segment> usesSegment;
			/**
			 * Bibliographic citation for the external resource.
			 */
			ClassLibrary::ComplexDataTypes::Annotation citationOfExternalMaterial;
		};
	}
}

#endif
