#ifndef DESIGN_H
#define DESIGN_H

namespace ClassLibrary {
	namespace MethodologyPattern {
		/**
 * Definition
 * ============
 * The design pattern class may be used to specify or, again, defines how a process will be performed in general. The design informs a specific or implemented process as to its general parameters.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Process Design
 */
class Design : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString overview;
		};
	}
}

#endif
