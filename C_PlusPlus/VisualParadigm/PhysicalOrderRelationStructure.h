#ifndef PHYSICALORDERRELATIONSTRUCTURE_H
#define PHYSICALORDERRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace FormatDescription {
		/**
 * Definition
 * ============
 * PhysicalStructureOrder orders thePhysicalRecordSegments  which map to a LogicalRecord.
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * The same LogicalRecordLayout may be the sourceMember in several adjacency lists. This can happen when PhysicalRecordSegments are also population specific. In this instance each adjacency list associated with a LogicalRecordLayout is associated with a different population.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class PhysicalOrderRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Member relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::PhysicalRecordSegmentRelation> hasMemberRelation;
		};
	}
}

#endif
