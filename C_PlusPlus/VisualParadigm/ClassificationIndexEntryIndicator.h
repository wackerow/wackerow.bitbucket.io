#ifndef CLASSIFICATIONINDEXENTRYINDICATOR_H
#define CLASSIFICATIONINDEXENTRYINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Member Indicator for use with member type ClassificationIndexEntry
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ClassificationIndexEntryIndicator {

		private:
			/**
			 * Index value of member in an ordered array
			 */
			int index;
		};
	}
}

#endif
