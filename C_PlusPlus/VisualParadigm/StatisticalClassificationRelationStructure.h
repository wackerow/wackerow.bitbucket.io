#ifndef STATISTICALCLASSIFICATIONRELATIONSTRUCTURE_H
#define STATISTICALCLASSIFICATIONRELATIONSTRUCTURE_H

namespace ClassLibrary {
	namespace Representations {
		/**
 * Definition
 * ============
 * A structure for describing the complex relationship between statistical classifications in a classification series
 * 
 * Examples
 * ==========
 * A classification series that branches into separately versioned classifications
 * 
 * Explanatory notes
 * ===================
 * Can use relation specification information to more fully describe the relationship between members such as parent/child, whole/part, general/specific, equivalence, etc.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class StatisticalClassificationRelationStructure : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.
			 */
			ClassLibrary::ComplexDataTypes::RelationSpecification hasRelationSpecification;
			/**
			 * Provides semantic context for the relationship
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry semantic;
			/**
			 * Type of relation in terms of totality with respect to an associated collection.
			 */
			ClassLibrary::EnumerationsRegExp::TotalityType totality;
			/**
			 * Member relations that comprise the relation structure
			 */
			std::vector<ClassLibrary::ComplexDataTypes::StatisticalClassificationRelation> hasMemberRelation;
		};
	}
}

#endif
