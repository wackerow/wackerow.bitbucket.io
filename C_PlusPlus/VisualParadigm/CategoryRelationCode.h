#ifndef CATEGORYRELATIONCODE_H
#define CATEGORYRELATIONCODE_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum CategoryRelationCode {
			Nominal, 
			Ordinal, 
			Interval, 
			Ratio, 
			Continuous
		};
	}
}

#endif
