#ifndef COLLECTIONTYPE_H
#define COLLECTIONTYPE_H

namespace ClassLibrary {
	namespace EnumerationsRegExp {
		enum CollectionType {
			Bag, 
			Set
		};
	}
}

#endif
