#ifndef AGENTINDICATOR_H
#define AGENTINDICATOR_H

namespace ClassLibrary {
	namespace ComplexDataTypes {
		/**
 * Definition
 * ============
 * Member Indicator for use with member type Agent
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class AgentIndicator {

		private:
			/**
			 * Index value within an ordered array
			 */
			int index;
		};
	}
}

#endif
