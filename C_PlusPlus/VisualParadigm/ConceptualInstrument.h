#ifndef CONCEPTUALINSTRUMENT_H
#define CONCEPTUALINSTRUMENT_H

namespace ClassLibrary {
	namespace DataCapture {
		/**
 * Definition
 * ============
 * Design plan for creating a data capture tool. 
 * 
 * Examples
 * ==========
 * Design of a questionnaire regardless of mode; Design of an experiment
 * 
 * Explanatory notes
 * ===================
 * A single ConceptualInstrument would contain workflow sequences of the intended flow logic of the instrument, which could be implemented as a web survey or paper survey, utilizing the same sequence and questions.
 * Similarly a single ConceptualInstrument would contain workflow sequences of the intended flow logic of the instrument, which could be implemented as a protocol delivered by a nurse or by self-administration.
 * 
 * Synonyms
 * ==========
 * Idea; Intention; Theory; Design; LogicalInstrument
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class ConceptualInstrument : ClassLibrary::Workflows::WorkflowProcess {

		private:
			/**
			 * A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::LabelForDisplay> displayLabel;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * A description of the purpose or use of the Member. May be expressed in multiple languages and supports the use of structured content.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString description;
			/**
			 * Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString usage;
		};
	}
}

#endif
