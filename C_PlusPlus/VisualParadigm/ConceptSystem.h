#ifndef CONCEPTSYSTEM_H
#define CONCEPTSYSTEM_H

namespace ClassLibrary {
	namespace Conceptual {
		/**
 * Definition
 * ============
 * A set of Concepts structured by the relations among them. [GSIM 1.1] 
 * 
 * Examples
 * ==========
 * 1) Concept of Sex: Male, Female, Other 2) Concept of Household Relationship: Household Head, Spouse of Household Head, Child of Household Head, Unrelated Household Member, etc.  
 * 
 * Explanatory notes
 * ===================
 * Note that this structure can be used to structure Concept, Classification, Universe, Population, Unit Type and any other class that extends from Concept.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * c:ConceptSchemeType
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Concept System
 */
class ConceptSystem : ClassLibrary::Identification::AnnotatedIdentifiable {

		private:
			/**
			 * Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.
			 */
			ClassLibrary::EnumerationsRegExp::CollectionType type;
			/**
			 * A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.
			 */
			ClassLibrary::ComplexDataTypes::InternationalStructuredString purpose;
			/**
			 * MemberIndicator Allows for the identification of the member and optionally provides an index for the member within an ordered array
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ConceptIndicator> contains;
			/**
			 * If members are ordered set to true, if unordered set to false.
			 */
			boolean isOrdered;
		};
	}
}

#endif
