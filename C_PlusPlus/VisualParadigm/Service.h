#ifndef SERVICE_H
#define SERVICE_H

namespace ClassLibrary {
	namespace ProcessPattern {
		/**
 * Definition
 * ============
 * A means of performing a Process Step as part of a Business Function (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve). 
 * 
 * 
 * 
 * Examples
 * ==========
 * 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Business Service
 */
class Service : ClassLibrary::Identification::Identifiable {

		private:
			/**
			 * Specifies where the service can be accessed.
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry serviceLocation;
		};
	}
}

#endif
