#ifndef LANGUAGE_H
#define LANGUAGE_H

namespace ClassLibrary {
	namespace XMLSchemaDatatypes {
		/**
 * language is like the derived datatype "language" of the XML Schema datatypes.
 * 
 * Definition from XML Schema Part 2: Datatypes Second Edition:
 * "language" represents natural language identifiers as defined by by [RFC 3066]. The value space of "language" is the set of all strings that are valid language identifiers as defined [RFC 3066] . The lexical space of language is the set of all strings that conform to the pattern [a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*. The base type of "language" is token.
 * 
 * For details see at https://www.w3.org/TR/xmlschema-2/#language.
 */
class language {
		};
	}
}

#endif
