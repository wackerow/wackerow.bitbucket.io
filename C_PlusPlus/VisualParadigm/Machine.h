#ifndef MACHINE_H
#define MACHINE_H

namespace ClassLibrary {
	namespace Agents {
		/**
 * Definition
 * ============
 * Mechanism or computer program used to implement a process.
 * 
 * Examples
 * ==========
 * SAS program, photocopier
 * 
 * Explanatory notes
 * ===================
 * May be used as the target to describe how an action was performed. Relevent to data capture and data processing or wherever a role is performed by a Machine.
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 */
class Machine : ClassLibrary::Agents::Agent {

		private:
			/**
			 * The kind of machine used - software, web service, physical machine, from a controlled vocabulary
			 */
			ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry typeOfMachine;
			/**
			 * The name of the machine. A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ObjectName> name;
			/**
			 * The locations where the machine can be access
			 */
			ClassLibrary::ComplexDataTypes::AccessLocation hasAccessLocation;
			/**
			 * The function of the machine
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry> function;
			/**
			 * Specified the machine interface. Supports the use of a controlled vocabulary.
			 */
			std::vector<ClassLibrary::ComplexDataTypes::ExternalControlledVocabularyEntry> machineInterface;
			/**
			 * Contact information for the owner/operator including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.
			 */
			ClassLibrary::ComplexDataTypes::ContactInformation ownerOperatorContact;
		};
	}
}

#endif
