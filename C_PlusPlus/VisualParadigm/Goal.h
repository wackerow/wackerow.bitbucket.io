#ifndef GOAL_H
#define GOAL_H

namespace ClassLibrary {
	namespace Methodologies {
		/**
 * Definition
 * ============
 * Goals are the "things" a method aims to achieve. A goal may be a business function (GSIM) corresponding to a function in a catalog of functions such as GSBPM or GLBMN. However, goals may be specified more broadly. For example, conducting a clinical trial might be the goal of a method. Machine learning might be the goal of a method. 
 * 
 * 
 * 
 * Examples
 * ==========
 * To further distinguish a "goal" from an "output", consider the construction of a statistic. A goal may be the construction of the statistic. The output is the statistic. The output is data. The statistic by itself -- the output -- is arguably meaningless even if we capture the process of its construction. 
 * 
 * Explanatory notes
 * ===================
 * 
 * 
 * Synonyms
 * ==========
 * 
 * 
 * DDI 3.2 mapping
 * =================
 * 
 * 
 * RDF mapping
 * =============
 * 
 * 
 * GSIM mapping
 * ==============
 * Business Function
 */
class Goal : ClassLibrary::Methodologies::BusinessFunction {
		};
	}
}

#endif
